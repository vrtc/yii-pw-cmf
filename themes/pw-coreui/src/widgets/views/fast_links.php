<?php
/**
 * Created by new_trisport.
 * User: Dmitriy Kindeev
 * Email: dkindeev@mail.ru
 * Date: 03.11.2019
 * Time: 15:07
 */

?>
<div class="row">


    <div class="col-6 col-lg-3">
        <div class="card">
            <div class="card-body p-3 d-flex align-items-center">
                <i class="fa fa-user bg-warning p-3 font-2xl mr-3"></i>
                <div>
                    <div class="text-value-sm text-warning"><?= $data['userCount'] ?></div>
                    <div class="text-muted text-uppercase font-weight-bold small">Покупатели</div>
                </div>
            </div>
            <div class="card-footer px-3 py-2">
                <a class="btn-block text-muted d-flex justify-content-between align-items-center" href="<?= \yii\helpers\Url::to(['/pw-users/manage/index']) ?>">
                    <span class="small font-weight-bold">Подробнее</span>
                    <i class="fa fa-angle-right"></i>
                </a>
            </div>
        </div>
    </div>

    <!--<div class="col-6 col-lg-3">
        <div class="card">
            <div class="card-body p-3 d-flex align-items-center">
                <i class="fa fa-bell bg-danger p-3 font-2xl mr-3"></i>
                <div>
                    <div class="text-value-sm text-danger">$1.999,50</div>
                    <div class="text-muted text-uppercase font-weight-bold small">Income</div>
                </div>
            </div>
            <div class="card-footer px-3 py-2">
                <a class="btn-block text-muted d-flex justify-content-between align-items-center" href="#">
                    <span class="small font-weight-bold">View More</span>
                    <i class="fa fa-angle-right"></i>
                </a>
            </div>
        </div>
    </div>-->

</div>
