<?php
/**
 * Created by new_trisport.
 * User: Dmitriy Kindeev
 * Email: dkindeev@mail.ru
 * Date: 03.11.2019
 * Time: 15:04
 */

namespace pw\coreui\widgets;

use pw\core\Widget;
use pw\users\models\Users;
use yii\helpers\ArrayHelper;

class FastLinks extends Widget
{
  public function run()
  {

      $totalUser = Users::find()->count();
      $data['userCount'] = $totalUser;
      return $this->render('fast_links', [
          'data' => $data
      ]);
  }
}

