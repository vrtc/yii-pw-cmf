<?php

namespace pw\coreui\widgets;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

class Menu extends \pw\menu\widgets\Menu
{

    public $activeCssClass = 'active open';
    public $activateItems = true;
    public $activateParents = true;
    public $itemOptions = ['class' => 'nav-item'];
    public $parentClass = 'nav-item nav-dropdown';
    public $submenuTemplate = '<ul class="nav-dropdown-items">{items}</ul>';
    public $linkTemplate = '<a class="nav-link" href="{url}">{icon} {label}</a>';
    public $labelTemplate = '<a href="javascript:;" class="nav-link nav-dropdown-toggle">{icon} {label}</a>';
    public $options = ['class' => 'nav'];

    /**
     * @inheritdoc
     */
    protected function renderItems($items, $lvl = 1)
    {
        $n = count($items);
        $lines = [];
        foreach ($items as $i => $item) {
            $options = array_merge($this->itemOptions, ArrayHelper::getValue($item, 'options', []));
            if ($lvl === 2 && !empty($item['items'])) {
                $options['class'] = 'dropdown more-dropdown-sub';
            }
            $tag = ArrayHelper::remove($options, 'tag', 'li');
            $class = [];
            if ($item['active']) {
                $class[] = $this->activeCssClass;
            }
            if ($i === 0 && $this->firstItemCssClass !== null) {
                $class[] = $this->firstItemCssClass;
            }
            if ($i === $n - 1 && $this->lastItemCssClass !== null) {
                $class[] = $this->lastItemCssClass;
            }
            if (!empty($class)) {
                if (empty($options['class'])) {
                    $options['class'] = implode(' ', $class);
                } else {
                    $options['class'] .= ' ' . implode(' ', $class);
                }
            }
            if(isset($item['items']) && count($item['items']) > 0){
                $options['class'] = $this->parentClass;
            }

            $menu = $this->renderItem($item);
            if (!empty($item['items'])) {
                $submenuTemplate = ArrayHelper::getValue($item, 'submenuTemplate', $this->submenuTemplate);
                $menu .= strtr($submenuTemplate, [
                    '{items}' => $this->renderItems($item['items'], $lvl + 1),
                ]);
            }
            $lines[] = Html::tag($tag, $menu, $options);
        }
        return implode("\n", $lines);
    }


}