<?php
namespace pw\coreui;

use yii\web\AssetBundle;

class BaseAssets extends AssetBundle{

    public $sourcePath = '@pw/coreui/assets';

    public $css = [
        'vendors/@coreui/icons/css/coreui-icons.min.css',
        'vendors/flag-icon-css/css/flag-icon.min.css',
        'vendors/font-awesome/css/font-awesome.min.css',
        'vendors/simple-line-icons/css/simple-line-icons.css',
        'vendors/datetimepicker-2.5.20/build/jquery.datetimepicker.min.css',
     //   '//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css',
        'css/style.css',
        'css/custom.css',
        'vendors/pace-progress/css/pace.min.css',

    ];

    public $js = [
        'vendors/popper.js/js/popper.min.js',
        'vendors/bootstrap/js/bootstrap.min.js',
        'vendors/pace-progress/js/pace.min.js',
        'vendors/perfect-scrollbar/js/perfect-scrollbar.min.js',
        'vendors/@coreui/coreui/js/coreui.min.js',
        'vendors/datetimepicker-2.5.20/build/jquery.datetimepicker.full.js',
        'vendors/@coreui/coreui-plugin-chartjs-custom-tooltips/js/custom-tooltips.min.js',
        'vendors/editable/editable.js',
        'js/moment.js',
        '//cdn.jsdelivr.net/npm/sweetalert2@9'
        //'js/main.js'
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap5\BootstrapAsset',
        'yii\bootstrap5\BootstrapPluginAsset'
    ];
}
