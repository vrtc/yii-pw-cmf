<?php

use yii\helpers\Html;

/**
 * @var $tools bool
 * @var $panelOptions array
 * @var $panelTitleOptions array
 * @var $panelBodyOptions array
 * @var $actions array
 * @var $isItForm bool
 */
$panelOptions['class'] = 'card';
$panelTitleOptions['class'] = 'card-header';
$panelBodyOptions['class'] = 'card-body';
?>
<?php if ($isItForm): ?>
<div class="row justify-content-center">
    <div class="col-md-9">
        <?php endif; ?>
        <div <?= Html::renderTagAttributes($panelOptions) ?>>
            <?php if ($tools || $actions): ?>
                <div <?= Html::renderTagAttributes($panelTitleOptions) ?>>
                    <?php if ($tools): ?>
                        <div class="tools">
                            <a class="collapse"
                               title="<?= Yii::t('ui', 'Свернуть') ?>"
                               data-expand-title="<?= Yii::t('ui', 'Развернуть') ?>"
                               data-collapse-title="<?= Yii::t('ui', 'Свернуть') ?>">
                            </a>
                            <a class="fullscreen" href="#" title="<?= Yii::t('ui', 'На весь экран') ?>"></a>
                        </div>
                    <?php endif; ?>
                    <?php if ($actions): ?>
                        <div class="actions">
                            <?php foreach ($actions as $action):
                                if ($action !== null) {
                                    echo Html::tag($action['tag'], $action['icon'] . ' ' . $action['label'], $action['options']) . ' ';
                                }
                            endforeach; ?>
                        </div>
                    <?php endif; ?>

                </div>
            <?php endif; ?>
            <?php if ($title != null): ?>
                <div <?= Html::renderTagAttributes($panelTitleOptions) ?>>
                    <h4><?= $title ?></h4>
                </div>
            <?php endif; ?>
            <div <?= Html::renderTagAttributes($panelBodyOptions) ?>>
                <?= $content ?>
            </div>
        </div>
        <?php if ($isItForm): ?>
    </div>
</div>
<?php endif; ?>
