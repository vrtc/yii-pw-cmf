<?php
/**
 * Created by pw-basic.
 * User: Dmitriy Kindeev
 * Email: dkindeev@mail.ru
 * Date: 10.01.19
 * Time: 21:08
 */
?>
<?php

use yii\helpers\Url;
use yii\helpers\Html;

?>
<?php if ($isItForm): ?>
<div class="row justify-content-center">
    <div class="col-md-9">
        <?php endif; ?>
        <div <?= Html::renderTagAttributes($panelOptions) ?>>
            <div <?= Html::renderTagAttributes($panelTitleOptions) ?>>
                <?php if ($actions): ?>
                    <div class="actions text-right col-md-12">
                        <?php foreach ($actions as $action):
                            if ($action !== null) {
                                echo Html::tag($action['tag'], $action['icon'] . ' ' . $action['label'], $action['options']) . ' ';
                            }
                        endforeach; ?>
                    </div>
                <?php endif; ?>
                <ul class="nav nav-tabs">
                    <?php foreach ($tabs as $i => $tab): ?>
                        <li class="nav-item">
                            <?php

                            $nav_link = 'nav-link';

                            if (isset($tab['active'])) {
                                $nav_link .= ' active';
                            }
                            ?>
                            <?php if (!empty($tab['url'])): ?>
                                <a href="<?= Url::to($tab['url']) ?>" class="<?= $nav_link ?>">
                                    <?php if (!empty($tab['icon'])) : ?>
                                        <?= \pw\ui\icons\Icons::show($tab['icon'], ['class' => 'font-yellow-gold']) ?>
                                    <?php endif; ?>
                                    <?= $tab['label'] ?>
                                </a>
                            <?php else: ?>
                                <a href="#panel-<?= $panelOptions['id'] ?>-tab<?= $i ?>" data-toggle="tab"
                                   aria-expanded="true" class="<?= $nav_link ?>">
                                    <?php if (!empty($tab['icon'])) : ?>
                                        <?= \pw\ui\icons\Icons::show($tab['icon'], ['class' => 'font-yellow-gold']) ?>
                                    <?php endif; ?>
                                    <?= $tab['label'] ?>
                                </a>
                            <?php endif ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
                <?php if ($tools) : ?>
                    <div class="tools">
                        <a class="collapse"
                           title="<?= Yii::t('ui', 'Свернуть') ?>"
                           data-expand-title="<?= Yii::t('ui', 'Развернуть') ?>"
                           data-collapse-title="<?= Yii::t('ui', 'Свернуть') ?>">
                        </a>
                        <a class="fullscreen" href="#" title="<?= Yii::t('ui', 'На весь экран') ?>"></a>
                    </div>
                <?php endif; ?>
            </div>
            <div <?= Html::renderTagAttributes($panelBodyOptions) ?>>
                <div class="tab-content">
                    <?php foreach ($tabs as $i => $tab) : ?>
                        <div class="tab-pane <?php if (isset($tab['active'])) : ?>
                                    active
                                <?php endif; ?>"
                             id="panel-<?= $panelOptions['id'] ?>-tab<?= $i ?>">
                            <?= $tab['content'] ?>
                        </div>
                    <?php endforeach; ?>
                </div>
                <?= $content ?>
            </div>
        </div>
        <?php if ($isItForm): ?>
    </div>
</div>
<?php endif; ?>
