<?php
/** @var $this \pw\web\View */
/** @var $grid \pw\ui\grid\GridView */

/** @var $model \pw\core\Model */

use pw\ui\panel\Panel;

?>
<?php if ($showPanel): ?><?php Panel::begin([
    'title' => $title,
    'icon' => $icon,
    'actions' => $actions,
]) ?><?php endif; ?>
<div class="table-container">
    <?php if ($grid->showOnEmpty || $grid->dataProvider->getCount() > 0): ?>
  <div class="row">
    <div class="col-6">
        <?= $grid->renderPager([
            'linkContainerOptions' => [
                'class' => 'page-item'
            ],
            'linkOptions' => [
                'class' => 'page-link'
            ],
            'disabledListItemSubTagOptions' => [
                'class' => 'page-link'
            ],
            'firstPageLabel' => 'Первая',
            'lastPageLabel' => 'Последняя'
        ]);
        ?>
    </div>
    <div class="col-4 text-right"> <?= $grid->renderSummary(); ?></div>
      <div class="col-md-2 col-sm-12">
          <select onchange="location = this.value;">
              <?php
              $values = [25, 50, 100, 150, 200];
              $current = $dataProvider->getPagination()->getPageSize();
              ?>
              <?php foreach ($values as $value): ?>
                  <option value="<?= \yii\helpers\Url::current(['per-page' => $value]) ?>"
                          <?php if ($current == $value): ?>selected="selected"<?php endif; ?>><?= $value ?></option>
              <?php endforeach; ?>
          </select>
      </div>
  </div>
</div>
<?= $grid->renderItems(); ?>
  <div class="row">
    <div class="col-8">
        <?= $grid->renderPager([
            'linkContainerOptions' => [
                'class' => 'page-item'
            ],
            'linkOptions' => [
                'class' => 'page-link'
            ],
            'disabledListItemSubTagOptions' => [
                'class' => 'page-link'
            ],
            'firstPageLabel' => 'Первая',
            'lastPageLabel' => 'Последняя'
        ]);
        ?>
    </div>
    <div class="col-4 text-right"> <?= $grid->renderSummary(); ?></div>
  </div>
<?php else: ?>
  <div><?= $grid->emptyText ?></div>
<?php endif; ?>
</div>
<?php if ($showPanel): ?><?php Panel::end() ?><?php endif; ?>
