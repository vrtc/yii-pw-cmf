<?php
/** @var  $this \pw\web\View */

use pw\ui\panel\Panel;

?>
<?php Panel::begin([
    'title' => $title,
    'icon' => $icon,
    'actions' => $actions,
    'tabs' => $tabs,
    'isItForm' => true,
]) ?>
<?= $content ?>
<?php Panel::end() ?>
