<?php
/* @var $this \pw\web\View */

/* @var $loginForm \pw\users\backend\models\LoginForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->registerJs(
    "  jQuery('#forget-password').click(function() {
            jQuery('.login-block').hide();
            jQuery('.forget-block').show();
        });

        jQuery('.back-btn').click(function() {
            jQuery('.login-block').show();
            jQuery('.forget-block').hide();
        });"
)
?>
<div class="row justify-content-center">
  <div class="col-md-6">
    <div class="card-group">
      <div class="card p-4 login-block">
          <?php
          $form = ActiveForm::begin([
              'id' => 'login-form',
              'options' => ['class' => 'login-form'],
              'method' => 'post'
          ]);
          ?>
        <div class="card-body">
          <h1><?= Yii::t('admin', 'Войти в аккаунт') ?></h1>
          <p class="text-muted">Sign In to your account</p>
            <?php if ($loginForm->hasErrors()): ?>
              <div class="alert alert-danger">
                <button class="close" data-close="alert"></button>
                  <?php foreach ($loginForm->getErrors() as $errors): ?>
                      <?= implode('<br/>', $errors) ?><br />
                  <?php endforeach; ?>
              </div>
            <?php endif; ?>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="icon-user"></i>
                    </span>
            </div>
              <?= Html::activeTextInput($loginForm, 'email',
                  ['placeholder' => $loginForm->getAttributeLabel('email'), 'class' => 'form-control placeholder-no-fix']) ?>
          </div>
          <div class="input-group mb-4">
            <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="icon-lock"></i>
                    </span>
            </div>
              <?= Html::activePasswordInput($loginForm, 'password', [
                  'placeholder' => $loginForm->getAttributeLabel('password'),
                  'class' => 'form-control placeholder-no-fix'
              ]) ?>
          </div>
          <div>
              <?= Html::activeCheckbox($loginForm, 'rememberMe', [
                  'label' => '<span></span>' . $loginForm->getAttributeLabel('rememberMe'),
                  'labelOptions' => ['class' => 'check mt-checkbox mt-checkbox-outline']
              ]) ?>
          </div>
          <div class="row">
            <div class="col-6">
                <?= Html::submitButton(Yii::t('admin', 'Войти'), ['class' => 'btn btn-primary px-4']) ?>
            </div>
            <div class="col-6 text-right">
              <a href="javascript:;" id="forget-password" class="btn btn-link px-0"><?= Yii::t('admin', 'Забыли пароль?') ?></a>
            </div>
          </div>
        </div>
          <?php ActiveForm::end() ?>
      </div>
      <div class="card text-white bg-primary py-5 d-md-down-none forget-block" style="width:44%; display: none;">
          <?php $form = ActiveForm::begin([
              'id' => 'forget-form',
              'options' => ['class' => 'forget-form'],
              'method' => 'post'
          ]) ?>
        <div class="card-body text-center">
          <div>
            <h2><?= Yii::t('admin', 'Забыли пароль?') ?></h2>
            <p>
                <?= Yii::t('admin', 'Введите Ваш email адрес, чтобы сбросить пароль.') ?>
            </p>
            <div class="form-group">
                <?= Html::activeTextInput($loginForm, 'email',
                    ['placeholder' => $loginForm->getAttributeLabel('email'), 'class' => 'form-control placeholder-no-fix']) ?>
            </div>
            <div class="form-actions">
                <?= Html::submitButton(Yii::t('admin', 'Сбросить пароль'),
                    ['class' => 'btn btn-primary active mt-3']) ?>
            </div>
            <a class="active mt-3 back-btn" href="javascript::void(0)">Назад</a>
          </div>
        </div>
          <?php ActiveForm::end(); ?>
      </div>
    </div>
  </div>
</div>
