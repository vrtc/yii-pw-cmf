<?php
/**
 * Created by pw-coreui.
 * User: Dmitriy Kindeev
 * Email: dkindeev@mail.ru
 * Date: 19.12.18
 * Time: 16:10
 */
/* @var $content string */

use yii\helpers\Html;

/*\yii\web\YiiAsset::register($this);
LoginAssets::register($this);*/
$asset = \pw\coreui\BaseAssets::register($this);
\yii\bootstrap\BootstrapPluginAsset::register($this);
?><?php $this->beginPage(); ?>
  <!DOCTYPE html><!--
* CoreUI - Free Bootstrap Admin Template
* @version v2.1.10
* @link https://coreui.io
* Copyright (c) 2018 creativeLabs Łukasz Holeczek
* Licensed under MIT (https://coreui.io/license)
--><!--[if IE 8]>
  <html lang="<?=Yii::$app->language?>" class="ie8 no-js"> <![endif]--><!--[if IE 9]>
  <html lang="<?=Yii::$app->language?>" class="ie9 no-js"> <![endif]--><!--[if !IE]><!-->
  <html lang="<?= Yii::$app->language ?>">
  <!--<![endif]-->
  <head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
      <?= Html::csrfMetaTags() ?>
      <?php $this->head(); ?>
  </head>
  <body class="app flex-row align-items-center">
  <?php $this->beginBody(); ?>
  <div class="container">
      <?= $content ?>
  </div>
  <?php $this->endBody() ?>
  </body>
  </html>
<?php $this->endPage() ?>
