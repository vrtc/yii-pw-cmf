<?php
namespace pw\coreui;

use pw\modules\ThemeInstaller;
use pw\web\Theme;

class Installer extends ThemeInstaller
{

    public function getKey(): string
    {
        return 'pw-coreui';
    }

    public function getVersion(): string
    {
        return '0.1';
    }

    public function getName(): string
    {
        return 'Pw CoreUI theme';
    }

    public function getAuthors(): array
    {
        return ['i@yar.pw'];
    }

    public function getLink(): string
    {
        return 'http://yar.pw';
    }

    public function getType()
    {
        return Theme::THEME_BACKEND;
    }

    public function getSourceLanguage(): string
    {
        return 'ru-RU';
    }
}
