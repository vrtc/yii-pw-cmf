<?php
namespace pw\coreui;

use pw\ui\assets\DataTablesAsset;
use pw\ui\grid\GridView;
use pw\ui\icons\Icons;
use Yii;

class Theme extends \pw\web\Theme
{


    public function init()
    {
        parent::init();
        Yii::$container->set(GridView::class, [
            'pageSize' => 50,
        ]);
/*
        Yii::$container->set('pw\ui\assets\DataTablesAsset', [
            'sourcePath' => '@themes/amber-metronic/src/assets/plugins/datatables',
            'css' => [
                'datatables.min.css'
            ],
            'js' => [
                'datatables.all.min.js',
                'plugins/bootstrap/datatables.bootstrap.js',
            ]
        ]);

        Yii::$container->set('pw\ui\assets\Select2Asset', [
            'sourcePath' => '@themes/amber-metronic/src/assets/plugins/select2',
            'js'=>[
                'js/select2.full.min.js',
            ],
            'css' => [
                'css/select2.min.css',
                'css/select2-bootstrap.min.css'
            ]
        ]);*/

        Yii::$container->set('pw\ui\form\ActiveForm', [
            'layout' => 'horizontal',
            'fieldConfig' => [
                'horizontalCssClasses' => [
                    'label' => 'col-md-2',
                    'wrapper' => 'col-md-9',
                    'error' => '',
                    'hint' => 'col-md-offset-2',
                ],
            ]
        ]);
        Yii::$app->icons->setDefault('font-awesome');
    }

    public function registerBaseAssets($view)
    {
        BaseAssets::register($view);
    }
}
