<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;

\pw\front_default\BaseCustomAssets::register($this);
Yii::$app->asset = \pw\front_default\BaseAssets::register($this);
$asset = Yii::$app->asset;
$mobile = \Yii::$app->mobileDetect->isMobile();
$token = Yii::$app->request->csrfToken;

$urlIndex = \yii\helpers\Url::to(['/']);

$params = Yii::$app->getConfig('pw-front_default');



?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
  <meta charset="<?= Yii::$app->charset ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= $this->renderDynamic('return yii\bootstrap5\Html::csrfMetaTags();') ?>


  <title><?= Html::encode($this->title) ?></title>
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <!--<script type="text/javascript" src="https://vk.com/js/api/openapi.js?162"></script>-->
  <link rel="shortcut icon" href="<?= $asset->baseUrl ?>/img/favicon.ico"/>
    <?php $this->head() ?>
  <script type="text/javascript">
      var token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
  </script>
</head>

<body>
<?php $this->beginBody(); ?>
<!-- HEADER -->
<?php $flash = Yii::$app->session->getAllFlashes(true); ?>
<?php if (!empty($flash)) { ?>
  <div class="container ">
    <div class="animated fadeIn">
        <?php

        foreach ($flash as $key => $messages): ?>

            <?php $messages = (array)$messages; ?>

            <?php foreach ($messages as $message): ?><?php
                if (!in_array($key,
                    ['success', 'warning', 'danger', 'info', 'primary', 'secondary', 'light', 'dark'])) continue;

                ?>

                <?= \yii\bootstrap4\Alert::widget([
                    'options' => [
                        'class' => 'margin-top-20 alert-' . $key,
                    ],
                    'body' => $message,
                ]); ?>
            <?php endforeach; ?>

        <?php endforeach; ?>
    </div>
  </div>
<?php } ?>
<?= $content ?>

<?php $this->endBody();
?>
</body>
</html>
<?php $this->endPage() ?>
