<?php
use pw\seo\models\Seo;


/**
 * @var $model \pw\web\models\Contacts
 */
$this->title = 'Контактная информация интернет-магазина Триспорт';

$seo = \pw\seo\models\Seo::findOne(['route' => Yii::$app->requestedRoute]);
$this->params['h1'] = ($seo && isset($seo->h1) && $seo->h1 !='') ? $seo->h1 : 'Контакты';
?>
<style>
    p.bg-warning {
        background-color: #fcf8e3 !important;
    }
</style>
<div class="container pt-2">
    <div class="row justify-content-center pt-2 bg-white">
        <h1 class="pl-3 h2"><?= $this->params['h1']; ?></h1>
        <hr/>
        <div class="col-12 bg-white">
            <div class="contacts-info-block pb-2">
                <b>Адрес магазина:</b>

                <span>Москва</span>

                <span>шоссе Энтузиастов, д.1к1</span>
            </div>
            <div class="contacts-info-block pb-2">
                <b>Телефон:</b>

                <div>Телефон в магазине: <span>8 499 350 44 06</span></div>

                <div>Бесплатный звонок по России: <span>8 800 775 07 23</span></div>
            </div>
            <div class="contacts-info-block pb-2">
                <b>&#127809;Режим работы&#127809;:</b>
                <div><span>пн-пт: 11.00 - 20.00</span></div>
                <div><span>сб-вс: 10.00 - 19.00</span></div>
            </div>

            <div class="contacts-info-block min-cont-h whois">
                <p class="bg-warning">
                    <b>Как доехать:</b>

                    Из станции метро Римская выходите на Бульвар Энтузиастов через выход №4. Двигайтесь по тротуару
                    против движения транспорта. Высотка бизнес-центра должна оказаться у вас по правую руку на
                    противоположной стороне дороги. Пройдите под ж/д мостом. Первый дом слева - место назначения.
                    Пройдите через шлагбаум и обойдите дом. 20 метров от угла - и вы у входа! Добро пожаловать!
                </p>

            </div>


            <h3>
                <noindex><span>Схема проезда</span></noindex>
            </h3>
            <script type="text/javascript" charset="utf-8" async
                    src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A56593914eafa3c2f226b7a25896fac767fb975b33a4c455fe21ff98bce44a720&amp;height=300&amp;lang=ru_RU&amp;scroll=true"></script>

        </div>
    </div>
    <div class="row justify-content-center pb-2">
        <div class="col-sm-4 bg-white">
            <div class="contacts-info-block ">
                <h4>Реквизиты</h4>

                <span>ТриСпорт - лучшее для триатлона</span><br>
                ООО «ТРИ-СПОРТ» <br>
                ИНН - 7751148236 ; <br>
                КПП - 775101001 ; <br>
                Р/сч 40702810100001025585 в ИНВЕСТИЦИОННЫЙ БАНК "ВЕСТА" (ООО)<br>
                БИК 044525801<br>
                К/счет 30101810645250000801<br>
                ОГРН -1187746802429
            </div>
        </div>
        <div class="col-8 col-md-8 col-lg-8 col-xl-8 bg-white">
            <?php $form = \yii\bootstrap\ActiveForm::begin([
                'options' => ['class' => 'pb-2 callback row'],
                'id' => 'callback',
                'enableClientValidation' => false
            ]); ?>
            <div class="col-6">
                <?= $form->field($model, 'email')->textInput(); ?>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'name')->textInput(); ?>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'subject')->dropDownList([
                  'Вопрос по товару',
                    'Проблемы с заказом',
                    'Нашел ошибку на сайте',
                    'Предложение к сотрудничеству',
                    'Оптовые закупки',
                    'Другое'
                ]); ?>
            </div>
            <div class="col-12">
                <?= $form->field($model, 'text')->textarea(['rows' => 5, 'class' => 'form-control']); ?>
            </div>
            <div class="col-12">
                <?= $form->field($model, 'reCaptcha')->widget(\himiklab\yii2\recaptcha\ReCaptcha2::class, [
                        'siteKey' => env('RECAPCHA_SITE')
                ])->label(false) ?>
            </div>
            <div class="col-12">
                <?= \yii\helpers\Html::submitButton('Отправить', ['class' => 'btn callback pl-2']) ?>
            </div>
            <?php \yii\bootstrap\ActiveForm::end(); ?>
        </div>
    </div>
</div>
