<?php

use yii\helpers\Html;

$asset = \pw\front_default\BaseAssets::register($this);
Yii::$app->asset = $asset;
$name = substr($name, -4);
$name = substr($name, 0, -1);
$this->title = $name;
?>
<div class="container">
  <div class="row justify-content-center error404">
    <div class="col-12 text-center m-5">
      <span><i class="fas fa-exclamation-circle" style="font-size: 70px; color: #009fe3"></i></span><br>
      <span class="h2" style="font-size: 150px"><?= Html::encode($this->title) ?></span><br>
      <span class="h3 text-center"><?= $message ?></span>
    </div>
  </div>
</div>

