<?php
$this->title = 'HTML карта: удобный поиск каталогов через упорядоченные списки в магазине Триспорт';

$seo = \pw\seo\models\Seo::findOne(['route' => Yii::$app->requestedRoute]);
$this->params['h1'] = ($seo && isset($seo->h1) && $seo->h1 !='') ? $seo->h1 : 'Карта сайта';
/**
 * @var $models \ct\catalog\models\Category[]
 */
$js = <<<JS
$(document).ready(function() {
  $('.children').css('display', 'none');
});
$('.parent').on('click', function() {
    if($(this).next().css('display') == 'none'){
        $(this).next().fadeIn();
    }else{
        $(this).next().fadeOut();
    }
});
JS;
$this->registerJs($js, \pw\web\View::POS_END);
?>
<style>
    .parent{
        cursor: pointer;
    }
</style>
<div class="row justify-content-center mt-1 mb-1">
    <div class="col-9 bg-white">
        <h1 class="h1 bg-white text-left pb-4 pl-2 pt-5 border-bottom border-dark "><?= $this->params['h1']; ?></h1>
        <div class="row">
            <?php foreach ($models as $model): ?>
                <div class="col-4 mt-3 mb-3">
                    <ul class="list-group">
                        <li class="list-group-item list-group-item-primary h3 <?= $model->children ? 'parent' : '' ?>">
                            <a href="<?= \yii\helpers\Url::to(['/ct-catalog/default/index', 'slug' => $model->slug]) ?>">  <?= $model->name ?></a>
                        </li>
                        <?php if ($model->children): ?>
                            <ul class="list-group children">
                                <?php foreach ($model->children as $child): ?>
                                    <li class="list-group-item h5 pl-3 <?= $child->children ? 'parent list-group-item-secondary' : '' ?>">
                                        <a href="<?= \yii\helpers\Url::to(['/ct-catalog/default/index', 'slug' => $child->slug]) ?>"> <?= $child->name ?></a>
                                    </li>
                                    <?php if ($child->children): ?>
                                        <ul class="list-group children">
                                            <?php foreach ($child->children as $child1): ?>
                                                <li class="list-group-item pl-5"><a
                                                            href="<?= \yii\helpers\Url::to(['/ct-catalog/default/index', 'slug' => $child1->slug]) ?>"> <?= $child1->name ?></a>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </ul>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>