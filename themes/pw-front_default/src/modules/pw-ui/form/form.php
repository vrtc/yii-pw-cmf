<?php
/* @var $this yii\web\View */
/* @var $form \pw\ui\form\ActiveForm */

/* @var $model pw\core\Model */

use pw\ui\panel\Panel;
use yii\helpers\Html;

$isActiveRecord = $model instanceof \pw\core\db\ActiveRecord;
if(\pw\i18n\widgets\LanguageSwitchWidget::widget(['template' => 'form-language-switch']) != "") {
    $actions = array_merge($actions, [
        \pw\i18n\widgets\LanguageSwitchWidget::widget(['template' => 'form-language-switch'])
    ]);
}
?>
<?= Html::beginForm($action, $method, $options); ?>
<?= Html::hiddenInput('language', Yii::$app->language) ?>
<?= $content ?>
<?= Html::endForm() ?>