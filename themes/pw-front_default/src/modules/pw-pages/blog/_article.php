<?php
/**
 * @var $model \pw\pages\models\Pages
 */
if($model->pic){
    $image = "url(".Yii::$app->image->getThumbnail($model, true, 480, 398, \pw\filemanager\models\File::THUMBNAIL_CROP).")";
    $modelId = $model->id;
    $js = <<<JS
    $(document).ready(function() {
        $('#{$modelId}').css('background-image', '{$image}');
    });
JS;
$this->registerJs($js, \pw\web\View::POS_END);
}
?>
<div class="news-row-item">
    <div class="news-row-item-category-date">
        <div>Категория:
          <?php $parent = $model->getParent()->one();?>
            <a href="<?= \yii\helpers\Url::to(['/pw-pages/blog/article', 'slug' => $parent->slug]) ?>"><?= $parent->name ?></a>
        </div>
        <div><?= Yii::$app->formatter->asDate($model->created_time) ?></div>
    </div>
    <div class="news-row-item-bottom">
        <div class="news-row-item-title">
            <a href="<?= \yii\helpers\Url::to(['/pw-pages/blog/article', 'slug' => $model->slug]) ?>" class="a-article">
                <?= $model->name ?>
            </a>
        </div>
        <div class="news-row-item-teaser">
            <a href="<?= \yii\helpers\Url::to(['/pw-pages/blog/article', 'slug' => $model->slug]) ?>" class="a-article">
                <?= \yii\helpers\StringHelper::truncate(strip_tags(\yii\helpers\Html::decode($model->introtext)), 150, '...', null, true) ?>
            </a>
        </div>
        <div class="news-row-item-readmore"><a
                    href="<?= \yii\helpers\Url::to(['/pw-pages/blog/article', 'slug' => $model->slug]) ?>">Подробнее</a>
        </div>
    </div>
    <div class="news-row-item-overlay"></div>
    <div class="news-row-item-underlay" id="<?=$model->id?>"></div>
</div>
