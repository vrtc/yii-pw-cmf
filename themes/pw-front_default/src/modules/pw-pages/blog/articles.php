<?php
/**
 * @var $dataProvider \yii\data\ActiveDataProvider
 */
Yii::$app->asset = \pw\front_default\BaseAssets::register($this);
$this->params['groupTitle'] = $model->name;
?>

    <?= \yii\widgets\ListView::widget([
        'dataProvider' => $dataProvider,
        'options' => ['class' => 'row no-gutters news-row', 'tag' => 'div'],
        'itemView' => '_article',
        'itemOptions' => ['class' => 'col-xl-4 col-lg-6 col-md-6 news-teaser-col', 'tag' => 'div'],
        'layout' => "{items}",
    ]) ?>
<div class="row no-gutters">
    <div class="col-12">
        <div class="catalog-nav">
            <?= \pw\front_default\components\CustomLinkPager::widget([
                'pagination' => $dataProvider->getPagination(['pageSize' => 12]),
                'prevPageCssClass' => 'catalog-nav-left',
                'nextPageCssClass' => 'catalog-nav-right',
                'prevPageLabel' => '<img src="' . Yii::$app->asset->baseUrl . '/img/white-arrow-left.png" alt="Влево" title="Влево">',
                'nextPageLabel' => '<img src="' . Yii::$app->asset->baseUrl . '/img/white-arrow-right.png" alt="Вправо" title="Вправо">',
                'linkContainerOptions' => [
                    'tag' => 'div'
                ],
                'options' => [
                    'class' => 'catalog-nav-pagination',
                    'tag' => 'div',
                ],]) ?>
        </div>
    </div>
</div>
