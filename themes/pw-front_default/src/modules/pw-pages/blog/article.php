<?php
/**
 * @var $this \pw\web\View
 * @var $page \pw\pages\models\Pages
 */
Yii::$app->asset = \pw\front_default\BaseAssets::register($this);
$image = \pw\filemanager\models\File::findOne(['id' => $page->image]);

?>
<style>
  .news-item-title {
    position: relative;
    min-height: 470px;
    overflow: hidden;
    background-color: black;
  }
</style>
<div class="news-item-title">
  <div class="row">
      <?php if ($image): ?>
        <div class="col-md-12">
          <img src="<?= $image ? $image->getThumbnailUrl(324, 324) : Yii::$app->asset->baseUrl . '/img/news-item-title-1.png' ?>"
               class="img-fluid pl-4 pt-4">
        </div>
      <?php endif; ?>
  </div>
  <span class="news-item-title-text"><?= $page->name ?></span>
</div>
<div class="main-content-wrapper mobile-device-content">
    <?= \yii\helpers\Html::decode($page->description) ?>
  <div class="clearfix"></div>
</div>
