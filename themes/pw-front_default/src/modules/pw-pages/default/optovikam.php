<?php
/**
 * @var $this \pw\web\View
 * @var $page \pw\pages\models\Pages
 */
Yii::$app->asset = \pw\front_default\BaseAssets::register($this);
?>
<style>
    iframe{
        width: 100%;
    }
</style>

<div class="row justify-content-center mt-1 mb-1">
    <div class="col-md-8 col-12 bg-white">
        <h1 class="h1 bg-white text-left pb-4 pl-2 pt-5 border-bottom border-dark "><?= $page->name; ?></h1>
        <?= $page->description; ?>
        <div class="col-8 col-md-8 col-lg-8 col-xl-8 bg-white">
            <div class="h4 pb-4">Оставьте заявку и получите оптовый прайс-лист и условия сотрудничества</div>
            <?php $form = \yii\bootstrap\ActiveForm::begin([
                'options' => ['class' => 'pb-2 callback row'],
                'id' => 'optovikam',
                'enableClientValidation' => false
            ]); ?>
            <div class="col-6">
                <?= $form->field($model, 'email')->label('Ваш E-mail')->textInput(); ?>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'name') ->label('Ваше имя')->textInput(); ?>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'phone')->label('Телефон')->textInput(); ?>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'site')->label('Сайт')->textInput(); ?>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'organization')->label('Организация')->textInput(); ?>
            </div>
            <div class="col-12">
                <?= $form->field($model, 'reCaptcha')->widget(\himiklab\yii2\recaptcha\ReCaptcha2::class, [
                    'siteKey' => env('RECAPCHA_SITE')
                ])->label(false) ?>
            </div>
            <div class="col-12">
                <?= \yii\helpers\Html::submitButton('Отправить', ['class' => 'btn callback pl-2']) ?>
            </div>
            <?php \yii\bootstrap\ActiveForm::end(); ?>
        </div>
    </div>
</div>
