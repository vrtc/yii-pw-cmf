<?php
/**
 * @var $this \pw\web\View
 * @var $page \pw\pages\models\Pages
 */
Yii::$app->asset = \pw\front_default\BaseAssets::register($this);
?>
<div class="row justify-content-center mt-1 mb-1">
    <div class="col-8 text-center bg-white">
        <div class="h1 bg-white text-left pb-4 pl-2 pt-5 border-bottom border-dark ">Скоро появится</div>
        <div class="h1 pb-5">Страница находится в разработке</div>
    </div>
</div>
