<?php
/**
 * @var $this \pw\web\View
 * @var $page \pw\pages\models\Pages
 */
Yii::$app->asset = \pw\front_default\BaseAssets::register($this);
?>
<div class="container justify-content-center">
  <div class=" row  mt-1 mb-1">
    <div class="col-12 bg-white">
      <h1 class="h1 bg-white text-left pb-4 pl-2 pt-5 border-bottom border-dark "><?= $page->name ?></h1>
        <?= $page->description ?>
    </div>
  </div>
</div>
