<?php
/**
 * @var $userModel \pw\users\models\Users
 * @var $profileModel \pw\users\models\UserData
 */
Yii::$app->asset = \pw\front_default\BaseAssets::register($this);
$this->title = 'Изменить контактную информацию';
$this->params['h1'] = $this->title;

$js = <<<JS
     $(document).ready(function() {
         let searchParams = new URLSearchParams(window.location.search)
         if(searchParams.has('signup') && searchParams.get('signup') == 1) {
             fbq('track', 'CompleteRegistration');
         }
     });
JS;

$this->registerJs($js);
?>
<style>
    #edit-profile-form .form-control {
        height: 65px;
    }
    .btn.btn-buy{
        width: auto;
    }
</style>
<?php $form = \pw\ui\form\ActiveForm::begin([
   // 'model' => $model,
    'id' => 'edit-profile-form',
    'options' => ['class' => 'row no-gutters pl-3 pt-3']]); ?>
<div class="col-lg-6 col-sm-12">
    <?= $form->field($profileModel, 'lastname')->textInput()->label('Фамилия') ?>
</div>
<div class="col-lg-6 col-sm-12">
    <?= $form->field($profileModel, 'firstname')->textInput()->label('Имя'); ?>
</div>
<div class="col-lg-6 col-sm-12">
    <?= $form->field($userModel, 'email')->textInput(); ?>
</div>
<div class="col-lg-6 col-sm-12">
    <?php
    $params = [];
    if(!is_null($userModel->phone)){
      $params = ['disabled' => 'disabled'];
    }
    ?>
    <?= $form->field($userModel, 'phone')->widget(\yii\widgets\MaskedInput::class, [
        'mask' => '+7(999)-999-99-99',
    ])->textInput($params)->label('Телефон'); ?>
</div>
<div class="col-lg-6 col-sm-12">
    <?= $form->field($userModel, 'subscribe')->checkbox(); ?>
</div>
<div class="col-lg-7 col-sm-12 ml-3 mb-3">
        <?= \yii\helpers\Html::submitButton('Сохранить', ['class' => 'btn btn-buy']) ?>
</div>
<?php \pw\ui\form\ActiveForm::end(); ?>
