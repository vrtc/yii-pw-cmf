<?php
/**
 * @var $model \pw\users\models\Users
 */
Yii::$app->asset = \pw\front_default\BaseAssets::register($this);
$this->title = 'Изменить пароль';
$this->params['h1'] = $this->title;
?>
<style>
    #edit-profile-form .form-control {
        height: 65px;
    }
    .btn.btn-buy{
        width: auto;
    }
    .control-label{
        max-width: 100%;
    }
</style>
<?php $form = \pw\ui\form\ActiveForm::begin([
    'model' => $model,
    'id' => 'edit-profile-form',
    'options' => [
      'class' => 'row no-gutters pl-3 pt-3'
    ]
]); ?>
<div class="col-lg-6 col-sm-12">
    <?= $form->field($model, 'password')->passwordInput()->label('Новый пароль:') ?>
</div>
<div class="col-lg-6 col-sm-12">
    <?= $form->field($model, 'password_repeat')->passwordInput()->label('Повторите пароль:'); ?>
</div>
<div class="col-lg-7 col-sm-12 ml-3 mb-3">
    <?= \yii\helpers\Html::submitButton('Сохранить', ['class' => 'btn btn-buy']) ?>
</div>
<?php \pw\ui\form\ActiveForm::end(); ?>
