<?php
/**
 * @var $models \pw\users\models\WishList[]
 */

$bundle = $this->assetManager->getBundle('pw\front_default\BaseAssets');
$this->title = 'Изменить закладки';

?>
<?php foreach ($models->each() as $model): ?>
    <?php if($model->product): ?>
    <div class="row order-row bg-white text-center" id="<?= $model->id ?>">
        <div class="col-lg-2 col-md-5 col-sm-12 order-td">
            <?= Yii::$app->image->getThumbnail($model->product->mainPhoto, false, 100, 100, \pw\filemanager\models\File::THUMBNAIL_FILL, ['class' => 'img-fluid']) ?>
        </div>
        <div class="col-lg-5 col-md-7 col-sm-12 order-td"><?= $model->product->name ?></div>
        <div class="col-lg-1 col-md-6 col-sm-12 order-td"><?= $model->product->instock ? 'В наличии' : 'Нет в наличии' ?></div>
        <div class="col-lg-1 col-md-6 col-sm-12 order-td font-weight-bold"><?= Yii::$app->formatter->asCurrency($model->product->priceint) ?></div>
        <div class="col-lg-1 col-md-6 col-sm-12 order-td">
            <?php if ($model->product->instock): ?>
                <button class="btn-close buy" data-id="<?= $model->product->id ?>"
                        data-char="<?= $model->product->modifications ? 'yes' : 'no' ?>">
                    <img src="<?= $bundle->baseUrl ?>/img/cart.png" alt="Корзина" title="Корзина" class="img-buy">
                </button>
            <?php endif; ?>
            <button class="btn-close delete" data-id="<?= $model->id ?>">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <?php endif; ?>
<?php endforeach; ?>
<?php if (empty($models)): ?>
    <div class="row bg-white no-gutters text-center justify-content-center">
        <div class="col-lg-6 col-md-8 col-sm-12 h1">
            Вы пока ничего не добавили...
        </div>
    </div>
<?php endif; ?>
    <style>
        .img-buy {
            padding-bottom: 8px;
        }

        .row-profile {
            max-height: 500px;
            overflow: auto;
        }

        .btn-close.buy {
            border: unset;
        }

        .order-row {
            border-bottom: 1px solid #dcdfe4;
            padding-top: 20px;
            padding-bottom: 10px;
            min-height: 70px;
        }

        .order-row.row {
            margin: unset;
        }

        .modal-body {
            padding-left: 5%;
            padding-right: 5%;
        }

        @media (min-width: 700px) {
            .order-dialog {
                max-width: 80%;
            }
        }

        @media (min-width: 1000px) {
            .order-dialog {
                max-width: 50%;
            }
        }

        @media (max-width: 767px) {
            .order-row {
                height: unset;
                padding-bottom: 20px;
            }

            .order-th {
                height: 40px;
            }
        }
    </style>
<?php
$url = \yii\helpers\Url::to(['/pw-users/profile/wishlist']);
$buyUrl = \yii\helpers\Url::to(['/cr-cart/cart/fast-add']);
$token = Yii::$app->request->csrfToken;
$js = <<<JS
    $('.btn-close.delete').on('click', function(){
        $.ajax({
            url: '{$url}',
            type: 'GET',
            data: {'id' : $(this).data('id')},
            success: function(res) {
                $('#'+res.id).remove();
            },
        });
    });
    $('.btn-close.buy').on('click', function(){
        if($(this).data('id')){
            if($(this).data('char') == 'yes'){
                $('#buy-footer').css('display', 'none');
            $('#buyModal').modal('show');
            }
            $.ajax({
                url: '{$buyUrl}',
                type: 'POST',
                data: {'id': $(this).data('id'), '_csrf': '{$token}'},
                success: function(res){
                    if(res.success){
                        $('#buyModal').modal('dispose');
                        showCart();
                    }else{
                    $('#buy-content').html(res.html);
                    $('.checkout-button').attr('disabled', false);
                    }
                },
                error: function(){
                console.log('error');
                }
            });
        }
    });
JS;
$this->registerJs($js, \pw\web\View::POS_END);
?>
