<?php
/**
 * @var $discount \rd\order\models\Order[]
 */

Yii::$app->asset = \pw\front_default\BaseAssets::register($this);
$this->title = 'Карта ТриСпорт';
$this->params['h1'] = $this->title;

?>

<div class="row no-gutters pl-3 pt-3">
    Ваша скидка: <?= $discount['percent'] ?>%<br>
    Осталось до следующего уровня накоплений: <?= Yii::$app->formatter->asCurrency($discount['remains']) ?>
</div>
