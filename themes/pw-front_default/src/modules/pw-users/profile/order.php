<?php
/**
 * @var $order \rd\order\models\Order
 */
Yii::$app->asset = \pw\front_default\ModalAsset::register($this);
?>
<div class="row">
    <div class="col-lg-6 col-md-6 col-12 text-left">
        <b>Детали заказа</b>
    </div>
    <div class="col-lg-3 col-md-3 col-12">
        № Заказа: #<?= $order->id ?>
    </div>
    <div class="col-lg-3 col-md-3 col-12">
        Дата: <?= Yii::$app->formatter->asDate($order->created_time) ?>
    </div>
</div>
<div class="row ship-pay-row">
    <div class="col-lg-6 col-md-6 col-12">
        <b>Способ оплаты:</b> <?= \rd\order\models\Order::getPaymentNames()[$order->payment_method_name] ?>
    </div>
    <div class="col-lg-6 col-md-6 col-12">
        <b>Способ доставки:</b> <?= \rd\order\models\Order::getShippingNames()[$order->shipping_method_name] ?>
    </div>
</div>
<div class="row addr-container">
    <div class="col-lg-6 col-md-6 col-12 addr-table">
        <div class="col-12 addr-row-first"><b>Адрес плательщика:</b></div>
        <div class="col-12 addr-row-first"><?= $order->firstname . ' ' . $order->lastname ?></div>
        <div class="col-12 addr-row-first"><?= $order->city ?></div>
        <?php if($order->region && ($region = \zn\zones\models\District::findOne(['id' => $order->region]))):?>
        <div class="col-12 addr-row-first"><?= $order->region ? \zn\zones\models\District::findOne(['id' => $order->region])->name : '' ?></div>
        <?php else: ?>
            <div class="col-12 addr-row-first"></div>
        <?php endif; ?>
    </div>
    <div class="col-lg-6 col-md-6 col-12 addr-table">
        <div class="col-12 addr-row"><b>Адрес доставки</b></div>
        <div class="col-12 addr-row"><?= $order->firstname . ' ' . $order->lastname ?></div>
        <div class="col-12 addr-row"><?= $order->city ?></div>
        <?php if($order->region && ($region = \zn\zones\models\District::findOne(['id' => $order->region]))):?>
            <div class="col-12 addr-row-first"><?= $order->region ? \zn\zones\models\District::findOne(['id' => $order->region])->name : '' ?></div>
        <?php else: ?>
            <div class="col-12 addr-row-first"></div>
        <?php endif; ?>
    </div>
</div>
<div class="row product-row text-center">
    <div class="col-lg-2 col-md-6 col-12"><b>Наименование товара</b></div>
    <div class="col-lg-2 col-md-6 col-12"><b>Модель</b></div>
    <div class="col-lg-2 col-md-6 col-12"><b>Количество</b></div>
    <div class="col-lg-2 col-md-6 col-12"><b>Цена</b></div>
    <div class="col-lg-2 col-md-6 col-12"><b>Итого</b></div>
    <div class="col-lg-2 col-md-6 col-12"></div>
</div>
<?php foreach ($order->items as $item): ?>
    <div class="row product-row text-center">
        <div class="col-lg-2 col-md-6 col-12"><?= $item->product_name ?></div>
        <div class="col-lg-2 col-md-6 col-12"><?= $item->type == \cr\cart\models\CartItem::PRODUCT_TYPE ? $item->product->model : '-' ?></div>
        <div class="col-lg-2 col-md-6 col-12"><?= $item->quantity ?></div>
        <div class="col-lg-2 col-md-6 col-12"><b><?= Yii::$app->formatter->asCurrency($item->price) ?></b></div>
        <div class="col-lg-2 col-md-6 col-12"><b><?= Yii::$app->formatter->asCurrency($item->total) ?></b></div>
        <div class="col-lg-2 col-md-6 col-12"><b>Возврат товара</b></div>
    </div>
<?php endforeach; ?>
<div class="row total-row">
    <div class="col-6">Сумма:</div>
    <div class="col-6 text-right"><?= Yii::$app->formatter->asCurrency($order->total - $order->shipping_price) ?></div>
</div>
<?php if ($order->shipping_method_name != 'pickup'): ?>
    <div class="row total-row">
        <div class="col-6"><?= \rd\order\models\Order::getShippingNames()[$order->shipping_method_name] ?></div>
        <div class="col-6"><?= Yii::$app->formatter->asCurrency($order->shipping_price) ?></div>
    </div>
<?php endif; ?>
<div class="row total-row">
    <div class="col-6">Итого:</div>
    <div class="col-6 text-right"><?= Yii::$app->formatter->asCurrency($order->total) ?></div>
</div>
<style>
    .row {
        padding-top: 10px;
        padding-bottom: 10px;
    }

    .ship-pay-row {
        background-color: #f2f5f8;
        padding-top: 20px;
        padding-bottom: 20px;
    }

    .product-row {
        background-color: #f2f5f8;
        border-bottom: 1px solid #dcdfe4;
    }

    .total-row {
        border-bottom: 1px solid #dcdfe4;
    }
    .addr-row-first{
        padding-top: 20px;
        padding-bottom: 20px;
        border-bottom: 2px dashed #dcdfe4;
        border-right: 2px dashed #dcdfe4;
    }
    .addr-row{
        padding-top: 20px;
        padding-bottom: 20px;
        border-bottom: 2px dashed #dcdfe4;
    }
    .addr-table{
        padding-left: unset;
        padding-right: unset;
    }
    .addr-container{
        padding: unset;
    }
</style>
