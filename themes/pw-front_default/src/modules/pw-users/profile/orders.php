<?php
use yii\helpers\Url;
/**
 * @var $orders \rd\order\models\Order[]
 * @var $payComponent \pm\payment\components\PayAnyWay
 */

?>
<div class="row order-row bg-white text-center font-weight-bold">
  <div class="col-lg-3 col-md-6 col-sm-12 order-th">Заказ</div>
  <div class="col-lg-2 col-md-6 col-sm-12 order-th">Дата</div>
  <div class="col-lg-2 col-md-6 col-sm-12 order-th">Товаров</div>
  <div class="col-lg-2 col-md-6 col-sm-12 order-th">Итого</div>
  <div class="col-lg-3 col-md-6 col-sm-12 order-th">Статус</div>
</div>
<?php foreach ($orders as $key => $order): ?>
  <div class="row order-row bg-white text-center">
    <div class="col-lg-3 col-md-6 col-sm-12 order-td">№ <?= $order->id ?></div>
    <div class="col-lg-2 col-md-6 col-sm-12 order-td"><?= Yii::$app->formatter->asDate($order->created_time) ?></div>

    <div class="col-lg-2 col-md-6 col-sm-12 order-td"><span
        class="d-none d-md-inline d-lg-none">Товаров:</span> <?= $order->getItems()->count() ?></div>
    <div class="col-lg-2 col-md-6 col-sm-12 order-td font-weight-bold"><?= Yii::$app->formatter->asCurrency($order->total) ?></div>
    <div class="col-lg-3 col-md-6 col-sm-12 order-td"><?= isset(\rd\order\models\Order::getStatuses()[$order->status]) ? \rd\order\models\Order::getStatuses()[$order->status] : '' ?></div>

    <div class="col-12 mt-3">
      <div class="row justify-content-end">
        <div>
          <a href="#" class="order-modal btn btn-default btn-sm mr-3" data-id="<?= $order->id ?>">Просмотр</a>
          <a target="_blank" href="<?= \yii\helpers\Url::to(['/cr-cart/cart/add-order', 'id' => $order->id]); ?>" class=" btn btn-success btn-sm mr-3" >Повторить</a>

          <?php if(in_array($order->status, \rd\order\models\Order::paymentStatuses())) : ?>
              <a target="_blank" href="<?= Url::to(['/rd-order/checkout/pay-link', 'orderId' => $order->id]) ?>" class="btn btn-primary btn-sm mr-1"  data-id="<?= $order->id ?>">Оплатить</a>
              <!-- <a target="_blank" href="<?= Url::to(['/pw-users/profile/payment', 'id' => $order->id]) ?>" class="btn btn-primary btn-sm mr-1"  data-id="<?= $order->id ?>">Оплатить</a> -->
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
<?php endforeach; ?>
<div class="modal fade" id="orderModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
  <div class="modal-dialog order-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Заказ</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
    </div>
  </div>
</div>
<style>
  .row-profile {
    max-height: 500px;
    overflow: auto;
  }

  .order-row {
    border-bottom: 1px solid #dcdfe4;
    padding-top: 20px;
    padding-bottom: 10px;
    min-height: 70px;
  }

  .order-row.row {
    margin: unset;
  }

  .modal-body {
    padding-left: 5%;
    padding-right: 5%;
  }

  @media (min-width: 700px) {
    .order-dialog {
      max-width: 80%;
    }
  }

  @media (min-width: 1000px) {
    .order-dialog {
      max-width: 50%;
    }
  }

  @media (max-width: 767px) {
    .order-row {
      height: unset;
      padding-bottom: 20px;
    }

    .order-th {
      height: 40px;
    }

    .order-td {
      height: 40px;
    }
  }
</style>
<?php
$url = \yii\helpers\Url::to(['/pw-users/profile/get-order']);
$js = <<<JS
    $('.order-modal').on('click', function(e){
        e.preventDefault();
        $.ajax({
            url: '{$url}',
            type: 'get',
            data: {id: $(this).data('id')},
            success: function(res){
                $('#orderModal').find('.modal-body').html(res);
                $('#orderModal').modal('show');
            },
            error: function(){
                            console.log('error');
            }
        }); 
    });
JS;
$this->registerJs($js, \pw\web\View::POS_END);
?>
