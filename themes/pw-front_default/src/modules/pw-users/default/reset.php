<?php
/**
 * Created by new_trisport.
 * User: Dmitriy Kindeev
 * Email: dkindeev@mail.ru
 * Date: 22.07.2019
 * Time: 11:25
 */

/** @var $this \amber\core\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \pw\users\frontend\models\ResetPasswordForm;

Yii::$app->asset = \pw\front_default\BaseAssets::register($this);
?>

<div id="col-main" class="container justify-content-center">


      <?php $form = ActiveForm::begin([
          'id' => 'form-reset',
          'fieldConfig' => [
              'inputOptions' => [
                  'class' => 'form-control'
              ],
          ],
          'options' => [
              'class' => 'row justify-content-center bg-white',
          ],
      ]); ?>
    <div class="h2 text-center col-12 mt-5 mb-5">Восстановление пароля</div>

      <?php if ($model->scenario == ResetPasswordForm::SCENARIO_STEP1): ?>

        <div class="row justify-content-center">

          <div class="text-center">
            <div class="margin-bottom-10  col-12 pr-5 pl-5">
                <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::class, [
                    'mask' => '+7(999)-999-99-99',
                ])->textInput(['placeholder' => 'Телефон'])->label(false) ?>
            </div>
            <div class="col-12">
                <?= Html::submitButton(Yii::t('users', 'Восстановить'), ['class' => 'btn btn-success btn-lg mb-5']) ?>
            </div>
          </div>
        </div>


        <p class="help-block help-block-error"></p>

      <?php else: ?>
        <div class="margin-bottom-10 col-lg-12 col-12 pr-5 pl-5">
            <?= $form->field($model, 'password')->passwordInput() ?>
        </div>
        <div class="margin-bottom-10 col-lg-12 col-12 pr-5 pl-5">
            <?= $form->field($model, 'password_repeat')->passwordInput() ?>
        </div>

          <?= Html::submitButton(Yii::t('users', 'Создать новый пароль'), ['class' => 'btn btn-success btn-lg mb-5']) ?>

      <?php endif; ?>

      <?php ActiveForm::end() ?>

</div>
