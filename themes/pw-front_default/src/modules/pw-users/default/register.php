<?php
Yii::$app->asset = \pw\front_default\BaseAssets::register($this);
?>

<div class="row login">
<div class="col-12 text-center">
    <img src="<?=Yii::$app->asset->baseUrl?>/img/img-login.png" class="img-fluid">
</div>
<div class="col-12 text-center">
    <b>Пройти регистрацию</b>
</div>
<div class="col-12 text-center">
    Если вы уже зарегистрированы, перейдите на страницу<br/><a href="<?=\yii\helpers\Url::to(['/pw-users/default/login'])?>" id="auth-link">входа в систему</a>
</div>
<?php $form = \pw\ui\form\ActiveForm::begin([
    'id' => 'form-signup',
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'action' => ['/pw-users/default/signup'],
    'options' => [
        'class' => 'col-12',
    ],
    'validationUrl' => ['/pw-users/default/validate'],
    'fieldConfig' => [
        'inputOptions' => [
            'class' => 'form-control col-md-9'
        ],
    ],
]); ?>
<div class="form-group row field-orderform-firstname col-12 row justify-content-center">
    <div class="col-4">
        <?= $form->field($SignupForm, 'firstname', ['template' => '{input}{error}{hint}'])->textInput(['class' => 'col-12 form-control', 'placeholder' => 'Имя, Отчество:'])->label(false); ?>
    </div>
</div>
<div class="form-group row field-orderform-firstname col-12 row justify-content-center">
    <div class="col-4">
        <?= $form->field($SignupForm, 'lastname', ['template' => '{input}{error}{hint}'])->textInput(['class' => 'col-12 form-control', 'placeholder' => 'Фамилия:'])->label(false); ?>
    </div>
</div>
<div class="form-group row field-orderform-firstname col-12 row justify-content-center">
    <div class="col-4">
        <?= $form->field($SignupForm, 'email', ['template' => '{input}{error}{hint}'])->textInput(['class' => 'col-12 form-control', 'placeholder' => 'E-mail:'])->label(false); ?>
    </div>
</div>
<div class="form-group row field-orderform-firstname col-12 row justify-content-center">
    <div class="col-4">
        <?= $form->field($SignupForm, 'phone', ['template' => '{input}{error}{hint}'])
            ->widget(\yii\widgets\MaskedInput::class, [
                'mask' => '+7(999)-999-99-99',
            ])->textInput(['placeholder' => 'Телефон:', 'class' => 'col-12 form-control'])->label('Телефон:'); ?>
    </div>
</div>
<div class="form-group row field-orderform-firstname col-12 row justify-content-center">
    <div class="col-4">
        <?= $form->field($SignupForm, 'address', ['template' => '{input}{error}{hint}'])->textInput(['class' => 'col-12 form-control', 'placeholder' => 'Адрес:'])->label(false); ?>
    </div>
</div>
<div class="form-group row field-orderform-firstname col-12 row justify-content-center">
    <div class="col-4">
        <?= $form->field($SignupForm, 'region', ['template' => '{input}{error}{hint}'])->dropDownList(\zn\zones\models\District::getDistrictList(), ['id' => 'region_id', 'placeholder' => 'Регион'])//->textInput(['class' => 'col-12 form-control', 'placeholder' => 'Регион:'])->label(false); ?>
    </div>
</div>
<div class="form-group row field-orderform-firstname col-12 row justify-content-center">
    <div class="col-4">
        <?= $form
            ->field($SignupForm, 'city', ['template' => '{input}{error}{hint}'])
            ->widget(\kartik\depdrop\DepDrop::class, [
                'options'=>['id'=>'subcat-id'],
                'pluginOptions'=>[
                    'depends'=>['region_id'],
                    'placeholder'=>'Город:',
                    'url'=>\yii\helpers\Url::to(['/zn-zones/default/sub-city'])
                ]
            ]); ?>
    </div>
</div>
<div class="col-12 row justify-content-center">
    <?= \yii\helpers\Html::submitButton(Yii::t('users', 'Зарегистрироваться'), ['class' => 'btn ha', 'id' => 'ajax-sub']) ?>
</div>
<?php \pw\ui\form\ActiveForm::end() ?>
</div>
<style>
    .ha {
        text-transform: uppercase;
        color: #fff;
        background-color: #e6004c;
        padding: 20px 50px;
        border-radius: 50px;
    }

    .ha {
        text-transform: uppercase;
        height: 60px;
        border-radius: 55px;
    }
    .login {
        padding: 15px;
    }
</style>
