<?php

use pw\ui\form\ActiveForm;

$authUrl = \yii\helpers\Url::to(['/pw-users/default/login']);
Yii::$app->asset = \pw\front_default\ModalAsset::register($this);
\yii\web\YiiAsset::register($this);
$userSettings = Yii::$app->settings['pw-users']['settings'];
?>
<div class="col-12 text-center">
    <img src="<?= Yii::$app->asset->baseUrl ?>/img/img-login.png" class="img-fluid">
</div>
<div class="col-12 text-center">
    <b>Войти в личный кабинет</b>
</div>
<?php $form = ActiveForm::begin([
    'id' => 'form-auth',
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'validateOnBlur' => false,
    'validateOnChange' => false,
    'validateOnSubmit' => true,
    'validationUrl' => ['/pw-users/default/validate'],
    'fieldConfig' => [
        'inputOptions' => [
            'class' => 'form-control col-md-9'
        ],
    ],
]); ?>
<div class="form-group row field-orderform-firstname">
    <div class="col-md-12">
        <?= $form->field($loginForm, 'phone', ['template' => '{input}{error}{hint}'])->widget(\yii\widgets\MaskedInput::class, [
            'mask' => '+7(999)-999-99-99',
        ])->textInput(['class' => 'col-12 form-control', 'placeholder' => 'Телефон'])->label(false); ?>
    </div>
</div>
<div class="form-group row field-orderform-firstname">
    <div class="col-md-12">
        <?= $form->field($loginForm, 'password', ['template' => '{input}{error}{hint}'])->passwordInput(['class' => 'col-12 form-control', 'placeholder' => 'Пароль'])->label(false); ?>
    </div>
</div>
<div class="col-12">
    <?= \yii\helpers\Html::submitButton(Yii::t('users', 'Войти'), ['class' => 'btn ha btn-60', 'id' => 'ajax-sub']) ?>
</div>
<?php ActiveForm::end() ?>
<div class="col-12 text-center">ИЛИ</div>
<div class="col-12">
    <a class="btn btn-blue btn-60 init-modal" id="register" data-id="reg" href="<?= \yii\helpers\Url::to(['/pw-users/default/signup']) ?>">Пройдите
        регистрацию</a>
</div>
<div class="col-12 text-center mt-3"><a href="<?= \yii\helpers\Url::to(['/pw-users/default/reset']) ?>">Забыли
        пароль?</a></div>
<div class="col-12 text-center">
    А так же, вы можете пройти быструю авторизацию<br/> через социальные сети
    <div class="row justify-content-center">
        <?= yii\authclient\widgets\AuthChoice::widget([
            'baseAuthUrl' => ['/pw-users/default/auth'],
            'popupMode' => false,
        ]) ?>
    </div>
    <!--    <a href="https://oauth.vk.com/authorize?client_id=-->
    <? //=$userSettings['authVkId']?><!--&display=page&redirect_uri=http://pw_basic.my&scope=email&response_type=code&v=5.101"><img-->
    <!--                src="--><? //= Yii::$app->asset->baseUrl ?><!--/img/instagram.png" alt="Instagram" title="Instagram"></a>-->
</div>
<style>
    .ha {
        text-transform: uppercase;
        color: #fff;
        background-color: #e6004c;
        padding: 20px 50px;
        border-radius: 50px;
    }

    .ha {
        text-transform: uppercase;
        height: 60px;
        border-radius: 55px;
    }
</style>
