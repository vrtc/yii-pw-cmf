<?php
/**
 * @var $loginForm \pw\users\frontend\models\LoginForm
 */
Yii::$app->asset = \pw\front_default\BaseAssets::register($this);

?>
<?php $form = \pw\ui\form\ActiveForm::begin([
    'id' => 'form-auth',
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'validationUrl' => ['/pw-users/default/validate'],
    'fieldConfig' => [
        'inputOptions' => [
            'class' => 'form-control col-md-9'
        ],
    ],
]); ?>
<div class="container">
  <div class="row login">
    <div class="col-12 text-center">
      <img src="<?= Yii::$app->asset->baseUrl ?>/img/img-login.png" class="img-fluid">
    </div>
    <div class="col-12 text-center">
      <b>Войти в личный кабинет</b>
    </div>

    <div class="form-group row field-orderform-firstname col-12 row justify-content-center">
      <div class="col-md-4">
          <?= $form->field($loginForm, 'phone', ['template' => '{input}{error}{hint}'])
              ->widget(\yii\widgets\MaskedInput::class, [
              'mask' => '+7(999)-999-99-99',
          ])->textInput(['class' => 'col-12 form-control', 'placeholder' => 'Телефон'])->label(false); ?>
      </div>
    </div>
    <div class="form-group row field-orderform-firstname col-12 row justify-content-center">
      <div class="col-md-4">
          <?= $form->field($loginForm, 'password', ['template' => '{input}{error}{hint}'])->passwordInput(['class' => 'col-12 form-control', 'placeholder' => 'Пароль'])->label(false); ?>
      </div>
    </div>
    <div class="col-12 row justify-content-center">
        <?= \yii\helpers\Html::submitButton(Yii::t('users', 'Войти'), ['class' => 'btn ha', 'id' => 'ajax-sub']) ?>
    </div>

    <div class="col-12 text-center">ИЛИ</div>
    <div class="col-12 row justify-content-center">
      <a href="<?= \yii\helpers\Url::to(['/pw-users/default/signup']) ?>" class="btn btn-blue" id="register" style="padding-top: 10px">Регистрация</a>
    </div>
    <div class="col-12 text-center mt-3"><a href="<?= \yii\helpers\Url::to(['/pw-users/default/reset']) ?>">Забыли пароль?</a></div>
  </div>
  <div class="col-12 text-center">
    А так же, вы можете пройти быструю авторизацию<br/> через социальные сети
    <div class="row justify-content-center">
        <?= yii\authclient\widgets\AuthChoice::widget([
            'baseAuthUrl' => ['/pw-users/default/auth'],
            'popupMode' => false,
        ]) ?>
    </div>
  </div>
</div>
<?php \pw\ui\form\ActiveForm::end() ?>
