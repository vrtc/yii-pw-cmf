<?php
/**
 * @var $SignupForm \pw\users\frontend\models\SignupForm
 */

use pw\ui\form\ActiveForm;

$authUrl = \yii\helpers\Url::to(['/pw-users/default/signup']);
Yii::$app->asset = \pw\front_default\ModalAsset::register($this);
$js = <<<JS
                    $('#auth-link').on('click', function(e){
                       e.preventDefault();
                       $('.init-modal[data-id="auth"]').click();
                    });
     $('#form-signup').on('submit', function(){
         $('#ajax-sub').attr('disabled', true);
         var data = $(this).serialize();
	     
         $.ajax({
            url: '{$authUrl}',
            type: 'POST',
            data: data,
            success: function(res){
                if(res.success){
                $('#otherModal').modal('hide');
                $('#auth-sign').css('display', 'none');
                $('#title-check').removeClass('col-lg-6');
                $('#title-check').addClass('col-lg-12');
	            window.location.reload();
	            }
            },
            error: function(){
               console.log('error');
            }
         });
         $('#ajax-sub').attr('disabled', false);
	 return false;
     });
JS;
//$this->registerJs($js, \pw\web\View::POS_END);

?>
<div class="col-12 text-center">
  <img src="<?= Yii::$app->asset->baseUrl ?>/img/img-login.png" class="img-fluid">
</div>
<div class="col-12 text-center">
  <b>Пройти регистрацию</b>
</div>
<div class="col-12 text-center">
  Если вы уже зарегистрированы, перейдите на страницу<br/><a href="#" id="auth-link">входа в систему</a>
</div>
<?php $form = ActiveForm::begin([
    'id' => 'form-signup',
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'action' => ['/pw-users/default/signup'],
    'options' => [
        'class' => 'col-12',
    ],
    'validationUrl' => ['/pw-users/default/validate'],
    'fieldConfig' => [
        'inputOptions' => [
            'class' => 'form-control col-md-9'
        ],
    ],
]); ?>
<div class="form-group row field-orderform-firstname">
  <div class="col-md-12">
      <?= $form->field($SignupForm, 'phone', ['template' => '{input}{error}{hint}'])
          ->widget(\yii\widgets\MaskedInput::class, [
              'mask' => '+7(999)-999-99-99',
          ])->textInput(['placeholder' => 'Телефон:', 'class' => 'col-12 form-control'])->label('Телефон:'); ?>
  </div>
</div>
<div class="form-group row field-orderform-firstname">
  <div class="col-md-12">
      <?= $form->field($SignupForm, 'firstname', ['template' => '{input}{error}{hint}'])->textInput(['class' => 'col-12 form-control', 'placeholder' => 'Имя:'])->label(false); ?>
  </div>
</div>
<div class="form-group row field-orderform-firstname">
  <div class="col-md-12">
      <?= $form->field($SignupForm, 'lastname', ['template' => '{input}{error}{hint}'])->textInput(['class' => 'col-12 form-control', 'placeholder' => 'Фамилия:'])->label(false); ?>
  </div>
</div>
<div class="form-group row field-orderform-firstname">
  <div class="col-md-12">
      <?= $form->field($SignupForm, 'email', ['template' => '{input}{error}{hint}'])->textInput(['class' => 'col-12 form-control', 'placeholder' => 'E-mail:'])->label(false); ?>
  </div>
</div>
<div class="col-12">
    <?= \yii\helpers\Html::submitButton(Yii::t('users', 'Зарегистрироваться'), ['class' => 'btn ha', 'id' => 'ajax-sub']) ?>
</div>
<?php ActiveForm::end() ?>
