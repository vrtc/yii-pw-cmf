<?php
/**
 * Created by trisport_pw.
 * User: Dmitriy Kindeev
 * Email: dkindeev@mail.ru
 * Date: 24.02.2020
 * Time: 16:04
 */
Yii::$app->asset = Yii::$app->asset != null ? Yii::$app->asset : \pw\front_default\BaseAssets::register($this);
?>
<?php if (Yii::$app->user->isGuest){ ?>
    <a href="#" class="init-modal d-none" data-id="reg"></a>
    <a href="<?= \yii\helpers\Url::to(['/pw-users/default/login']) ?>" class="init-modal"
       data-id="auth"><img
            src="<?= Yii::$app->asset->baseUrl ?>/img/login.png" alt="Войти" title="Войти"><br/>Войти</a>
<?php }else{ ?>

    <div class="dropdown show">
        <a class="btn btn-default dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img src="<?= Yii::$app->asset->baseUrl ?>/img/login.png" alt="Войти" title="Войти" class=""/> <span
                class="d-none d-xl-inline-block"><?= Yii::$app->user->identity->data ? Yii::$app->user->identity->data->firstname . ' ' . Yii::$app->user->identity->data->lastname : Yii::$app->user->identity->email ?></span>
        </a>

        <div class="dropdown-menu lk-dropdown" aria-labelledby="dropdownMenuLink">
            <a class="dropdown-item"
               href="<?= \yii\helpers\Url::to(['/pw-users/profile/orders']) ?>">Заказы</a>
            <a class="dropdown-item"
               href="<?= \yii\helpers\Url::to(['/pw-users/profile/wishlist']) ?>">Список
                желаемого</a>
            <a class="dropdown-item"
               href="<?= \yii\helpers\Url::to(['/pw-users/profile/edit']) ?>">Мои данные</a>
            <a class="dropdown-item"
               href="<?= \yii\helpers\Url::to(['/pw-users/profile/discount']) ?>">Карта</a>
            <a class="dropdown-item"
               href="<?= \yii\helpers\Url::to(['/pw-users/default/logout']) ?>">Выйти</a>
        </div>
    </div>

<?php } ?>
