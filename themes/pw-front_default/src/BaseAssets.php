<?php

namespace pw\front_default;

use yii\web\AssetBundle;

class BaseAssets extends AssetBundle
{

    public $sourcePath = '@pw/front_default/assets';

    public $css = [

    ];

    public $js = [
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap5\BootstrapAsset',
        'yii\bootstrap5\BootstrapPluginAsset'
    ];

}
