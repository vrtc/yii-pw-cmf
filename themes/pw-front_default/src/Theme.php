<?php
namespace pw\front_default;

use pw\ui\assets\DataTablesAsset;
use pw\ui\grid\GridView;
use pw\ui\icons\Icons;
use Yii;

class Theme extends \pw\web\Theme
{

    public $copyright = '© 2011-2018 Все права защищены';
    public $phone = '';
    public $storePhone = '';
    public $email = '';
    public $addressTop = '';
    public $addressBottom = '';
    public $firstCategory = '';
    public $secondCategory = '';

    public function init()
    {
        parent::init();
        Yii::$container->set(GridView::class, [
            'pageSize' => 10,
        ]);

        Yii::$container->set('pw\ui\form\ActiveForm', [
            'layout' => 'horizontal',
            'fieldConfig' => [
                'horizontalCssClasses' => [
                    'label' => 'col-md-2',
                    'wrapper' => 'col-md-9',
                    'error' => '',
                    'hint' => 'col-md-offset-2',
                ],
            ]
        ]);
        Yii::$app->icons->setDefault('font-awesome');
    }

    public function registerBaseAssets($view)
    {
        return BaseAssets::register($view);
    }
}
