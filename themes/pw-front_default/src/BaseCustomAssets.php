<?php

namespace pw\front_default;

use yii\web\AssetBundle;

class BaseCustomAssets extends AssetBundle
{

    public $sourcePath = '@pw/front_default/assets';

    public $css = [
    ];
    public $js = [
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];

}
