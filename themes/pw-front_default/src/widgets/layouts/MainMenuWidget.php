<?php

namespace pw\front_default\widgets\layouts;

use ct\catalog\models\Category;
use ct\catalog\models\Tree;
use yii\helpers\VarDumper;

class MainMenuWidget extends \pw\core\Widget
{
    public function run()
    {
       /// $query = Category::find()->roots()->active()->canMainMenu()->andWhere(['disable_on_main_menu' => 0])->addOrderBy('lft');

        /*$categories = \Yii::$app->cache->getOrSet(md5($query->createCommand()->rawSql), function () use ($query) {
            return $query->all();
        }, 86400);*/
        ///$categories = $query->all();

       /* $categories = Tree::find()->roots()->addOrderBy('root, lft')->one();
        $categories = Tree::find()->roots()->addOrderBy('root, lft')->one();*/

        $query = Tree::find()->where(['depth' => 1, 'status' => 1, 'disable_on_main_menu' => 0, 'empty' => Category::NOT_EMPTY])->addOrderBy('lft');
        $categories = $query->all();

      //  $categories = Category::find()->roots()->addOrderBy('root, lft')->active()->canMainMenu()->all();
        return $this->render('main_menu', [
            'categories' => $categories,
        ]);
    }
}
