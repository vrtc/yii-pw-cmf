<?php

namespace pw\front_default\widgets\layouts;

use ct\catalog\models\Category;
use pw\pages\models\Pages;

class FooterMenuWidget extends \pw\core\Widget
{
    public $view;

    public function run()
    {
        if($this->view == 'footer_categories') {
            $pages = Category::find()->roots()->active()->all();
        }elseif($this->view == 'footer_system'){
            $pages = Pages::findOne(['slug' => 'sistemnye-stranicy'])->getChildren()->all();

        }elseif($this->view == 'footer_buyer'){
            $pages = [];
        }
        return $this->render($this->view, [
            'pages' => $pages,
        ]);
    }
}
