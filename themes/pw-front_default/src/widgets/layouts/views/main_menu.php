<?php

/**
 * @var $this \pw\web\View
 * @var $categories \ct\catalog\models\Category[]
 * @var $child \ct\catalog\models\Category
 */

use ct\catalog\models\Category;
?>


<nav class="navbar navbar-expand-lg navbar-light bg-light-" id="top_nav">


      <div class="col-8-- d-block d-md-none order-0">
        <form action="<?= \yii\helpers\Url::to(['/ct-catalog/default/search']) ?>" class="nav-search search-container-2" method="GET" id="search">
          <input class="inp-s-2 searchclick" type="text" placeholder="Поиск"  name="q" autocomplete="off" value="<?= Yii::$app->request->get('q', ''); ?>">
          <button type="submit"><i class="fas fa-search"></i></button>
        </form>
      </div>


  <button class="navbar-toggler order-2" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <?php if ($this->beginCache('main_menu_header', ['duration' => 86400])) { ?>
    <ul class="navbar-nav mr-auto">
        <?php  foreach ($categories as $category) { ?>
            <?php if ($categoryChildren = $category->children(1)->andWhere(['disable_on_main_menu' => 0, 'empty' => Category::NOT_EMPTY])->all()){

                ?>
            <li class="nav-item dropdown">

              <a class="nav-link dropdown-toggle" href="<?= \yii\helpers\Url::to(['/ct-catalog/default/index', 'slug' => $category->slug]) ?>" id="navbarDropdown<?= $category->id ?>"
                 role="button"
                 data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <?= $category->name ?>
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown<?= $category->id ?>">
                <div class="container">
                  <div class="row-column">
                      <?php foreach ($categoryChildren as $child) { ?>
                        <div class="submenu">
                          <a class="dropdown-item text-left"
                             href="<?= \yii\helpers\Url::to(['/ct-catalog/default/index', 'slug' => $child->slug]) ?>"><span><?= $child->name ?></span></a>

                        </div>
                      <?php } ?>
                      <div class="submenu">
                          <?php if(isset($category->sales_empty) && !$category->sales_empty) : ?>
                              <a class="dropdown-item text-left sales-link" href="<?= \yii\helpers\Url::to(['/ct-catalog/default/sales', 'slug' => $category->slug]) ?>"><span>Акции</span></a>
                          <?php endif; ?>
                          <?php if(isset($category->latest_empty) && !$category->latest_empty) : ?>
                              <a class="dropdown-item text-left sales-link" href="<?= \yii\helpers\Url::to(['/ct-catalog/default/latest', 'slug' => $category->slug]) ?>"><span>Новинки</span></a>
                          <?php endif; ?>
                      </div>
                  </div>
                </div>
              </div>
            </li>
            <?php } else {?>
            <li class="nav-item">
              <a class="nav-link"
                 href="<?= \yii\helpers\Url::to(['/ct-catalog/default/index', 'slug' => $category->slug]) ?>"><?= $category->slug == 'podarki-na-novyy-god' ? '<img src="' . Yii::$app->asset->baseUrl . '/img/hy.svg" class="hy_button" alt="подарки на новый год">' : $category->name ?></a>
            </li>
            <?php } ?>
        <?php } ?>
      <li class="nav-item">
        <a class="nav-link sales-link" href="<?= \yii\helpers\Url::to(['/ct-catalog/default/sales']) ?>">Акции</a>
      </li>
      <li class="nav-item">
        <a class="nav-link latest" href="<?= \yii\helpers\Url::to(['/ct-catalog/default/latest']) ?>">Новинки</a>
      </li>
    </ul>

    <?php
    $this->endCache();
}?>


  </div>
  <div class="justify-content-center cart-in-menu pl-3 pr-4 order-1 show-mini-cart"></div>
</nav>
<?php
$url_search = \yii\helpers\Url::to(['/ct-catalog/default/search']);
$baseUrl = Yii::$app->asset->baseUrl;

$js = <<<JS
    var search_url = '{$url_search}';
    var picurl = '{$baseUrl}';
JS;
$this->registerJs($js, \pw\web\View::POS_HEAD);
?>
