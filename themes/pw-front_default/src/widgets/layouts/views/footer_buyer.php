<div class="col-md-3 col-sm-6">
    <div class="footer-col">
        <div class="footer-title">
            Покупателю
        </div>
        <div class="footer-title-line"></div>
        <div class="footer-col-link">
            <a href="<?=\yii\helpers\Url::to(['/ct-catalog/default/brands'])?>">
                Производители
            </a>
        </div>
        <div class="footer-col-link">
            <a href="<?=\yii\helpers\Url::to(['/rd-order/checkout/voucher'])?>">
                Подарочные сертификаты
            </a>
        </div>
        <div class="footer-col-link">
            <a href="<?=\yii\helpers\Url::to(['/ct-catalog/default/sales'])?>">
                Скидки
            </a>
        </div>
        <div class="footer-col-link">
            <a href="<?=\yii\helpers\Url::to(['/pw-web/default/contacts'])?>">
                Контакты
            </a>
        </div>
<!--        <div class="footer-col-link">-->
<!--            <a href="--><?//=\yii\helpers\Url::to(['/pw-pages/default/error'])?><!--">-->
<!--                Возврат товара-->
<!--            </a>-->
<!--        </div>-->
        <div class="footer-col-link">
            <a href="<?=\yii\helpers\Url::to(['/pw-web/default/map'])?>">
                Карта сайта
            </a>
        </div>
        <div class="footer-col-link">
            <a href="<?=\yii\helpers\Url::to(['/pw-pages/default/system', 'slug' => 'policy'])?>">
                Политика конфиденциальности
            </a>
        </div>
    </div>
</div>