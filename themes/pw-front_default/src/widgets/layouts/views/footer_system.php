<?php
/**
 * @var $pages \pw\pages\models\Pages[]
 */
?>
<div class="col-md-3 col-sm-6">
    <div class="footer-col">
        <div class="footer-title">
            О компании
        </div>
        <div class="footer-title-line"></div>
        <?php foreach ($pages as $page): ?>
            <?php if ($page->slug == 'policy' || $page->slug == 'voucher') {
                continue;
            } ?>
            <div class="footer-col-link">
                <a href="<?= \yii\helpers\Url::to(['/pw-pages/default/system', 'slug' => $page->slug]) ?>">
                    <?= $page->name ?>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
</div>
