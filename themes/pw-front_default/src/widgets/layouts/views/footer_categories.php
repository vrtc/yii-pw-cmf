<?php
/**
 * @var $pages \ct\catalog\models\Category[]
 */
?>
<div class="col-md-3 col-sm-6">
    <div class="footer-col">
        <div class="footer-title">
            Категории
        </div>
        <div class="footer-title-line"></div>
        <?php foreach ($pages as $page): ?>
            <div class="footer-col-link">
                <a href="<?= \yii\helpers\Url::to(['/ct-catalog/default/index', 'slug' => $page->slug]) ?>">
                    <?= $page->name ?>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
</div>