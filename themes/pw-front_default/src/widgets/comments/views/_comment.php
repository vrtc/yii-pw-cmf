<?php

use yii\helpers\Html;
use pw\comments\helpers\CommentsHelper;

/** @var $model \pw\comments\models\Comments */
/** @var $nestedLevel */
/** @var $maxNestedLevel */
/** @var $widget */

?>
<div class="card <?= isset($innerClass) ? $innerClass : ''; ?>">
  <div class="card-body">

        <p>
          <a class="float-left" href="#"><strong><?= $model->getAuthorName() ?></strong></a>
            <?php for ($i = 0; $i < $model->score; $i++): ?>
              <span class="float-right"><i class="text-warning fa fa-star"></i></span>
            <?php endfor; ?>

        </p>
        <div class="clearfix"></div>
        <p><?= Html::encode($model->content); ?></p>
        <p class="text-secondary text-left"><?= $model->getPostedDate() ?></p>
        <p>
          <a class="float-right btn btn-outline-primary ml-2 comment-reply" data-action="reply" href="#">
            <i class="fa fa-reply"></i> Ответить</a>
        </p>
      <?php if ($nestedLevel < $maxNestedLevel): ?><?php if ($model->hasChildren()) : ?><?php $nestedLevel++; ?><?php foreach ($model->getChildren() as $children) : ?>
        <div class="media-0" data-key="<?php echo CommentsHelper::encodeId($children->id); ?>">
            <?= $this->render('_comment', [
                'model' => $children,
                'nestedLevel' => $nestedLevel,
                'maxNestedLevel' => $maxNestedLevel,
                'widget' => $widget,
                'innerClass' => 'card-inner'
            ]) ?>
        </div>
      <?php endforeach; ?><?php endif; ?><?php endif; ?>
      <div class="d-none" itemprop="review" itemscope itemtype="http://schema.org/Review">
          <span itemprop="name">Отзыв</span>
          <span itemprop="author"><?= $model->getAuthorName() ?></span>
          <meta itemprop="datePublished" content="<?= $model->getPostedDate() ?>">
          <div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
          <meta itemprop="worstRating" content="1">
          <span itemprop="ratingValue"><?= (!empty($model->score)) ? $model->score : 5 ?></span>
          <span itemprop="bestRating">5</span>
          </div>
          <span itemprop="description"><?= Html::encode($model->content); ?></span>
      </div>
  </div>
</div>
