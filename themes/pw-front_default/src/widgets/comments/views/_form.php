<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use pw\ui\form\ActiveForm;
use pw\comments\helpers\CommentsHelper;

/** @var $commentModel */
/** @var $widget */

?>
<div id="<?= $widget->formContainerId ?>" class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 comments-input">

        <?php if (Yii::$app->user->isGuest && ($widget->guestComments === false)): ?>

            <div class="disabled-form">
                <?= Yii::t('comments', '<a href="{url}">Log in</a> to post a comment.', ['url' => Url::to(Yii::$app->getUser()->loginUrl)]) ?>
            </div>

        <?php else: ?>

            <?php  $form = ActiveForm::begin([
                'action' => Url::to(
                    [
                        '/pw-comments/default/create', 'data' => CommentsHelper::encryptData(
                            [
                                'url' => $commentModel->url,
                                'model' => $commentModel->model,
                                'model_key' => $commentModel->model_key
                            ]
                        )
                    ]
                ),
                'enableAjaxValidation' => true,
                'validationUrl' => Url::to(['/pw-comments/default/validate']),
                'validateOnChange' => false,
                'validateOnBlur' => false,
                'enableClientValidation' => false,
                'options' => [
                    'id' => 'comment_'.$widget->formId
                ]
            ]) ?>

            <div id="media" class="media-0">
                <div class="media-body row">
                    <div class="col-md-12 pl-md-5 pl-5 pr-5">
                        <?= $form->field($commentModel, 'score')->widget(\kartik\rating\StarRating::class, [
                            'pluginOptions' => [
                                'step' => 1,
                                'filledStar' => '&#x2605;',
                                'emptyStar' => '&#x2606;'
                            ],
                        ])->label(false);?>
                    <?= $form->field($commentModel, 'content', ['template' => '{input}{error}'])->textarea(['placeholder' => Yii::t('comments', 'Напишите здесь свой отзыв...'), 'rows' => 3]) ?>
                    </div>
                    <div class="media-buttons col-md-12 col-12 pl-5 pr-5">
                        <div class="row nospace comment-buttons">
                            <?php if (Yii::$app->user->isGuest){ ?>
                                <?php if ($commentModel->username === null || $commentModel->email === null) { ?>
                                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 user-data">
                                        <?= $form->field($commentModel, 'username')->textInput([
                                            'maxlength' => true,
                                            'class' => 'form-control input-sm',
                                            'placeholder' => Yii::t('comments', 'Имя')
                                        ])->label(false) ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 user-data">
                                        <?= $form->field($commentModel, 'email')->textInput([
                                            'maxlength' => true,
                                            'email' => true,
                                            'class' => 'form-control input-sm',
                                            'placeholder' => Yii::t('comments', 'Email')
                                        ])->label(false) ?>
                                    </div>
                                <?php }  ?>
                            <?php } ?>


                            <?php if (Yii::$app->user->isGuest && ($commentModel->username === null || $commentModel->email === null)) { ?>
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-6 text-right">

                                    <?= Html::submitButton(Yii::t('comments', 'Добавить'), [
                                        'id' => $widget->submitButtonId,
                                        'class' => 'btn btn-primary',
                                        'data' => [
                                            'action' => Url::to(
                                                [
                                                    '/pw-comments/default/create', 'data' => CommentsHelper::encryptData(
                                                    [
                                                        'url' => $commentModel->url,
                                                        'model' => $commentModel->model,
                                                        'model_key' => $commentModel->model_key
                                                    ]
                                                )
                                                ]
                                            )
                                        ]
                                    ]) ?>
                                </div>
                            <?php } else { ?>
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-6 text-right">
                                  <?= Html::submitButton(Yii::t('comments', 'Добавить'), [
                                        'id' => $widget->submitButtonId,
                                        'class' => 'btn btn-primary btn-xs',
                                        'data' => [
                                            'action' => Url::to(
                                                [
                                                    '/pw-comments/default/create', 'data' => CommentsHelper::encryptData(
                                                        [
                                                            'url' => $commentModel->url,
                                                            'model' => $commentModel->model,
                                                            'model_key' => $commentModel->model_key
                                                        ]
                                                    )
                                                ]
                                            )
                                        ]
                                    ]) ?>
                                </div>
                            <?php } ?>

                        </div>
                    </div>
                    <!-- To check -->
                    <noscript>
                        <div class="media-buttons active-media-buttons text-right">
                            <?= Html::button(
                                Yii::t('comments', 'Отмена'), [
                                    'id' => 'reply-cancel',
                                    'class' => 'btn btn-default btn-xs reply-cancel',
                                    'type' => 'reset',
                                    'data' => [
                                        'action' => 'cancel-reply'
                                    ]
                                ]
                            )
                            ?>
                            <?= Html::submitButton(
                                Yii::t('comments', 'Отправить'), [
                                    'class' => 'btn btn-primary btn-xs comment-submit',
                                ]
                            )
                            ?>
                        </div>
                    </noscript>

                </div>
            </div>

            <?php $form->end(); ?>

        <?php endif; ?>

    </div>
</div>
