<?php
/**
 * Created by trisport_pw.
 * User: Dmitriy Kindeev
 * Email: dkindeev@mail.ru
 * Date: 21.05.19
 * Time: 15:38
 */
?>
<?php

use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\widgets\ListView;
use pw\comments\helpers\CommentsHelper;
use pw\comments\Module as CommentsModule;

/** @var $commentModel */
/** @var $commentsCounter */
/** @var $dataProvider */
/** @var $widget */

$cacheKey = $commentModel->url . $commentModel->model . $commentModel->model_key;
$cacheTag = Url::previous(CommentsModule::getInstance()->urlCacheSessionKey);
$cacheProperties = CommentsHelper::getCacheProperties($cacheTag);
?>
<div class="col-12">
  <div class="product-main-reviews row">
    <div class="product-main-reviews-left col-lg-8 col-8 text-center text-md-left">
      <div>Отзывы</div>
      <div>
        <img src="<?= Yii::$app->asset->baseUrl ?>/img/star-review.png"><img src="<?= Yii::$app->asset->baseUrl ?>/img/star-review.png"><img src="<?= Yii::$app->asset->baseUrl ?>/img/star-review.png"><img src="<?= Yii::$app->asset->baseUrl ?>/img/star-review.png"><img src="<?= Yii::$app->asset->baseUrl ?>/img/star-review-2.png">
      </div>
      <div class="product-main-reviews-number"> (<?= $commentsCounter->count() ?>) отзывов</div>
    </div>
    <div class="product-main-reviews-right col-lg-4 col-4 text-center text-md-right">
      <a href="#<?=$id?>" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="comments-collapse">Подробнее
        <img src="<?= Yii::$app->asset->baseUrl ?>/img/readmore.png" alt="Подробнее" class="d"></a>
    </div>
  </div>
</div>
<div class="col-12">
  <div id="<?=$id?>" class="collapse">
    <div id="<?= $widget->wrapperId ?>" class="comments">
      <div id="<?= $widget->fullCommentsId ?>" >
          <?php Pjax::begin([
              'id' => $widget->pjaxContainerId
          ]); ?>
          <?= ListView::widget(
              array_merge(
                $widget->getListViewConfig(),
                  [
                      'dataProvider' => $dataProvider,
                      'emptyText' => 'Ваш отзыв будет первым!'

                  ]
              )
          ) ?>
          <?= $this->render('_form', [
              'commentModel' => $commentModel,
              'widget' => $widget
          ]); ?>

          <?php Pjax::end(); ?>
      </div>
    </div>
  </div>
</div>
