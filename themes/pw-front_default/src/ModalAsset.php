<?php

namespace pw\front_default;

use yii\web\AssetBundle;

class ModalAsset extends AssetBundle
{

    public $sourcePath = '@pw/trisport/assets';

    public $css = [
    ];

    public $js = [
    ];


}
