<?php
/**
 * Created by trisport_pw.
 * User: Dmitriy Kindeev
 * Email: dkindeev@mail.ru
 * Date: 17.05.19
 * Time: 11:00
 */
if(!\Yii::$app->request->isConsoleRequest) {
    Yii::$app->asset = \pw\front_default\BaseAssets::register($this);
}
$asset = isset(Yii::$app->asset) ? Yii::$app->asset : null;
?>
<!doctype html>
<html>
<head>
  <meta name="viewport" content="width=device-width" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title><?= $title; ?></title>
  <style>


    /* -------------------------------------
        RESPONSIVE AND MOBILE FRIENDLY STYLES
    ------------------------------------- */
    @media only screen and (max-width: 620px) {
      table[class=body] h1 {
        font-size: 28px !important;
        margin-bottom: 10px !important;
      }

      table[class=body] p,
      table[class=body] ul,
      table[class=body] ol,
      table[class=body] td,
      table[class=body] span,
      table[class=body] a {
        font-size: 16px !important;
      }

      table[class=body] .wrapper,
      table[class=body] .article {
        padding: 10px !important;
      }

      table[class=body] .content {
        padding: 0 !important;
      }

      table[class=body] .container {
        padding: 0 !important;
        width: 100% !important;
      }

      table[class=body] .main {
        border-left-width: 0 !important;
        border-radius: 0 !important;
        border-right-width: 0 !important;
      }

      table[class=body] .btn table {
        width: 100% !important;
      }

      table[class=body] .btn a {
        width: 100% !important;
      }

      table[class=body] .img-responsive {
        height: auto !important;
        max-width: 100% !important;
        width: auto !important;
      }
    }

    /* -------------------------------------
        PRESERVE THESE STYLES IN THE HEAD
    ------------------------------------- */
    @media all {
      .ExternalClass {
        width: 100%;
      }

      .ExternalClass,
      .ExternalClass p,
      .ExternalClass span,
      .ExternalClass font,
      .ExternalClass td,
      .ExternalClass div {
        line-height: 100%;
      }

      .apple-link a {
        color: inherit !important;
        font-family: inherit !important;
        font-size: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
        text-decoration: none !important;
      }

      .btn-primary table td:hover {
        background-color: #34495e !important;
      }

      .btn-primary a:hover {
        background-color: #34495e !important;
        border-color: #34495e !important;
      }
    }
  </style>
</head>
<body class="">
<span class="preheader"><?= $title; ?></span>
<?php if($asset) { ?>
<div style="text-align: center; padding-top: 30px;"><img style="max-width: 150px" src="<?= \yii\helpers\Url::base(true) ?><?= $asset->baseUrl ?>/img/logo1.png" alt="Лучшее для триатлона" title="Лучшее для триатлона"></div>
<?php } ?>
<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body">
  <tr>
    <td>&nbsp;</td>
    <td class="container">
      <div class="content">
        <!-- START CENTERED WHITE CONTAINER -->
        <table role="presentation" class="main">
          <!-- START MAIN CONTENT AREA -->
          <tr>
            <td class="wrapper">
              <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td>
                      <?= $content; ?>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <!-- END MAIN CONTENT AREA -->
        </table>
        <!-- END CENTERED WHITE CONTAINER -->
        <!-- START FOOTER -->
        <div class="footer">
          <table role="presentation" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td class="content-block">
                <span class="apple-link"><?= Yii::$app->settings['pw-front_default']['settings']['phone'] ?></span><br>
                <span class="apple-link"><?= Yii::$app->settings['pw-front_default']['settings']['addressTop'] ?></span>
              </td>
            </tr>
            <tr>
              <td class="content-block powered-by">

                <a href="<?= env('BASE_URL') ?>">Три-спорт</a>.
              </td>
            </tr>
          </table>
        </div>
        <!-- END FOOTER -->
      </div>
    </td>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>

