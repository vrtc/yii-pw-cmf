<?php
namespace pw\front_default;

use pw\modules\ThemeInstaller;
use pw\pages\models\Pages;
use pw\pages\models\PagesGroup;
use pw\web\Theme;

class Installer extends ThemeInstaller
{

    public function getVersion(): string
    {
        return '0.1';
    }

    public function getKey(): string
    {
        return "pw-front_default";
    }

    public function getName(): string
    {
        return 'Pw Default theme';
    }

    public function getAuthors(): array
    {
        return ['i@yar.pw'];
    }

    public function getLink(): string
    {
        return 'http://yar.pw';
    }

    public function getType()
    {
        return Theme::THEME_FRONTEND;
    }

    public function getSourceLanguage(): string
    {
        return 'ru-RU';
    }

    public function afterInstall()
    {
        if(\Yii::$app->getModule('pw-banners')){

        }
    }

    public function afterReinstall()
    {
        if(\Yii::$app->getModule('pw-banners')){

        }
    }
}
