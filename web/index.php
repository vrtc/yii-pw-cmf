<?php
define('PW_BEGIN_TIME', microtime(true));

///include 'block/index.php';
require(__DIR__ . '/../vendor/autoload.php');

// Environment
require(__DIR__ . '/../helpers/env.php');

require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

try {
    $dotenv->required('DB_HOST')->notEmpty();
    $dotenv->required('DB_NAME')->notEmpty();
    $dotenv->required('DB_USER')->notEmpty();
    $dotenv->required('INSTALLED')->allowedValues(['true']);
} catch (\Exception $e) {
    $config = [
        'id' => 'web-installer',
        'basePath' => dirname(__DIR__, 1),
    ];
    $application = new \pw\installer\WebApplication($config);
    $application->run();
    exit;
}
$config = [
    'id' => 'frontend',
    'basePath' => dirname(__DIR__, 1),
];

$application = new \pw\web\Application($config);
$application->run();
