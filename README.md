Pw basic application
============================


DIRECTORY STRUCTURE
-------------------

      modules/            contains pw modules
      runtime/            contains files generated during runtime
      vendor/             contains dependent 3rd-party packages
      web/                contains the entry script and Web resources


 
REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 7.2+ or 8+

INSTALATION & CONFIGURATION 
---------------------------
`git clone git@gitlab.com:vrtc/yii-pw-cmf.git`

`composer install`
 
`php yii installer/install`


default admin user: admin@example.com password: 123qwe

install only one extension

`php yii installer/install pw/users`

Create module migration
`php pw migrate/create 'pw\users\migrations\createUserTable'`
