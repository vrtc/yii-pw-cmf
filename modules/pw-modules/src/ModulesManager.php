<?php

namespace pw\modules;

use Yii;
use yii\db\Exception;
use yii\db\Query;
use yii\helpers\Json;
use pw\modules\models\Modules;

class ModulesManager
{
    const TYPE_EXTENSION = 'module';
    const TYPE_THEME = 'theme';

    private $_installers = [];
    private $_modules;
    private $_notInstalled;
    private $_type;

    public function installed($module = null): array
    {
        return Yii::$app->cache->getOrSet('installed_modules_'.$module, function () use($module) {


            if ($this->_modules === null) {
                $this->_modules = [];
                try {
                    $installed = Modules::find()->with('settings')->all();
                    foreach ($installed as $item) {
                        $key = $item['key'];
                        $this->_modules[$key] = $item->toArray();
                        foreach ($item->settings as $setting) {
                            $value = Json::decode($setting['value']);
                            if ((\is_array($value) && !$value) || $value === null) {
                                $value = Json::decode($setting['value_default']);
                            }
                            $this->_modules[$key]['settings'][$setting['key']] = $value;
                        }
                    }
                } catch (Exception $e) {
                    return [];
                }
            }
            if ($module !== null && isset($this->_modules[$module])) {
                return $this->_modules[$module];
            }
            if ($this->_type !== null) {
                $modules = [];
                foreach ($this->_modules as $key => $module) {
                    if ($this->_type === self::TYPE_EXTENSION && $module['type'] === Modules::TYPE_EXTENSION) {
                        $modules[$key] = $module;
                    } elseif ($this->_type === self::TYPE_THEME && ($module['type'] === Modules::TYPE_THEME_BACKEND || $module['type'] === Modules::TYPE_THEME_FRONTEND || $module['type'] === Modules::TYPE_THEME_EMAIL)) {
                        $modules[$key] = $module;
                    }
                }
                return $modules;
            }

            return $this->_modules;
        }, 43800);
    }

    public function themes()
    {
        $this->_type = self::TYPE_THEME;

        return $this;
    }

    public function modules()
    {
        $this->_type = self::TYPE_EXTENSION;

        return $this;
    }

    public function load()
    {
        $modules  = [];
        $basePath = Yii::$app->basePath;
        foreach ($this->installed() as $key => $module) {
            Yii::setAlias(
                '@' . \str_replace('\\', '/', $module['namespace']),
                "{$basePath}{$module['path']}/src"
            );
            if (!file_exists($basePath . $module['path'] . '/src/Module.php')) {
                continue;
            }
            $modules[$key] = [
                'class' => $module['namespace'] . '\Module',
                'uuid'  => $module['id']
            ];
            if ($module['type'] !== Modules::TYPE_EXTENSION) {
                $modules[$key]['basePath'] = Yii::$app->basePath . '/' . $module['path'];
            }
            if ($module['bootstrap']) {
                Yii::$app->bootstrap[] = $key;
            }
        }
        if ($modules) {
            Yii::$app->setModules($modules);
        }
    }

    public function notInstalled()
    {
        if ($this->_notInstalled === null) {
            $themes = $modules = [];
            if (file_exists(Yii::getAlias('@vendor/pw/modules.php'))) {
                $modules = require Yii::getAlias('@vendor/pw/modules.php');
            }
            $localModules = $this->getLocalModules(Yii::getAlias('@modules'));
            if (file_exists(Yii::getAlias('@vendor/pw/themes.php'))) {
                $themes = require Yii::getAlias('@vendor/pw/themes.php');
            }
            $localThemes         = $this->getLocalModules(Yii::getAlias('@themes'));
            $modules          = array_merge($modules, $localModules, $themes, $localThemes);
            $this->_modules   = null;
            $this->_notInstalled = array_diff_key($modules, $this->installed());
        }
        if ($this->_type !== null) {
            $modules = [];
            foreach ($this->_notInstalled as $key => $module) {
                if ($this->_type === self::TYPE_EXTENSION && $module['type'] === Modules::TYPE_EXTENSION) {
                    $modules[$key] = $module;
                } elseif ($this->_type === self::TYPE_THEME && ($module['type'] === Modules::TYPE_THEME_BACKEND || $module['type'] == Modules::TYPE_THEME_FRONTEND)) {
                    $modules[$key] = $module;
                }
            }

            return $modules;
        }

        return $this->_notInstalled;
    }

    public function all()
    {
        return \array_merge($this->installed(), $this->notInstalled());
    }

    private function getLocalModules($path)
    {
        $modules = [];
        $basePath   = Yii::$app->basePath;
        foreach (new \DirectoryIterator($path) as $dir) {
            if (!$dir->isDot() && $dir->isDir() && file_exists($dir->getPathname() . '/src/Installer.php')) {
                $key              = $dir->getFilename();
                $baseName         = $dir->getPathname();
                $installer        = $this->getModuleInstaller($key);
                $modules[$key] = [
                    'name'      => $installer->getName(),
                    'alias'     => ['@' . $key => $baseName],
                    'type'      => $installer->getType(),
                    'namespace' => $installer->getNamespace(),
                    'version'   => $installer->getVersion(),
                    'authors'   => $installer->getAuthors(),
                    'link'      => $installer->getLink(),
                    'path'      => $installer->getBasePath(),
                ];

            }
        }

        return $modules;
    }

    public function install($key, $onlyMigration = false, $bootstrap = true)
    {

        if (isset($this->_modules[$key])) {
            throw new InstallerException("Module $key already installed");
        }


        $installer = $this->getModuleInstaller($key);


        if($key == 'pw/i18n'){ //т.к. нам требуются таблицы i18n в первую очередь, то сначала мигрируем их
            $installer->runMigrations('up');
        }


        if ($bootstrap) {
            $installer->bootstrap();
        }

        if ($onlyMigration) {

            return $installer->runMigrations('up');
        }


        //if()
        //var_dump($key);exit;
        $module = $installer->run(BaseInstaller::COMMAND_INSTALL);

        if ($module !== false) {
            $this->_modules[$key] = $module;

            return true;
        }

        return false;
    }

    public function reInstall($key, $onlyMigration = false, $bootstrap = true)//reinstall module
    {
        if (!isset($this->_modules[$key])) {
            throw new InstallerException("Module $key not installed");
        }
        $installer = $this->getModuleInstaller($key);

        $this->delete($key);//delete row from pw_module

        if ($bootstrap) {
            $installer->bootstrap();
        }
        if ($onlyMigration) {
            return $installer->runMigrations('down');//migrate down
        }
        $extenion = $installer->run(BaseInstaller::COMMAND_REINSTALL);
        if ($extenion !== false) {
            $this->_modules[$key] = $extenion;

            return true;
        }

        return false;
    }

    public function migrate($key)
    {
        if(!isset($this->_modules[$key])){
            throw new InstallerException("Module $key not installed");
        }
        $installer = $this->getModuleInstaller($key);
        return $installer->runMigrations('up');
    }

    public function update($key, $params = [])
    {
        $module = Modules::findOne(['key' => $key]);
        if ($module) {
            $module->setAttributes($params);

            return $module->save();
        }

        return false;
    }

    public function disable($key)
    {
        return Modules::updateAll(['is_active' => Modules::STATUS_DISABLED], ['key' => $key]);
    }

    public function enable($key)
    {
        return Modules::updateAll(['is_active' => Modules::STATUS_ENABLED], ['key' => $key]);
    }

    public function delete($key)
    {
        return Modules::deleteAll(['key' => $key]);
    }

    /**
     * @param $module
     * @return mixed
     * @throws InstallerException
     */
    protected function getModuleInstaller($module)
    {
        $module = str_replace('-', '\\', $module);
        if (!isset($this->_installers[$module])) {
            $this->_installers[$module] = null;
            $installer                     = str_replace('/', '\\', $module) . '\Installer';
            if (!class_exists($installer)) {
                throw new InstallerException("Installer for $module not found");
            }
            $this->_installers[$module] = new $installer;
        }

        return $this->_installers[$module];
    }

    public function getSettings($key)
    {
        if(isset($this->_modules[$key])){
            return $this->_modules[$key]['settings'];
        }
        return [];
    }
}
