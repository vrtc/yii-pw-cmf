<?php
namespace pw\modules\migrations;

use pw\core\db\Migration;

class M170730102740Init extends Migration
{
    public function up()
    {
        $this->createTable('{{%pw_modules}}', [
            'id'           => $this->bigPrimaryKey()->unsigned(),
            'key'          => $this->string(128)->notNull(),
            'type'         => $this->smallInteger()->notNull(),
            'name'         => $this->string(),
            'version'      => $this->string()->notNull(),
            'authors'      => $this->string(),
            'link'         => $this->string(),
            'namespace'    => $this->string()->notNull(),
            'path'         => $this->string()->notNull(),
            'bootstrap'    => $this->boolean()->notNull(),
            'created_time' => $this->timestamp()->notNull(),
            'updated_time' => $this->timestamp(),
            'is_active'    => $this->boolean()->notNull()
        ]);

        $this->createIndex('idx_key', '{{%pw_modules}}', 'key');
        $this->createIndex('idx_status', '{{%pw_modules}}', 'is_active');

        $this->createTable('{{%pw_modules_widgets}}', [
            'id'           => $this->bigPrimaryKey()->unsigned(),
            'module_id' => $this->bigInteger()->unsigned()->notNull(),
            'class'        => $this->string()->notNull(),
            'params'       => $this->text()
        ]);

        $this->createIndex('idx_extension', '{{%pw_modules_widgets}}', 'module_id');

        $this->addForeignKey(
            'fk_modules_widgets_extension',
            '{{%pw_modules_widgets}}',
            'module_id',
            '{{%pw_modules}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );


        $this->createTable('{{%pw_modules_settings}}', [
            'id'            => $this->primaryKey(),
            'module_id'  => $this->bigInteger()->unsigned(),
            'key'           => $this->string(),
            'value'         => $this->text()->null(),
            'value_default' => $this->text()->notNull(),
            'updated_time'  => $this->timestamp()
        ]);

        $this->addForeignKey(
            'fk_modules_settings_extension',
            '{{%pw_modules_settings}}',
            'module_id',
            '{{%pw_modules}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );
    }

    public function down()
    {
        $this->dropForeignKey('fk_modules_settings_extension', '{{%pw_modules_settings}}');
        $this->dropForeignKey('fk_modules_widget_extension', '{{%pw_modules_widget}}');
        $this->dropTable('{{%pw_modules_settings}}');
        $this->dropTable('{{%pw_modules_widgets}}');
        $this->dropTable('{{%pw_modules}}');
    }

}
