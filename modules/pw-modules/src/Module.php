<?php

namespace pw\modules;

class Module extends \pw\core\Module
{


    public function getNavigation(): array
    {
        return [
            'system' => [
                [
                    'label'  => 'Расширения',
                    'icon'   => 'cubes',
                    'items' => [
                         [
                            'label' => 'Установленные',
                            'url'   => ['/pw-modules/manage/index'],
                        ],
                        [
                            'label' => 'Каталог расширений',
                            'url'   => ['/pw-modules/store/index'],
                            'render'=> false,
                        ],
                    ]
                ]
            ],
            'settings' => [
                [
                    'label' => 'Настройки расширений',
                    'url'   => ['/pw-modules/setting/index'],
                    'icon' => 'sliders-h'
                ],
            ]
        ];
    }

}
