<?php

namespace pw\modules\models;

use yii\behaviors\TimestampBehavior;
use paulzi\jsonBehavior\JsonBehavior;
use pw\core\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "{{%pw_modules}}".
 *
 * @property string $id
 * @property integer $type
 * @property string $key
 * @property string $name
 * @property string $version
 * @property string $authors
 * @property string $link
 * @property string $namespace
 * @property string $path
 * @property boolean $bootstrap
 * @property string $created
 * @property string $updated
 * @property integer $status
 *
 * @property Widgets[] $widgets
 * @property Settings[] $settings
 */
class Modules extends ActiveRecord
{

    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    const TYPE_EXTENSION = 1;
    const TYPE_THEME_FRONTEND = 2;
    const TYPE_THEME_BACKEND = 3;
    const TYPE_THEME_EMAIL = 4;

    public static function tableName()
    {
        return '{{%pw_modules}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_time', 'updated_time'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_time'],
                ],
                'value' => new Expression("NOW()"),
            ],
            [
                'class' => JsonBehavior::class,
                'attributes' => ['authors'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_active'], 'integer'],
            [['type'], 'in', 'strict' => true, 'range' => [self::TYPE_EXTENSION, self::TYPE_THEME_FRONTEND, self::TYPE_THEME_BACKEND, self::TYPE_THEME_EMAIL]],
            [['bootstrap'], 'boolean'],
            [['key'], 'unique'],
            [['key', 'name', 'version', 'link', 'namespace', 'path'], 'string', 'max' => 255],
            [['authors', 'created_time', 'updated_time'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Key',
            'class' => 'Class',
            'name' => 'Name',
            'version' => 'Version',
            'authors' => 'Authors',
            'link' => 'Link',
            'updated' => 'Last Update',
            'created' => 'Installed',
            'is_active' => 'Status',
        ];
    }

    public static function find()
    {
        return new ModulesQuery(get_called_class());
    }

    public function getWidgets()
    {
        return $this->hasMany(Widgets::class, ['module_id' => 'id']);
    }

    public function getSettings()
    {
        return $this->hasMany(Settings::class, ['module_id' => 'id']);
    }
}
