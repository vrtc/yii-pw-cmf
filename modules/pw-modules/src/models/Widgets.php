<?php
namespace pw\modules\models;

use pw\core\db\ActiveRecord;

/**
 * This is the model class for table "{{%modules_widgets}}".
 *
 * @property string $id
 * @property string $module_id
 * @property string $class
 * @property string $params
 *
 */
class Widgets extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%pw_modules_widgets}}';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['class'], 'string', 'max' => 255],
            [['params'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'class' => 'Class',
            'params' => 'Params',
            'status' => 'Status',
        ];
    }
}
