<?php
namespace pw\modules\models;

use yii\db\ActiveQuery;

class ModulesQuery extends ActiveQuery
{

    public function enabled($state = true)
    {
        $this->andOnCondition(['is_active' => $state ? Modules::STATUS_ENABLED : Modules::STATUS_DISABLED]);
        return $this;
    }

    public function type($type = null)
    {
        if ($type && in_array($type, [Modules::TYPE_EXTENSION, Modules::TYPE_THEME_BACKEND, Modules::TYPE_THEME_FRONTEND])) {
            $this->andOnCondition(['type' => $type]);
        }
        return $this;
    }
}
