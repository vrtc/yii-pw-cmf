<?php
namespace pw\modules\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%modules_settings}}".
 *
 * @property string $module_id
 * @property string $key
 * @property string $value
 * @property string $value_default
 * @property string $updated_time
 *
 * @property Modules $extension
 */
class Settings extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%pw_modules_settings}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key'], 'string', 'max' => 255],
            [['value','value_default'],'string'],
            [['updated_time'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key'=>'Key',
            'value'=>'Значение',
            'value_default'=>'Значение по умолчанию',
            'updated_time'=>'Время обновления'
        ];
    }


    public function getModule()
    {
        return $this->hasOne(Modules::class, ['id' => 'module_id']);
    }
}
