<?php

use pw\ui\grid\GridView;
use yii\helpers\Json;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var pw\modules\backend\models\ModulesSearch $searchModel
 */
?>
<?= GridView::widget([
    'title' => Yii::t('modules', 'Расширения'),
    'icon' => 'cube',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        'name',
        'version',
        [
            'attribute' => 'authors',

            'value' => function ($model) {
                return implode(',', Json::decode($model->authors));
            }
        ],
        'link:url',
        // 'namespace',
        // 'path',
        // 'bootstrap',
        //'created',
        // 'updated',
        'is_active',
    ],
]); ?>
