<?php
/**
 * Created by new_trisport.
 * User: Dmitriy Kindeev
 * Email: dkindeev@mail.ru
 * Date: 01.10.2019
 * Time: 23:04
 */


use DeepCopyTest\Matcher\Y;
use pw\ui\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

$this->title = 'Настройки расширений';
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var pw\modules\backend\models\ModulesSearch $searchModel
 */
?>
<?= GridView::widget([
    'title' => Yii::t('modules', 'Настройки расширений'),
    'icon' => 'cube',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        'key',
        [
            'attribute' => 'value',
            'content' => function($model){
                $value = Json::decode($model->value);

                if ((\is_array($value))) {
                    $value =  implode(', ', $value);
                }
                return $value;
            }
        ],
        [
            'attribute' => 'value_default',
            'content' => function($model){
                $value = Json::decode($model->value);

                if ((\is_array($value))) {
                    $value =  implode(', ', $value);
                }
                return $value;
            }
        ],
        [
            'attribute' => 'module_id',
            'content' => function ($model) {
                return $model->module->name;
            },
            'filter' => ArrayHelper::map(Yii::$app->modulesManager->installed(), 'id', 'name')
        ],
        // 'namespace',
        // 'path',
        // 'bootstrap',
        //'created',
        // 'updated',
        'updated_time:datetime',
    ],
]);
