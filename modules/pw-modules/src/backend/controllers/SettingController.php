<?php
/**
 * Created by new_trisport.
 * User: Dmitriy Kindeev
 * Email: dkindeev@mail.ru
 * Date: 01.10.2019
 * Time: 22:58
 */

namespace pw\modules\backend\controllers;


use pw\modules\backend\models\ModulesSearch;
use pw\modules\backend\models\SettingSearch;
use pw\web\Controller;


class SettingController extends Controller{
    /**
     * Lists all settings
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SettingSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }
}

