<?php

namespace pw\modules\backend\models;

use pw\modules\models\Settings;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use pw\modules\models\Modules;

/**
 * ModulesSearch represents the model behind the search form about `modules\modules\models\Modules`.
 */
class SettingSearch extends Modules
{
    public $module_id;
    public $value;
    public $value_default;
    public function rules()
    {
        return [
            [[ 'module_id'], 'integer'],
            [['key', 'value', 'value_default',  'updated_time'], 'safe'],
        ];
    }

    public function search($params)
    {
        $query = Settings::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'module_id' => $this->module_id,
            'updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'key', $this->key])
            ->andFilterWhere(['like', 'value', $this->value])
            /*->andFilterWhere(['like', 'value_default', $this->value_default])*/;

        return $dataProvider;
    }
}
