<?php

namespace pw\modules\backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use pw\modules\models\Modules;

/**
 * ModulesSearch represents the model behind the search form about `modules\modules\models\Modules`.
 */
class ModulesSearch extends Modules
{
    public function rules()
    {
        return [
            [['id', 'bootstrap', 'is_active'], 'integer'],
            [['key', 'name', 'version', 'authors', 'link', 'namespace', 'path', 'created_time', 'updated_time'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Modules::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'bootstrap' => $this->bootstrap,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'key', $this->key])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'version', $this->version])
            ->andFilterWhere(['like', 'authors', $this->authors])
            ->andFilterWhere(['like', 'link', $this->link])
            ->andFilterWhere(['like', 'namespace', $this->namespace])
            ->andFilterWhere(['like', 'path', $this->path]);

        return $dataProvider;
    }
}
