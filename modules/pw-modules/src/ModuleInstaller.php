<?php

namespace pw\modules;


use Yii;
use yii\helpers\Console;
use yii\helpers\Inflector;
use yii\helpers\Json;
use pw\console\ClearStdOutFilter;
use pw\modules\models\Modules;
use pw\menu\models\Menu;

abstract class ModuleInstaller extends BaseInstaller
{

    final public function getType()
    {
        return self::TYPE_EXTENSION;
    }

    public function afterMigrationUp()
    {
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function afterMigrationDown()
    {
    }


    public function getPermissions(): array
    {
        return [];
    }


    public function getCronJobs(): array
    {
        return [];
    }


    protected function install()
    {
        $transaction = Yii::$app->getDb()->beginTransaction();
        try {
            $module = new Modules();
            $module->setAttributes([
                'type' => Modules::TYPE_EXTENSION,
                'key' => $this->key,
                'namespace' => $this->namespace,
                'version' => $this->getVersion(),
                'authors' => $this->getAuthors(),
                'link' => $this->getLink(),
                'path' => $this->basePath,
                'name' => $this->getName(),
                'bootstrap' => $this->bootstrap,
                'is_active' => 1,
            ]);
            if ($module->save()) {
                $this->id = $module->id;
                $this->loadTranslations();
                $this->extractPermissions();
                $permissions = $this->getPermissions();
                if ($permissions && is_array($permissions)) {
                    $this->addPermissions($permissions);
                }
                $cronJobs = $this->getCronJobs();
                if ($cronJobs && is_array($cronJobs)) {
                    $this->addCronJobs($cronJobs);
                }
                $transaction->commit();

                return $module;
            }
            $transaction->rollBack();
        } catch (\Exception $e) {
            var_dump($e);
            $transaction->rollBack();
            exit;
        }

        return false;
    }


    protected function reInstall()
    {
        $transaction = Yii::$app->getDb()->beginTransaction();
        try {
            $module = new Modules();
            $module->setAttributes([
                'type' => Modules::TYPE_EXTENSION,
                'key' => $this->key,
                'namespace' => $this->namespace,
                'version' => $this->getVersion(),
                'authors' => $this->getAuthors(),
                'link' => $this->getLink(),
                'path' => $this->basePath,
                'name' => $this->getName(),
                'bootstrap' => $this->bootstrap,
                'is_active' => 1,
            ]);
            if ($module->save()) {
                $this->id = $module->id;
                $this->loadTranslations();
                $this->removePermissions();
                $this->extractPermissions();
                $permissions = $this->getPermissions();
                if ($permissions && is_array($permissions)) {
                    $this->addPermissions($permissions);
                }
                $cronJobs = $this->getCronJobs();
                if ($cronJobs && is_array($cronJobs)) {
                    $this->addCronJobs($cronJobs);
                }
                $transaction->commit();

                return $module;
            }
            $transaction->rollBack();
        } catch (\Exception $e) {
            var_dump($e);
            $transaction->rollBack();
            exit;
        }

        return false;
    }

    final protected function removePermissions()
    {
        $paths = [
            '' => Yii::getAlias("@app/$this->basePath/src/controllers/*"),
            'frontend' => Yii::getAlias("@app/$this->basePath/src/frontend/controllers/*"),
            'backend' => Yii::getAlias("@app/$this->basePath/src/backend/controllers/*")
        ];
        $authManager = Yii::$app->authManager;

        foreach ($paths as $type => $path) {
            $iterator = new \GlobIterator($path);
            if ($iterator->count()) {
                $rootPermission = $authManager->getPermission("/$type");
                $permissionName = "/$this->key";
                if ($type) {
                    $permissionName = "/$type" . $permissionName;
                }
                $modulePermission = $authManager->getPermission($permissionName);
                $authManager->remove($modulePermission);
                //   $authManager->addChild($rootPermission, $modulePermission);
                foreach ($iterator as $item) {
                    if ($item->isFile()) {
                        $name = $item->getBasename('.php');
                        $controllerName = Inflector::camel2id(substr($name, 0, -10));
                        $controllerPermission = $authManager->createPermission("$permissionName/$controllerName");
                        $authManager->remove($controllerPermission);
                        //   $authManager->addChild($modulePermission, $controllerPermission);

                        if ($type) {
                            $class = "\\$this->namespace\\$type\\controllers\\$name";
                        } else {
                            $class = "\\$this->namespace\\controllers\\$name";
                        }
                        $controller = new \ReflectionClass($class);
                        $methods = $controller->getMethods(\ReflectionMethod::IS_PUBLIC);
                        foreach ($methods as $method) {
                            if (preg_match('#^action([A-Z]{1}\w+)#', $method->name)) {
                                $permission = $authManager->createPermission(
                                    "$permissionName/$controllerName/" . Inflector::camel2id(substr($method->name, 6))
                                );
                                $authManager->remove($permission);
                                //     $authManager->addChild($controllerPermission, $permission);
                            }
                        }

                    }
                }
            }
        }
    }

    protected function update()
    {

        if (Yii::$app->getModulesManager()->update($this->key, [
            'namespace' => $this->namespace,
            'version' => $this->getVersion(),
            'authors' => $this->getAuthors(),
            'link' => $this->getLink(),
            'path' => $this->basePath,
            'name' => $this->getName(),
            'bootstrap' => class_exists($this->namespace . '\Bootstrap')
        ], true)
        ) {
            $oldSettings = Yii::$app->settings->get($this->key);
            $newSettings = $this->getDefaultSettings($this->namespace . '\Module');
            $moduleSettings = array_diff_key($newSettings, $oldSettings);
            if ($moduleSettings) {
                $settings = [];
                foreach ($moduleSettings as $k => $v) {
                    $settings[$k] = ['value' => $v, 'default' => $v];
                }
                Yii::$app->settings->add($this->key, $settings);
            }
            //$this->addAccessRules();
            //$this->addTranslations();

            $this->addMenu($this->getNavigation());

            return true;
        }

        return false;
    }

    public function remove()
    {
    }

    final public function run(string $command = self::COMMAND_INSTALL)
    {

        if ($command === self::COMMAND_INSTALL || $command === self::COMMAND_UPDATE) {
            $this->runMigrations('up');
            $this->afterMigrationUp();
        }
        if ($command === self::COMMAND_REINSTALL) {
            $this->runMigrations('down', true);
            $this->afterMigrationDown();
            $this->runMigrations('up');
            $this->afterMigrationUp();
        }
        if ($command === self::COMMAND_REMOVE) {
            $this->runMigrations('down');
            $this->afterMigrationDown();
        }

        return parent::run($command);
    }

    final public function runMigrations(string $command = 'up', bool $reInstall = false)
    {

        $basePrefix = Yii::$app->getDb()->tablePrefix;

        list($vendorPrefix, $module) = explode('-', $this->key);

        Yii::$app->controllerMap['migrate']['migrationNamespaces'] = ['\\'.$vendorPrefix.'\\'.$module.'\\migrations'];
        $migrationPath = Yii::$app->basePath . "/$this->basePath/src/migrations";

        if (is_dir($migrationPath)) {
            stream_filter_register('clear', ClearStdOutFilter::class);
            $filter = stream_filter_prepend(STDOUT, 'clear');
            ob_start();
            if ($reInstall) {
                $dir = opendir($migrationPath);
                $count = 0;
                while ($file = readdir($dir)) {//считем количество миграций
                    if ($file == '.' || $file == '..' || is_dir($migrationPath . $file)) {
                        continue;
                    }
                    $count++;
                }
                for ($i = 0; $i < $count; $i++) {//откатываем все миграции
                    Yii::$app->runAction('pw-console/migrate/down-module', [
                        'interactive' => false,
                        'module_id' => $this->getKey(),
                       // 'migrationPath' => $migrationPath,
                    ]);
                }
            } else {

                Yii::$app->runAction('migrate/' . $command, [
                    'interactive' => false,
                    'module_id' => $this->getKey(),
                    // 'migrationPath' => $migrationPath,
                ]);

            }
            ob_end_clean();
            stream_filter_remove($filter);
        }
        Yii::$app->getDb()->tablePrefix = $basePrefix;
    }

    final protected function addPermissions(array $permissions)
    {

        if (!empty($permissions)) {
            $authManager = Yii::$app->getAuthManager();
            $rootPermission = $authManager->getPermission('/');
            $guestRole = $authManager->getRole('guest');
            foreach ($permissions as $key => $data) {
                $key = trim($key, '/');
                $modulePermission = $authManager->getPermission("/$key/$this->key");
                if (!$modulePermission) {
                    $modulePermission = $authManager->createPermission("/$key/$this->key");
                    $authManager->add($modulePermission);
                    $authManager->addChild($rootPermission, $modulePermission);
                }
                foreach ($data as $permName => $params) {
                    $perm = $authManager->getPermission("/$key/$this->key/$permName");
                    if (!$perm) {
                        $perm = $authManager->createPermission("/$key/$this->key/$permName");
                        $authManager->add($perm);
                        $authManager->addChild($modulePermission, $perm);
                    }
                    if (isset($params['description'])) {
                        $perm->description = $params['description'];
                    }
                    if (isset($params['ruleName'])) {
                        $perm->ruleName = $params['ruleName'];
                    }
                    if (isset($params['guest'])) {
                        $authManager->addChild($guestRole, $perm);
                    }
                    $authManager->update("/$key/$this->key/$permName", $perm);
                }
            }
        }
    }


    final protected function addCronJobs(array $jobs = [])
    {
        foreach ($jobs as $command => $params) {
            Yii::$app->cron->add($command, $params);
        }
    }

    final protected function addMenu(array $items = [], $parent = null)
    {
        if ($items) {
            foreach ($items as $key => $child) {
                if ($parent) {
                    $key = "$parent->key/$key";
                }
                $menu = Menu::findOne(['key' => $key]);
                if ($menu && is_array($child)) {
                    if (isset($child['label'])) {
                        $menu->translate($this->getSourceLanguage())->name = $child['label'];
                        if (isset($child['icon'])) {
                            $menu->icon = $child['icon'];
                        }
                        if (isset($child['sort'])) {
                            $menu->sort = $child['sort'];
                        }
                        $menu->save(false);
                    } else {
                        $this->addMenu($child, $menu);
                    }
                } elseif ($parent) {
                    $menu = new Menu(['key' => $key]);
                    $menu->application = 'backend';
                    if (isset($child['icon'])) {
                        $menu->icon = $child['icon'];
                    }
                    if (isset($child['icon'])) {
                        $menu->icon = $child['icon'];
                    }
                    if (isset($child['url'])) {
                        $menu->route = Json::encode([
                            'application' => 'backend',
                            'action' => $child['url'],
                            'idModel' => null
                        ]);
                    }
                    if (isset($child['label'])) {
                        $menu->translate($this->getSourceLanguage())->name = $child['label'];
                    }
                    $menu->appendTo($parent)->save(false);
                    if (!isset($child['label'])) {
                        $this->addMenu($child, $menu);
                    }
                }
                if (isset($child['childs'])) {
                    $childs = $child['childs'];
                    $this->addMenu($childs, $menu);
                }
            }
        }
    }

    final protected function addUrlManagerRules(array $rules)
    {
        if ($rules && is_array($rules)) {
            $prefix = '/' . str_replace('\\', '-', $this->key);
            foreach ($rules as $k => $v) {
                if (is_array($v)) {
                    $rules[$k]['route'] = $prefix . '/' . trim($v['route'], '/');
                } else {
                    $rules[$k] = $prefix . '/' . trim($v, '/');
                }
                $rules[$k]['module'] = $this->uuid;
            }

            Yii::$app->urlManager->addRules($rules);
        }
    }

    final protected function loadTranslations()
    {
        $paths = [
            'frontend' => Yii::getAlias("@app/$this->basePath/src/frontend/messages/*"),
            'backend' => Yii::getAlias("@app/$this->basePath/src/backend/messages/*"),
            'common' => Yii::getAlias("@app/$this->basePath/src/common/messages/*")
        ];
        foreach ($paths as $type => $path) {
            foreach (new \GlobIterator($path) as $item) {
                if ($item->isFile()) {
                    $name = $item->getBasename('.php');
                    //exit;
                }
            }
        }
    }

    final protected function extractPermissions()
    {
        $paths = [
            '' => Yii::getAlias("@app/$this->basePath/src/controllers/*"),
            'frontend' => Yii::getAlias("@app/$this->basePath/src/frontend/controllers/*"),
            'backend' => Yii::getAlias("@app/$this->basePath/src/backend/controllers/*")
        ];
        $authManager = Yii::$app->authManager;

        foreach ($paths as $type => $path) {
            $iterator = new \GlobIterator($path);
            if ($iterator->count()) {
                $rootPermission = $authManager->getPermission("/$type");
                $permissionName = "/$this->key";
                if ($type) {
                    $permissionName = "/$type" . $permissionName;
                }
                $modulePermission = $authManager->createPermission($permissionName);
                $authManager->add($modulePermission);
                $authManager->addChild($rootPermission, $modulePermission);
                foreach ($iterator as $item) {
                    if ($item->isFile()) {
                        $name = $item->getBasename('.php');
                        $controllerName = Inflector::camel2id(substr($name, 0, -10));
                        $controllerPermission = $authManager->createPermission("$permissionName/$controllerName");
                        $authManager->add($controllerPermission);
                        $authManager->addChild($modulePermission, $controllerPermission);

                        if ($type) {
                            $class = "\\$this->namespace\\$type\\controllers\\$name";
                        } else {
                            $class = "\\$this->namespace\\controllers\\$name";
                        }
                        $controller = new \ReflectionClass($class);
                        $methods = $controller->getMethods(\ReflectionMethod::IS_PUBLIC);
                        foreach ($methods as $method) {
                            if (preg_match('#^action([A-Z]{1}\w+)#', $method->name)) {
                                $permission = $authManager->createPermission(
                                    "$permissionName/$controllerName/" . Inflector::camel2id(substr($method->name, 6))
                                );
                                $authManager->add($permission);
                                $authManager->addChild($controllerPermission, $permission);
                            }
                        }

                    }
                }
            }
        }
    }
}
