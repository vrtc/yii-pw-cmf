<?php
namespace pw\modules;

use Yii;
use pw\modules\models\Modules;

abstract class ThemeInstaller extends BaseInstaller
{

    public function install()
    {
        $transaction = Yii::$app->getDb()->beginTransaction();
        try {
            $theme = new Modules();
            $theme->setAttributes([
                'type' => $this->getType(),
                'key' => $this->key,
                'namespace' => $this->namespace,
                'version' => $this->getVersion(),
                'authors' => $this->getAuthors(),
                'link' => $this->getLink(),
                'path' => $this->basePath,
                'name' => $this->getName(),
                'bootstrap' => false,
                'is_active' => 1,
            ]);
            if ($theme->save()) {
                $this->id = $theme->id;
                $this->installSettings();
                $transaction->commit();
                return $theme;
            }
            $transaction->rollBack();
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            $transaction->rollBack();
        }
        return false;
    }


    protected function reInstall()
    {
        $transaction = Yii::$app->getDb()->beginTransaction();
        try {
            $theme = new Modules();
            $theme->setAttributes([
                'type' => $this->getType(),
                'key' => $this->key,
                'namespace' => $this->namespace,
                'version' => $this->getVersion(),
                'authors' => $this->getAuthors(),
                'link' => $this->getLink(),
                'path' => $this->basePath,
                'name' => $this->getName(),
                'bootstrap' => false,
                'is_active' => 1,
            ]);
            if ($theme->save()) {
                $this->id = $theme->id;
                $this->deleteSettings();

             //   $this->installSettings();
                $transaction->commit();
                return $theme;
            }
            $transaction->rollBack();
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            $transaction->rollBack();
        }
        return false;
    }

    public function update()
    {
        if (Yii::$app->getThemesManager()->add($this->key, [
            'namespace' => $this->namespace,
            'version' => $this->getVersion(),
            'authors' => $this->getAuthors(),
            'link' => $this->getLink(),
            'path' => $this->basePath,
            'name' => $this->getName(),
            'type' => $this->getType()
        ], true)
        ) {
            $newSettings = $this->getDefaultSettings($this->namespace . '\Theme');
            $oldSettings = Yii::$app->settings->get($this->key);
            $themeSettings = array_diff_key($newSettings, $oldSettings);
            $settings = [];
            if ($themeSettings) {
                foreach ($themeSettings as $k => $v) {
                    $settings[$k] = ['value' => $v, 'default' => $v];
                }
            }
            $settings['basePath'] = ['value' => $this->basePath, 'default' => null];
            Yii::$app->settings->add($this->key, $settings);
            return true;
        }
        return false;
    }

    public function remove()
    {

    }
}
