<?php
namespace pw\modules;

class InstallerException extends \yii\base\Exception
{
    const NOT_INSTALLED = 0;
    const ALREADY_INSTALLED = 1;


    /**
     * Constructor.
     * @param string $message
     * @param string $module name of module
     * @param integer $code Installer error code
     * @param \Exception $previous The previous exception used for the exception chaining.
     */
    public function __construct($message, $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return string the user-friendly name of this exception
     */
    public function getName()
    {
        return 'Installer Exception';
    }
}
