<?php

namespace pw\modules;

use pw\modules\models\Modules;
use pw\modules\models\Settings;
use pw\modules\models\Widgets;
use Yii;
use yii\base\BootstrapInterface;
use yii\helpers\Json;
use yii\i18n\PhpMessageSource;

abstract class BaseInstaller
{
    const COMMAND_INSTALL   = 'install';
    const COMMAND_UPDATE    = 'update';
    const COMMAND_REMOVE    = 'remove';
    const COMMAND_MIGRATE   = 'migrate';
    const COMMAND_REINSTALL = 'reInstall';

    const TYPE_EXTENSION = Modules::TYPE_EXTENSION;
    const TYPE_THEME_FRONTEND = Modules::TYPE_THEME_FRONTEND;
    const TYPE_THEME_BACKEND = Modules::TYPE_THEME_BACKEND;

    protected $id;
    protected $key;
    protected $namespace;
    protected $basePath;
    protected $bootstrap = false;
    private $_reflection;
    private $_settings = [];

    abstract public function getSourceLanguage(): string;

    abstract public function getVersion(): string;

    abstract public function getKey(): string;

    abstract public function getName(): string;

    abstract public function getAuthors(): array;

    abstract public function getLink(): string;

    abstract public function getType();

    public function getNamespace(): string
    {
        return $this->namespace;
    }

    public function isBootstrap(): bool
    {
        return $this->bootstrap;
    }

    public function getBasePath(): string
    {
        return $this->basePath;
    }

    public function getWidgets(): array
    {
        return [];
    }


    public function beforeInstall()
    {
    }

    public function beforereInstall()
    {
    }

    public function afterreInstall()
    {
    }

    public function afterInstall()
    {
    }

    public function beforeUpdate()
    {
    }

    public function afterUpdate()
    {
    }

    public function beforeRemove()
    {
    }

    public function afterRemove()
    {
    }

    public function __construct()
    {
        $reflect         = new \ReflectionClass($this);
        $basePath        = dirname(pathinfo($reflect->getFileName(), PATHINFO_DIRNAME), 1);
        $this->basePath  = str_replace(Yii::$app->basePath, '', $basePath);
        $this->namespace = $reflect->getNamespaceName();
        $this->key       = $key = str_replace('\\', '-', $this->namespace);
        if (file_exists("$basePath/src/Module.php")) {
            $this->_reflection = new \ReflectionClass("$this->namespace\\Module");
            $this->bootstrap   = $this->_reflection->implementsInterface(BootstrapInterface::class);
        } elseif (file_exists("$basePath/src/Theme.php")) {
            $this->_reflection = new \ReflectionClass("$this->namespace\\Theme");
        }
        Yii::setAlias(
            '@' . \str_replace('\\', '/', $this->getNamespace()),
            Yii::$app->getBasePath() . $this->getBasePath() . '/src'
        );
        Yii::$app->i18n->translations["$this->key/*"] = [
            'class'          => PhpMessageSource::class,
            'sourceLanguage' => $this->getSourceLanguage(),
            'basePath'       => "$this->basePath/src/messages",
        ];
    }

    public function run(string $command)
    {
        $commandName   = ucfirst($command);
        $beforeCommand = 'before' . $commandName;
        $afterCommand  = 'after' . $commandName;

        $this->$beforeCommand();
        $module = $this->{$command}();

        if ($module !== false) {

            $this->installSettings();
            $this->installWidgets();
            $this->$afterCommand();

            return $module;
        }

        return false;
    }

    protected function installSettings()
    {
        if ($this->_reflection) {
            $module       = $this->_reflection->getName();
            $this->_settings = $module::getDefaultSettings();
            if ($this->_settings) {
                $settings = [];
                foreach ($this->_settings as $key => $value) {
                    $value      = Json::encode($value);
                    $settings[] = [$this->id, $key, $value, $value];
                }
                Yii::$app->db->createCommand()->batchInsert('{{%pw_modules_settings}}', [
                    'module_id',
                    'key',
                    'value',
                    'value_default'
                ], $settings)->execute();
            }
        }
    }

    protected function deleteSettings()
    {
        if ($this->_reflection) {
            $settings = Settings::find()->where(['module_id' => $this->id])->all();
            foreach($settings as $setting){
                $setting->delete();
            }
        }
    }

    protected function installWidgets()
    {
        $widgets = $this->getWidgets();
        foreach($widgets as $widget){
            $model = new Widgets();
            $model->module_id = $this->id;
            $model->class  = $widget;
            $model->params = Json::encode($widget::getDefaultSettings());
            $model->save();
        }
    }

    public function bootstrap()
    {
        if (file_exists(Yii::$app->basePath . $this->basePath . '/src/Module.php')) {
            $module = [
                'class' => "$this->namespace\\Module",
            ];
            Yii::$app->setModule($this->key, $module);
            if ($this->bootstrap) {
                $module = Yii::$app->getModule($this->key);
                $module->bootstrap(Yii::$app);
            }
        }
    }
}
