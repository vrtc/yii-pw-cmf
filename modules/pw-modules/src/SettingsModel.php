<?php
namespace pw\modules;

use pw\modules\models\Settings;
use Yii;
use pw\core\Model;
use yii\helpers\Json;
use yii\db\Exception;

class SettingsModel extends Model
{

    private $_tableName = '{{%pw_modules_settings}}';

    public function init()
    {
        $this->attributes = $this->getModule(get_called_class())->getSettings();
    }

    public function save()
    {
        if ($this->validate()) {
            $db = Yii::$app->getDb();
            $transaction = $db->beginTransaction();
            try {
                $id =$this->getModule(get_called_class())->getUUID(true);
                foreach ($this->toArray() as $key => $value) {
                    if ($value !== null) {
                        $value = Json::encode($value);
                    }
                    $model = Settings::findOne(['module_id' => $id, 'key' => $key]);
                    if($model == null){
                        $model = new Settings();
                        $model->module_id = $id;
                        $model->key = $key;
                        $model->value_default = $value;
                    }
                    $model->value = $value;
                    $model->save(false);
                }
                $transaction->commit();
                return true;
            } catch (Exception $e) {
                var_dump($e->getMessage());
                $transaction->rollBack();
                exit;
            }
        }
        return false;
    }
}
