<?php
namespace pw\history\migrations;
use pw\core\db\Migration;
use pw\history\models\History;

class m170316_064910_create_table_history extends Migration
{
    public function up()
    {
        $this->createTable(History::tableName(), [
            'id' => $this->primaryKey(),
            'user_id' => $this->bigInteger()->unsigned(), // user_id
            'ip' => $this->string(32),
            'event' => $this->string(255),
            'class' => $this->string(255),
            'table_name' => $this->string(64),
            'row_id' => $this->string(32),
            'log' => $this->json(),
            'created_at' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey('fk-history-to-user', History::tableName(), 'user_id', \pw\users\models\Users::tableName(), 'id', 'SET NULL', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%pw_history}}');
    }
}
