<?php

namespace pw\history\migrations;

use pw\core\db\Migration;

class m210224_103308_init_history_search_queries extends Migration
{
    public function up()
    {
        $this->createTable('{{pw_history_search_queries}}', [
            'id' => $this->primaryKey(),
            'type' => $this->string(20),
            'query' => $this->string(),
            'created_at' => $this->integer()->notNull(),
        ]);

    }

    public function down()
    {
        $this->dropTable('{{pw_history_search_queries}}');
    }

}
