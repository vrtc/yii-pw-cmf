<?php


namespace pw\history\models;


use pw\core\db\ActiveRecord;

/**
 * @property integer $id
 * @property string $type
 * @property string $query
 * @property integer $created_at
 */

class HistorySearchQueries extends ActiveRecord
{

    const TYPE_FAST = 'fast';

    public static function tableName()
    {
        return '{{%pw_history_search_queries}}';
    }

    public function rules(): array
    {
        return [
            [['id', 'created_at'], 'integer'],
            [['type', 'query'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип поиска',
            'query' => 'Поисковый запрос',
            'created_at' => 'Дата запроса',
        ];
    }

    public static function getSearchName(){
        return [
            'fast' => 'Быстрый',
            'ordinary' => 'Обычный'
        ];
    }

    public static function dataCSV() {
        $separator = ";";

        $data = "Дата запроса $separator Запрос" . PHP_EOL;
        $model = HistorySearchQueries::find()->orderBy(['id' => SORT_DESC])->all();
        foreach ($model as $query) {
            $data .= date('d.m.y H:i', $query->created_at) . $separator  .  $query->query . PHP_EOL;
        }
        return iconv('utf-8', 'windows-1251', $data);
    }
}