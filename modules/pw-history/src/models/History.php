<?php
/**
 * Created by trisport_pw.
 * User: Dmitriy Kindeev
 * Email: dkindeev@mail.ru
 * Date: 03.11.2020
 * Time: 12:00
 */

namespace pw\history\models;
use paulzi\jsonBehavior\JsonBehavior;
use pw\users\models\Users;
use yii\db\BaseActiveRecord;

class History extends \pw\core\db\ActiveRecord{
    public static function tableName()
    {
        return '{{%pw_history}}';
    }

    public function rules()
    {
        return [
            [['user_id', 'created_at'], 'integer'],
            [['ip', 'event', 'class', 'table_name', 'row_id', 'log'], 'string'],
        ];
    }
    public function behaviors()
    {
        return [
            [
                'class' => JsonBehavior::class,
                'attributes' => ['log'],
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ip' => 'IP',
            'event' =>'Событие',
            'class' => 'Класс события',
            'table_name' => 'Тыблица БД',
            'row_id' => 'ID записи',
            'log' => 'Изменения',
            'created_at' => 'Дата события',
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::class, ['id' => 'user_id']);
    }
    public static function getEventsList()
    {
        return [
            BaseActiveRecord::EVENT_AFTER_INSERT => BaseActiveRecord::EVENT_AFTER_INSERT,
            BaseActiveRecord::EVENT_AFTER_UPDATE => BaseActiveRecord::EVENT_AFTER_UPDATE,
            BaseActiveRecord::EVENT_AFTER_DELETE => BaseActiveRecord::EVENT_AFTER_DELETE,
        ];
    }

    public static function getEventName(){
        return [
            BaseActiveRecord::EVENT_AFTER_INSERT => 'Создание',
            BaseActiveRecord::EVENT_AFTER_UPDATE => 'Изменение',
            BaseActiveRecord::EVENT_AFTER_DELETE => 'Удаление',
        ];
    }
}
