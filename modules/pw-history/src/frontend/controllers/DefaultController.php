<?php

namespace pw\history\frontend\controllers;

use pw\history\models\HistorySearchQueries;
use Yii;

class DefaultController extends \pw\web\Controller
{
    public function actionSaveQuery() {
        if (Yii::$app->request->isAjax) {
            $post = \Yii::$app->request->post();

            $saveQuery = new HistorySearchQueries();
            $saveQuery->type = HistorySearchQueries::TYPE_FAST;
            $saveQuery->query =  $post['query'];
            $saveQuery->created_at = time();
            $saveQuery->save(0);

            return;

        }
    }
}