<?php
/**
 * Created by trisport_pw.
 * User: Dmitriy Kindeev
 * Email: dkindeev@mail.ru
 * Date: 05.11.2020
 * Time: 10:56
 */

namespace pw\history\backend\controllers;

use pw\history\backend\models\HistorySearchQueriesS;
use pw\history\backend\models\HistorySearch;
use pw\history\models\History;
use pw\history\models\HistorySearchQueries;
use Yii;

class ManageController extends \pw\web\Controller
{
    public function actionIndex()
    {
        $this->view->title = Yii::t('history', 'История изменений');
        $searchModel = new HistorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id){
        $model = History::findOne($id);
        return $this->render('view', ['model' => $model]);
    }

    public function actionSearchQueries() {
        $this->view->title = Yii::t('history', 'История поисковых запросов');
        $searchModel = new HistorySearchQueriesS();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('search_queries', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSearchQueriesExport($format) {
        if(!empty($format)) {

            switch ($format) {
                case 'csv':
                    $fileName = "Поисковые запросы";
                    return \Yii::$app->response->sendContentAsFile(HistorySearchQueries::dataCSV(), $fileName . ".csv", [
                        'mimeType' => 'application/csv',
                        'inline'   => false
                    ]);
            }
        }
    }

}
