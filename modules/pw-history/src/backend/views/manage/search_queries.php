<?php

/* @var $dataProvider yii\data\ActiveDataProvider */

use pw\ui\grid\GridView;
use pw\history\models\HistorySearchQueries;
use \kartik\daterange\DateRangePicker;

?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'model' => $searchModel,
    'actions' => [
        [
            'url' => ['search-queries-export','format'=>'csv'],
            'label' => Yii::t('export', 'Экспорт в CSV'),
            'icon' => 'file-csv',
        ]
    ],
    'columns' => [
        //'id',
        //'created_at:datetime',
        [
            'attribute' => 'created_at',
            'format' => 'datetime',
            'filter' => DateRangePicker::widget([
                'model' => $searchModel,
                'attribute' => 'createTimeRange',
                'convertFormat' => true,
                'startAttribute' => 'createTimeStart',
                'endAttribute' => 'createTimeEnd',
                'pluginOptions' => [
                    'timePicker' => false,
                    'timePicker24Hour' => true,
                    'timePickerIncrement' => 30,
                    'locale' => [
                        'format' => 'Y-m-d'
                    ]
                ]
            ]),
        ],
        'query',
        /*
        [
            'attribute' => 'type',
            'content' => function($model){
                return HistorySearchQueries::getSearchName()[$model->type];
            },
            'filter' => HistorySearchQueries::getSearchName()
        ],
        */
    ],
]);