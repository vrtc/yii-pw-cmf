<?php
/**
 * Created by trisport_pw.
 * User: Dmitriy Kindeev
 * Email: dkindeev@mail.ru
 * Date: 05.11.2020
 * Time: 11:02
 */
use pw\ui\grid\GridView;
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'model' => $searchModel,
    'columns' => [
       'id',
        'created_at:datetime',
        [
            'attribute' => 'user_id',
            'content' => function($model){
                if(isset($model->user->phone)){
                    return $model->user->phone . ', (id: '.$model->user->id.')';
                }
                return 'консоль';
            }
        ],
        'ip',
        [
            'attribute' => 'event',
            'content' => function($model){
                return \pw\history\models\History::getEventName()[$model->event];
            },
            'filter' => \pw\history\models\History::getEventName()
        ],
        'class',
        'table_name',
        'row_id',
       // 'log',

        [
        'class' => \pw\ui\grid\ActionColumn::class,
            'template' => '{view}',
            'buttons' => [
                'view' => function ($url) {
                    return \yii\helpers\Html::a(
                        '<i class="fa fa-user-circle" aria-hidden="true"></i>',
                        $url,
                        [
                            'title' => 'Просмотр изменений',
                            'target' => '_blank',
                            'class' => 'btn btn-primary btn-sm'
                        ]
                    );
                },
            ]
        ]
    ],
]);
