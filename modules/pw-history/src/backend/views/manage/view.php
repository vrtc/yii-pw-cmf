<?php
/**
 * Created by trisport_pw.
 * User: Dmitriy Kindeev
 * Email: dkindeev@mail.ru
 * Date: 05.11.2020
 * Time: 11:52
 */

use yii\db\BaseActiveRecord;

$this->title = 'Сравнение';

?>
<h1><?= $model->class ?> <?= $model->event ?>: <?= $model->row_id ?></h1>
<table class="table">
  <thead>
  <tr>
    <th></th>
    <th>Было</th>
    <th>Стало</th>
  </tr>
  </thead>
    <?php if ($model->event == BaseActiveRecord::EVENT_AFTER_INSERT) { ?>
        <?php foreach ($model->log['new'] as $k => $item) { ?>
        <tr>
        <td><b><?= $k ?></b></td>
        <td> -</td>
        <td>
            <?php echo !is_array($item) ? $item : ''; ?>
        </td>
        <?php } ?>
      </tr>
    <?php } ?>
    <?php if ($model->event == BaseActiveRecord::EVENT_AFTER_UPDATE) { ?>
        <?php foreach ($model->log['old'] as $k => $item) { ?>
        <tr>
        <td><b><?= $k ?></b></td>
        <td><?php echo !is_array($item) ? $item : ''; ?></td>
        <td><?= isset($model->log['new'][$k]) ? $model->log['new'][$k] : 'удалено'; ?>
        </td>
        <?php } ?>
      </tr>
    <?php } ?>
    <?php if ($model->event == BaseActiveRecord::EVENT_AFTER_DELETE) { ?>
        <?php foreach ($model->log['old'] as $k => $item) { ?>
        <tr>
        <td><b><?= $k ?></b></td>
        <td><?php echo !is_array($item) ? $item : ''; ?></td>
        <td> -</td>
        <?php } ?>
      </tr>
    <?php } ?>
</table>
