<?php
/**
 * Created by trisport_pw.
 * User: Dmitriy Kindeev
 * Email: dkindeev@mail.ru
 * Date: 05.11.2020
 * Time: 10:58
 */
namespace pw\history\backend\models;

use pw\history\models\History;
use yii\data\ActiveDataProvider;

class HistorySearch extends History{
    public function search($params){
        $query = History::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['start_time' => SORT_DESC]]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }


        return $dataProvider;
    }
}
