<?php


namespace pw\history\backend\models;


use pw\history\models\HistorySearchQueries;
use yii\data\ActiveDataProvider;

class HistorySearchQueriesS extends HistorySearchQueries
{


    public $id;
    public $query;
    public $type;

    public $createTimeRange;
    public $createTimeStart;
    public $createTimeEnd;

    public function rules(): array
    {
        return [
            [['id', 'created_at'], 'integer'],
            [['type', 'query'], 'string'],
            [['createTimeRange'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
            [['createTimeStart', 'createTimeEnd'], 'safe'],
        ];
    }

    public function search($params): ActiveDataProvider
    {
        $query = HistorySearchQueries::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        /*
        $query->andFilterWhere([
            'type' => $this->type,
        ]);
        */
        $query->andFilterWhere(['like', 'query', $this->query]);

        if ($this->createTimeRange != null) {
            $this->createTimeEnd = strtotime($this->createTimeEnd) + 60 * 60 * 24 - 1;

            $query->andFilterWhere(['>=', 'created_at', strtotime($this->createTimeStart)])
                ->andFilterWhere(['<=', 'created_at',  $this->createTimeEnd]);
        }

        return $dataProvider;
    }

}