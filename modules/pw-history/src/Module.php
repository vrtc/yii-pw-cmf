<?php
/**
 * Created by trisport_pw.
 * User: Dmitriy Kindeev
 * Email: dkindeev@mail.ru
 * Date: 03.11.2020
 * Time: 11:45
 */

namespace pw\history;

use Yii;
use yii\base\Application;
use yii\base\BootstrapInterface;

class Module extends \pw\core\Module implements BootstrapInterface
{


    public function getNavigation(): array
    {
        return [
            'system' => [
                [
                    'label' => 'История',
                    'url'   => ['/pw-history/manage/index'],
                    'icon'  => 'clock'
                ],
                [
                    'label' => 'Поисковые запросы',
                    'url'   => ['/pw-history/manage/search-queries'],
                    'icon'  => 'search'
                ],
            ]
        ];
    }

}
