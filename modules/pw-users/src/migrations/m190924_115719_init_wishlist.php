<?php

namespace pw\users\migrations;
use pw\core\db\Migration;

class m190924_115719_init_wishlist extends Migration
{
    public function up()
    {
        $this->createTable('{{%pw_wish_list}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'user_id' => $this->integer(),
        ]);
    }

    public function down()
    {
    }

}
