<?php
namespace pw\users\migrations;
use pw\core\db\Migration;

class m190626_062500_add_subscribe_news extends Migration
{
    public function up()
    {
        $this->addColumn('{{%pw_users}}', 'subscribe', $this->integer()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('{{%pw_users}}', 'subscribe');
    }

}
