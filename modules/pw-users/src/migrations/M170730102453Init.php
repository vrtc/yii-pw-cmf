<?php
namespace pw\users\migrations;

use pw\core\db\Migration;

class M170730102453Init extends Migration
{
    public function up()
    {
        $this->createTable('{{%pw_users}}', [
            'id' => $this->bigPrimaryKey()->unsigned(),
            'group_id' => $this->bigInteger()->unsigned(),
            'language' => $this->string(16),
            'name' => $this->string(),
            'username' => $this->string(),
            'email' => $this->string(),
            'password' => $this->string(),
            'old_password' => $this->string(),
            'auth_key' => $this->string(),
            'created_time' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'login_time' => $this->timestamp()->null()->defaultValue(null),
            'old_id' => $this->integer(),
            'status' => $this->smallInteger()
        ]);
        $this->createIndex('idx_email', '{{%pw_users}}', 'email', true);

        $this->createTable('{{%pw_users_accounts}}', [
            'id' => $this->bigPrimaryKey(),
            'user_id' => $this->bigInteger()->unsigned(),
            'source' => $this->string()->notNull(),
            'idSource' => $this->string()->notNull()
        ]);

        $this->createTable(
            '{{%pw_user_group}}',
            [
                'id' => $this->bigPrimaryKey()->unsigned(),
                'old_id' => $this->integer(),
                'name' => $this->string(),
                'description' => $this->text(),
                'group_discount' => $this->integer(),
            ]
        );

        $this->createTable(
            '{{%pw_user_data}}',
            [
                'id' => $this->bigPrimaryKey()->unsigned(),
                'user_id' => $this->bigInteger()->unsigned(),
                'firstname' => $this->string(),
                'lastname' => $this->string(),
                'avatar' => $this->string(),
                'address' => $this->string(1024),
                'region' => $this->integer(),
                'city' => $this->integer(),
                'telephone' => $this->string(),
                'created_time' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
                'updated_time' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            ]
        );

        $this->createIndex('idx_user', '{{%pw_users_accounts}}', 'user_id');

        $this->addForeignKey(
            'fk_users_accounts_user',
            '{{%pw_users_accounts}}',
            'user_id',
            '{{%pw_users}}',
            'id',
            'cascade',
            'cascade'
        );

        $this->addForeignKey(
            'fk_users_user_group',
            '{{%pw_users}}',
            'group_id',
            '{{%pw_user_group}}',
            'id',
            'cascade',
            'cascade'
        );
        $this->addForeignKey(
            'fk-user_data_users',
            '{{%pw_user_data}}',
            'user_id',
            '{{%pw_users}}',
            'id',
            'cascade',
            'cascade'
        );
    }

    public function down()
    {
        $this->dropForeignKey('fk_users_accounts_user', '{{%pw_users_accounts}}');
        $this->dropForeignKey('fk-user_data_users', '{{%pw_user_data}}');
        $this->dropForeignKey('fk_users_user_group', '{{%pw_users}}');
        $this->dropTable('{{%pw_user_group}}');
        $this->dropTable('{{%pw_user_data}}');
        $this->dropTable('{{%pw_users_accounts}}');
        $this->dropTable('{{%pw_users}}');
    }

}
