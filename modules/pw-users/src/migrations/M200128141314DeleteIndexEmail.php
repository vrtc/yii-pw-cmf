<?php

namespace pw\users\migrations;

use pw\core\db\Migration;
use pw\users\models\Users;

class M200128141314DeleteIndexEmail extends Migration
{
    public function up()
    {
        $this->dropIndex('idx_email', Users::tableName());
        $this->createIndex('idx_email', Users::tableName(), 'email', false);
    }

    public function down()
    {
        echo "M200128141314DeleteIndexEmail cannot be reverted.\n";

        return false;
    }

}
