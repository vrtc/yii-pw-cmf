<?php

namespace pw\users\migrations;
use pw\core\db\Migration;

class m190820_064823_add_retail_id extends Migration
{
    public function up()
    {
        $this->addColumn('{{%pw_users}}', 'retail_id', $this->integer()->null());
    }

    public function down()
    {
        $this->dropColumn('{{%pw_users}}', 'retail_id');
    }

}
