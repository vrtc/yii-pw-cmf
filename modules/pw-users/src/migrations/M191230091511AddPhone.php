<?php

namespace pw\users\migrations;

use pw\core\db\Migration;
use pw\users\models\UserData;
use pw\users\models\Users;

class M191230091511AddPhone extends Migration
{
    public function up()
    {
        $this->addColumn(Users::tableName(), 'phone', $this->string(255));

        $model = Users::find()->all();

        foreach ($model as $user){
            try {
                $user->phone = UserData::formatPhone($user->data->telephone);
            }catch (\Exception $e){
                $user->phone = null;
            }
            $user->save(false);
        }
    }

    public function down()
    {
        echo "M191230091511AddPhone cannot be reverted.\n";

        return false;
    }

}
