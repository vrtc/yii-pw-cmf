<?php

namespace pw\users\migrations;

use pw\core\db\Migration;
use pw\users\models\Tokens;

class M190722083834Tokens_init extends Migration
{
    public function up()
    {
        $this->createTable(Tokens::tableName(),[
            'id'=>$this->bigPrimaryKey()->unsigned(),
            'code'=>$this->string(),
            'name'=>$this->string(),
            'data'=>$this->string(),
            'expireTime'=>$this->integer(),
            'expireQuantity'=>$this->integer(),
            'created' => $this->timestamp()->null()->defaultValue(null)
        ],$this->tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%Tokens}}');
    }

}
