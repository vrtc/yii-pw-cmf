<?php

namespace pw\users\migrations;
use pw\core\db\Migration;

class m190917_080423_add_address extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%pw_user_data}}', 'city', $this->string(500)->null());
        $this->addColumn('{{%pw_user_data}}', 'flat', $this->string(20)->null());
        $this->addColumn('{{%pw_user_data}}', 'house', $this->string(20)->null());
    }

    public function down()
    {
    }

}
