<?php


namespace pw\users\migrations;
use pw\core\db\Migration;

class m191119_093519_add_mass_edit_field extends Migration
{
    public function up()
    {
        $this->addColumn('{{%pw_user_group}}', 'mass_edit', $this->tinyInteger(3)->notNull()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('{{%pw_user_group}}', 'mass_edit');
    }

}
