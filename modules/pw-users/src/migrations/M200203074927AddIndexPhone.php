<?php

namespace pw\users\migrations;

use pw\core\db\Migration;
use pw\users\models\Users;

class M200203074927AddIndexPhone extends Migration
{
    public function up()
    {
        $this->createIndex('user_phone', Users::tableName(), 'phone');

    }

    public function down()
    {
        echo "M200203074927AddIndexPhone cannot be reverted.\n";

        return false;
    }

}
