<?php
/* @var $this yii\web\View */
/* @var $model \pw\users\backend\models\SettingsForm */
/* @var $form yii\widgets\ActiveForm */

use pw\ui\form\ActiveForm;

?>
<?php $form = ActiveForm::begin(['model'=>$model]); ?>

<?php $form->beginTab('auth',Yii::t('users','Авторизация через соц.сети'))?>
<fieldset>
    <legend><?=Yii::t('users','Вконтакте')?></legend>
    <p><a href="https://vk.com/editapp?act=create" target="_blank"><?=Yii::t('users','Создать приложение')?></a></p>
    <?= $form->field($model, 'authVkId')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'authVkKey')->textInput(['maxlength' => 255]) ?>
</fieldset>
<fieldset>
    <legend><?=Yii::t('users','Facebook')?></legend>
    <p><a href="https://developers.facebook.com/quickstarts/?platform=web" target="_blank"><?=Yii::t('users','Создать приложение')?></a></p>
    <?= $form->field($model, 'authFbId')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'authFbKey')->textInput(['maxlength' => 255]) ?>
</fieldset>
<?php $form->endTab()?>

<?php $form->beginTab('backend',Yii::t('users','Авторизация в административную часть'));?>

    <?= $form->field($model, 'whiteIP')->textarea()->hint(Yii::t('users','по одному на строку')) ?>

<?php $form->endTab()?>

<?php ActiveForm::end(); ?>
