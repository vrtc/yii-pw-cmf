<?php

use pw\ui\grid\GridView;
use yii\helpers\Html;
use pw\users\models\Users;
use pw\ui\icons\Icons;
use pw\users\Module;

/* @var $this \pw\web\View */
/* @var $searchModel \pw\users\backend\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$gridColumns =     [
    'email:email',
    'phone',
    [
        'attribute' => 'status',
        'content' => function ($model) {
            $label = 'success';
            if ($model->status == Users::STATUS_NOT_ACTIVATED)
                $label = 'default';
            elseif ($model->status == Users::STATUS_BANNED)
                $label = 'danger';
            return '<span class="label label-' . $label . '">' . $model->getStatus() . '</span>';
        },
        'filter' => Users::getStatuses()
    ],
    'created_time',
    [
        'class' => \pw\ui\grid\ActionColumn::class,
        'template' => '{status}{update}{login}',
        'buttons' => [
            'status' => function ($url, $model) {
                if ($model->status !== Users::STATUS_BANNED) {
                    return Html::a('<i class="fa fa-ban"></i> ', ['ban', 'id' => $model->id],
                        ['class' => 'btn btn-danger btn-sm']);
                }
                if ($model->status !== Users::STATUS_ACTIVE) {
                    return Html::a('<i class="fa fa-check"></i>', ['active', 'id' => $model->id],
                        ['class' => 'btn btn-success btn-sm']);
                }
            },
            'login' => function ($url) {
                return Html::a(
                    '<i class="fa fa-user-circle" aria-hidden="true"></i>',
                    $url,
                    [
                        'title' => 'Войти как ',
                        'target' => '_blank',
                        'class' => 'btn btn-primary btn-sm'
                    ]
                );
            },
        ]
    ],
];
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'model' => $searchModel,
    'title' => Module::t('users', 'Пользователи'),
    'icon' => 'users',
    'actions' => [
        [
            'url' => ['create'],
            'label' => Module::t('users', 'Добавить пользователя')
        ]
    ],
    'bulkActions' => [
        'banAll' => Module::t('users', 'Забанить'),
        'unBanAll' => Module::t('users', 'Разбанить'),
        'deleteAll' => Module::t('users', 'Удалить')
    ],
    'columns' => $gridColumns
]); ?>
