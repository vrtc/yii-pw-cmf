<?php
/* @var $this yii\web\View */
/* @var $model Users */
/* @var $form yii\widgets\ActiveForm */
/* @var $roles array */

/**
 * @var $dataModel \pw\users\models\UserData
 */

use pw\ui\form\ActiveForm;
use pw\users\models\Users;

?>

<?php $form = ActiveForm::begin([
    'title' => $model->isNewRecord ? Yii::t('users', 'Добавить нового пользователя') : Yii::t('users', 'Редактирование пользователя: {email}', ['email' => $model->email]),
    'icon' => 'user',
    'model' => $model,
    'enableClientValidation' => true
]); ?>
<?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
<?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>
<?= $form->field($model, 'phone')->textInput(['maxlength' => 255]) ?>
<?/*= $form->field($dataModel, 'telephone')->textInput() */?>
<?= $form->field($dataModel, 'city')->textInput() ?>
<?= $form->field($dataModel, 'address')->textInput() ?>
<?= $form->field($dataModel, 'house')->textInput() ?>
<?= $form->field($dataModel, 'flat')->textInput() ?>
<?= $form->field($model, 'role')->dropDownList($roles, ['value' => $model->role])?>
<?php if ($model->isNewRecord): ?>
    <?= $form->field($model, 'password')->textInput(['maxlength' => 255]) ?>
<?php else: ?>
    <?= $form->field($model, 'newPassword')->textInput(['maxlength' => 255]) ?>
<?php endif;?>
<?= $form->field($model, 'status')->dropDownList(Users::getStatuses()) ?>

<?php ActiveForm::end(); ?>

