<?php

use pw\ui\grid\GridView;
use pw\users\Module;

/* @var $this \pw\web\View */
/* @var $searchModel \pw\users\backend\models\GroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'model' => $searchModel,
    'title' => Module::t('user_groups', 'Группы пользователей'),
//    'icon' => 'global',
    'actions' => [
        [
            'url' => ['create'],
            'label' => Module::t('users', 'Добавить группу')
        ]
    ],
    'columns' => [
        'name',
        'group_discount',
        [
            'class' => \pw\ui\grid\ActionColumn::class,
            'template' => '{update}{delete}',
        ],
    ],
]); ?>
