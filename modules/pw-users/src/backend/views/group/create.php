<?php
/* @var $this yii\web\View */
/* @var $model \pw\users\models\UserGroup */

/* @var $form ActiveForm */

use pw\ui\form\ActiveForm;

?>

<?php $form = ActiveForm::begin([
    'model' => $model
]); ?>

<?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
<?= $form->field($model, 'group_discount')->textInput(['maxlength' => 255]) ?>
<?= $form->field($model, 'description')->widget(\mihaildev\ckeditor\CKEditor::class) ?>

<?php ActiveForm::end(); ?>

