<?php

namespace pw\users\backend\controllers;

use pw\users\backend\models\GroupSearch;
use pw\users\models\UserGroup;
use Yii;
use pw\web\Controller;
use yii\db\Exception;
use yii\web\NotFoundHttpException;

class GroupController extends Controller
{
    public function actionIndex()
    {
        $this->setPageTitle(Yii::t('user_groups', 'Группы пользователей'));
        $this->addBreadcrumb(Yii::t('user_groups', 'Группы пользователей'));
        $searchModel = new GroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new UserGroup();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('users', 'Группа успешно добавлена'));
            return $this->redirect(['update', 'id' => $model->id->toString()]);
        }
        return $this->render('create', [
            'model' => $model
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('users', 'Группа успешно добавлена'));

            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model
        ]);
    }

    private function findModel($id)
    {
        if (!$model = UserGroup::findOne(['id' => $id])) {
            throw new NotFoundHttpException('Группа не найдена');
        }
        return $model;
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if($model->delete()){
            Yii::$app->session->setFlash('success',Yii::t('users','Группа удалена'));
        }
        return $this->redirect(['index']);
    }

}