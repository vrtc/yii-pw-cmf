<?php

namespace pw\users\backend\controllers;

use pw\users\models\UserToken;
use pw\users\backend\models\UserForm;
use pw\users\models\UserData;
use Yii;
use pw\web\Controller;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use pw\users\models\Users;
use pw\users\backend\models\UsersSearch;
use yii\web\User;

class ManageController extends Controller
{

    public function actionIndex()
    {


        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->setPageTitle(Yii::t('users', 'Пользователи'));
        $this->addBreadcrumb(Yii::t('users', 'Пользователи'));
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionCreate()
    {

        $this->setPageTitle(Yii::t('users', 'Добавить нового пользователя'));
        $this->addBreadcrumb(Yii::t('users', 'Пользователи'), ['index']);
        $this->addBreadcrumb(Yii::t('users', 'Добавить нового пользователя'));

        $model = new UserForm();
        $dataModel = new UserData();
        $dataModel->user_id = $model->id;
        $role = Yii::$app->authManager->getRolesByUser($model->id);
        $model->role = key($role);
        $roles = ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'description');
        //unset($roles['guest']);
        unset($roles['owner']);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $model->setPassword($model->password);
            if ($model->save()) {
                if ($dataModel->load(Yii::$app->request->post()) && $dataModel->validate()) {
                    $dataModel->save();
                }
                Yii::$app->session->setFlash('success', Yii::t('users', 'Пользователь добавлен'));
                return $this->redirect(['update', 'id' => $model->id]);
            }
        }

        return $this->render('form', ['model' => $model, 'roles' => $roles, 'dataModel' => $dataModel]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $dataModel = ($dataModel = UserData::findOne(['user_id' => $id])) ? $dataModel : new UserData();
        if ($dataModel->isNewRecord) {
            $dataModel->user_id = $model->id;
        }
        $this->view->title = Yii::t('users', 'Редактирование пользователя:') . ' ' . $model->email;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($dataModel->load(Yii::$app->request->post()) && $dataModel->validate()) {
                $dataModel->save();
            }
            $model->save();
            $role = Yii::$app->authManager->getRole($model->role);
            Yii::$app->authManager->revokeAll($model->id);
            Yii::$app->authManager->assign($role, $model->id);
            Yii::$app->session->setFlash('success', Yii::t('users', 'Изменения сохранены'));
            return $this->redirect(['update', 'id' => $model->id]);
        }
        $role = Yii::$app->authManager->getRolesByUser($model->id);
        $model->role = key($role);
        $roles = ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'description');
        //unset($roles['guest']);
        unset($roles['owner']);
        $this->setPageTitle(Yii::t('users', 'Редактирование пользователя'));
        $this->addBreadcrumb(Yii::t('users', 'Пользователи'), ['index']);
        $this->addBreadcrumb(Yii::t('users', 'Редактирование пользователя'));

        return $this->render('form', ['model' => $model, 'roles' => $roles, 'dataModel' => $dataModel]);
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model->delete()) {
            Yii::$app->session->setFlash('success', Yii::t('users', 'Пользователь удалён'));
        }
        return $this->redirect(['index']);
    }

    public function actionBan($id)
    {
        $model = $this->findModel($id);
        $model->status = Users::STATUS_BANNED;
        if ($model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('users', 'Пользователь забанен'));
        }
        return $this->redirect(['index']);
    }

    public function actionActive($id)
    {
        $model = $this->findModel($id);
        $model->status = Users::STATUS_ACTIVE;
        if ($model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('users', 'Пользователь активирован'));
        }
        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\base\Exception
     * @throws NotFoundHttpException
     */
    public function actionLogin($id)
    {
        $model = $this->findModel($id);
        $tokenModel = UserToken::create(
            $model->getId(),
            UserToken::TYPE_LOGIN_PASS,
            60
        );

        return $this->redirect(
            Yii::$app->urlManager->createAbsoluteUrl(['/pw-users/default/login-by-pass', 'token' => $tokenModel->token, 'toFrontend' => true], true)
        );
    }

    public function findModel($id)
    {
        /**
         * @var $model UserForm
         */
        if (($model = UserForm::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
