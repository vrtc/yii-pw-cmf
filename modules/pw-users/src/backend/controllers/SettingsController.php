<?php
namespace pw\users\backend\controllers;

use Yii;
use pw\web\Controller;
use pw\users\backend\models\SettingsModel;

class SettingsController extends Controller{

    public function actionIndex(){
        $model = new SettingsModel();
        if($model->load(Yii::$app->request->post()) && $model->save()){
            Yii::$app->session->setFlash('success',Yii::t('users','Изменения сохранены'));
            return $this->redirect(['index']);
        }

        $this->setPageTitle(Yii::t('users','Настройки пользователей'));
        $this->addBreadcrumb(Yii::t('users','Настройки пользователей'));

        return $this->render('index',['model'=>$model]);
    }
}