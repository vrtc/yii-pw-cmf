<?php
namespace pw\users\backend\controllers;

use Yii;
use pw\web\Controller;
use pw\users\backend\models\LoginForm;

class DefaultController extends Controller{

    public $layout = '//guest';


    public function actionLogin(){
        $form = new LoginForm();

        if($form->load(Yii::$app->request->post()) && $form->login()){
            return $this->goBack();
        }
        return $this->render('login',['loginForm'=>$form]);
    }

    public function actionLogout(){

        if(Yii::$app->user->logout()) {
            return $this->goHome();
        }
        return $this->goBack();
    }

}