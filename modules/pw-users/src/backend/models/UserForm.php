<?php
/**
 * Created by new_trisport.
 * User: Dmitriy Kindeev
 * Email: dkindeev@mail.ru
 * Date: 01.10.2019
 * Time: 21:24
 */

namespace pw\users\backend\models;

use pw\users\models\Users;

class UserForm extends Users{

    public function rules()
    {
        return [
            [['status', 'subscribe'], 'integer'],
            ['email', 'email'],
            [['email'], 'required'],
            ['email', 'unique', 'targetClass' => Users::class, 'filter' => function ($query) {
                if (!$this->isNewRecord) {
                    $query->andWhere(['not', ['id' => $this->id]]);
                }
            }],
            [['language', 'created_time', 'login_time', 'role', 'group_id'], 'safe'],
            [['newPassword', 'repeatPassword'], 'string', 'min' => 6],
            [['name', 'email', 'password'], 'string', 'max' => 255],
        ];
    }

}
