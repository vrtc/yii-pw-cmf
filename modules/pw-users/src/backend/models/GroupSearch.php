<?php

namespace pw\users\backend\models;

use pw\users\models\UserGroup;
use Yii;
use pw\users\models\Users;
use yii\base\Model;
use yii\data\ActiveDataProvider;


class GroupSearch extends Model
{
    public $id;
    public $group_discount;
    public $name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'safe'],
            [['group_discount'], 'integer'],
            [['name'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserGroup::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if ($this->group_discount != null) {
            $query->andWhere([
                'group_discount' => $this->group_discount,
            ]);
        }

        if ($this->id != null) {
            $query->andWhere([
                'id' => $this->id,
            ]);
        }
        if ($this->name != null) {
            $query->andWhere(['like', 'name', $this->name]);
        }

        return $dataProvider;
    }
}
