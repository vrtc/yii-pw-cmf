<?php
namespace pw\users\backend\models;

use Yii;
use pw\users\models\Users;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;


class UsersSearch extends Users
{


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id',  'status'], 'integer'],
            [['email', 'password', 'auth_key', 'created_time', 'login_time', 'phone'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Users::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_time' => $this->created_time,
            'login_time' => $this->login_time,
            'status' => $this->status,
            'phone' => $this->phone,
        ]);

        $query->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}
