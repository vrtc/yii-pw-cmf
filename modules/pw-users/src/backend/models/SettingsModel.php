<?php
namespace pw\users\backend\models;

use Yii;

class SettingsModel extends \pw\modules\SettingsModel {


    public $enableRegistration;

    public $enableGeneratingPassword;

    public $enableConfirmation;

    public $enableUnconfirmedLogin;

    public $enablePasswordRecovery;

    public $loginUrl;

    public $authVkId;
    public $authVkKey;

    public $authFbId;
    public $authFbKey;

    public $whiteIP;


    public function rules(){
        return [
            [['authVkId','authVkKey','authFbId','authFbKey','whiteIP'],'string'],
        ];
    }

    public function attributeLabels(){
        return [
            'authVkId'=>Yii::t('users','ID приложения'),
            'authVkKey'=>Yii::t('users','Защищенный ключ'),
            'authFbId'=>Yii::t('users','ID приложения'),
            'authFbKey'=>Yii::t('users','Защищенный ключ'),
            'whiteIP'=>Yii::t('users','Вход с IP без подтверждения')
        ];
    }
}
