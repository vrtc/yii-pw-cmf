<?php

namespace pw\users;

use pw\users\components\Tokens;
use Yii;
use pw\users\models\Users;
use yii\base\Application;
use yii\base\BootstrapInterface;

class Module extends \pw\core\Module implements BootstrapInterface
{

    public $enableRegistration = true;

    public $enableGeneratingPassword = false;

    public $enableConfirmation = true;

    public $enableUnconfirmedLogin = false;

    public $enablePasswordRecovery = true;

    public $loginUrl;

    public $authVkId;
    public $authVkKey;

    public $authFbId;
    public $authFbKey;

    public $whiteIP;

    public function models(Application $app): array
    {
        return [
          Users::class=>Yii::t('users','Пользователь')
        ];
    }

    public function getUserModel()
    {
        return Users::class;
    }

    public function components(Application $app): array
    {
        if (!$app->request->isConsoleRequest) {
            Yii::$container->set('yii\web\User', [
                'enableAutoLogin' => true,
                'loginUrl'        => ['/pw-users/default/login'],
                'identityClass'   => Users::class,
            ]);

            return [
                'tokens' => Tokens::class,
                'authClientCollection' => [
                    'class'   => 'yii\authclient\Collection',
                    'clients' => [
                        'vkontakte' => [
                            'class'        => 'yii\authclient\clients\VKontakte',
                            'clientId'     => Yii::$app->settings['pw-users']['settings']['authVkId'],
                            'clientSecret' => Yii::$app->settings['pw-users']['settings']['authVkKey'],
                            'scope'        => 'email',
                        ],
                        'facebook'  => [
                            'class'        => 'yii\authclient\clients\Facebook',
                            'clientId'     => Yii::$app->settings['pw-users']['settings']['authFbId'],
                            'clientSecret' => Yii::$app->settings['pw-users']['settings']['authFbKey'],
                        ]
                    ],
                ]
            ];
        }

        return [];
    }

    public function routes(Application $app): array
    {
        return [
            [
                'pattern' => '/login',
                'route'   => '/pw-users/default/login',
                'name'    => self::t('route', 'Авторизация'),
            ],
            [
                'pattern' => '/logout',
                'route'   => '/pw-users/default/logout',
                'name'    => self::t('route', 'Выход'),
                'internal' => true
            ],
            [
                'pattern' => '/reset/<token:.+>',
                'route'   => '/pw-users/default/reset',
                'name'    => self::t('route', 'Восстановление пароля - введите полученный код'),
                'internal'=>true
            ],
            [
                'pattern' => '/reset',
                'route'   => '/pw-users/default/reset',
                'name'    => self::t('route', 'Восстановление пароля'),
            ],
            [
                'pattern' => '/signup',
                'route'   => '/pw-users/default/signup',
                'name'    => self::t('route', 'Регистрация'),
            ],
            [
                'pattern' => '/signup/confirm/<token:.+>',
                'route'   => '/pw-users/default/confirm',
                'name'    => self::t('route', 'Подтверждение регистрации - введите полученный код'),
                'internal'=>true
            ],
            [
                'pattern' => '/signup/confirm',
                'route'   => '/pw-users/default/confirm',
                'name'    => self::t('route', 'Подтверждение регистрации'),
                'internal'=>true
            ],
            [
                'pattern' => '/profile',
                'route'   => '/pw-users/profile/index',
                'name'    => self::t('route', 'Мой профиль'),
            ],
            [
                'pattern' => '/profile/orders',
                'route'   => '/pw-users/profile/orders',
                'name'    => self::t('route', 'Мои заказы'),
            ],
            [
                'pattern' => '/profile/edit',
                'route'   => '/pw-users/profile/edit',
                'name'    => self::t('route', 'Изменить заказ'),
            ],
            [
                'pattern' => '/profile/edit',
                'route'   => '/pw-users/profile/edit',
                'name'    => self::t('route', 'Редактирование профиля'),
            ],
            [
                'pattern' => '/profile/discount',
                'route'   => '/pw-users/profile/discount',
                'name'    => self::t('route', 'Карта ТриСпорт'),
            ],
            [
                'pattern' => '/profile/payment/<id:\d+>',
                'route'   => '/pw-users/profile/payment',
                'name'    => self::t('route', 'Paykeeper'),
            ],
        ];
    }

    public function getNavigation(): array
    {
        return [
            'system'   => [
                [
                    'label' => 'Пользователи',
                    'icon'  => 'users',
                    'url'   => ['/pw-users/manage/index'],
                ],
            ],
            'settings' => [
                [
                    'label' => 'Пользователи',
                    'url'   => ['/pw-users/settings/index'],
                    'icon'  => 'users'
                ],
                [
                    'label' => 'Группы пользователей',
                    'url'   => ['/pw-users/group/index'],
                    'icon'  => 'users-cog'
                ]
            ]
        ];
    }
}
