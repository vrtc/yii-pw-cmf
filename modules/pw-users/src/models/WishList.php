<?php

namespace pw\users\models;

use ct\catalog\models\product\Product;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\IdentityInterface;
use pw\users\Module;

/**
 * This is the model class for table "{{%pw_wish_list}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $product_id
 *
 * @property Product $product
 *
 */
class WishList extends \pw\core\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pw_wish_list}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'user_id'], 'integer'],
        ];
    }

    public function getProduct()
    {
        return $this->hasOne(Product::class, ['id' => 'product_id']);
    }
}
