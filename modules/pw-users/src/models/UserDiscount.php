<?php


namespace pw\users\models;


use rd\order\models\Statuses;
use rd\order\models\Order;

class UserDiscount
{
    //для брендов и категорий процент скидки не стандартный
    const CATEGORIES = [231, 226, 363, 228, 230, 227];
    const BRANDS = [];

    //для брендов и категорий процент скидки отключен
    const CATEGORIES_DISABLED = [];
    const BRANDS_DISABLED = [93, 320, 468, 507, 42, 381, 376, 71, 509, 508, 510, 513];


    private $user_id;
    private $product_category;
    private $product_brand;

    public function __construct($product_category = null, $product_brand = null)
    {
        $this->user_id = \Yii::$app->user->id;
        $this->product_category = $product_category;
        $this->product_brand = $product_brand;
    }

    public function getDiscount() {
        $total = $this->getOrdersTotal();

        return [
            'order_total' => $total,
            'percent' => $this->getPercent($total),
            'remains' => $this->getAmountRemains($total)
        ];
    }

    public function getUserPercent() {
        return $this->getPercent($this->getOrdersTotal());
    }

    public function getProductPercent() {
        $percent = $this->getPercent($this->getOrdersTotal());
        return $this->checkProductPercent($percent);
    }

    public function getPercent($total) {

        $percent = 12;
        if($total < 50000) $percent = 0;
        else if($total >= 50000 && $total < 150000) $percent = 5;
        else if($total >= 150000 && $total < 250000) $percent = 8;

        return $percent;
    }

    public function getOrdersTotal() {
        $total = 0;
        $yearTime = 31536000;
        $lastYear = time() - $yearTime;

        $orders = Order::find()->select(['created_time', 'total'])
            ->where([
                'user_id' => $this->user_id,
                'status' => Statuses::STATUS_COMPLETED
            ])->orderBy(['created_time' => SORT_DESC])
            ->all();
         foreach ($orders as $order) {
            $createdTime = strtotime($order->created_time);
            if($createdTime < $lastYear) break; //если пользователь не делал попуки год скидка аннулируется
            $total += $order->total;
            $lastYear = $createdTime - $yearTime;
        }
        return $total;
    }

    public function getAmountRemains($total) {
        $remains = 0;
        if($total < 50000) $remains = 50000 - $total;
        else if($total >= 50000 && $total < 150000) $remains = 150000 - $total;
        else if($total >= 150000 && $total < 250000) $remains = 250000 - $total;

        return $remains;
    }

    //изменить процент скидки для определенных категорий
    public function checkProductPercent($percent) {
        if($percent) {
            if(in_array($this->product_category, self::CATEGORIES) || in_array($this->product_brand, self::BRANDS)) {
                switch ($percent) {
                    case 5:
                        $percent = 0;
                        break;
                    case 8:
                        $percent = 5;
                        break;
                    case 12:
                        $percent = 8;
                        break;
                }
            }
            else if(in_array($this->product_category, self::CATEGORIES_DISABLED) || in_array($this->product_brand, self::BRANDS_DISABLED)) $percent = 0;
        }
        return $percent;
    }

}