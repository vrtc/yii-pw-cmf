<?php

namespace pw\users\models;

use pw\history\behaviors\HistoryBehavior;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\IdentityInterface;
use pw\users\Module;

/**
 * This is the model class for table "{{%user_data}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $region
 * @property integer $city
 * @property string $firstname
 * @property string $lastname
 * @property string $flat
 * @property string $house
 * @property string $avatar
 * @property string $address
 * @property string $telephone
 * @property string $created_time
 * @property string $updated_time
 *
 */
class UserData extends \pw\core\db\ActiveRecord
{

    public $email;
    public $subscribe;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pw_user_data}}';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_time', 'updated_time']
                ],
                'value' => new Expression('NOW()')
            ],
            'history' => [
                'class' => HistoryBehavior::class,
                'skipAttributes' => ['updated_time'] // attributes, that should be ignored
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'safe'],
            [['firstname', 'lastname', 'telephone', 'avatar', 'address', 'city', 'flat', 'house', 'email'], 'string'],
            [['region', 'subscribe'], 'integer'],
            [['created_time', 'updated_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Module::t('model', 'ID'),
            'firstname' => Module::t('model', 'Имя'),
            'lastname' => Module::t('model', 'Фамилия'),
            'email' => Module::t('model', 'E-mail'),
            'telephone' => Module::t('model', 'Телефон'),
            'avatar' => Module::t('model', 'Аватар'),
            'flat' => Module::t('model', 'Квартира/Офис'),
            'subscribe' => Module::t('model', 'Подписка на новости'),
            'house' => Module::t('model', 'Номер дома'),
            'city' => Module::t('model', 'Город'),
            'address' => Module::t('model', 'Улица'),
            'created_time' => Module::t('model', 'Дата создания'),
            'updated_time' => Module::t('model', 'Дата обновления'),
        ];
    }

    public static function formatPhone($phone)
    {
        try {
            $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
            $formatPhone = $phoneUtil->parse($phone, "RU");
            return $phoneUtil->format($formatPhone, \libphonenumber\PhoneNumberFormat::E164);
        }catch (\Exception $e){
            return null;
        }

    }
}
