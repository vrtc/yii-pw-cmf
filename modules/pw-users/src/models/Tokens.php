<?php
/**
 * Created by new_trisport.
 * User: Dmitriy Kindeev
 * Email: dkindeev@mail.ru
 * Date: 22.07.2019
 * Time: 11:34
 */


namespace pw\users\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%Tokens}}".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @Property string $data
 * @property integer $expireTime
 * @property integer $expireQuantity
 * @property string $created
 */
class Tokens extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pw_tokens}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['expireTime', 'expireQuantity'], 'integer'],
            [['name', 'data'], 'string'],
            [['created'], 'safe'],
            [['code'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('tokens', 'ID'),
            'code' => Yii::t('tokens', 'Code'),
            'expireTime' => Yii::t('tokens', 'Expire Time'),
            'expireQuantity' => Yii::t('tokens', 'Expire Quantity'),
            'created' => Yii::t('tokens', 'Created'),
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created',
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->code = Yii::$app->security->generateRandomString(64);
            return true;
        }
        return false;
    }

    public function isValid()
    {
        return (strtotime($this->created) + $this->expireTime) > time();
    }
}

