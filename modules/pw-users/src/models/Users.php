<?php

namespace pw\users\models;

use pw\history\behaviors\HistoryBehavior;
use rd\order\models\Order;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\IdentityInterface;
use pw\users\Module;

/**
 * This is the model class for table "{{%users}}".
 *
 * @property integer $id
 * @property integer $old_id
 * @property integer $group_id
 * @property integer $language
 * @property integer $subscribe
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $username
 * @property string $password
 * @property string $old_password
 * @property string $auth_key
 * @property string $created_time
 * @property string $login_time
 * @property integer $status
 *
 * @property UsersAccounts[] $accounts
 * @property UserData $data
 * @property WishList $wishlist
 */
class Users extends \pw\core\db\ActiveRecord implements IdentityInterface
{
    const STATUS_NOT_ACTIVATED = 1;
    const STATUS_ACTIVE = 2;
    const STATUS_BANNED = 3;

    private static $statuses;

    public $lastname;
    public $firstname;

    public $role;
    public $newPassword;
    public $repeatPassword;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pw_users}}';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_time', 'login_time']
                ],
                'value' => new Expression('NOW()')
            ],
            'history' => [
                'class' => HistoryBehavior::class,
                'skipAttributes' => ['login_time'] // attributes, that should be ignored
            ],

        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'subscribe'], 'integer'],
            [['language', 'created_time', 'login_time', 'role', 'group_id', 'phone'], 'safe'],
            [['newPassword', 'repeatPassword'], 'string', 'min' => 6],
           // [['newPassword', 'repeatPassword'], 'changePass', 'skipOnEmpty' => false],
            [['name', 'email', 'password', 'old_password'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Module::t('model', 'ID'),
            'language' => Module::t('model', 'Язык'),
            'name' => Module::t('model', 'Имя'),
            'username' => Module::t('model', 'Логин'),
            'email' => Module::t('model', 'E-Mail'),
            'password' => Module::t('model', 'Пароль'),
            'newPassword' => Module::t('model', 'Новый пароль'),
            'role' => Module::t('model', 'Роль'),
            'created_time' => Module::t('model', 'Зарегистрирован'),
            'login_time' => Module::t('model', 'Был активен'),
            'status' => Module::t('model', 'Статус'),
            'phone' => Module::t('model', 'Телефон'),
            'subscribe' => Module::t('model', 'Подписка на новости'),
        ];
    }

    public function changePass()
    {
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccounts()
    {
        return $this->hasMany(UsersAccounts::class, ['user_id' => 'id']);
    }

    public function getData()
    {
        return $this->hasOne(UserData::class, ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(UserGroup::class, ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::class, ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }


    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException;
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * @param $phone
     * @return Users|null
     */
    public static function findByPhone($phone, $format = false)
    {
        if($format){
            $phone = Users::formatPhone($phone);
        }
        return static::findOne(['phone' => $phone]);
    }


    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getWishlist()
    {
        return $this->hasMany(WishList::class, ['user_id' => 'id'])->indexBy('product_id');
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        $res = $this->getAuthKey() === $authKey;
        if ($res) {
            $this->touch('login_time');
        }
        return $res;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        if($this->password == null){
            return md5($password) == $this->old_password;
        }
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     * @throws \yii\base\Exception
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->getSecurity()->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->authKey = Yii::$app->getSecurity()->generateRandomString();
    }


    public function getStatus()
    {
        return self::getStatuses()[$this->status];
    }

    public static function getStatuses()
    {
        if (!self::$statuses) {
            self::$statuses = [
                self::STATUS_ACTIVE => Yii::t('users', 'Активен'),
                self::STATUS_BANNED => Yii::t('users', 'Забанен'),
                self::STATUS_NOT_ACTIVATED => Yii::t('users', 'Не подтверждён')
            ];
        }
        return self::$statuses;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->newPassword) {
                $this->setPassword($this->newPassword);
            }
            return true;
        }
    }

    public function afterDelete()
    {
        parent::afterDelete();
        Yii::$app->authManager->revokeAll($this->id);
    }


    public function getAvatar($options = [])
    {
        return \cebe\gravatar\Gravatar::widget([
            'email' => $this->email,
            'options' => $options,
            'size' => 32
        ]);
    }

    public static function formatPhone($phone)
    {
        try {
            $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
            $formatPhone = $phoneUtil->parse($phone, "RU");
            return $phoneUtil->format($formatPhone, \libphonenumber\PhoneNumberFormat::E164);
        }catch (\Exception $e){
            return false;
        }
    }
    /**
     * @param bool $is_number
     * @param int $length
     * @return int|string
     */
    public static function generatePassword($is_number = true, $length = 5)
    {
        if($is_number){
            return (int)self::random_number($length);
        }

        $password = "";
        $possible = "2346789bcdfghjkmnpqrtvwxyzBCDFGHJKLMNPQRTVWXYZ";
        $maxlength = strlen($possible);

        if ($length > $maxlength) {
            $length = $maxlength;
        }
        $i = 0;

        while ($i < $length) {
            $char = substr($possible, mt_rand(0, $maxlength - 1), 1);
            if (!strstr($password, $char)) {
                $password .= $char;
                $i++;
            }
        }
        return $password;
    }

    /**
     * @param $length
     * @return string
     */
    public static function random_number($length)
    {
        return join('', array_map(function($value) { return $value == 1 ? mt_rand(1, 9) : mt_rand(0, 9); }, range(1, $length)));
    }

}
