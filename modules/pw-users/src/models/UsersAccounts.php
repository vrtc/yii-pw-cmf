<?php

namespace pw\users\models;

use Yii;

/**
 * This is the model class for table "{{%users_accounts}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $source
 * @property string $idSource
 *
 * @property Users $idUser0
 */
class UsersAccounts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pw_users_accounts}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'source', 'idSource'], 'required'],
            [['user_id'], 'integer'],
            [['source', 'idSource'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('users', 'ID'),
            'user_id' => Yii::t('users', 'Id User'),
            'source' => Yii::t('users', 'Source'),
            'idSource' => Yii::t('users', 'Id Source'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::class, ['id' => 'user_id']);
    }
}
