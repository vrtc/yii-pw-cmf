<?php

namespace pw\users\models;

use pw\core\helpers\StringHelper;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\IdentityInterface;
use pw\users\Module;

/**
 * This is the model class for table "{{%user_group}}".
 *
 * @property integer $id
 * @property integer $old_id
 * @property integer $description
 * @property string $name
 * @property string $group_discount
 *
 * @property Users[] $users
 */
class UserGroup extends \pw\core\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pw_user_group}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_discount'], 'integer'],
            [['name', 'description'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Module::t('model', 'ID'),
            'description' => Module::t('model', 'Описание'),
            'group_discount' => Module::t('model', 'Скидка группы'),
            'name' => Module::t('model', 'Название'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Users::class, ['group_id' => 'id']);
    }
}
