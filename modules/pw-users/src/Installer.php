<?php

namespace pw\users;

use pw\mailer\models\Template;
use Yii;
use pw\users\models\Users;

class Installer extends \pw\modules\ModuleInstaller
{

    public function getVersion(): string
    {
        return '0.1';
    }

    public function getAuthors(): array
    {
        return ['i@yar.pw'];
    }

    public function getLink(): string
    {
        return 'http://yar.pw';
    }

    public function getName(): string
    {
        return 'Users';
    }

    public function getSourceLanguage(): string
    {
        return 'ru-RU';
    }


    public function afterInstall()
    {
        if (!Users::find()->count()) {
            $user        = new Users();
            $user->name  = 'admin';
            $user->email = 'admin@example.com';
            $user->setPassword('123qwe');
            $user->status = Users::STATUS_ACTIVE;
            $user->save();

            $authManager = Yii::$app->getAuthManager();
            $root        = $authManager->getRole('root');
            $authManager->assign($root, $user->id);
        }

        $template = new Template();
        $template->key = 'users.signup';
        $template->save(false);


        $template = new Template();
        $template->key = 'users.resetPassword';
        $template->save(false);
    }

    public function getPermissions(): array
    {
        return [
            'backend'  => [
                'default/login' => [
                    'description' => 'Авторизация',
                    'guest'       => true,
                ]
            ],
            'frontend' => [
                'default/login'  => [
                    'description' => 'Авторизация',
                    'guest'       => true,
                ],
                'default/signup' => [
                    'description' => 'Регистрация',
                    'guest'       => true,
                ],
            ],
        ];
    }
}
