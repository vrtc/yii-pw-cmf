<?php
namespace pw\users\widgets;

use Yii;
use pw\core\Widget;

class UserMenu extends Widget {

    public function run(){

        return $this->render('user-menu',[
            'user'=>Yii::$app->user->identity,
            'isGuest'=>Yii::$app->user->getIsGuest()
        ]);
    }
}