<?php


namespace pw\users\commands;


use pw\console\Controller;
use pw\users\models\Users;

class AdminUpdateController extends Controller
{
    public function actionRefreshPassword($email = 'admin@example.com', $password = '123qwe')
    {
        $admin = Users::findOne(['email' => $email]);
        if(!$admin) {
            echo 'Пользователь ' . $email . ' не найден!' . PHP_EOL;
            return;
        }
        $admin->setPassword($password);
        if(!$admin->save()) {
            var_dump($admin->errors);
            return;
        }
        echo 'Пароль для пользователя ' . $admin->name . ' успешно изменён!' . PHP_EOL;
    }
}