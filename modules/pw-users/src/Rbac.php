<?php
namespace pw\users;

class Rbac
{

    const PERMISSION_PROFILE_EDIT = 'pw-users/profile/edit';
    const PERMISSION_PROFILE_VIEW = 'pw-users/profile/view';
    const PERMISSION_LOGIN = 'pw-users/default/login';
    const PERMISSION_SIGNUP = 'pw-users/default/signup';
}
