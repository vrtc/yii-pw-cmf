<?php
/** @var $this \pw\core\View */
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;

?>
<div id="col-main" class="col-md-20 register-page clearfix">
    <div class="col-md-6">
        <?php $form = ActiveForm::begin([
            'id' => 'form-signup',
            'fieldConfig' => [
                'inputOptions' => [
                    'class' => 'form-control col-md-9'
                ],
            ],
        ]); ?>
        <?= $form->field($loginForm, 'email')->textInput() ?>
        <?= $form->field($loginForm, 'password')->passwordInput() ?>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-9">
                <?= Html::submitButton(Yii::t('users', 'Войти'), ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>
</div>