<?php
/** @var $this \pw\core\View */
/* @var $searchModel \cubicbundle\bundles\frontend\models\OrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use \yii\helpers\Url;
use yii\widgets\LinkPager;
$dataProvider->getCount();
$pagination = $dataProvider->getPagination();
?>

<div id="col-main" class="col-md-20 clearfix">
    <ul class="nav nav-pills" role="tablist">
        <li role="presentation" class="active"><a href="<?= Url::to(['index']) ?>"><?= Yii::t('users', 'Покупки') ?></a>
        </li>
        <li role="presentation"><a href="<?= Url::to(['edit']) ?>"><?= Yii::t('users', 'Настройки') ?></a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active">
            <?php if ($dataProvider->getCount()): ?>
                <?php if ($pagination->getPageCount() > 1): ?>
                    <div class="pagination clearfix">
                        <div class="col-md-20">
                            <?= LinkPager::widget(['pagination' => $pagination, 'options' => ['class' => 'list-inline']]) ?>
                        </div>
                    </div>
                <?php endif; ?>
                <table>
                    <tr>
                        <th><?= Yii::t('bundles', 'Дата приобретения') ?></th>
                        <th><?= Yii::t('bundles', 'Название Бандла') ?></th>
                        <th><?= Yii::t('bundles', 'Название игры ') ?></th>
                        <th><?= Yii::t('bundles', 'Магазин ') ?></th>
                        <th><?= Yii::t('bundles', 'Ключ ') ?></th>
                    </tr>
                    <?php foreach($dataProvider->getModels() as $order):?>
        <tr>
        <td><?=Yii::$app->formatter->asDatetime($order->created)?></td>
        <td><?=$order->bundle->name?></td>
        <?php foreach($order->keys as $i=>$key):?>
                <?php if($i>0):?>
                    </tr><tr><td></td><td></td>
                <?php endif?>
                <td><?= $i && $order->keys[$i-1]->game->name == $key->game->name ? '' : $key->game->name?></td>
                <td><?=$key->shop->name?></td>
                <td><?=$key->key?></td>
            </tr>
        <?php endforeach;?>
        </tr>
    <?php endforeach; ?>
                </table>
                <?php if ($pagination->getPageCount() > 1): ?>
                    <div class="pagination clearfix">
                        <div class="col-md-20">
                            <?= LinkPager::widget(['pagination' => $pagination, 'options' => ['class' => 'list-inline']]) ?>
                        </div>
                    </div>
                <?php endif; ?>
            <?php else: ?>

            <?php endif; ?>
        </div>
    </div>

</div>
