<?php
/** @var $this \pw\core\View */
/** @var $changeNameModel \pw\users\frontend\models\ChangeNameForm */
/** @var $changePasswordModel \pw\users\frontend\models\ChangePasswordForm */
use \yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<div id="col-main" class="col-md-20 clearfix">
    <ul class="nav  nav-pills" role="tablist">
        <li role="presentation"><a href="<?=Url::to(['index'])?>"><?=Yii::t('users','Покупки')?></a></li>
        <li role="presentation" class="active"><a href="<?=Url::to(['edit'])?>"><?=Yii::t('users','Настройки')?></a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" >
            <?php $form = ActiveForm::begin([
                'id' => 'change-name',
                'layout'=>'horizontal',
                'fieldConfig' => [
                    'inputOptions' => [
                        'class' => 'form-control col-md-9'
                    ],
                ],
            ]); ?>
            <fieldset>
                <legend><?=Yii::t('users','Сменить имя')?></legend>
            <?=$form->field($changeNameModel,'name')->textInput()?>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-9">
                            <?= Html::submitButton(Yii::t('users', 'Изменить'), ['class' => 'btn']) ?>
                        </div>
                    </div>
                </div>
            </fieldset>
            <?php ActiveForm::end()?>

            <?php $form = ActiveForm::begin([
                'id' => 'change-password',
                'layout'=>'horizontal',
                'enableClientValidation'=>false,
                'fieldConfig' => [
                    'inputOptions' => [
                        'class' => 'form-control col-md-9'
                    ],
                ],
            ]); ?>
            <fieldset>
                <legend><?=Yii::t('users','Сменить пароль')?></legend>
            <?=$form->field($changePasswordModel,'oldPassword')->textInput()?>
            <?=$form->field($changePasswordModel,'password')->textInput()?>
            <?=$form->field($changePasswordModel,'password_repeat')->textInput()?>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-9">
                            <?= Html::submitButton(Yii::t('users', 'Изменить'), ['class' => 'btn']) ?>
                        </div>
                    </div>
                </div>
            </fieldset>
            <?php ActiveForm::end()?>
        </div>
    </div>

</div>
