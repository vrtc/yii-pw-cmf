<?php

namespace pw\users\frontend\controllers;

use pm\payment\components\PayAnyWay;
use pw\reference\Reference;
use pw\users\frontend\models\UserContactForm;
use pw\users\models\UserData;
use pw\users\models\UserDiscount;
use pw\users\models\Users;
use pw\users\models\WishList;
use rd\order\models\Order;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use pw\web\Controller;
use pw\users\frontend\models\ChangeNameForm;
use pw\users\frontend\models\ChangePasswordForm;
use yii\web\Response;
use pm\payment\components\PayKeeper;

class ProfileController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            $this->layout = '/profile.php';
            $this->view->title = Yii::t('users', 'Мои покупки');
            $this->view->registerMetaTag(['name' => 'description', 'content' => Yii::t('users', 'Описание: Мои покупки')], 'description');
            Url::remember();
            //     $searchModel = new OrdersSearch();
            $params = Yii::$app->request->queryParams;
            $params['OrdersSearch']['idUser'] = Yii::$app->user->id;
            $params['OrdersSearch']['email'] = Yii::$app->user->identity->email;
            //     $dataProvider = $searchModel->search($params);

            return $this->render('index', [
                //       'searchModel' => $searchModel,
                //       'dataProvider' => $dataProvider,
            ]);
        } else {
            return $this->redirect(['/']);
        }
    }


    public function actionEdit()
    {
        if (!Yii::$app->user->isGuest) {
            $this->layout = '/profile.php';

            $userModel = UserContactForm::findOne(['id' => Yii::$app->user->id]);
            $profileModel = ($profileModel = UserData::findOne(['user_id' => $userModel->id])) ? $profileModel : new UserData();

            if ($profileModel->isNewRecord) {
                $profileModel->user_id = $userModel->id;
            }
            if ($profileModel->load(Yii::$app->request->post()) && $userModel->load(Yii::$app->request->post()) && $profileModel->validate() && $userModel->validate()) {

                $userModel->save();
                $profileModel->save();
                Yii::$app->session->setFlash('success', 'Контактная информация обновлена');
                return $this->redirect(['edit']);
            }
            return $this->render('edit', [
                'profileModel' => $profileModel,
                'userModel' => $userModel
            ]);
        } else {
            return $this->redirect(['/']);
        }
    }


    public function actionChangePassword()
    {
        if (!Yii::$app->user->isGuest) {
            $this->layout = '/profile.php';
            $model = new ChangePasswordForm();
           // $model = Users::findOne(['id' => Yii::$app->user->id]);
            if ($model->load(Yii::$app->request->post()) && $model->change()) {
                /*$model->setPassword($model->newPassword);
                $model->save();*/
                Yii::$app->session->setFlash('success', 'Пароль успешно изменен');
                return $this->redirect(['change-password']);
            }
            return $this->render('change_password', [
                'model' => $model,
            ]);
        } else {
            return $this->redirect(['/']);
        }
    }


    /**
     * Список желаемого
     * @return array|string
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionWishlist()
    {
        $this->layout = '/profile.php';
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (isset(Yii::$app->request->get()['id'])) {
                $model = WishList::findOne(['id' => Yii::$app->request->get('id'), 'user_id' => Yii::$app->user->id]);
                if ($model) {
                    $model->delete();
                }
                return ['success' => true, 'id' => Yii::$app->request->get('id')];
            }
            return ['success' => false, 'id' => Yii::$app->request->get('id')];
        }
        $models = WishList::find()->where(['user_id' => Yii::$app->user->id]);
        return $this->render('wishlist', ['models' => $models]);
    }


    public function actionOrders()
    {
        if (!Yii::$app->user->isGuest) {
            //$reference = new Reference();
            //$data = $reference->get('payanyway');
            //$payComponent = new PayAnyWay($data);
            $this->layout = '/profile.php';
            $this->view->title = Yii::t('users', 'Мои заказы');
            $orders = Order::find()->where(['user_id' => Yii::$app->user->id])->orderBy(['id' => SORT_DESC])->all();
            return $this->render('orders', ['orders' => $orders]);
        }
    }

    public function actionDiscount()
    {
        if (!Yii::$app->user->isGuest) {
            $this->layout = '/profile.php';
            $userDiscount = new UserDiscount();
            $discount = $userDiscount->getDiscount();
            return $this->render('discount', ['discount' => $discount]);
        } else {
            return $this->redirect(['/']);
        }
    }

    public function actionGetOrder($id)
    {
        if (!Yii::$app->user->isGuest) {
            $order = Order::findOne(['id' => $id]);
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_HTML;
                return $this->renderAjax('order', ['order' => $order]);
            } else {
                return $this->render('order', ['order' => $order]);
            }
        }
    }

    public function actionSubscribe()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->post()) {
            $subscribe = Yii::$app->request->post('subscribe');
            if ($subscribe == 'true') {
                Yii::$app->getModule('subscribers')->addSubscribers(Yii::$app->user->identity->email);
                return ['message' => Yii::t('users', 'Подписка оформлена')];
            }
            Yii::$app->getModule('subscribers')->deleteSubscribers(Yii::$app->user->identity->email);
            return ['message' => Yii::t('users', 'Подписка отменена')];
        }
        return;
    }

    public function actionPayment($id) {

        if (!Yii::$app->user->isGuest && $id) {
            $order = Order::findOne($id);
            $order->payment_method_name = PayKeeper::getKey().'.card';
            if($order->save(false)) {
                $payComponent = new PayKeeper();
                $link = $payComponent->run($order);
                if ($link) {
                    return $this->redirect($link);
                }
            }
        }

        Yii::$app->session->setFlash('warning', 'Произошел сбой. Обратитесь к администрации');
        return $this->redirect(['/pw-users/profile/orders']);
    }

}
