<?php

namespace pw\users\frontend\controllers;

use pw\users\models\UserToken;
use pw\ui\form\ActiveForm;
use pw\users\frontend\models\ResetPasswordForm;
use Yii;
use yii\captcha\CaptchaAction;
use yii\helpers\ArrayHelper;
use pw\web\Controller;
use pw\users\models\Users;
use pw\users\models\UsersAccounts;
use pw\users\frontend\models\LoginForm;
use pw\users\frontend\models\SignupForm;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class DefaultController extends Controller
{

    public function actions()
    {
        return [
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
            ],
            'verify' => [
                'class' => CaptchaAction::class
            ]
        ];
    }

    public function actionLogin($id = null)
    {
         $loginform = new LoginForm();
        $signupForm = new SignupForm();
        if (Yii::$app->request->isAjax) {
            Yii::$app->assetManager->bundles = [
                'tr\trisport\BaseAsssets' => false,
                'yii\web\JqueryAsset' => false,
            ];
            \Yii::$app->response->format = Response::FORMAT_JSON;
            if ($id == 'reg') {
                return ['html' => $this->renderAjax('signup', ['SignupForm' => $signupForm])];
            } elseif ($id == 'auth') {

                return ['html' => $this->renderAjax('auth', ['loginForm' => $loginform])];
            }
        } else {
            if($loginform->load(Yii::$app->request->post()) && $loginform->validate() && $loginform->login()){
                return $this->redirect(['/pw-users/profile/edit']);
            }
            return $this->render('login', ['loginForm' => $loginform]);
        }
    }

    /**
     * @param $client ClientInterface
     * @throws \yii\db\Exception
     */
    public function onAuthSuccess($client)
    {
        $attributes = $client->getUserAttributes();
        /** @var Auth $auth */
        $auth = UsersAccounts::find()->where([
            'source' => $client->getId(),
            'idSource' => $attributes['id'],
        ])->one();
        if (Yii::$app->user->isGuest) {
            if ($auth) {
                $user = $auth->user;
                if ($user->status == Users::STATUS_BANNED) {
                    Yii::$app->getSession()->setFlash('danger', Yii::t('users', 'Ваш аккаунт удалён администратом.'));
                    $this->redirect(['login']);
                }
                Yii::$app->user->login($user);
            } else {
                if (isset($attributes['email'])) {
                    $email = $attributes['email'];
                } else {
                    $auth_params = $client->getAccessToken()->getParams();
                    $email = ArrayHelper::getValue($auth_params, 'email', '');
                }
                if ($email && Users::findByEmail($email)) {
                    Yii::$app->getSession()->setFlash('danger', [
                        Yii::t('users',
                            'Пользователь с таким email как в  {client} уже существует, но не связан. Авторизуйтесь используя email и пароль.',
                            ['client' => $client->getTitle()]),
                    ]);
                    Url::remember(['auth', 'authclient' => $client->getId()], 'social');
                    return $this->redirect(['login']);
                } else {
                    $password = Yii::$app->security->generateRandomString(6);
                    $form = new SignupForm();
                    $form->firstname = isset($attributes['first_name']) ? $attributes['first_name'] : $attributes['name'];
                    $form->email = $email;
                    $form->password = $password;
                    $form->password_repeat = $password;
                    //$form->language = Yii::$app->language;
                    $form->isSocial = true;
                    $transaction = Yii::$app->getDb()->beginTransaction();
                    $idUser = $form->signup();
                    if ($idUser) {
                        $user = Users::findOne(['id' => $idUser->id]);
                        $user->status = Users::STATUS_ACTIVE;
                        $user->save();
                        $auth = new UsersAccounts([
                            'user_id' => $idUser->id,
                            'source' => $client->getId(),
                            'idSource' => (string)$attributes['id'],
                        ]);
                        if ($auth->save()) {
                            $transaction->commit();
                            Yii::$app->user->login($user);
                            return $this->goHome();

                        }else{
                        }
                    }
                    $transaction->rollBack();

                }
            }
        } else { // user already logged in
            if (!$auth) { // add auth provider
                $auth = new UsersAccounts([
                    'user_id' => Yii::$app->user->id,
                    'source' => $client->getId(),
                    'idSource' => '' . $attributes['id'],
                ]);
                if (!$auth->save()) {
                    Yii::error($auth->getErrors(), 'auth');
                    exit;
                }
            }
        }
    }

    public function actionConfirm($token = null)
    {
        if ($token) {
            $data = Yii::$app->tokens->isValid('users.signup', $token);
            if ($data === false)
                throw new NotFoundHttpException();
            $user = Users::findOne($data['id']);
            $user->status = Users::STATUS_ACTIVE;
            $user->save();
            Yii::$app->session->setFlash('success', Yii::t('users', 'Ваш аккаунт активирован'));
            Yii::$app->user->login($user);
            return $this->goBack();
        }
        $this->view->title = Yii::t('users', 'Активация аккаунта');
        $this->view->registerMetaTag(['name' => 'description', 'content' => Yii::t('users', 'Описание: Активация аккаунта')], 'description');
        return $this->render('confirm');
    }


    /**
     * @return array|string|Response
     */
//    public function actionLogin()
//    {
//        /* if (!Yii::$app->user->isGuest)
//             return $this->goBack();*/
//        $loginForm = new LoginForm();
//        $signupForm = new SignupForm();
//
//
//        if (Yii::$app->request->isAjax) {
//
//            \Yii::$app->response->format = Response::FORMAT_JSON;
//            if ($loginForm->load(Yii::$app->request->post()) && $loginForm->login()) {
//                return ['success' => 'success'];
//            } else {
//                return ActiveForm::validate($loginForm);
//            }
//        }
//
//
//        if ($loginForm->load(Yii::$app->request->post()) && $loginForm->login()) {
//            $back = Url::previous('social');
//            if ($back)
//                return $this->redirect($back);
//            return $this->goBack();
//        }
//        if ($signupForm->load(Yii::$app->request->post()) && $signupForm->signup()) {
//            return $this->redirect(['confirm']);
//        }
//        $this->view->title = Yii::t('users', 'Авторизация');
//        $this->view->registerMetaTag(['name' => 'description', 'content' => Yii::t('users', 'Описание: Авторизация')], 'description');
//        return $this->render('login', [
//            'loginForm' => $loginForm,
//            'signupForm' => $signupForm,
//        ]);
//    }

    public function actionValidate()
    {
        $loginForm = new LoginForm();
        if (Yii::$app->request->isAjax) {
            if ($loginForm->load(Yii::$app->request->post())) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($loginForm);
            }
        }
        $signupForm = new SignupForm();
        if (Yii::$app->request->isAjax) {
            if ($signupForm->load(Yii::$app->request->post())) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($signupForm);
            }
        }
    }

    public function actionLogout()
    {
        if (Yii::$app->user->logout())
            return $this->goHome();
        return $this->goBack();
    }

    public function actionReset($token = null)
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['/pw-users/profile/index']);
        }
        $this->view->title = Yii::t('users', 'Восстановление пароля');
        $this->view->registerMetaTag(['name' => 'description', 'content' => Yii::t('users', 'Описание: Восстановление пароля')], 'description');
        $form = new ResetPasswordForm();

        if ($token) {
            $data = Yii::$app->tokens->isValid('users.resetPassword', $token);
            if ($data === false)
                throw new NotFoundHttpException('');
            $form->email = $data['email'];
            $form->scenario = ResetPasswordForm::SCENARIO_STEP2;
            if ($form->load(Yii::$app->request->post()) && $form->change()) {
                $user = Users::findByEmail($form->email);
                Yii::$app->user->login($user);
                Yii::$app->session->setFlash('success', Yii::t('users', 'Пароль успешно изменён'));
                return $this->goHome();
            }
        } else {
            $form->scenario = ResetPasswordForm::SCENARIO_STEP1;
            if ($form->load(Yii::$app->request->post()) && $form->send()) {
                Yii::$app->session->setFlash('success', Yii::t('users', 'На {phone} отправлен новый пароль.', ['phone' => $form->phone]));
                return $this->redirect(['reset']);
            }
        }
        return $this->render('reset', ['model' => $form]);
    }

    public function actionSignup()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['/pw-users/profile/index']);
        }
            $signupForm = new SignupForm();
            if ($signupForm->load(Yii::$app->request->post()) && $user = $signupForm->signup()) {
                Yii::$app->user->login($user, 3600 * 24 * 30);
                if (Yii::$app->user->isGuest) {
                    return ['fail' => 'failed'];
                }
                return $this->redirect(['/pw-users/profile/edit', 'signup' => 1]);
            }
            return $this->render('register', ['SignupForm' => $signupForm]);
       // }
    }

    /**
     * @param $token
     * @return array|string|Response
     * @throws ForbiddenHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionLoginByPass($token)
    {

        if(!Yii::$app->user->isGuest){
            Yii::$app->user->logout();
        }

        $user = UserToken::use($token, UserToken::TYPE_LOGIN_PASS);

        if ($user === null) {
            throw new ForbiddenHttpException();
        }

        Yii::$app->user->login($user);
        return $this->goHome();
    }

    public function actionUserLogin(){
        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('login-logout');
        }
    }
}
