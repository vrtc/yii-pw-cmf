<?php
/**@var $this \pw\core\View */
use yii\helpers\Url;
?>
<?php if(Yii::$app->user->isGuest):?>
<div class="customer-links">
    <ul id="accounts" class="list-inline">
        <li class="register">
            <a href="<?=Url::to(['users/signup'])?>" id="customer_register_link"><?=Yii::t('main','Войти/Зарегистрироваться')?></a>
        </li>
    </ul>
</div>
<?php else:?>
<?php endif;?>
