<?php
namespace pw\users\frontend\widgets;

use yii\base\Widget;

class LoginWidget extends Widget {

    public function run(){
        return $this->render('login');
    }
}