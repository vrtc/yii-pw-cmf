<?php
namespace pw\users\frontend\models;

use pw\users\models\Users;
use Yii;
use yii\base\Model;
class ChangePasswordForm extends Model
{
    public $oldPassword;
    public $password;
    public $password_repeat;

    private $user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password','password_repeat'], 'required'],
           // ['oldPassword', 'validatePassword'],
            [['password_repeat'],'compare','compareAttribute'=>'password'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (!Yii::$app->user->identity->validatePassword($this->oldPassword)) {
                $this->addError($attribute, Yii::t('users','Неверный пароль'));
            }
        }
    }

    public function attributeLabels(){
        return [
            'oldPassword'=>Yii::t('users','Старый Пароль'),
            'password'=>Yii::t('users','Пароль'),
            'password_repeat'=>Yii::t('users','Повторите новый пароль'),
        ];
    }


    public function change()
    {
        if ($this->validate()) {
            $user = $this->getUser();
            $user->setPassword($this->password);
            if($user->save(false)){
                return true;
            }
        }
        return false;
    }

    public function getUser(){
        /**
         * @var $userModel Users
         */
        if(!$this->user){
            $userModel = Users::class;
            $this->user = $userModel::findOne(Yii::$app->user->id);
        }
        return $this->user;
    }
}
