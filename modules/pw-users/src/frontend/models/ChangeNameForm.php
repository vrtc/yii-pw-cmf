<?php
namespace pw\users\frontend\models;

use Yii;
use yii\base\Model;
class ChangeNameForm extends Model
{
    public $name;

    private $user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'string'],
        ];

    }

    public function attributeLabels(){
        return [
            'name'=>Yii::t('users','Новое имя'),
        ];
    }


    public function change()
    {
        if ($this->validate()) {
            $user = $this->getUser();
            $user->name = $this->name;
            if($user->save(false)){
                return true;
            }
        }
        return false;
    }

    public function getUser(){
        if(!$this->user){
            $userModel = Yii::$app->getModule('users')->getUserModel();
            $this->user = $userModel::findOne(Yii::$app->user->id);
        }
        return $this->user;
    }
}
