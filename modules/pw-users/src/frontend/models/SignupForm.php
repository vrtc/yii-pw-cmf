<?php

namespace pw\users\frontend\models;

use pw\users\models\UserData;
use Yii;
use yii\base\Model;
use yii\db\Exception;
use pw\users\models\Users;

//use himiklab\yii2\recaptcha\ReCaptchaValidator;

class SignupForm extends Model
{
    public $email;
    public $firstname;
    public $lastname;
    public $address;
    public $password;
    public $password_repeat;
    public $phone;
    public $region;
    public $city;
    public $isSocial = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email','firstname'], 'required'],
            ['phone', 'required', 'when' => function(SignupForm $model){
                return !$model->isSocial;
            }],
            ['phone', 'validatePhone'],
            [['email'], 'email'],
            [['address', 'lastname'], 'string'],
            /*[
                ['email'],
                'unique',
                'targetClass' => Users::class,
                'message'     => Yii::t('users', 'Пользователь с таким email уже существует.'),
                'targetAttribute' => ['email' => 'email'],
            ],*/
            [
                ['phone'],
                'unique',
                'targetClass' => Users::class,
                'message'     => Yii::t('users', 'Пользователь с таким Телефоном уже существует.'),
                'targetAttribute' => ['phone' => 'phone'],
            ]
            //[['verifyCode'], \himiklab\yii2\recaptcha\ReCaptchaValidator::class]

        ];
    }

    public function validatePhone($attribute, $params)
    {
        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
        $formatPhone = $phoneUtil->parse($this->phone, "RU");
        $phone = $phoneUtil->format($formatPhone, \libphonenumber\PhoneNumberFormat::E164);
        if(strlen($phone) < 12){
            return $this->addError('phone', 'Введите полный номер');
        }
        $this->phone = $phone;
    }


    public function attributeLabels()
    {
        return [
            'name'            => Yii::t('users', 'Имя'),
            'email'           => Yii::t('users', 'Email'),
            'phone'           => Yii::t('users', 'Телефон'),
            'password'        => Yii::t('users', 'Пароль'),
            'email_repeat'    => Yii::t('users', 'Повторите email'),
            'password_repeat' => Yii::t('users', 'Повторите пароль'),
            'firstname'       => Yii::t('users', 'Имя'),
            'lastname'        => Yii::t('users', 'Фамилия'),
            'language'        => Yii::t('users', 'Язык'),
            'agreement'       => Yii::t('users', 'Я согласен с условиями использования'),
            'subscribe'       => Yii::t('users', 'Я хочу получать рассылку об акциях и скидах'),
            'verifyCode'      => Yii::t('users', 'Введите код'),
        ];
    }



    /**
     * @return bool|Users
     * @throws Exception
     */
    public function signup()
    {
        if ($this->validate()) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                $user        = new Users();
                $user->email = $this->email;
                $user->name  = $this->firstname;
                $user->phone  = $this->phone;
                $password = Users::generatePassword();
                $user->setPassword($password);
                $user->status        = Users::STATUS_ACTIVE;

                if ($user->save()) {
                    $data = new UserData();
                    $data->user_id = $user->id;
                    $data->firstname = $this->firstname;
                    $data->lastname = $this->lastname;
                    $data->address = $this->address;
                    $data->region = $this->region;
                    $data->city = $this->city;
                   // $data->telephone = UserData::formatPhone($this->phone);
                    $data->save();
                    $userRole = Yii::$app->authManager->getRole('user');
                    Yii::$app->authManager->assign($userRole, $user->id);
                    $transaction->commit();
                    \Yii::$app->user->login($user);
                    try {
                        if($user->phone != null) {
                            \Yii::$app->sms->send(new \alexeevdv\sms\ru\Sms([
                                "to" => $user->phone,
                                "text" => 'Ваш пароль: ' . $password . ' Спасибо за регистрацию!',
                            ]));
                        }

                        Yii::$app->mailer->compose('users.signup', [
                            '{user.email}' => $user->email,
                            '{user.name}' => $user->name,
                            '{user.password}' => $password
                        ])->setTo($user->email)->send();


                    }catch (\Exception $e){
                        Yii::error($e, 'signup_mail_error');
                    }
                    return $user;
                }

                return false;

            }
            catch (Exception $e){
                $transaction->rollBack();
                throw $e;
            }
        }

        return false;
    }

}
