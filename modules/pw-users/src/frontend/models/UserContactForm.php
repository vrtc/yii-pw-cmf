<?php
/**
 * Created by trisport_pw.
 * User: Dmitriy Kindeev
 * Email: dkindeev@mail.ru
 * Date: 29.01.2020
 * Time: 03:53
 */
namespace pw\users\frontend\models;
use pw\users\models\Users;

class UserContactForm extends Users{
    public function rules()
    {
        return[
            [['email'], 'email'],
            [['email', 'phone'], 'required'],
            [['subscribe'], 'integer'],
        ];
    }
}
