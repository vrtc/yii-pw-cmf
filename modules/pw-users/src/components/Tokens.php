<?php
/**
 * Created by new_trisport.
 * User: Dmitriy Kindeev
 * Email: dkindeev@mail.ru
 * Date: 22.07.2019
 * Time: 11:35
 */
namespace pw\users\components;

use yii\base\Component;
use pw\users\models\Tokens as TokenModel;

class Tokens extends Component{


    public function create($name,$data,$expireTime = null){
        if(!$expireTime){
            $expireTime = 3600*24 * 30;
        }
        $token = new TokenModel();
        $token->name = $name;
        $token->data = serialize($data);
        $token->expireTime = $expireTime;
        if($token->save())
            return $token->code;
        return false;
    }

    public function isValid($name,$token){
        $token = TokenModel::findOne(['name'=>$name,'code'=>$token]);
        if($token){
            if($token->isValid()){
                return unserialize($token->data);
            }
            $token->delete();
        }
        return false;
    }

}
