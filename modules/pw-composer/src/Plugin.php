<?php
namespace pw\composer;

use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;

class Plugin implements PluginInterface
{
    /**
     * @inheritdoc
     */
    public function activate(Composer $composer, IOInterface $io)
    {
        $installer = new PwInstaller($io, $composer);
        $composer->getInstallationManager()->addInstaller($installer);
    }
}
