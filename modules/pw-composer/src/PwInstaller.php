<?php
namespace pw\composer;

use Composer\Package\PackageInterface;

class PwInstaller extends \yii\composer\Installer
{

    const TYPE_THEME = 'pw-theme';
    const TYPE_EXTENSION = 'pw-module';

    const EXTENSION_FILE = 'pw/modules.php';
    const THEME_FILE = 'pw/themes.php';

    /**
     * @inheritdoc
     */
    public function supports($packageType)
    {
        return $packageType === self::TYPE_THEME || $packageType === self::TYPE_EXTENSION;
    }

    protected function addPackage(PackageInterface $package)
    {
        $type = $package->getType();
        if ($type === self::TYPE_EXTENSION) {
            parent::addPackage($package);
        } elseif ($type === self::TYPE_THEME) {
            $theme = [
                'name' => $package->getName(),
                'version' => $package->getVersion(),
            ];

            $alias = $this->generateDefaultAlias($package);
            if (!empty($alias)) {
                $theme['alias'] = $alias;
            }

            $themes = $this->loadThemes();
            $themes[$package->getName()] = $theme;
            $this->saveThemes($themes);
        }
    }

    protected function loadThemes()
    {
        $file = $this->vendorDir . '/' . static::THEME_FILE;
        if (!is_file($file)) {
            return [];
        }
        // invalidate opcache of themes.php if exists
        if (function_exists('opcache_invalidate')) {
            opcache_invalidate($file, true);
        }
        $themes = require($file);

        $vendorDir = str_replace('\\', '/', $this->vendorDir);
        $n = strlen($vendorDir);

        foreach ($themes as &$theme) {
            if (isset($theme['alias'])) {
                foreach ($theme['alias'] as $alias => $path) {
                    $path = str_replace('\\', '/', $path);
                    if (strpos($path . '/', $vendorDir . '/') === 0) {
                        $theme['alias'][$alias] = '<vendor-dir>' . substr($path, $n);
                    }
                }
            }
        }

        return $themes;
    }

    /**
     * @param array $themes
     * @throws \Exception
     */
    protected function saveThemes(array $themes)
    {
        $file = $this->vendorDir . '/' . static::THEME_FILE;
        $dir = dirname($file);
        if (!is_dir($dir) && !@mkdir($dir, 0777, true) && !is_dir($dir)) {
            throw new \Exception('Cannot create ' . $dir . ' directory');
        }
        $array = str_replace("'<vendor-dir>", '$vendorDir . \'', var_export($themes, true));
        file_put_contents($file, "<?php\n\n\$vendorDir = dirname(__DIR__);\n\nreturn $array;\n");
        // invalidate opcache of modules.php if exists
        if (function_exists('opcache_invalidate')) {
            opcache_invalidate($file, true);
        }
    }
}
