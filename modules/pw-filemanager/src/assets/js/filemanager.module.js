var fileManagerModule = {
    //params for input selector
    fieldId: null,
    cropRatio: null,
    cropViewMode: 1,
    defaultFileId: null,
    selectType: null,
    //current selected image
    selectedFile: null,
    //language
    message: null,
    //init imageManager
    init: function () {
        //init cropper
        $('#module-filemanager > .row .col-image-editor .image-cropper .image-wrapper img#image-cropper').cropper({
            viewMode: fileManagerModule.cropViewMode
        });

        //preselect image if image-id isset
        if (fileManagerModule.defaultFileId !== "") {
            fileManagerModule.selectFile(fileManagerModule.defaultFileId);
        }

        //set selected after pjax complete
        $('#pjax-filemanager').on('pjax:complete', function () {
            if (fileManagerModule.selectedFile !== null) {
                fileManagerModule.selectFile(fileManagerModule.selectedFile.id);
            }
        });
    },
    //filter result
    filterFileResult: function (searchTerm) {
        //set new url
        var newUrl = window.queryStringParameter.set(window.location.href, "FileSearch[globalSearch]", searchTerm);
        //set pjax
        $.pjax({
            url: newUrl,
            container: "#pjax-filemanager",
            push: false,
            replace: false,
            timeout: 5000,
            scrollTo: false
        });
    },
    selectFile: function (id) {
        //set selected class
        $("#module-filemanager .item-overview .item").removeClass("selected");
        $("#module-filemanager .item-overview .item[data-key='" + id + "']").addClass("selected");
        //get details
        fileManagerModule.getDetails(id);
    },
    //pick the selected image
    pickFile: function () {
        //switch between select type
        switch (fileManagerModule.selectType) {
            //default widget selector
            case "input":
                //get id data
                var sFieldId = fileManagerModule.fieldId;
                var sFieldNameId = sFieldId + "_name";
                var sFieldFileId = sFieldId + "_file";
                var fieldName = $('#' + sFieldId, window.parent.document).attr("name");
                //set input data
                console.log('#' + sFieldId, fileManagerModule.selectedFile.id);
                if (fileManagerModule.selectedFile.id != null) {
                    $('.input-group', window.parent.document).append($('<input>').attr({value: fileManagerModule.selectedFile.id, name: fieldName, type: "hidden"}));
                }
                $('#' + sFieldFileId, window.parent.document).attr("src", fileManagerModule.selectedFile.thumbnailUrl).parent().removeClass("hide");
                //trigger change
                $(".input-group", window.parent.document).trigger('change');
                parent.$('#' + sFieldId).trigger('change');
                //show delete button
                $(".delete-selected-file[data-input-id='" + sFieldId + "']", window.parent.document).removeClass("hide");
                //close the modal
                window.parent.fileManagerInput.closeModal();
                break;
            //CKEditor selector
            case "ckeditor":
            //TinyMCE Selector
            case "tinymce":
                //check if isset image
                if (fileManagerModule.selectedFile !== null) {
                    //call action by ajax
                    $.ajax({
                        url: fileManagerModule.baseUrl + "/get-original-image",
                        type: "POST",
                        data: {
                            FileManager_id: fileManagerModule.selectedFile.id,
                            _csrf: $('meta[name=csrf-token]').prop('content')
                        },
                        dataType: "json",
                        success: function (responseData, textStatus, jqXHR) {
                            //set attributes for each selector
                            if (fileManagerModule.selectType == "ckeditor") {
                                var sField = window.queryStringParameter.get(window.location.href, "CKEditorFuncNum");
                                window.top.opener.CKEDITOR.tools.callFunction(sField, responseData);
                                window.self.close();
                            } else if (fileManagerModule.selectType == "tinymce") {
                                var sField = window.queryStringParameter.get(window.location.href, "tag_name");
                                window.opener.document.getElementById(sField).value = responseData;
                                window.close();
                                window.opener.focus();
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            alert("Error: can't get item");
                        }
                    });
                } else {
                    alert("Error: file can't picked");
                }
                break;
        }


    },
    //delete the selected file
    deleteSelectedFile: function () {
        //confirm message
        result = confirm(fileManagerModule.message.deleteMessage);
        if (result) {
            fileManagerModule.editor.close();
            //check if isset image
            if (fileManagerModule.selectedFile !== null) {
                //call action by ajax
                $.ajax({
                    url: fileManagerModule.baseUrl + "/delete",
                    type: "POST",
                    data: {
                        id: fileManagerModule.selectedFile.id,
                        _csrf: $('meta[name=csrf-token]').prop('content')
                    },
                    dataType: "json",
                    success: function (responseData, textStatus, jqXHR) {
                        //check if delete is true
                        if (responseData.delete === true) {
                            //delete item element
                            $("#module-filemanager .item-overview .item[data-key='" + fileManagerModule.selectedFile.id + "']").remove();
                            //add hide class to info block
                            $("#module-filemanager .file-info").addClass("hide");
                            //set selectedImage to null
                            fileManagerModule.selectedFile = null;
                            //close edit
                        } else {
                            alert("Error: item is not deleted");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("Error: can't delete item");
                    }
                });
            } else {
                alert("Error: file can't delete, no file isset set");
            }
        }
    },
    //get file details
    getDetails: function (id, pickAfterGetDetails) {
        //set propertie if not set
        pickAfterGetDetails = pickAfterGetDetails !== undefined ? pickAfterGetDetails : false;
        //call action by ajax
        $.ajax({
            url: fileManagerModule.baseUrl + "/view",
            type: "POST",
            data: {
                id: id,
                _csrf: $('meta[name=csrf-token]').prop('content')
            },
            dataType: "json",
            success: function (responseData, textStatus, jqXHR) {
                fileManagerModule.selectedFile = responseData;
                if (pickAfterGetDetails) {
                    fileManagerModule.pickFile();
                } else {
                    //set text elements
                    $("#module-filemanager .file-info .fileName").text(responseData.fileName).attr("title", responseData.fileName);
                    $("#module-filemanager .file-info .created").text(responseData.created);
                    $("#module-filemanager .file-info .fileSize").text(responseData.fileSize);
                    $("#module-filemanager .file-info .thumbnail").html(responseData.thumbnail);
                    $("#module-filemanager .file-info").removeClass("hide");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert("Can't view image. Error: " + jqXHR.responseText);
            }
        });
    },
    //upload file
    uploadSuccess: function (uploadResponse) {
        //close editor
        fileManagerModule.editor.close();
        //reload pjax container
        $.pjax.reload('#pjax-filemanager', {push: false, replace: false, timeout: 5000, scrollTo: false});
    },
    //editor functions
    editor: {
        //open editor block
        open: function () {
            //show editer / hide overview
            $("#module-filemanager > .row .col-image-editor").show();
            $("#module-filemanager > .row .col-overview").hide();
        },
        //close editor block
        close: function () {
            //show overview / hide editer
            $("#module-filemanager > .row .col-overview").show();
            $("#module-filemanager > .row .col-image-editor").hide();
        },
        //open cropper
        openCropper: function () {
            //check if isset image
            if (fileManagerModule.selectedFile !== null) {
                //call action by ajax
                $.ajax({
                    url: fileManagerModule.baseUrl + "/get-original-image",
                    type: "POST",
                    data: {
                        ImageManager_id: fileManagerModule.selectedFile.id,
                        _csrf: $('meta[name=csrf-token]').prop('content')
                    },
                    dataType: "json",
                    success: function (responseData, textStatus, jqXHR) {
                        //hide cropper
                        $("#module-filemanager > .row .col-image-cropper").css("visibility", "hidden");
                        //set image in cropper
                        $('#module-filemanager > .row .col-image-editor .image-cropper .image-wrapper img#image-cropper').one('built.cropper', function () {
                            //show cropper
                            $("#module-filemanager > .row .col-image-cropper").css("visibility", "visible");
                        })
                            .cropper('reset')
                            .cropper('setAspectRatio', parseFloat(fileManagerModule.cropRatio))
                            .cropper('replace', responseData);
                        //open editor
                        fileManagerModule.editor.open();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("Error: can't get item");
                    }
                });
            } else {
                alert("Error: image can't crop, no image isset set");
            }
        },
        //apply crop
        applyCrop: function (pickAfterCrop) {
            //set propertie if not set
            pickAfterCrop = pickAfterCrop !== undefined ? pickAfterCrop : false;
            //check if isset image
            if (fileManagerModule.selectedFile !== null) {
                //set image in cropper
                var oCropData = $('#module-filemanager > .row .col-image-editor .image-cropper .image-wrapper img#image-cropper').cropper("getData");
                //call action by ajax
                $.ajax({
                    url: fileManagerModule.baseUrl + "/crop",
                    type: "POST",
                    data: {
                        ImageManager_id: fileManagerModule.selectedFile.id,
                        CropData: oCropData,
                        _csrf: $('meta[name=csrf-token]').prop('content')
                    },
                    dataType: "json",
                    success: function (responseData, textStatus, jqXHR) {
                        //set cropped image
                        if (responseData !== null) {
                            //if pickAfterCrop is true? select directly else
                            if (pickAfterCrop) {
                                fileManagerModule.getDetails(responseData, true);
                                //else select the image only
                            } else {
                                //set new image
                                fileManagerModule.selectFile(responseData);
                                //reload pjax container
                                $.pjax.reload('#pjax-filemanager', {
                                    push: false,
                                    replace: false,
                                    timeout: 5000,
                                    scrollTo: false
                                });
                            }
                        }
                        //close editor
                        fileManagerModule.editor.close();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("Error: item is not cropped");
                    }
                });
            } else {
                alert("Error: image can't crop, no image isset set");
            }
        }
    }
};

$(document).ready(function () {
    fileManagerModule.init();
    $(document).on("click", "#module-filemanager .item-overview .item", function () {
        var file_id = $(this).data("key");
        fileManagerModule.selectFile(file_id);
    });
    $(document).on("click", "#module-filemanager .file-info .pick-file-item", function () {
        fileManagerModule.pickFile();
        return false;
    });
    $(document).on("click", "#module-filemanager .file-info .delete-file-item", function () {
        fileManagerModule.deleteSelectedFile();
        return false;
    });
    $(document).on("click", "#module-filemanager .file-info .crop-image-item", function () {
        fileManagerModule.editor.openCropper();
        return false;
    });
    $(document).on("click", "#module-filemanager .image-cropper .apply-crop", function () {
        fileManagerModule.editor.applyCrop();
        return false;
    });
    $(document).on("click", "#module-filemanager .image-cropper .apply-crop-select", function () {
        fileManagerModule.editor.applyCrop(true);
        return false;
    });
    $(document).on("click", "#module-filemanager .image-cropper .cancel-crop", function () {
        fileManagerModule.editor.close();
        return false;
    });
    $(document).on("keyup change", "#input-filemanager-search", function () {
        fileManagerModule.filterFileResult($(this).val());
    });

});

/*
 * return new get param to url
 */
window.queryStringParameter = {
    get: function (uri, key) {
        var reParam = new RegExp('(?:[\?&]|&amp;)' + key + '=([^&]+)', 'i');
        var match = uri.match(reParam);
        return (match && match.length > 1) ? match[1] : null;
    },
    set: function (uri, key, value) {
        //replace brackets
        var keyReplace = key.replace("[]", "").replace(/\[/g, "%5B").replace(/\]/g, "%5D");
        //replace data
        var re = new RegExp("([?&])" + keyReplace + "=.*?(&|$)", "i");
        var separator = uri.indexOf('?') !== -1 ? "&" : "?";
        if (uri.match(re)) {
            return uri.replace(re, '$1' + keyReplace + "=" + value + '$2');
        }
        else {
            return uri + separator + keyReplace + "=" + value;
        }
    }
};