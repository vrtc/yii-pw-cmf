var fileManagerInput = {
	baseUrl: null,
	//language
	message: null,
	//init fileManagerInput
	init: function(){
		//create modal
		fileManagerInput.initModal();
	},
	//creat image Manager modal
	initModal: function(){
		//check if modal not jet exists
		if($("#modal-filemanager").length === 0){
			//set html modal in var
			var sModalHtml = '<div tabindex="-1" role="dialog" class="fade modal" id="modal-filemanager">';
				sModalHtml += '<div class="modal-dialog modal-lg">';
					sModalHtml += '<div class="modal-content">';
						sModalHtml += '<div class="modal-header">';
							sModalHtml += '<button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>';
						sModalHtml += '</div>';
						sModalHtml += '<div class="modal-body">';
							sModalHtml += '<iframe src="#"></iframe>';
						sModalHtml += '</div>';
					sModalHtml += '</div>';
				sModalHtml += '</div>';
			sModalHtml += '</div>';
			//prepend data to body
			$('body').prepend(sModalHtml);
		}
	},
	//open media manager modal
	openModal: function(inputId){
		//get selected item
		var fileId = $("#"+inputId).val();
		var srcImageIdQueryString = "";
		if(fileId !== ""){
			srcImageIdQueryString = "&image-id="+fileId;
		}
		//create iframe url
		var queryStringStartCharacter = ((fileManagerInput.baseUrl).indexOf('?') == -1) ? '?' : '&';
		var filemanagerUrl = fileManagerInput.baseUrl+queryStringStartCharacter+"view-mode=iframe&input-id="+inputId+srcImageIdQueryString;
		$("#modal-filemanager iframe").attr("src",filemanagerUrl);
                $("#modal-filemanager .modal-dialog .modal-header h4").text(fileManagerInput.message.filemanager);
		$("#modal-filemanager").modal("show");
	},
	//close media manager modal
	closeModal: function(){
		$("#modal-filemanager").modal("hide");
	},
	//delete picked image
	deletePickedImage: function(inputId){
		//remove value of the input field
		var sFieldId = inputId;
		var sFieldNameId = sFieldId+"_name";
		var sImagePreviewId = sFieldId+"_image";
		//var bShowConfirm = JSON.parse($(".delete-selected-image[data-input-id='"+inputId+"']").data("show-delete-confirm"));
		//show warning if bShowConfirm == true
	//	if(bShowConfirm){
		//	if(confirm(fileManagerInput.message.detachWarningMessage) == false){
		//		return false;
		//	}
	//	}
		//set input data
		$("input[value='"+inputId+"']").remove();
	//	$('#'+sFieldId).val("");
	//	$('#'+sFieldNameId).val("");
		//trigger change
		$('#'+sFieldId).trigger("change");
		//hide image
		$('#'+sImagePreviewId).attr("src","").parent().addClass("hide");	
		//delete hide class
		$(".delete-selected-file[data-input-id='"+inputId+"']").addClass("hide");
	}
};

$(document).ready(function () {
	//init Image manage
	fileManagerInput.init();
	
	//open media manager modal
	$(document).on("click", ".open-filemanager", function () {
		var inputId = $(this).data("input-id");
		fileManagerInput.openModal(inputId);
	});	
	
	//delete picked image
	$(document).on("click", ".delete-selected-image", function () {
		var inputId = $(this).data("input-id");
		$(this).closest('.file-preview').remove();
		//open selector id
		fileManagerInput.deletePickedImage(inputId);
	});	
});