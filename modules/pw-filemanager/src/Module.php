<?php

namespace pw\filemanager;

use pw\filemanager\components\FileManager;
use Yii;
use Intervention\Image\ImageManagerStatic as Image;
use yii\base\Application;
use yii\base\BootstrapInterface;

class Module extends \pw\core\Module implements BootstrapInterface
{

    public $driver = 'gd';

    public $thumbsDir = '@webroot/thumbs';

    public $imageQuality = 84;


    public function init()
    {
        parent::init();
        Image::configure(['driver' => $this->driver]);
    }

    public function delete($path)
    {
        foreach (glob(Yii::getAlias($this->thumbsDir) . '/*/' . $path) as $file) {
            @unlink($file);
        }
    }

    public function components(Application $app): array
    {
        return [
            'fileManager' => FileManager::class,
            'image' => [
                'class' => \pw\filemanager\components\Image::class,
                'storePath' => '@webroot/files',
            ],
        ];
    }

    public function routes(Application $app): array
    {
        return [
            [
                'pattern'  => '/thumbs/<method:(crop|resize|fill)>/<ext:.+>/<width:\d+>x<height:\d+>/<file:.+>',
                'route'    => '/pw-filemanager/image/<method>',
                'name'     => 'Генерация превью',
                'internal' => true
            ],
            [
                'pattern'  => '/thumbs/<method:(crop|resize|fill)>/<ext:.+>/-x<height:\d+>/<file:.+>',
                'route'    => '/pw-filemanager/image/<method>',
                'name'     => 'Генерация превью',
                'internal' => true
            ],
            [
                'pattern'  => '/thumbs/<method:(crop|resize|fill)>/<ext:.+>/<width:\d+>x-/<file:.+>',
                'route'    => '/pw-filemanager/image/<method>',
                'name'     => 'Генерация превью',
                'internal' => true
            ],
            [
                'pattern'  => '/file/<file:.+>',
                'route'    => '/pw-filemanager/file/download',
                'name'     => 'Скачивание файла',
                'internal' => true
            ],
        ];
    }

    public function getNavigation(): array
    {
        return [
            'system'   => [
                [
                    'label' => 'Файловый менеджер',
                    'url'   => ['/pw-filemanager/manage/index'],
                    'icon' => 'photo-video',
                //    'render'=> false,
                ]
            ],
            'settings' => [
                [
                    'label' => 'Файловый менеджер',
                    'url'   => ['/pw-filemanager/settings/index'],
                    'icon' => 'photo-video'
              //      'render'=> false,
                ]
            ]
        ];
    }

}
