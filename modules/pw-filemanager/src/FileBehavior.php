<?php

namespace pw\filemanager;


use pw\filemanager\models\File;
use yii\base\Behavior;
use pw\core\db\ActiveRecord;
use pw\filemanager\models\FileOwner;

class FileBehavior extends Behavior
{


    public $attributes = [];

    protected $_files = [];

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'addOwner',
            ActiveRecord::EVENT_AFTER_UPDATE => 'updateOwner',
            ActiveRecord::EVENT_AFTER_DELETE => 'deleteOwner',
            ActiveRecord::EVENT_AFTER_LOAD =>'afterLoad'
        ];
    }

    public function afterLoad($event){

        $data = $event->load;
        $formName = $this->owner->formName();
        foreach ($this->attributes as $k => $v) {
            if (\is_numeric($k) && isset($data[$formName][$v])) {
                $this->_files[$v] = $data[$formName][$v];
            } elseif (\is_array($v) && isset($v['multiple'],$data[$formName][$k])) {
                $this->_files[$k] = $data[$formName][$k];
            }
        }
    }

    public function addOwner()
    {
       foreach($this->_files as $attribute=>$files){
           $files = (array)$files;
           foreach($files as $file){
                   $owner = new FileOwner();
                   $owner->file_id = $file;
                   $owner->model_attribute = $attribute;
                   $owner->model_id = $this->owner->getPrimaryKey();
                   $owner->save();
           }
       }
    }

    public function updateOwner()
    {
        foreach($this->_files as $attribute=>$files) {
            $files = (array)$files;
            FileOwner::deleteAll(['model_id' => $this->owner->getPrimaryKey(), 'model_attribute' => $attribute]);
            foreach ($files as $file) {
                $owner = new FileOwner();
                $owner->file_id = $file;
                $owner->model_attribute = $attribute;
                $owner->model_id = $this->owner->getPrimaryKey();
                $owner->save();
            }
        }
    }

    public function deleteOwner()
    {
        FileOwner::deleteAll(['model_id'=>$this->owner->getPrimaryKey()]);
    }

    protected function loadFiles()
    {

        if ($this->_files === []) {
            foreach ($this->attributes as $k => $v) {
                if (\is_numeric($k)) {
                    $this->_files[$v] = null;
                } elseif (\is_array($v) && isset($v['multiple'])) {
                    $this->_files[$k] = [];
                }
            }
            $files = FileOwner::find()->with('file')->where(['model_id' => $this->owner->getPrimaryKey()])->all();
            foreach ($files as $fileowner) {
                if (isset($this->attributes[$fileowner->model_attribute]['multiple'])) {
                    $this->_files[$fileowner->model_attribute][] = $fileowner->file;
                } else {
                    $this->_files[$fileowner->model_attribute] = $fileowner->file;
                }
            }
            foreach ($this->attributes as $k => $v) {
                if (\is_numeric($k) && empty($this->_files[$v])) {
                    $this->_files[$v] = new File();
                } elseif (\is_array($v) && isset($v['multiple']) && empty($this->_files[$k])) {
                    $this->_files[$k] = [new File()];
                }
            }
        }

        return $this->_files;
    }

    /**
     * @inheritdoc
     */
    public function canGetProperty($name, $checkVars = true):bool
    {
        return isset($this->attributes[$name]) || \in_array($name, $this->attributes) ?: parent::canGetProperty($name, $checkVars);
    }


    /**
     * @inheritdoc
     */
    public function canSetProperty($name, $checkVars = true)
    {

        return isset($this->attributes[$name]) || \in_array($name, $this->attributes)  ?: parent::canSetProperty($name, $checkVars);
    }

    /**
     * @inheritdoc
     */
    public function __get($name)
    {
        return $this->loadFiles()[$name];
    }

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidParamException
     */
    public function __set($name, $value)
    {
        if(isset($this->attributes[$name]['multiple'])){
            $this->_files[$name][] = $value;
        }
        else {
            $this->_files[$name] = $value;
        }
    }
}