<?php

namespace pw\filemanager\commands;

use function GuzzleHttp\Psr7\mimetype_from_filename;
use pw\console\Controller;
use pw\filemanager\models\File;
use pw\filemanager\models\Store;
use pw\reference\models\Reference;
use pw\reference\models\ReferenceValue;
use yii\db\Expression;

class DefaultController extends Controller
{
    private $no_image_name = 'no_image.jpg';

   public function init()
   {
       $this->no_image_name = File::NO_IMAGE;
       parent::init(); // TODO: Change the autogenerated stub
   }

    public function actionAddNoImage()
    {

        $store = Store::find()->one();//Получаем хранилище файлов

        $storePath = \Yii::getAlias(json_decode($store->params)->path);//Путь к хранилищу

        $imagePath = $storePath . '/' . $this->no_image_name;//Путь к картинке
        if (file_exists($imagePath)) {

            $tmp = explode('/', $imagePath);//Получаем имя картинки

            $filename = array_pop($tmp);//Имя картинки


            $mimeType = mimetype_from_filename($imagePath);

            $fileData = [];//Данные о файле
            $fileData['relativePath'] = $this->no_image_name;
            $fileData['storeId'] = $store->id;
            $fileData['filename'] = $filename;
            $fileData['mimeType'] = $mimeType;
            $fileData['fileSize'] = filesize($imagePath);

            $model = new File();
            $model->store_id = $fileData['storeId'];
            $model->filename = $fileData['filename'];
            $model->path = $fileData['relativePath'];
            $model->type = $fileData['mimeType'];
            $model->size = $fileData['fileSize'];
            $model->created_time = new Expression('NOW()');
            $model->save();
        }
    }
}