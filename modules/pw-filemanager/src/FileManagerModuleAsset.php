<?php

namespace pw\filemanager;

use pw\web\AssetBundle;


class FileManagerModuleAsset extends AssetBundle
{
    public $sourcePath = '@pw-filemanager/assets';

    public $css = [
        'css/filemanager.module.css',
    ];

    public $js = [
        'js/cropper.min.js',
        'js/filemanager.module.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}