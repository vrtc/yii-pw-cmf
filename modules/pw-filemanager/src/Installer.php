<?php

namespace pw\filemanager;

use pw\filemanager\models\Store;
use pw\rbac\rules\OwnerRule;
use Yii;

class Installer extends \pw\modules\ModuleInstaller
{


    public function getVersion(): string
    {
        return '0.1';
    }

    public function getAuthors(): array
    {
        return ['i@yar.pw'];
    }

    public function getLink(): string
    {
        return 'http://yar.pw';
    }

    public function getName(): string
    {
        return 'Filemanager';
    }

    public function getSourceLanguage(): string
    {
        return 'ru-RU';
    }



    public function afterInstall()
    {
        $store          = new Store();
        $store->name    = 'Локальное хранилище';
        $store->adapter = 'creocoder\flysystem\LocalFilesystem';
        $store->params  = ['path' => '@webroot/files'];
        $store->slug     = '/files';
        if (!$store->save()) {
            \var_dump($store->getErrors());
            exit;
        }
    }

    public function getUrlRules(): array
    {

    }
}
