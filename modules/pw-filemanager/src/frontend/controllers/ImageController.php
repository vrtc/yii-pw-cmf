<?php

namespace pw\filemanager\frontend\controllers;

use pw\filemanager\models\File;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\Response;
use Intervention\Image\ImageManagerStatic as Image;

class ImageController extends \pw\web\Controller
{
    protected $newExt = 'webp';

    public function __construct($id, $module, $config = [])
    {
        $os = get_operating_system();
        if(\Yii::$app->mobileDetect->isiOS() || $os == 'Mac' || $os == 'IPhone' || $os == 'IPod' || $os == 'IPad'){
            $this->newExt = null;
        }else{
            $this->newExt = 'webp';
        }
        parent::__construct($id, $module, $config);
    }

    public function actionFill($width = '', $height = '', $file = '', $color = '#FFFFFF', $quality = null, $ext = 'webp')
    {
        if(!is_null($this->newExt)) {
            $file = $this->replace_extension($file, $ext, true);
        }

        if (!file_exists(Yii::getAlias('@webroot') . '/files/' . $file)) {
            return $this->emptyGIF($width, $height, File::THUMBNAIL_FILL);
        }


        try {
            if ($width || $height) {
                $fileModel = $this->findModel($file);
                if ($fileModel) {
                    $savePath = Yii::getAlias($this->module->thumbsDir) . "/fill/$ext/{$width}x$height/";

                    if (FileHelper::createDirectory($savePath . \pathinfo($file, \PATHINFO_DIRNAME))) {

                        $background = Image::canvas($width, $height);
                        $background->fill($color);
                        $image = Image::make($fileModel->readStream())->resize($width, $height, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });

                        if(!is_null($this->newExt)) {
                            $image->encode('webp');
                            $file = $this->replace_extension($file, 'webp', true);
                        }
                        $background->insert($image, 'center');

                        $background->save("$savePath/$file", $quality ? $quality : $this->module->imageQuality, $this->newExt);
                        //  $image->save("$savePath/$file", $this->module->imageQuality);
                        $file = FileHelper::getMimeType("$savePath/$file");
                        Yii::$app->response->format = Response::FORMAT_RAW;
                        Yii::$app->response->headers->set('Content-Type', $file);
                        return $background;

                    }
                }
            }
        } catch (\Exception $e) {
            Yii::error($e, 'image_error');
        }

        return $this->emptyGIF($width, $height, File::THUMBNAIL_FILL);
    }

    protected function replace_extension($filename, $new_extension, $inDir = false) {
        $info = pathinfo($filename);

        if($inDir){
            $dirname = $info['dirname'] == '.' ? '' : $info['dirname'] . '/';
            return $dirname  . $info['filename'] . '.' . $new_extension;
        }else {
            return $info['filename'] . '.' . $new_extension;
        }
    }

    public function actionResize($width = '', $height = '', $file = '', $ext = 'webp')
    {

        if(!is_null($this->newExt)) {
            $file = $this->replace_extension($file, $ext, true);
        }

        if (!file_exists(Yii::getAlias('@webroot') . '/files/' . $file)) {
            return $this->emptyGIF($width, $height, File::THUMBNAIL_RESIZE);
        }
        try {
            if ($width || $height) {
                $fileModel = $this->findModel($file);
                if ($fileModel) {
                    $savePath = Yii::getAlias($this->module->thumbsDir) . "/resize/$ext/{$width}x$height/";
                    if (FileHelper::createDirectory($savePath . \pathinfo($file, \PATHINFO_DIRNAME))) {
                        $image = Image::make($fileModel->readStream());
                        $image->resize($width, $height, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });
                        if(!is_null($this->newExt)) {
                            $image->encode('webp');
                            $file = $this->replace_extension($file, 'webp', true);
                        }
                        $image->save("$savePath/$file", $this->module->imageQuality, $this->newExt);
                        $file = FileHelper::getMimeType("$savePath/$file");
                        Yii::$app->response->format = Response::FORMAT_RAW;
                        Yii::$app->response->headers->set('Content-Type', $file);

                        return $image;
                    }
                }
            }
        } catch (\Exception $e) {
            Yii::error($e, 'image_error');
        }
        return $this->emptyGIF($width, $height, File::THUMBNAIL_RESIZE);
    }

    public function actionCrop($width = '', $height = '', $file = '', $ext = 'webp')
    {
        if(!is_null($this->newExt)) {
            $file = $this->replace_extension($file, $ext, true);
        }

        if (!file_exists(Yii::getAlias('@webroot') . '/files/' . $file)) {
            return $this->emptyGIF($width, $height, File::THUMBNAIL_CROP);
        }
        try {
            if ($width || $height) {
                $fileModel = $this->findModel($file);
                if ($fileModel) {
                    $savePath = Yii::getAlias($this->module->thumbsDir) . "/crop/$ext/{$width}x$height/";
                    if (FileHelper::createDirectory($savePath . \pathinfo($file, \PATHINFO_DIRNAME))) {
                        $image = Image::make($fileModel->readStream());
                        $image->fit($width, $height);
                        if(!is_null($this->newExt)) {
                            $image->encode('webp');
                            $file = $this->replace_extension($file, 'webp', true);
                        }
                        $image->save("$savePath/$file", $this->module->imageQuality, $this->newExt);
                        $file = FileHelper::getMimeType("$savePath/$file");
                        Yii::$app->response->format = Response::FORMAT_RAW;
                        Yii::$app->response->headers->set('Content-Type', $file);

                        return $image;
                    }
                }
            }
        } catch (\Exception $e) {
            Yii::error($e, 'image_error');
        }
        return $this->emptyGIF($width, $height, File::THUMBNAIL_CROP);
    }

    protected function findModel($file)
    {
        return File::find()->where(['path' => $file])->one();
    }

    /**
     * @return string
     */
    protected function emptyGIF($width = 80, $height = 80, $method = File::THUMBNAIL_RESIZE)
    {
        /*        $file = File::findOne(['filename' => File::NO_IMAGE]);*/
        return $this->actionFill($width, $height, File::NO_IMAGE);
//        \Yii::$app->response->format = Response::FORMAT_RAW;
//        \Yii::$app->response->headers->add('content-type','image/png');
//        \Yii::$app->response->data = file_get_contents($this->module->basePath.'/assets/img/img_no-image.png');
//        return \Yii::$app->response->data;
    }
}
