<?php

namespace pw\filemanager;

use pw\web\AssetBundle;


class FileManagerAsset extends AssetBundle
{
    public $sourcePath = '@pw/filemanager/assets';

    public $css = [
        'css/filemanager.input.css',
    ];

    public $js = [
        'js/filemanager.input.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}