<?php
/* @var $model \pw\filemanager\models\File */

use yii\helpers\Html;
use yii\helpers\StringHelper; ?>

<div class="thumbnail col-md-3">
    <?= $model->getThumbnail(110, 110,\pw\filemanager\models\File::THUMBNAIL_CROP) ?>
    <div class="filename" title="<?= Html::encode($model->filename) ?>">
    </div>
</div>
