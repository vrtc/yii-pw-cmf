<?php
/** @var $this \pw\web\View */
/** @var $model \pw\filemanager\models\File */

/** @var $dataProvider \yii\data\ActiveDataProvider */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use kartik\file\FileInput;

?>
<div id="module-filemanager" class="container-fluid <?= $selectType ?>">
    <ul class="nav navbar-nav navbar-right">
        <li class="col-options">
            <?= FileInput::widget([
                'name' => 'File[]',
                'id' => 'filemanager-files',
                'options' => [
                    'multiple' => true
                ],
                'pluginOptions' => [
                    'uploadUrl' => Url::to(['manage/upload']),
                    'uploadAsync' => false,
                    'showPreview' => false,
                    'showRemove' => false,
                    'showUpload' => false,
                    'showCancel' => false,
                    'browseClass' => 'btn btn-primary btn-block',
                    'browseIcon' => '<i class="fa fa-upload"></i> ',
                    'browseLabel' => Yii::t('filemanager', 'Загрузить')
                ],
                'pluginEvents' => [
                    'filebatchselected' => 'function(event, files){ 
                        $(".msg-invalid-file-extension").addClass("hide"); $(this).fileinput("upload");
                    }',
                    'filebatchuploadsuccess' => 'function(event, data, previewId, index) {
						fileManagerModule.uploadSuccess(data.jqXHR.responseJSON.filemanagerFiles);
					}',
                    'fileuploaderror' => 'function(event, data) { 
                        $(".msg-invalid-file-extension").removeClass("hide"); 
                    }',
                ],
            ]) ?>
        </li>
    </ul>
    <?php Pjax::begin([
        'id' => 'pjax-filemanager',
        'timeout' => '5000'
    ]); ?>
    <div class="card">
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemOptions' => ['class' => 'item img-thumbnail'],
            'pager' => [
                'linkContainerOptions' => [
                    'class' => 'page-item'
                ],
                'linkOptions' => [
                    'class' => 'page-link'
                ],
                'disabledListItemSubTagOptions' => [
                    'class' => 'page-link'
                ],
                'firstPageLabel' => 'Первая',
                'lastPageLabel' => 'Последняя',
                'pagination' => $dataProvider->pagination,
                'class' => \yii\widgets\LinkPager::class
            ],
            'layout' => '<div class="item-overview row card-body">{items}</div> <div class="pagination">{pager}</div>',
            'itemView' => function ($model, $key, $index, $widget) {
                return $this->render("_file", ['model' => $model]);
            },
        ]) ?>
    </div>
    <?php Pjax::end(); ?>
    <div class="file-info hide">
        <div class="thumbnail">
            <img src="">
        </div>
        <div class="details">
            <div class="fileName"></div>
            <div class="created"></div>
            <div class="fileSize"></div>
            <div class="row">
                <a href="#" class="btn btn-xs btn-danger delete-file-item"><span class="fa fa-trash"
                                                                                 aria-hidden="true"></span> <?= Yii::t('filemanager', 'Удалить') ?>
                </a>
                <?php if ($viewMode === "iframe"): ?>
                    <a href="#"
                       class="btn btn-primary btn-block pick-file-item"><?= Yii::t('filemanager', 'Выбрать') ?></a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>