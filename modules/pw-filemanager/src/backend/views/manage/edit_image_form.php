<?php
/**
 * Created by trisport_pw.
 * User: Dmitriy Kindeev
 * Email: dkindeev@mail.ru
 * Date: 27.02.2020
 * Time: 16:13
 */
use pw\ui\form\ActiveForm;
use yii\helpers\Url;

?>
<?php $form = ActiveForm::begin([
    'options' => ['enctype' => 'multipart/form-data'],
    'model' => $model,
    'id' => 'image-edits'
]); ?>

<?= $form->field($model, 'alt')->textInput(); ?>
<?= $form->field($model, 'title')->textInput(); ?>

<?php

ActiveForm::end();
$urlEditFile = Url::to(['/pw-filemanager/manage/edit', 'id' => $model->id]);
$js = <<<JS
 $(document).on('beforeSubmit', 'form#image-edits', function(){
     var data = $(this).serialize();
     $.ajax({
         url: '{$urlEditFile}',
         type: 'POST',
         data: data,
         success: function(res){
             $('.alt_{$model->id}').text(res.alt);
             $('.title_{$model->id}').text(res.title);
             $('.swal2-close').click();
         },
         error: function(){
            alert('Error!');
         }
     });
     return false;
     });
JS;
$this->registerJs($js);
