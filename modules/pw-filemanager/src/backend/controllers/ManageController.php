<?php

namespace pw\filemanager\backend\controllers;

use pw\filemanager\FileManagerModuleAsset;
use pw\filemanager\models\File;
use pw\filemanager\models\Store;
use Yii;
use pw\web\Controller;
use pw\filemanager\backend\models\FileSearch;
use yii\db\Expression;
use yii\helpers\Inflector;
use yii\helpers\Url;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class ManageController extends Controller
{

    public function beforeAction($action)
    {
        if ($action->id == 'ckeditor_image_upload') {
            $this->enableCsrfValidation = false;

        }

        return parent::beforeAction($action);
    }

    public function actionIndex()
    {

        $viewMode        = Yii::$app->request->get('view-mode', 'page');
        $selectType      = Yii::$app->request->get('select-type', 'input');
        $inputFieldId    = Yii::$app->request->get('input-id');
        $cropAspectRatio = Yii::$app->request->get('aspect-ratio');
        $cropViewMode    = Yii::$app->request->get('crop-view-mode', 1);
        $defaultFileId  = Yii::$app->request->get('file-id');

        if ($viewMode === 'iframe') {
            $this->layout = 'blank';
        }
        FileManagerModuleAsset::register($this->view);
        $deleteMessage = Json::encode([
            'deleteMessage' => Yii::t('filemanager', 'Are you sure you want to delete this image?'),
        ]);
        $sBaseUrl = Url::to(['/pw-filemanager/manage']);

        $this->view->registerJs("fileManagerModule.baseUrl = '$sBaseUrl';", 3);
        $this->view->registerJs("fileManagerModule.defaultFileId = '$defaultFileId';", 3);
        $this->view->registerJs("fileManagerModule.fieldId = '$inputFieldId';", 3);
        $this->view->registerJs("fileManagerModule.cropRatio = '$cropAspectRatio';", 3);
        $this->view->registerJs("fileManagerModule.cropViewMode = ' $cropViewMode';", 3);
        $this->view->registerJs("fileManagerModule.selectType = '$selectType';", 3);
        $this->view->registerJs("fileManagerModule.message = $deleteMessage;", 3);

        $searchModel  = new FileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'model'        => $searchModel,
            'dataProvider' => $dataProvider,
            'viewMode'     => $viewMode,
            'selectType'   => $selectType,
        ]);
    }

    public function actionEdit($id){
        $model = File::findOne(['id' => $id]);

        if ($model->load(Yii::$app->request->post()) /*&& $model->validate()*/) {

            if($model->save(false) && \Yii::$app->request->isAjax){
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return [
                    'alt' => $model->alt,
                    'title' => $model->title
                ];

            }

        }

        return $this->renderAjax('edit_image_form', ['model' => $model]);
    }

    public function actionUpload($name = 'File', $result = false)
    {

        $files = UploadedFile::getInstancesByName($name);

        $store = Store::find()->one();
        foreach ($files as $k => $file) {
            if (!$file->hasError) {
                $dir    = chunk_split(substr(\md5($file->name), 0, 4), 2, '/');
                $path   = $dir . Inflector::slug($file->baseName) . '.' . $file->extension;
                $stream = fopen($file->tempName, 'r+');
                if ($store->putStream($path, $stream)) {
                    $model               = new File();
                    $model->store_id     = $store->id;
                    $model->user_id      = Yii::$app->user->id;
                    $model->filename     = $file->name;
                    $model->path         = $path;
                    $model->type         = $file->type;
                    $model->size         = $file->size;
                    $model->created_time = new Expression('NOW()');
                    $model->save();
                }
                fclose($stream);
            }
        }

        if($result){
            return ['store' => $store, 'file' => $model];
        }

        return $this->asJson($files);
    }

    public function actionCkeditor_image_upload()
    {
        $funcNum = $_REQUEST['CKEditorFuncNum'];

        if($_FILES['upload']) {

            $file = $this->actionUpload('upload', true);

            $url = Yii::$app->urlManager->createUrl([$file['store']->slug.'/'.$file['file']->path, 'toFrontend' => true]);

            $message = 'Загружено';

            echo '<script type="text/javascript">window.parent.CKEDITOR.tools.callFunction("'.$funcNum.'", "'.$url.'", "'.$message.'" );</script>'; exit;

        }
    }

    /**
     * Crop image and create new ImageManager model.
     * @return mixed
     */
    public function actionCrop()
    {
        //return
        $return = null;
        //disable Csrf
        Yii::$app->controller->enableCsrfValidation = false;
        //set response header
        Yii::$app->getResponse()->format = Response::FORMAT_JSON;
        //set media path
        $sMediaPath = \Yii::$app->imagemanager->mediaPath;
        //get post
        $ImageManager_id = Yii::$app->request->post("ImageManager_id");
        $aCropData       = Yii::$app->request->post("CropData");
        //get details
        $modelOriginal = $this->findModel($ImageManager_id);
        //check if path is not null
        if ($modelOriginal->imagePathPrivate !== null && $aCropData !== null) {
            //dimension
            $iDimensionWidth  = round($aCropData['width']);
            $iDimensionHeight = round($aCropData['height']);
            //collect variables
            $sFileNameReplace = preg_replace("/_crop_\d+x\d+/", "", $modelOriginal->fileName);
            $sFileName        = pathinfo($sFileNameReplace, PATHINFO_FILENAME);
            $sFileExtension   = pathinfo($sFileNameReplace, PATHINFO_EXTENSION);
            $sDisplayFileName = $sFileName . "_crop_" . $iDimensionWidth . "x" . $iDimensionHeight . "." . $sFileExtension;

            //start transaction
            $transaction  = Yii::$app->db->beginTransaction();
            $bCropSuccess = false;

            //create a file record
            $model           = new ImageManager();
            $model->fileName = $sDisplayFileName;
            $model->fileHash = Yii::$app->getSecurity()->generateRandomString(32);
            //if file is saved add record
            if ($model->save()) {

                //do crop in try catch
                try {
                    // create file name
                    $sSaveFileName = $model->id . "_" . $model->fileHash . "." . $sFileExtension;

                    // get current/original image data
                    $imageOriginal          = Image::getImagine()->open($modelOriginal->imagePathPrivate);
                    $imageOriginalSize      = $imageOriginal->getSize();
                    $imageOriginalWidth     = $imageOriginalSize->getWidth();
                    $imageOriginalHeight    = $imageOriginalSize->getHeight();
                    $imageOriginalPositionX = 0;
                    $imageOriginalPositionY = 0;

                    // create/calculate a canvas size (if canvas is out of the box)
                    $imageCanvasWidth  = $imageOriginalWidth;
                    $imageCanvasHeight = $imageOriginalHeight;

                    // update canvas width if X position of croparea is lower than 0
                    if ($aCropData['x'] < 0) {
                        //set x postion to Absolute value
                        $iAbsoluteXpos = abs($aCropData['x']);
                        //set x position of image
                        $imageOriginalPositionX = $iAbsoluteXpos;
                        //add x position to canvas size
                        $imageCanvasWidth += $iAbsoluteXpos;
                        //update canvas width if croparea is biger than original image
                        $iCropWidthWithoutAbsoluteXpos = ($aCropData['width'] - $iAbsoluteXpos);
                        if ($iCropWidthWithoutAbsoluteXpos > $imageOriginalWidth) {
                            //add ouside the box width
                            $imageCanvasWidth += ($iCropWidthWithoutAbsoluteXpos - $imageOriginalWidth);
                        }
                    } else {
                        // add if crop partly ouside image
                        $iCropWidthWithXpos = ($aCropData['width'] + $aCropData['x']);
                        if ($iCropWidthWithXpos > $imageOriginalWidth) {
                            //add ouside the box width
                            $imageCanvasWidth += ($iCropWidthWithXpos - $imageOriginalWidth);
                        }
                    }

                    // update canvas height if Y position of croparea is lower than 0
                    if ($aCropData['y'] < 0) {
                        //set y postion to Absolute value
                        $iAbsoluteYpos = abs($aCropData['y']);
                        //set y position of image
                        $imageOriginalPositionY = $iAbsoluteYpos;
                        //add y position to canvas size
                        $imageCanvasHeight += $iAbsoluteYpos;
                        //update canvas height if croparea is biger than original image
                        $iCropHeightWithoutAbsoluteYpos = ($aCropData['height'] - $iAbsoluteYpos);
                        if ($iCropHeightWithoutAbsoluteYpos > $imageOriginalHeight) {
                            //add ouside the box height
                            $imageCanvasHeight += ($iCropHeightWithoutAbsoluteYpos - $imageOriginalHeight);
                        }
                    } else {
                        // add if crop partly ouside image
                        $iCropHeightWithYpos = ($aCropData['height'] + $aCropData['y']);
                        if ($iCropHeightWithYpos > $imageOriginalHeight) {
                            //add ouside the box height
                            $imageCanvasHeight += ($iCropHeightWithYpos - $imageOriginalHeight);
                        }
                    }

                    // round values
                    $imageCanvasWidthRounded       = round($imageCanvasWidth);
                    $imageCanvasHeightRounded      = round($imageCanvasHeight);
                    $imageOriginalPositionXRounded = round($imageOriginalPositionX);
                    $imageOriginalPositionYRounded = round($imageOriginalPositionY);
                    $imageCropWidthRounded         = round($aCropData['width']);
                    $imageCropHeightRounded        = round($aCropData['height']);
                    // set postion to 0 if x or y is less than 0
                    $imageCropPositionXRounded = $aCropData['x'] < 0 ? 0 : round($aCropData['x']);
                    $imageCropPositionYRounded = $aCropData['y'] < 0 ? 0 : round($aCropData['y']);

//                    echo "canvas: ". $imageCanvasWidth ." x ".$imageCanvasHeight ."<br />";
//                    echo "img pos x: ". $imageOriginalPositionX ." y ".$imageOriginalPositionY ."<br />";
//                    die();
//
                    //todo: check if rotaded resize canvas (http://stackoverflow.com/questions/9971230/calculate-rotated-rectangle-size-from-known-bounding-box-coordinates)

                    // merge current image in canvas, crop image and save
                    $imagineRgb   = new RGB();
                    $imagineColor = $imagineRgb->color('#FFF', 0);
                    // create image
                    Image::getImagine()->create(new Box($imageCanvasWidthRounded, $imageCanvasHeightRounded),
                        $imagineColor)
                        ->paste($imageOriginal,
                            new Point($imageOriginalPositionXRounded, $imageOriginalPositionYRounded))
                        ->crop(new Point($imageCropPositionXRounded, $imageCropPositionYRounded),
                            new Box($imageCropWidthRounded, $imageCropHeightRounded))
                        ->save($sMediaPath . "/" . $sSaveFileName);

                    //set boolean crop success to true
                    $bCropSuccess = true;

                    //set return id
                    $return = $model->id;

                    // Check if the original image must be delete
                    if ($this->module->deleteOriginalAfterEdit) {
                        $modelOriginal->delete();
                    }
                } catch (ErrorException $e) {

                }
            }

            //commit transaction if boolean is true
            if ($bCropSuccess) {
                $transaction->commit();
            }
        }

        //echo return json encoded
        return $return;
    }


    public function actionView()
    {
        $result = [];
        $file   = File::findOne(['id' => Yii::$app->request->post('id')]);
        if ($file) {
            $result = [
                'id'        => $file->id,
                'fileName'  => $file->filename,
                'created'   => Yii::$app->formatter->asDate($file->created_time),
                'fileSize'  => Yii::$app->formatter->asShortSize($file->size),
                'thumbnail' => $file->getThumbnail(400, 400),
                'thumbnailUrl' => $file->getThumbnailUrl(400, 400)
            ];
        }

        return $this->asJson($result);
    }

    /**
     * Get full image
     * @return mixed
     */
    public function actionGetOriginalImage()
    {
        //disable Csrf
        Yii::$app->controller->enableCsrfValidation = false;
        //set response header
        Yii::$app->getResponse()->format = Response::FORMAT_JSON;
        //get post
        $ImageManager_id = Yii::$app->request->post("ImageManager_id");
        //get details
        $model = $this->findModel($ImageManager_id);
        //set return
        $return = \Yii::$app->imagemanager->getImagePath($model->id, $model->imageDetails['width'],
            $model->imageDetails['height'], "inset");

        //return json encoded
        return $return;
    }

    /**
     * Deletes an existing ImageManager model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionDelete()
    {
        $result = ['delete' => false];
        $model  = $this->findModel(Yii::$app->request->post('id'));
        if ($model->delete()) {
            $result['delete'] = true;
        }

        return $this->asJson($result);
    }

    protected function findModel($id)
    {
        $model = File::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException('File not found');
        }

        return $model;
    }
}
