<?php

namespace pw\filemanager\models;

use Embed\Embed;
use pw\i18n\interfaces\Translateable;
use Yii;
use yii\base\InvalidCallException;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "{{%pw_filemanager_files}}".
 *
 * @property integer $id
 * @property integer $store_id
 * @property integer $user_id
 * @property string $filename
 * @property string $path
 * @property string $type
 * @property string $size
 * @property string $description
 * @property string $created_time
 * @property string $updated_time
 * @property string $hash_file
 */
class File extends \pw\i18n\db\ActiveRecord
{
    const THUMBNAIL_CROP = 'crop';
    const THUMBNAIL_FILL = 'fill';
    const THUMBNAIL_RESIZE = 'resize';
    const NO_IMAGE = 'no_image.jpg';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pw_filemanager_files}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_time', 'updated_time'], 'safe'],
            [['size'], 'integer'],
            [['filename', 'path', 'type', 'description', 'hash_file'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('store', 'ID'),
            'store_id' => Yii::t('store', 'Store ID'),
            'filename' => Yii::t('store', 'Filename'),
            'path' => Yii::t('store', 'Path'),
            'type' => Yii::t('store', 'Type'),
            'description' => Yii::t('store', 'Описание'),
            'size' => Yii::t('store', 'Size'),
            'created_time' => Yii::t('store', 'Created Time'),
            'updated_time' => Yii::t('store', 'Updated Time'),
        ];
    }

    public function getOwners()
    {
        return $this->hasMany(FileOwner::class, ['file_id' => 'id']);
    }

    public function getStore()
    {
        return $this->hasOne(Store::class, ['id' => 'store_id']);
    }

    public function __toString()
    {
        return $this->path;
    }

    public static function getNoImage($width = 80, $height = 80, $method = self::THUMBNAIL_RESIZE, $options = [])
    {
        return File::findOne(['filename' => File::NO_IMAGE])->getThumbnail($width, $height, $method, $options);
    }


    public function getPath()
    {
        return $this->path;
    }


    public function getOriginalUrl()
    {
        if ($this->path && $this->store) {
            return "{$this->store->slug}/{$this->path}";
        }
    }

    public function getThumbnailUrl($width = 80, $height = 80, $method = self::THUMBNAIL_RESIZE)
    {
        return \Yii::$app->image->getThumbnailUrl($this->path, $width, $height, $method);
    }


    public function getThumbnail($width = 80, $height = 80, $method = self::THUMBNAIL_RESIZE, $options = [])
    {
        $type = \explode('/', $this->type);
        if ($type[0] === 'image') {
            $url = $this->getThumbnailUrl($width, $height, $method);
            $options = ArrayHelper::merge(['alt' => $this->alt, 'title' => $this->title], $options);

            return Html::img($url, $options);
        }
        if ($this->type === 'video') {
            if (strpos($this->path, 'http') === 0) {
                $code = Yii::$app->cache->get($this->id);
                if (!$code) {
                    $source = Embed::create($this->path);
                    if ($source) {
                        $code = ['code' => $source->getCode(), 'thumb' => $source->getImage()];
                        Yii::$app->cache->set($this->id, $code);
                    }
                }

                return $code['thumb'];
            }
            if ($this->path[0] === '<') {
                return $this->path;
            }
        }
    }


    public function delete()
    {
        //$this->store->getFs()->delete($this->path);
        return parent::delete();
    }

    public function read()
    {
        return $this->store->read($this->path);
    }

    public function readStream()
    {
        return $this->store->readStream($this->path);
    }
}
