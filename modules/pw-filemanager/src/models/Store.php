<?php

namespace pw\filemanager\models;

use paulzi\jsonBehavior\JsonBehavior;
use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "{{%pw_filemanager_store}}".
 *
 * @property Uuid   $id
 * @property string $name
 * @property string $slug
 * @property string $adapter
 * @property string $params
 */
class Store extends \pw\core\db\ActiveRecord
{
    /**
     * @var \creocoder\flysystem\Filesystem[]
     */
    protected static $_filesystems = [];


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pw_filemanager_store}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug', 'adapter'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'      => Yii::t('store', 'ID'),
            'name'    => Yii::t('store', 'Name'),
            'slug'     => Yii::t('store', 'Url'),
            'adapter' => Yii::t('store', 'Adapter'),
            'params'  => Yii::t('store', 'Params'),
        ];
    }


    public function behaviors()
    {
        return [
            [
                'class'      => JsonBehavior::class,
                'attributes' => ['params'],
            ],
        ];
    }

    public function getFs()
    {
        return self::getFilesystem($this);
    }

    protected static function getFilesystem($store)
    {
        if (!isset(self::$_filesystems[$store->id])) {
            $object = \array_merge(['class' => $store->adapter], $store->params->toArray());
            if (isset($object['path'])) {
                $object['path'] = Yii::getAlias($object['path']);
            }
            self::$_filesystems[$store->id] = Yii::createObject($object);
        }

        return self::$_filesystems[$store->id];
    }

    public function read(string $path)
    {
        return $this->getFs()->read($path);
    }

    public function readStream(string $path)
    {
        return $this->getFs()->readStream($path);
    }

    public function write(string $path, string $contents, array $config = [])
    {
        return $this->getFs()->write($path, $contents, $config);
    }

    public function writeStream(string $path, $resource, array $config = [])
    {
        return $this->getFs()->writeStream($path, $resource, $config);
    }
    public function put(string $path, string $contents, array $config = [])
    {
        return $this->getFs()->put($path, $contents, $config);
    }

    public function putStream(string $path, $resource, array $config = [])
    {
        return $this->getFs()->putStream($path, $resource, $config);
    }
}
