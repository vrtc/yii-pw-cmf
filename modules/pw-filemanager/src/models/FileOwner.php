<?php

namespace pw\filemanager\models;

use Yii;

/**
 * This is the model class for table "{{%pw_filemanager_files_owners}}".
 *
 * @property integer $id
 * @property Uuid    $file_id
 * @property Uuid    $model_id
 * @property string  outer_path
 * @property string  $model_attribute
 */
class FileOwner extends \pw\core\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pw_filemanager_files_owners}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['model_attribute'], 'string', 'max' => 255],
        ];
    }

    public function getFile()
    {
        return $this->hasOne(File::class, ['id' => 'file_id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'              => Yii::t('store', 'ID'),
            'file_id'         => Yii::t('store', 'File ID'),
            'model_id'        => Yii::t('store', 'Model ID'),
            'outer_path'      => Yii::t('store', 'Img'),
            'model_attribute' => Yii::t('store', 'Model Attribute'),
        ];
    }
}
