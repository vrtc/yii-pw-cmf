<?php

namespace pw\filemanager\models\i18n;

use Yii;

/**
 * This is the model class for table "{{%pw_filemanager_files_i18n}}".
 *
 * @property Uuid $model_id
 * @property string $language
 * @property string $alt
 * @property string $title
 *
 */
class File extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pw_filemanager_files_i18n}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['model_id', 'language'], 'required'],
            [['language'], 'string', 'max' => 12],
            [['alt', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'model_id' => Yii::t('store', 'Model ID'),
            'language' => Yii::t('store', 'Language'),
            'alt' => Yii::t('store', 'Alt'),
            'title' => Yii::t('store', 'Title'),
        ];
    }
}
