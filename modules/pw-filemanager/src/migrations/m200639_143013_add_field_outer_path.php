<?php
namespace pw\filemanager\migrations;
use pw\core\db\Migration;

class m200639_143013_add_field_outer_path extends Migration
{
    public function up()
    {
        $this->addColumn('{{%pw_filemanager_files_owners}}', 'outer_path', $this->string()->after('model_id'));
    }

    public function down()
    {
        $this->dropColumn('{{%pw_filemanager_files_owners}}', 'outer_path');
        echo "m200309_143033_add_outer_path cannot be reverted.\n";

        return false;
    }

}
