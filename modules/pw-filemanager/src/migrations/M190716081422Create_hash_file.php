<?php

namespace pw\filemanager\migrations;

use pw\core\db\Migration;
use pw\filemanager\models\File;

class M190716081422Create_hash_file extends Migration
{
    public function up()
    {
        $this->addColumn(File::tableName(), 'hash_file', $this->string());
        $this->createIndex('idx_hash_file', File::tableName(), 'hash_file', false);
    }

    public function down()
    {
        echo "M190716081422Create_hash_file cannot be reverted.\n";

        return false;
    }

}
