<?php
namespace pw\filemanager\migrations;
use pw\i18n\db\Migration;
use yii\helpers\Json;

class M170730102730Init extends Migration
{
    public function up()
    {

        $this->createTable('{{%pw_filemanager_store}}', [
            'id'      => $this->bigPrimaryKey()->unsigned(),
            'name'    => $this->string(),
            'slug'     => $this->string(),
            'adapter' => $this->string(),
            'params'  => $this->text(),
        ]);

        $this->createTable('{{%pw_filemanager_files}}', [
            'id'           => $this->bigPrimaryKey()->unsigned(),
            'store_id'     => $this->bigInteger()->unsigned(),
            'user_id'     => $this->bigInteger()->unsigned(),
            'filename'     => $this->string(),
            'description'     => $this->string(),
            'path'         => $this->string(),
            'type'         => $this->string(),
            'size'         => $this->string(),
            'alt'          => $this->string()->i18n(),
            'title'        => $this->string()->i18n(),
            'created_time' => $this->timestamp(),
            'updated_time' => $this->timestamp()
        ]);

        $this->createIndex('idx_store', '{{%pw_filemanager_files}}', 'store_id');
        $this->createIndex('idx_user', '{{%pw_filemanager_files}}', 'user_id');
        $this->createIndex('idx_type', '{{%pw_filemanager_files}}', 'type');
        $this->createIndex('idx_created', '{{%pw_filemanager_files}}', 'created_time');

        $this->createTable('{{%pw_filemanager_files_owners}}', [
            'id'              => $this->primaryKey(),
            'file_id'         => $this->bigInteger()->unsigned(),
            'model_id'        => $this->bigInteger()->unsigned(),
            'model_language'  =>$this->string(12),
            'model_attribute' => $this->string()
        ]);

        $this->createIndex('idx_file', '{{%pw_filemanager_files_owners}}', 'file_id');
        $this->createIndex('idx_model', '{{%pw_filemanager_files_owners}}', 'model_id');
    }

    public function down()
    {
        echo "M170730102730Init cannot be reverted.\n";

        return false;
    }

}