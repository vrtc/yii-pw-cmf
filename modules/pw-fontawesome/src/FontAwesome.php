<?php
namespace pw\fontawesome;

use yii\web\AssetBundle;

class FontAwesome extends AssetBundle{

    public $sourcePath = '@vendor/fortawesome/font-awesome';

    public $css = [
        'css/all.css',
    ];
}