<?php
namespace pw\fontawesome;

use pw\installer\WebApplication;
use yii\base\Application;
use yii\base\BootstrapInterface;

class Module extends \pw\core\Module implements BootstrapInterface
{

    public function handlers(Application $app): array
    {
        return [
            [
                Application::class,
                Application::EVENT_BEFORE_REQUEST,
                function ($event) use ($app) {
                    $event->sender->icons->registerSet(\pw\fontawesome\Icons::class);
                }
            ],
        ];
    }
}
