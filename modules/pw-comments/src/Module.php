<?php

namespace pw\comments;

use pw\comments\models\Comments;
use pw\users\models\Users;
use Yii;
use yii\base\BootstrapInterface;
use yii\base\Application;

/**
 * Class Comments module definition class
 * @package pw\comments
 */
class Module extends \pw\core\Module implements BootstrapInterface
{
    /**
     * User model class name
     * @var
     */
    public $userModel;

    /**
     * @var string
     */
    public $commentModelClass = 'pw\comments\models\Comments';

    /**
     * @var string
     */
    public $commentRatingModelClass = 'pw\comments\models\CommentsRating';

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'pw\comments\controllers';

    /**
     * Sessions key required for saving guest username
     * @var string
     */
    public $guestUsernameSessionKey = '_username';

    /**
     * Sessions key required for saving guest email
     * @var string
     */
    public $guestEmailSessionKey = '_email';

    /**
     * Cookie key required for saving guest username
     * @var string
     */
    public $guestUsernameCookieName = '_username';

    /**
     * Cookie key required for saving guest email
     * @var string
     */
    public $guestEmailCookieName = '_email';

    /**
     * Cookie key required for saving guest username
     * @var string
     */
    public $upRatedCookieName = '_uprated';

    /**
     * Cookie key required for saving guest email
     * @var string
     */
    public $downRatedCookieName = '_downrated';

    /**
     * Number of seconds how long the information should be stored in cookie, default 360 days.
     * @var int
     */
    public $ratingCookieDuration = 31104000;

    /**
     * Number of seconds how long the information should be stored in cookie, default 30 days.
     * @var int
     */
    public $guestCookieDuration = 2592000;

    /**
     * Sessions key required to invalidate cache
     * @var string
     */
    public $urlCacheSessionKey = '_url';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if ($this->userModel === null) {
            $this->userModel = Users::class;
        }
    }

    /**
     * @param Application $app
     * @return array
     */
    public function routes(Application $app): array
    {
        return [
            [
                'pattern' => '/comment/rate',
                'route' => '/pw-comments/default/rate',
                'internal' => true
            ],
            [
                'pattern' => '/comment/create',
                'route' => '/pw-comments/default/create',
                'model' => Comments::class,
                'internal' => true
            ],
            [
                'pattern' => '/comment/validate',
                'route' => '/pw-comments/default/validate',
                'internal' => true,
                'model' => Comments::class,
            ],
        ];
    }

    /**
     * @return array
     */
    public function getNavigation(): array
    {
        return [
            'content' => [
                [
                    'label' => 'Комментарии',
                    'url' => ['/pw-comments/manage/index'],
                    'icon' => 'comments'
                ]
            ]
        ];
    }
}
