<?php

namespace pw\comments\assets;

/**
 * Class CommentsAsset
 * @package pw\comments\assets
 */
class CommentsAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@pw-comments/assets/sources/';

    public $css = [
        'css/comments.scss',
    ];

    public $js = [
        'js/comments.js',
    ];

}