<?php
namespace pw\comments\migrations;
use pw\core\db\Migration;

/**
 * Class m161126_113528_comments
 */
class m161126_113528_comments extends Migration
{
    public $tableOptions;

    public function safeUp()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%pw_comments}}', [

            'id' => $this->primaryKey(),
            'url' => $this->string(255),
            'model' => $this->string(),
            'model_key' => $this->string(),
            'main_parent_id' => $this->integer(),
            'parent_id' => $this->integer(),
            'email' => $this->string(128),
            'username' => $this->string(128),
            'score' => $this->integer(),
            'content' => $this->text(),
            'language' => $this->string(10),
            'created_by' => $this->bigInteger()->unsigned()->null(),
            'updated_by' => $this->bigInteger()->unsigned()->null(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'ip' => $this->string(46),
            'status' => $this->smallInteger(1)->notNull()->defaultValue(1)->comment('
                0-pending,
                1-published,
                2-spam
            '),

        ], $this->tableOptions);

        $this->createIndex('comments_url1_idx', '{{%pw_comments}}', 'url');
        $this->createIndex('comments_model2_idx', '{{%pw_comments}}', 'model');
        $this->createIndex('comments_model_key3_idx', '{{%pw_comments}}', 'model_key');
    }

    public function down()
    {
        $this->dropTable('{{%pw_comments}}');
    }
}
