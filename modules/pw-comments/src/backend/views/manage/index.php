<?php

use yii\helpers\Html;
use pw\ui\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel pw\comments\models\CommentsSearch */

$this->title = Yii::t('comments', 'Комментарии/отзывы');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comments-index">
  <h1><?= Html::encode($this->title) ?></h1>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'content:ntext',
            [
                'attribute' => 'url',
                'content' => function ($model) {
                    return Html::a(\pw\ui\icons\Icons::show('link'), $model->url, ['target' => '_blank']);
                }
            ],
            'model',
            // 'model_key',
            // 'main_parent_id',
            // 'parent_id',
            'email:email',
            'username',

            // 'language',
            'created_by',
            // 'updated_by',
            'created_at:datetime',
            'updated_at:datetime',
            'ip',
             'status',

            ['class' => \pw\ui\grid\ActionColumn::class],
        ],
    ]); ?>
</div>
