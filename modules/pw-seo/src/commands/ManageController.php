<?php

namespace pw\seo\commands;

use ct\catalog\models\Brand;
use ct\catalog\models\Category;
use ct\catalog\models\Discount;
use ct\catalog\models\product\Product;
use ct\catalog\models\ProductsToWareHouses;
use pw\console\Controller;
use pw\pages\models\Pages;
use pw\seo\models\Seo;
use yii\helpers\Console;
use yii\helpers\FileHelper;
use yii\helpers\VarDumper;

class ManageController extends Controller
{

    /**
     * sitemap.xml
     */
    public function actionGenerateSiteMap()
    {

        if (!is_dir(\Yii::getAlias('@runtime/feed/yml/'))) {
            FileHelper::createDirectory(\Yii::getAlias('@runtime/feed/yml/'));
        }
        $filename = \Yii::getAlias('@runtime/feed/yml/sitemap.xml');
        $xml = new \XMLWriter();
        $xml->openMemory();
        $xml->startDocument('1.0', 'UTF-8');
        $xml->startElement('urlset');
        $xml->writeAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
        file_put_contents($filename, $xml->flush());

        /***************Index Page***************/
        $xml->startElement('url');
        $xml->writeElement('loc', env('BASE_URL'));
        $xml->writeElement('lastmod', date('Y-m-d'));
        $xml->writeElement('changefreq', 'weekly');
        $xml->writeElement('priority', '1.0');
        $xml->endElement();
        file_put_contents($filename, $xml->flush(true), FILE_APPEND);

        /***************Категрии***************/
          $categories = Category::find()
          ->active()
            ->andWhere(['>=', 'depth', 1])
            ->orderBy(['lft' => SORT_ASC]);

        $count = $categories->count();
        $i = 0;
        Console::startProgress($i, $count, 'Категории');
        foreach ($categories->all() as $category) {
            $xml->startElement('url');
            $xml->writeElement('loc',  env('BASE_URL') . \Yii::$app->getUrlManager()->createUrl(['/ct-catalog/default/index', 'slug' => $category->slug]));
            $xml->writeElement('lastmod', date('Y-m-d'));
            $xml->writeElement('changefreq', 'weekly');
            $xml->writeElement('priority', '1.0');
            $xml->endElement();
            file_put_contents($filename, $xml->flush(true), FILE_APPEND);
            $i++;
            Console::updateProgress($i, $count);
        }
        Console::endProgress();

        /**************Товары******************/
        $pModels = Product::find()
            ->active()
            ->inStock()
            ->products()
            ->inCategory()
           // ->inCategories()
            ->inPrice()
            ->andWhere(['IS NOT', 'ct_products.main_photo_id', null])
            ->andWhere(['or',
                ['>=', 'ct_products.quntity', 1],
                ['ct_products.view'=> 'usluga']
            ])
            ->groupBy('ct_products.id');;
        $i = 0;
        $count = $pModels->count();
        Console::startProgress($i, $count, 'Товары');
        foreach ($pModels->all() as $pModel) {
            $date = date_create($pModel->updated_time);
                $xml->startElement('url');
                $xml->writeElement('loc', 'https://tri-sport.ru/product/' . $pModel->slug);
                $xml->writeElement('lastmod', date_format($date, 'Y-m-d'));
                $xml->writeElement('changefreq', 'weekly');
                $xml->writeElement('priority', '1.0');
                $xml->endElement();
                file_put_contents($filename, $xml->flush(true), FILE_APPEND);
                $i++;
            Console::updateProgress($i, $count);
        }
        Console::endProgress();

        /*******************Бренды*******************/
        $modelBrands =  Brand::find()->select(['ct_brands.name', 'ct_products.id as product_id', 'ct_brands.id as brand_id','ct_brands.slug'])->joinWith('productsActive')->active()->groupBy(['ct_brands.id']);

        $i = 0;
        $count = $modelBrands->count();
        Console::startProgress($i, $count, 'Бренды');

        /***************Brands Page***************/
        $xml->startElement('url');
        $xml->writeElement('loc', env('BASE_URL') . \Yii::$app->getUrlManager()->createUrl(['/ct-catalog/default/brands']));
        $xml->writeElement('lastmod', date('Y-m-d'));
        $xml->writeElement('changefreq', 'weekly');
        $xml->writeElement('priority', '1.0');
        $xml->endElement();
        file_put_contents($filename, $xml->flush(true), FILE_APPEND);

        foreach ($modelBrands->all() as $modelBrand) {
            $xml->startElement('url');
            $xml->writeElement('loc', env('BASE_URL') . \Yii::$app->getUrlManager()->createUrl(['/ct-catalog/default/brand', 'brand' => $modelBrand->slug]));
            $xml->writeElement('lastmod', date('Y-m-d'));
            $xml->writeElement('changefreq', 'weekly');
            $xml->writeElement('priority', '1.0');
            $xml->endElement();
            file_put_contents($filename, $xml->flush(true), FILE_APPEND);
            $i++;
            Console::updateProgress($i, $count);
        }
       Console::endProgress();



        /*******************Категории блога*******************/
        $modelPagesCategories = Pages::find()->where(['container' => Pages::CONTAINER_TRUE, 'status' => Pages::STATUS_ENABLED])
        ->andWhere(['NOT IN', 'id', '(259,258)']);

        $i = 0;
        $count = $modelPagesCategories->count();
        Console::startProgress($i, $count, 'Категории блога');
        $ids = [];
        foreach ($modelPagesCategories->all() as $modelPagesContain) {
            $xml->startElement('url');
            $xml->writeElement('loc', env('BASE_URL') . \Yii::$app->getUrlManager()->createUrl(['/pw-pages/blog/article', 'slug' => $modelPagesContain->slug]));
            $xml->writeElement('lastmod', date('Y-m-d'));
            $xml->writeElement('changefreq', 'weekly');
            $xml->writeElement('priority', '1.0');
            $xml->endElement();
            file_put_contents($filename, $xml->flush(true), FILE_APPEND);
            $i++;
            Console::updateProgress($i, $count);
            $ids[] = $modelPagesContain->id;
        }
        Console::endProgress();

        /*******************Страницы Блога*******************/
        $modelPagesCategories = Pages::find()->where(['container' => Pages::CONTAINER_FALSE, 'status' => Pages::STATUS_ENABLED, 'parent_id' => $ids]);

        $i = 0;
        $count = $modelPagesCategories->count();
        Console::startProgress($i, $count, 'Страницы блога');
        foreach ($modelPagesCategories->all() as $modelPagesContain) {
            $xml->startElement('url');
            $xml->writeElement('loc', env('BASE_URL') . \Yii::$app->getUrlManager()->createUrl(['/pw-pages/blog/article', 'slug' => $modelPagesContain->slug]));
            $xml->writeElement('lastmod', date('Y-m-d'));
            $xml->writeElement('changefreq', 'weekly');
            $xml->writeElement('priority', '1.0');
            $xml->endElement();
            file_put_contents($filename, $xml->flush(true), FILE_APPEND);
            $i++;
            Console::updateProgress($i, $count);
        }
        Console::endProgress();

        /*******************Статичные страницы*******************/
        $modelPagesCategories = Pages::find()->where(['container' => Pages::CONTAINER_FALSE, 'status' => Pages::STATUS_ENABLED, 'parent_id' => 259]);

        $i = 0;
        $count = $modelPagesCategories->count();
        Console::startProgress($i, $count, 'Статичные страницы');
        foreach ($modelPagesCategories->all() as $modelPagesContain) {
            if($modelPagesContain->slug == 'voucher') continue;
            $xml->startElement('url');
            $xml->writeElement('loc', env('BASE_URL') . \Yii::$app->getUrlManager()->createUrl(['/pw-pages/default/system', 'slug' => $modelPagesContain->slug]));
            $xml->writeElement('lastmod', date('Y-m-d'));
            $xml->writeElement('changefreq', 'weekly');
            $xml->writeElement('priority', '1.0');
            $xml->endElement();
            file_put_contents($filename, $xml->flush(true), FILE_APPEND);
            $i++;
            Console::updateProgress($i, $count);
        }

        foreach (self::routePages() as $route) {
            $xml->startElement('url');
            $xml->writeElement('loc', env('BASE_URL') . \Yii::$app->getUrlManager()->createUrl([$route]));
            $xml->writeElement('lastmod', date('Y-m-d'));
            $xml->writeElement('changefreq', 'weekly');
            $xml->writeElement('priority', '1.0');
            $xml->endElement();
            file_put_contents($filename, $xml->flush(true), FILE_APPEND);
        }

        Console::endProgress();

        /*******************Sales страницы*******************/
        $percents = $this->getPercents();
        $i = 0;
        $count = count($percents);
        Console::startProgress($i, $count, 'Sales страницы');
        foreach ($percents as $percent) {
            $xml->startElement('url');
            $xml->writeElement('loc', env('BASE_URL') . \Yii::$app->getUrlManager()->createUrl(['/ct-catalog/default/sales', 'percent' => $percent]));
            $xml->writeElement('lastmod', date('Y-m-d'));
            $xml->writeElement('changefreq', 'weekly');
            $xml->writeElement('priority', '1.0');
            $xml->endElement();
            file_put_contents($filename, $xml->flush(true), FILE_APPEND);
            $i++;
            Console::updateProgress($i, $count);
        }
        Console::endProgress();

        /*******************Дополнительные страницы*******************/
        $urls = self::addUrl();
        $i = 0;
        $count = count($urls);
        Console::startProgress($i, $count, 'Дополнительные страницы');
        foreach ($urls as $url) {
            $xml->startElement('url');
            $xml->writeElement('loc', env('BASE_URL') . $url);
            $xml->writeElement('lastmod', date('Y-m-d'));
            $xml->writeElement('changefreq', 'weekly');
            $xml->writeElement('priority', '1.0');
            $xml->endElement();
            file_put_contents($filename, $xml->flush(true), FILE_APPEND);
            $i++;
            Console::updateProgress($i, $count);
        }
        Console::endProgress();

        $xml->endElement();
        file_put_contents($filename, $xml->flush(true), FILE_APPEND);

    }

    public function actionClearSeo()
    {
        $models = Seo::find();
        $count = Seo::find()->count();
        $i = 0;
        Console::output('Очистка сео');
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            foreach ($models->each() as $model) {
                /**
                 * @var $model Seo
                 */
                if(!$class = $model->model_class::findOne(['id' => $model->model_id])){
                    $model->delete();
                    $i++;
                    Console::updateProgress($i, $count, 'Удалено');
                }
            }
            $transaction->commit();
        }catch (\Exception $e){
            $transaction->rollBack();
            throw $e;
        }
    }

    private static function addUrl() {
        return [
            '/catalog/beg/aksessuary-dlya-bega/page:2',
            '/catalog/beg/obuv-dlya-bega/dlya-triatlona/page:2',
            '/catalog/beg/obuv-dlya-bega/page:2',
            '/catalog/beg/obuv-dlya-bega/page:3',
            '/catalog/beg/obuv-dlya-bega/page:4',
            '/catalog/beg/obuv-dlya-bega/page:5',
            '/catalog/beg/obuv-dlya-bega/po-asfaltu/page:2',
            '/catalog/beg/obuv-dlya-bega/po-asfaltu/page:3',
            '/catalog/beg/obuv-dlya-bega/po-asfaltu/page:4',
            '/catalog/beg/obuv-dlya-bega/po-asfaltu/trenirovochnye/page:2',
            '/catalog/beg/obuv-dlya-bega/po-asfaltu/trenirovochnye/page:3',
            '/catalog/beg/obuv-dlya-bega/po-asfaltu/trenirovochnye/page:4',
            '/catalog/beg/odezhda-dlya-bega/futbolki-i-mayki-1/page:2',
            '/catalog/beg/odezhda-dlya-bega/futbolki-i-mayki-1/page:3',
            '/catalog/beg/odezhda-dlya-bega/kompressionnaya-odezhda/futbolki-i-kofty/page:2',
            '/catalog/beg/odezhda-dlya-bega/kompressionnaya-odezhda/getry/page:2',
            '/catalog/beg/odezhda-dlya-bega/kompressionnaya-odezhda/golfy/page:2',
            '/catalog/beg/odezhda-dlya-bega/kompressionnaya-odezhda/golfy/page:3',
            '/catalog/beg/odezhda-dlya-bega/kompressionnaya-odezhda/kompressionnaya-odezhda-dlya-bega/page:2',
            '/catalog/beg/odezhda-dlya-bega/kompressionnaya-odezhda/page:2',
            '/catalog/beg/odezhda-dlya-bega/kompressionnaya-odezhda/page:3',
            '/catalog/beg/odezhda-dlya-bega/kompressionnaya-odezhda/page:4',
            '/catalog/beg/odezhda-dlya-bega/kompressionnaya-odezhda/page:5',
            '/catalog/beg/odezhda-dlya-bega/kompressionnaya-odezhda/page:6',
            '/catalog/beg/odezhda-dlya-bega/kompressionnaya-odezhda/page:7',
            '/catalog/beg/odezhda-dlya-bega/kompressionnaya-odezhda/page:8',
            '/catalog/beg/odezhda-dlya-bega/noski-perchatki/legkie/page:2',
            '/catalog/beg/odezhda-dlya-bega/noski-perchatki/page:2',
            '/catalog/beg/odezhda-dlya-bega/noski-perchatki/page:3',
            '/catalog/beg/odezhda-dlya-bega/noski-perchatki/page:4',
            '/catalog/beg/odezhda-dlya-bega/page:10',
            '/catalog/beg/odezhda-dlya-bega/page:11',
            '/catalog/beg/odezhda-dlya-bega/page:12',
            '/catalog/beg/odezhda-dlya-bega/page:13',
            '/catalog/beg/odezhda-dlya-bega/page:14',
            '/catalog/beg/odezhda-dlya-bega/page:15',
            '/catalog/beg/odezhda-dlya-bega/page:16',
            '/catalog/beg/odezhda-dlya-bega/page:17',
            '/catalog/beg/odezhda-dlya-bega/page:18',
            '/catalog/beg/odezhda-dlya-bega/page:2',
            '/catalog/beg/odezhda-dlya-bega/page:3',
            '/catalog/beg/odezhda-dlya-bega/page:4',
            '/catalog/beg/odezhda-dlya-bega/page:5',
            '/catalog/beg/odezhda-dlya-bega/page:6',
            '/catalog/beg/odezhda-dlya-bega/page:7',
            '/catalog/beg/odezhda-dlya-bega/page:8',
            '/catalog/beg/odezhda-dlya-bega/page:9',
            '/catalog/beg/odezhda-dlya-bega/taytsy-i-shtany/page:2',
            '/catalog/beg/page:10',
            '/catalog/beg/page:11',
            '/catalog/beg/page:12',
            '/catalog/beg/page:13',
            '/catalog/beg/page:14',
            '/catalog/beg/page:15',
            '/catalog/beg/page:16',
            '/catalog/beg/page:17',
            '/catalog/beg/page:18',
            '/catalog/beg/page:19',
            '/catalog/beg/page:2',
            '/catalog/beg/page:20',
            '/catalog/beg/page:21',
            '/catalog/beg/page:22',
            '/catalog/beg/page:23',
            '/catalog/beg/page:24',
            '/catalog/beg/page:3',
            '/catalog/beg/page:4',
            '/catalog/beg/page:5',
            '/catalog/beg/page:6',
            '/catalog/beg/page:7',
            '/catalog/beg/page:8',
            '/catalog/beg/page:9',
            '/catalog/brand_page/brand:2xu/page:2',
            '/catalog/brand_page/brand:2xu/page:3',
            '/catalog/brand_page/brand:2xu/page:4',
            '/catalog/brand_page/brand:2xu/page:5',
            '/catalog/brand_page/brand:2xu/page:6',
            '/catalog/brand_page/brand:2xu/page:7',
            '/catalog/brand_page/brand:2xu/page:8',
            '/catalog/brand_page/brand:asics/page:2',
            '/catalog/brand_page/brand:asics/page:3',
            '/catalog/brand_page/brand:asics/page:4',
            '/catalog/brand_page/brand:asics/page:5',
            '/catalog/brand_page/brand:bmc/page:2',
            '/catalog/brand_page/brand:bmc/page:3',
            '/catalog/brand_page/brand:bmc/page:4',
            '/catalog/brand_page/brand:castelli/page:2',
            '/catalog/brand_page/brand:cep/page:2',
            '/catalog/brand_page/brand:cep/page:3',
            '/catalog/brand_page/brand:cervelo/page:2',
            '/catalog/brand_page/brand:compressport/page:2',
            '/catalog/brand_page/brand:compressport/page:3',
            '/catalog/brand_page/brand:compressport/page:4',
            '/catalog/brand_page/brand:craft/page:2',
            '/catalog/brand_page/brand:gu/page:2',
            '/catalog/brand_page/brand:irontrue/page:2',
            '/catalog/brand_page/brand:lezyne/page:2',
            '/catalog/brand_page/brand:louis-garneau/page:2',
            '/catalog/brand_page/brand:louis-garneau/page:3',
            '/catalog/brand_page/brand:louis-garneau/page:4',
            '/catalog/brand_page/brand:louis-garneau/page:5',
            '/catalog/brand_page/brand:orbea/page:2',
            '/catalog/brand_page/brand:orca/page:2',
            '/catalog/brand_page/brand:prologo/page:2',
            '/catalog/brand_page/brand:scott/page:2',
            '/catalog/brand_page/brand:scott/page:3',
            '/catalog/brand_page/brand:scott/page:4',
            '/catalog/brand_page/brand:scott/page:5',
            '/catalog/brand_page/brand:sis/page:2',
            '/catalog/brand_page/brand:tacx/page:2',
            '/catalog/brand_page/brand:tyr/page:2',
            '/catalog/brand_page/brand:tyr/page:3',
            '/catalog/brand_page/brand:tyr/page:4',
            '/catalog/brand_page/brand:tyr/page:5',
            '/catalog/brand_page/brand:wilier/page:2',
            '/catalog/brand_page/brand:wilier/page:3',
            '/catalog/brand_page/brand:wilier/page:4',
            '/catalog/brand_page/brand:wilier/page:5',
            '/catalog/brand_page/brand:wilier/page:6',
            '/catalog/brand_page/brand:zerorh/page:2',
            '/catalog/gadzhety/page:2',
            '/catalog/latest/page:2',
            '/catalog/latest/page:3',
            '/catalog/latest/page:4',
            '/catalog/medicina/page:2',
            '/catalog/pitanie/batonchiki-energeticheskie/page:2',
            '/catalog/pitanie/geli/page:2',
            '/catalog/pitanie/page:2',
            '/catalog/pitanie/page:3',
            '/catalog/pitanie/page:4',
            '/catalog/pitanie/page:5',
            '/catalog/pitanie/page:6',
            '/catalog/pitanie/page:7',
            '/catalog/plavanie/ochki-1/page:2',
            '/catalog/plavanie/page:2',
            '/catalog/plavanie/page:3',
            '/catalog/plavanie/page:4',
            '/catalog/plavanie/page:5',
            '/catalog/plavanie/plavki/page:2',
            '/catalog/triatlon/gadzhety-dlya-triatlona/chasi-dlya-triatlona/brand:garmin',
            '/catalog/triatlon/gadzhety-dlya-triatlona/page:2',
            '/catalog/triatlon/gadzhety-dlya-triatlona/velokompyutery-dlya-triatlona/brand:garmin',
            '/catalog/triatlon/gidrokostyumy-dlya-otkrytoy-vody-1/brand:2xu',
            '/catalog/triatlon/gidrokostyumy-dlya-otkrytoy-vody-1/brand:aqua-sphere',
            '/catalog/triatlon/gidrokostyumy-dlya-otkrytoy-vody-1/brand:huub',
            '/catalog/triatlon/gidrokostyumy-dlya-otkrytoy-vody-1/brand:orca',
            '/catalog/triatlon/gidrokostyumy-dlya-otkrytoy-vody-1/brand:tyr',
            '/catalog/triatlon/gidrokostyumy-dlya-otkrytoy-vody-1/page:2',
            '/catalog/triatlon/gidrokostyumy-dlya-otkrytoy-vody-1/page:3',
            '/catalog/triatlon/gidrokostyumy-dlya-otkrytoy-vody-1/sex:muzhskoy',
            '/catalog/triatlon/gidrokostyumy-dlya-otkrytoy-vody-1/sex:muzhskoy/page:2',
            '/catalog/triatlon/gidrokostyumy-dlya-otkrytoy-vody-1/sex:zhenskiy',
            '/catalog/triatlon/krossovki-dlya-triatlona/brand:asics',
            '/catalog/triatlon/neobhodimye-aksessuary/page:2',
            '/catalog/triatlon/neobhodimye-aksessuary/page:3',
            '/catalog/triatlon/page:10',
            '/catalog/triatlon/page:11',
            '/catalog/triatlon/page:12',
            '/catalog/triatlon/page:13',
            '/catalog/triatlon/page:2',
            '/catalog/triatlon/page:3',
            '/catalog/triatlon/page:4',
            '/catalog/triatlon/page:5',
            '/catalog/triatlon/page:6',
            '/catalog/triatlon/page:7',
            '/catalog/triatlon/page:8',
            '/catalog/triatlon/page:9',
            '/catalog/triatlon/startovye-kostyumy/brand:2xu',
            '/catalog/triatlon/startovye-kostyumy/brand:2xu/page:2',
            '/catalog/triatlon/startovye-kostyumy/brand:compressport',
            '/catalog/triatlon/startovye-kostyumy/brand:huub',
            '/catalog/triatlon/startovye-kostyumy/brand:louis-garneau',
            '/catalog/triatlon/startovye-kostyumy/brand:orca',
            '/catalog/triatlon/startovye-kostyumy/brand:tyr',
            '/catalog/triatlon/startovye-kostyumy/kupalniki-dlya-triatlona/page:2',
            '/catalog/triatlon/startovye-kostyumy/kupalniki-dlya-triatlona/page:3',
            '/catalog/triatlon/startovye-kostyumy/page:2',
            '/catalog/triatlon/startovye-kostyumy/page:3',
            '/catalog/triatlon/startovye-kostyumy/page:4',
            '/catalog/triatlon/startovye-kostyumy/razdelnye/page:2',
            '/catalog/triatlon/startovye-kostyumy/razdelnye/shorty:muzhskoy',
            '/catalog/triatlon/startovye-kostyumy/razdelnye/shorty:muzhskoy/page:2',
            '/catalog/triatlon/startovye-kostyumy/sex:muzhskoy',
            '/catalog/triatlon/startovye-kostyumy/sex:muzhskoy/page:2',
            '/catalog/triatlon/startovye-kostyumy/sex:zhenskiy',
            '/catalog/triatlon/startovye-kostyumy/sex:zhenskiy/page:2',
            '/catalog/triatlon/startovye-kostyumy/slitnye/page:2',
            '/catalog/triatlon/startovye-kostyumy/slitnye/page:3',
            '/catalog/triatlon/velosipedy-dlya-triatlona/brand:bmc',
            '/catalog/triatlon/velosipedy-dlya-triatlona/brand:Cervelo',
            '/catalog/triatlon/velosipedy-dlya-triatlona/brand:felt',
            '/catalog/triatlon/velosipedy-dlya-triatlona/brand:orbea',
            '/catalog/triatlon/velosipedy-dlya-triatlona/brand:Scott',
            '/catalog/triatlon/velosipedy-dlya-triatlona/brand:wilier',
            '/catalog/triatlon/velosipedy-dlya-triatlona/page:2',
            '/catalog/triatlon/velosipedy-dlya-triatlona/page:3',
            '/catalog/triatlon/velotufli-dlya-triatlona/brand:bont',
            '/catalog/triatlon/velotufli-dlya-triatlona/brand:louis-garneau',
            '/catalog/triatlon/velotufli-dlya-triatlona/brand:scott',
            '/catalog/triatlon/velotufli-dlya-triatlona/brand:sidi',
            '/catalog/veloaksessuary/flyagoderzhateli/page:2',
            '/catalog/veloaksessuary/page:10',
            '/catalog/veloaksessuary/page:11',
            '/catalog/veloaksessuary/page:12',
            '/catalog/veloaksessuary/page:13',
            '/catalog/veloaksessuary/page:14',
            '/catalog/veloaksessuary/page:15',
            '/catalog/veloaksessuary/page:16',
            '/catalog/veloaksessuary/page:17',
            '/catalog/veloaksessuary/page:18',
            '/catalog/veloaksessuary/page:19',
            '/catalog/veloaksessuary/page:2',
            '/catalog/veloaksessuary/page:20',
            '/catalog/veloaksessuary/page:21',
            '/catalog/veloaksessuary/page:22',
            '/catalog/veloaksessuary/page:23',
            '/catalog/veloaksessuary/page:24',
            '/catalog/veloaksessuary/page:25',
            '/catalog/veloaksessuary/page:3',
            '/catalog/veloaksessuary/page:4',
            '/catalog/veloaksessuary/page:5',
            '/catalog/veloaksessuary/page:6',
            '/catalog/veloaksessuary/page:7',
            '/catalog/veloaksessuary/page:8',
            '/catalog/veloaksessuary/page:9',
            '/catalog/veloaksessuary/ruliroga/page:2',
            '/catalog/veloaksessuary/sedla/page:2',
            '/catalog/veloaksessuary/shlemy/page:2',
            '/catalog/veloaksessuary/veloobuv/page:2',
            '/catalog/veloaksessuary/veloobuv/page:3',
            '/catalog/veloaksessuary/veloobuv/veloobuv-shosse/page:2',
            '/catalog/veloaksessuary/veloodezhda/dzhersi/page:2',
            '/catalog/veloaksessuary/veloodezhda/dzhersi/page:3',
            '/catalog/veloaksessuary/veloodezhda/dzhersi/page:4',
            '/catalog/veloaksessuary/veloodezhda/kurtki-i-dozhdeviki/page:2',
            '/catalog/veloaksessuary/veloodezhda/noski/page:2',
            '/catalog/veloaksessuary/veloodezhda/page:10',
            '/catalog/veloaksessuary/veloodezhda/page:11',
            '/catalog/veloaksessuary/veloodezhda/page:2',
            '/catalog/veloaksessuary/veloodezhda/page:3',
            '/catalog/veloaksessuary/veloodezhda/page:4',
            '/catalog/veloaksessuary/veloodezhda/page:5',
            '/catalog/veloaksessuary/veloodezhda/page:6',
            '/catalog/veloaksessuary/veloodezhda/page:7',
            '/catalog/veloaksessuary/veloodezhda/page:8',
            '/catalog/veloaksessuary/veloodezhda/page:9',
            '/catalog/veloaksessuary/veloodezhda/perchatki/page:2',
            '/catalog/veloaksessuary/veloodezhda/shorty-1/page:2',
            '/catalog/velosipedy/brand:bmc',
            '/catalog/velosipedy/brand:bmc/page:2',
            '/catalog/velosipedy/brand:bmc/page:3',
            '/catalog/velosipedy/brand:felt',
            '/catalog/velosipedy/brand:orbea',
            '/catalog/velosipedy/brand:scott',
            '/catalog/velosipedy/brand:scott/model:addict',
            '/catalog/velosipedy/brand:scott/model:addict-gravel',
            '/catalog/velosipedy/brand:scott/model:addict-rc',
            '/catalog/velosipedy/brand:scott/model:Contessa-Addict-Gravel',
            '/catalog/velosipedy/brand:scott/model:foil',
            '/catalog/velosipedy/brand:scott/model:speedster',
            '/catalog/velosipedy/brand:scott/model:speedster-gravel',
            '/catalog/velosipedy/brand:scott/page:2',
            '/catalog/velosipedy/brand:wilier',
            '/catalog/velosipedy/brand:wilier/page:2',
            '/catalog/velosipedy/brand:wilier/page:3',
            '/catalog/velosipedy/brand:wilier/page:4',
            '/catalog/velosipedy/graviynye/brand:scott',
            '/catalog/velosipedy/page:10',
            '/catalog/velosipedy/page:11',
            '/catalog/velosipedy/page:12',
            '/catalog/velosipedy/page:13',
            '/catalog/velosipedy/page:2',
            '/catalog/velosipedy/page:3',
            '/catalog/velosipedy/page:4',
            '/catalog/velosipedy/page:5',
            '/catalog/velosipedy/page:6',
            '/catalog/velosipedy/page:7',
            '/catalog/velosipedy/page:8',
            '/catalog/velosipedy/page:9',
            '/catalog/velosipedy/shosseynye/brand:bmc',
            '/catalog/velosipedy/shosseynye/brand:bmc/page:2',
            '/catalog/velosipedy/shosseynye/brand:felt',
            '/catalog/velosipedy/shosseynye/brand:orbea',
            '/catalog/velosipedy/shosseynye/brand:scott',
            '/catalog/velosipedy/shosseynye/brand:scott/page:2',
            '/catalog/velosipedy/shosseynye/brand:wilier',
            '/catalog/velosipedy/shosseynye/brand:wilier/page:2',
            '/catalog/velosipedy/shosseynye/brand:wilier/page:3',
            '/catalog/velosipedy/shosseynye/page:2',
            '/catalog/velosipedy/shosseynye/page:3',
            '/catalog/velosipedy/shosseynye/page:4',
            '/catalog/velosipedy/shosseynye/page:5',
            '/catalog/velosipedy/shosseynye/page:6',
            '/catalog/velosipedy/shosseynye/page:7',
            '/catalog/velosipedy/shosseynye/page:8',
            '/catalog/velosipedy/shosseynye/page:9',
            '/catalog/velosipedy/trekovye/brand:bmc',
            '/catalog/sales/percent:25/page:2',
            '/catalog/sales/percent:30/page:2',
            '/catalog/sales/percent:30/page:3',
            '/catalog/sales/percent:30/page:4',
            '/catalog/sales/percent:35/page:2',
            '/catalog/sales/percent:35/page:3',
            '/catalog/sales/percent:35/page:4',
            '/catalog/sales/percent:40/page:2',
            '/catalog/sales/percent:50/page:2',
            '/catalog/sales/percent:50/page:3',
            '/catalog/sales/percent:50/page:4',
            '/catalog/sales/percent:50/page:5',
            '/catalog/sales/percent:50/page:6',
            '/catalog/sales/percent:50/page:7',
            '/catalog/sales/percent:50/page:8'
        ];
    }

    private static function routePages() {
        return [
            '/pw-web/default/contacts',
            '/pw-web/default/map',
            '/rd-order/checkout/voucher',
            '/ct-catalog/default/latest'
        ];
    }


    private function getPercents()
    {
        $return = [];

        $id = 8;
        $discountIds = Discount::find()->active()->andWhere(['group_id' => $id])->select('product_id');
        $parents = [];

        $percents = Product::find()
            ->select('ct_products.*, LEAST(`ct_prices`.`value`, `ct_discounts`.`price`) as price, ROUND((ct_prices.value - ct_discounts.price)/ (ct_prices.value / 100)) AS percent')
            ->from('ct_products, ct_prices, ct_discounts')
            ->andWhere('ct_products.id = ct_prices.product_id')
            ->andWhere('ct_products.id = ct_discounts.product_id')
            ->andWhere(['in', 'ct_products.id', $discountIds])
            ->products()
            ->active()
            ->having(['>', 'percent', 0])
            ->andHaving(['<', 'percent', 100])
            ->groupBy('percent')
            ->andWhere(['in', 'ct_products.id', ProductsToWareHouses::find()->where(['>', 'value', 0])->select('product_id')]);

        foreach ($percents->all() as $percent) {
            $return[] = $percent->percent;
        }
        return $return;
    }

}
