<?php

namespace pw\seo;


use yii;
use yii\base\Behavior;
use pw\core\db\ActiveRecord;
use pw\seo\models\Seo;
use pw\seo\widgets\SeoFormWidget;

class SeoModelBehavior extends Behavior
{

    private $_models = null;
    public $ownerModel = null;

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'saveSeoData',
            ActiveRecord::EVENT_AFTER_UPDATE => 'saveSeoData',
            ActiveRecord::EVENT_AFTER_DELETE => 'deleteSeoData',
            ActiveRecord::EVENT_BEFORE_FORM_RENDER => 'formRender'
        ];
    }

    public function saveSeoData($event)
    {
        if (Yii::$app->request instanceof \yii\web\Request && Yii::$app->request->getIsPost()) {
            $model = $this->getModel();

            if (isset(Yii::$app->request->post()['Seo'])) {
                foreach (Yii::$app->request->post('Seo') as $key => $value) {

                    if ($model) {
                        $model->{$key} = $value;
                    }
                }
            }
            if(is_null($this->ownerModel)) {
                $this->ownerModel = get_class($this->owner);
            }
            $model->model_class = $this->ownerModel;
            $model->save();
        }
    }

    public function deleteSeoData($event)
    {
        $id = $this->owner->primaryKey;
        Seo::deleteAll(['model_id' => $id, 'model_class' => get_class($this->owner)]);
    }

    public function formRender($event)
    {
        SeoFormWidget::widget(['form' => $event->form, 'model' => $this->owner]);
    }

    protected function getModel()
    {
        if (empty($this->_models)) {

            if(is_null($this->ownerModel)) {
                $this->ownerModel = $this->owner->className();
            }

            $model = Seo::find()->alias('seo')->where(['seo.model_id' => $this->owner->id, 'model_class' => $this->ownerModel])->one();


            if ($model) {
                $this->_models = $model;
            } else {
                $model = new Seo();
                $model->model_class = $this->ownerModel;
                $model->model_id = $this->owner->primaryKey;
                $this->_models = $model;
            }
        }

        return $this->_models;
    }

    public function getSeo($id = null)
    {

        if ($id) {
            return $this->getModel();
        }
        return \current($this->getModel());
    }
}