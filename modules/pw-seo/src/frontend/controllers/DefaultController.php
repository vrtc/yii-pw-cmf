<?php
/**
 * Created by trisport_pw.
 * User: Dmitriy Kindeev
 * Email: dkindeev@mail.ru
 * Date: 15.10.19
 * Time: 10:56
 */

namespace pw\seo\frontend\controllers;

use pw\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class DefaultController extends Controller
{
    public function actionRobots()
    {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        \Yii::$app->response->headers->add('content-type','text/plain');

        if(isset(\Yii::$app->settings['pw-seo']['settings']['robots'])){
            return \Yii::$app->settings['pw-seo']['settings']['robots'];
        }
    }

    public function actionSitemap()
    {
        $filename = \Yii::getAlias('@runtime/feed/yml/sitemap.xml');
        if (file_exists($filename)) {
            \Yii::$app->response->format = Response::FORMAT_RAW;
            \Yii::$app->getResponse()
                ->getHeaders()
                ->set('Content-Type', 'application/xml; charset=UTF-8');
            return file_get_contents($filename);
        }
        throw new NotFoundHttpException();
    }
}
