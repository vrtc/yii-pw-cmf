<?php

namespace pw\seo\models;


use Yii;
use yii\web\NotFoundHttpException;
use pw\i18n\db\ActiveRecord;


/**
 * This is the model class for table "{{%seo}}".
 *
 * @property integer $id
 * @property string $model_class
 * @property string $route
 * @property string $model_id
 * @property integer $no_index
 * @property integer $no_follow
 * @property integer $in_sitemap
 * @property string $canonical
 *
 */
class Seo extends ActiveRecord
{
    public $modelName;
    public $modelTitle;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pw_seo}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_index', 'no_follow', 'in_sitemap'], 'integer'],
            [['model_id', 'model_class'], 'safe'],
            ['route', 'string'],
            ['canonical', 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'model_class' => Yii::t('seo', 'Класс сео'),
            'no_index' => Yii::t('seo', 'Не индексировать'),
            'no_follow' => Yii::t('seo', 'Не переходить по ссылкам'),
            'in_sitemap' => Yii::t('seo', 'Опубликовать в карте сайта'),
            'title' => Yii::t('seo', 'Заголовок (Title)'),
            'description' => Yii::t('seo', 'Описание'),
            'keywords' => Yii::t('seo', 'Ключевые слова'),
            'route' => Yii::t('seo', 'Маршрут'),
            'canonical' => Yii::t('seo', 'Canonical (абсолютный URL)'),
        ];
    }

    public function getModelInstance()
    {
        if ($this->model_class && $this->model_id) {
            $instance = $this->model_class::findOne($this->model_id);
            if (!$instance) {
                throw new NotFoundHttpException();
            }

            return $instance;
        }
    }
}
