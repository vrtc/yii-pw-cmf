<?php

namespace pw\seo\models;


use pw\core\db\ActiveRecord;
use Yii;
use yii\web\NotFoundHttpException;


/**
 * This is the model class for table "{{%redirects}}".
 *
 * @property integer $id
 * @property string $url_from
 * @property string $url_to
 * @property integer $status
 * @property integer $times_used
 * @property integer $response_code
 *
 */
class Redirect extends ActiveRecord
{

    const STATUS_ACTIVE = 1;
    const STATUS_NO_ACTIVE = 0;

    static private $statuses;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pw_redirects';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'response_code', 'times_used'], 'integer'],
            [['url_from', 'url_to'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'status' => Yii::t('seo', 'Статус'),
            'response_code' => Yii::t('seo', 'Код ответа'),
            'times_used' => Yii::t('seo', 'Количество переходов'),
            'url_from' => Yii::t('seo', 'Url откуда'),
            'url_to' => Yii::t('seo', 'Url куда'),
        ];
    }

    public function getStatus()
    {
        return self::getStatuses()[$this->status];
    }

    public static function getStatuses()
    {
        if (!self::$statuses) {
            self::$statuses = [
                self::STATUS_ACTIVE => \Yii::t('seo', 'Активен'),
                self::STATUS_NO_ACTIVE => \Yii::t('seo', 'Не активен'),
            ];
        }
        return self::$statuses;
    }

    public static function getResponseCodes()
    {
        return [
            301 => '301 перемещено навсегда',
            302 => '302 перемещено временно',
        ];
    }
}
