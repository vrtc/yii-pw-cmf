<?php

namespace pw\seo\models\i18n;

use Yii;

/**
 * This is the model class for table "{{%seo_i18n}}".
 *
 * @property integer $id
 * @property integer $model_id
 * @property string $language
 * @property string $title
 * @property string $h1
 * @property string $h1_single
 * @property string $description
 * @property string $keywords
 *
 * @property Seo $idSeo0
 */
class Seo extends \pw\core\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pw_seo_i18n}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['model_id'], 'integer'],
            [['language'], 'string', 'max' => 16],
            [['title', 'description', 'h1','h1_single', 'keywords'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'title'       => Yii::t('seo', 'Заголовок (Title)'),
            'description' => Yii::t('seo', 'Описание (Description)'),
            'keywords'    => Yii::t('seo', 'Ключевые слова (Keywords)'),
            'h1'          => Yii::t('seo', 'H1'),
            'h1_single'          => Yii::t('seo', 'H1 в единственном числе'),
        ];
    }
}
