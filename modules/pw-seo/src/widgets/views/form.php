<?php
/* @var $this pw\web\View */
/* @var $form pw\ui\form\ActiveForm */
/* @var $model pw\core\Model */

?>
<?php $form->beginTab('seo_'.get_class($model), 'SEO', 'info'); ?>
    <?php $seo = $model->getSeo($model);  ?>
    <?php if(!$seo){
        $seo = new \pw\seo\models\Seo();
        $seo->model_class = get_class($model);
        $seo->model_id = $model->id;
        $seo->save();
    }?>
    <fieldset>
    <?= $form->field($seo->translate(), "title")->textInput() ?>
    <?= $form->field($seo->translate(), "h1")->textInput() ?>
    <?= $form->field($seo->translate(), "h1_single")->textInput() ?>
    <?= $form->field($seo->translate(), "description")->textarea() ?>
    <?= $form->field($seo->translate(), "keywords")->textarea() ?>
    <?= $form->field($seo, "no_index")->checkbox() ?>
    <?= $form->field($seo, "no_follow")->checkbox() ?>
    <?= $form->field($seo, "in_sitemap")->checkbox() ?>
    </fieldset>
<?php $form->endTab() ?>
