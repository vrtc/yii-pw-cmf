<?php
namespace pw\seo\widgets;


use Yii;
use yii\base\Widget;
use yii\base\Exception;

class SeoFormWidget extends Widget
{

    public $title;

    public $description;

    public $inSitemap;

    public $noIndex;

    public $noFollow;

    public $model;

    public $form;

    public $routes;

    public $h1;

    public $view = 'form';

    public function run()
    {



//        if ($this->model->seo->getIsNewRecord()) {
//            $this->model->seo->setAttributes([
//                'title' => $this->title,
//                'description' => $this->description,
//                'in_sitemap' => $this->inSitemap,
//                'no_index' => $this->noIndex,
//                'no_follow' => $this->noFollow
//            ]);
//        }

        return $this->render($this->view, [
            'routes' =>$this->routes,
            'model' => $this->model,
            'form' => $this->form
        ]);
    }
}