<?php

namespace pw\seo\handlers;

use pw\pages\models\Pages;
use pw\seo\models\Redirect;
use \pw\seo\models\Seo;
use Yii;
use pw\web\View;
use pw\core\traits\GetModule;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use function GuzzleHttp\Psr7\str;
use yii\httpclient\Client;

class RedirectHandlers
{
    use GetModule;

    /**
     * @param $event
     * @return \yii\web\Response
     * @throws \yii\base\InvalidConfigException
     */
    public static function registerCode($event)
    {
        $requestUrl = Yii::$app->request->getUrl();
        $absoluteUrl = Yii::$app->request->getAbsoluteUrl();



        if(stristr($requestUrl, '/catalog/product/') ){
            $newUrlProduct = str_replace("/catalog/product/", "", $requestUrl);
            return self::redirect(Url::to(['/ct-catalog/default/product', 'slug' => $newUrlProduct]), 301);
        }

        if ($redirect = Redirect::findOne(['url_from' => [$requestUrl, $absoluteUrl], 'status' => Redirect::STATUS_ACTIVE])) {
            $redirect->times_used++;
            $redirect->save();
            return self::redirect($redirect->url_to, $redirect->response_code);
        } elseif (Yii::$app->response->statusCode == 404 && $redirect == null) {

                $redirect = false;
                $requestUrl = Yii::$app->request->getUrl();
                if(substr($requestUrl, -1) == '/'){
                    $redirect = true;
                    $requestUrl = substr($requestUrl, 0, -1);
                }
                $urlParts = explode('/', $requestUrl);
                array_shift($urlParts);
                $keyword = array_pop($urlParts);
                if (strrpos($keyword, '.html')) {
                    $keyword = explode('.', $keyword)[0];
                }

                if($keyword == 'search'){
                    var_dump(Yii::$app->request->getQueryParams());
                    if($q = Yii::$app->request->getQueryParam('q')) {
                        return self::redirect(['/ct-catalog/default/search', 'q' => $q], 301);
                    }
                }

                if($redirect){
                    return self::redirect($redirect, 301);
                }
        }
    }

    private static function redirect($url, $code){
        return \redirect($url, $code);
    }

}
