<?php

namespace pw\seo\handlers;

use \pw\seo\models\Seo;
use Yii;
use pw\web\View;
use pw\core\traits\GetModule;
use yii\helpers\Url;

class SeoHandlers
{
    use GetModule;

    public static function registerCode($event)
    {
        if (!Yii::$app->request->getIsAjax() && Yii::$app->response->getIsOk()) {
            $module = self::getModule();
            if (Yii::$app->isFrontend() && env('ENV') == 'prod') {
                if ($module->scriptHeader) {
                    $event->sender->registerCustomCode($module->scriptHeader, View::POS_HEAD, 1);
                }
                if ($module->scriptFooter) {
                    $event->sender->registerCustomCode($module->scriptFooter, View::POS_END, 1);
                }
            }

            $urlParams = Yii::$app->getUrlManager()->parseRequest(Yii::$app->request);


            //если главная страница
            if (Yii::$app->requestedRoute == '') {

                $seoSettings = Yii::$app->settings['pw-seo']['settings'];

                if (isset($seoSettings['titleHome'])) {
                    $event->sender->title = $seoSettings['titleHome'];
                }
                if (isset($seoSettings['descriptionHome'])) {
                    $event->sender->registerMetaTag(['name' => 'description', 'content' => $seoSettings['descriptionHome']]);
                }
                if (isset($seoSettings['keywordsHome'])) {
                    $event->sender->registerMetaTag(['name' => 'keywords', 'content' => $seoSettings['keywordsHome']]);
                }
                return true;
            }

            foreach (Yii::$app->getUrlManager()->getRules() as $rule) {

                if ($rule->route === Yii::$app->requestedRoute) {

                    if (isset($urlParams[1][key($urlParams[1])]) && $rule->model) {

                        $urlKey = $urlParams[1][key($urlParams[1])];


                        if ($model = $rule->model::findOne(['slug' => $urlKey])) {

                            $seo = Seo::find()->where(['model_class' => $rule->model, 'b.model_id' => $model->id])->one();
                            if (!$seo) {
                                $seo = new Seo();
                                $seo->model_class = $rule->model;
                                $seo->model_id = $model->id;
                                $seo->save();
                            }

                            if (method_exists($model, 'getName')) {

                                if (isset($seo->title) && (is_null($seo->title) || $seo->title == '')|| (isset($model->forcedSeoTitle) && $model->forcedSeoTitle)) {
                                    $seo->title = $model->getName();
                                } elseif (!isset($seo->title)) {
                                    $event->sender->title = $model->getName();
                                }

                            }

                            if (method_exists($model, 'getDescription')) {

                                if ((isset($seo->description) && (is_null($seo->description) || $seo->description == '')) || (isset($model->forcedSeoDescription) && $model->forcedSeoDescription)) {
                                    $seo->description = $model->getDescription();

                                } elseif (!isset($seo->description)) {
                                    $seo->description = $model->getDescription();
                                }
                            }

                            if($rule->model == Category::class && $model->checkFilter($event->sender->params)) {
                                $seo->title = $model->getNameFilter($event->sender->params);
                                $seo->description = $model->getDescriptionFilter($event->sender->params);
                            }

                        }
                    } elseif ($urlParams[0] != '') {

                        $seo = Seo::find()->where(['route' => $urlParams[0]])->one();


                    }
                    /**
                     * @var $view \pw\web\View
                     */
                    $view = $event->sender;

                    $canonicalFlag = false;
                    $canonical = '';
                    $params = Yii::$app->request->queryParams;

                    //canonical для страниц с параметром slug
                    if(isset($params['slug'])) {
                        $canonical = Url::to([
                            '/' . Yii::$app->requestedRoute,
                            'slug' => $params['slug']
                        ], true);
                        if(($rule->model == Category::class && isset($model) && $model->checkFilter($event->sender->params)) || $rule->model == Tag::class) {
                            $canonical .= $params['params'];
                        }
                        $view->registerLinkTag(['rel' => 'canonical', 'href' => $canonical]);
                        $canonicalFlag = true;
                    }

                    //canonical для абсолютных путей введенных в админке
                    if(isset($seo->canonical) && !empty(trim($seo->canonical)) && !$canonicalFlag && Url::canonical() != $seo->canonical) {
                        if(isset($params['params'])) $seo->canonical .= $params['params'];
                        $view->registerLinkTag(['rel' => 'canonical', 'href' => $seo->canonical]);
                        $canonicalFlag = true;
                    }

                    if(!$canonicalFlag) {
                        $canonical = Url::canonical();
                        if(isset($params['params'])) $canonical .= $params['params'];
                        $view->registerLinkTag(['rel' => 'canonical', 'href' => $canonical]);
                    }

                    if ($seo) {

                        if ($seo->title && empty($view->title)) {
                            $view->title = $seo->title;
                        }


                        $page = 1;
                        if (isset($view->params['page']) && $view->params['page'] > 1) {
                            $page = $view->params['page'];
                        }
                        else if (isset($urlParams[1]['page']) && $urlParams[1]['page'] > 1) {
                            $page = $urlParams[1]['page'];
                        }

                        if($page > 1) $view->title .= ' - Страница ' . $page;

                        if ($seo->description) {
                            $description = '';
                            if (!empty($view->params['description'])) {
                                $description = $view->params['description'];
                            } else {
                                $description = $seo->description;
                            }
                            if($page > 1) $description .= ' - Страница ' . $page;
                            $view->registerMetaTag(['name' => 'description', 'content' => $description]);
                        }

                        if ($seo->keywords && (!isset($view->params['per-page']) || $view->params['per-page'] == 0)) {
                            $view->registerMetaTag(['name' => 'keywords', 'content' => $seo->keywords]);
                        }
                        $robots = [];
                        if ($seo->no_index) {
                            $robots [] = 'noindex';
                        }
                        if ($seo->no_follow) {
                            $robots[] = 'nofollow';
                        }
                        if ($robots) {

                            $view->registerMetaTag(['name' => 'robots', 'content' => \implode(',', $robots)]);
                        }

                        break;
                    }
                }
            }

        }
    }

}
