<?php
namespace pw\seo;

use pw\modules\models\Modules;
use pw\modules\models\Settings;
use yii\db\Expression;

class Installer extends \pw\modules\ModuleInstaller
{

    public function getVersion():string
    {
        return '0.1';
    }

    public function getAuthors():array
    {
        return ['i@yar.pw'];
    }

    public function getLink():string
    {
        return 'http://yar.pw';
    }

    public function getName():string
    {
        return 'Seo';
    }

    public function getSourceLanguage():string
    {
        return 'ru-RU';
    }

    public function afterInstall()
    {
        parent::afterInstall(); // TODO: Change the autogenerated stub
    }
}
