<?php

namespace pw\seo\forms;

use pw\core\db\ActiveQuery;
use pw\core\Model;
use pw\seo\models\Seo;
use yii\db\Expression;

class SelectionForm extends Model
{
    const NOT_SELECTED = 0;
    const SELECTED = 1;
    const ALL = 2;

    public $title;
    public $keywords;
    public $description;
    public $selection;
    public $selection_all;
    public $action;
    public $no_index;
    public $no_follow;
    public $in_sitemap;


    public function attributeLabels()
    {
        return [
            'title' => 'Название',
            'keywords' => 'Ключевые слова',
            'description' => 'Описание',
            'action' => 'Изменить',
            'no_index' => 'Не индексировать',
            'no_follow' => 'Не переходить по ссылкам',
            'in_sitemap' => 'Опубликовать в карте сайта',
        ];
    }

    public function rules()
    {
        return [
            [['title', 'keywords', 'description'], 'string'],
            [['no_index', 'no_follow', 'in_sitemap'], 'integer'],
            [['selection_all', 'action'], 'integer'],
            [['selection'], 'safe'],
        ];
    }

    public static function getActions()
    {
        return [
            self::SELECTED => 'У выбранных',
            self::NOT_SELECTED => 'У не выбранных',
            self::ALL => 'У всех',
        ];
    }

    private function setNewValues(ActiveQuery $query)
    {
        foreach ($query->each() as $item) {
            $item->title = $this->title != null ? $this->title : $item->title;
            $item->keywords = $this->keywords != null ? $this->keywords : $item->keywords;
            $item->description = $this->description != null ? $this->description : $item->description;
            $item->no_index = $this->no_index != null ? $this->no_index : $item->no_index;
            $item->no_follow = $this->no_follow != null ? $this->no_follow : $item->no_follow;
            $item->in_sitemap = $this->in_sitemap != null ? $this->in_sitemap : $item->in_sitemap;
            $item->save();
        }
    }

    public function changeSeo()
    {
        switch ($this->action) {
            case self::ALL || self::SELECTED:
                $query = Seo::find()->where(['in', new Expression('`b`.`model_id`'), $this->selection]);
                break;
            case self::NOT_SELECTED:
                $query = Seo::find()->where(['not', ['in', new Expression('`b`.`model_id`'), $this->selection]]);
                break;
        }
        $this->setNewValues($query);
    }
}