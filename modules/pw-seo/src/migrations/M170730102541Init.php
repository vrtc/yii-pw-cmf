<?php
namespace pw\seo\migrations;

use pw\i18n\db\Migration;
use pw\modules\models\Modules;
use pw\modules\models\Settings;
use yii\db\Expression;

class M170730102541Init extends Migration
{
    public function up()
    {
        $this->createTable('{{%pw_seo}}', [
            'id'          => $this->bigPrimaryKey()->unsigned(),
            'model_class' => $this->string(),
            'model_id'    => $this->bigInteger()->unsigned(),
            'title'       => $this->string()->i18n(),
            'h1'          => $this->longText()->i18n(),
            'h1_single'   => $this->longText()->i18n(),
            'description' => $this->string()->i18n(),
            'keywords'    => $this->string()->i18n(),
            'no_index'    => $this->boolean(),
            'no_follow'   => $this->boolean(),
            'in_sitemap'  => $this->boolean(),
            'canonical'   => $this->string(),
            'route'     => $this->text()
        ]);

        $this->createIndex('idx_route', '{{%pw_seo}}', 'model_class');
        $this->createIndex('idx_model', '{{%pw_seo}}', 'model_id');
        $this->createIndex('idx_sitemap', '{{%pw_seo}}', 'in_sitemap');

    }

    public function down()
    {
        $this->dropTable('{{%pw_seo}}');
    }

}
