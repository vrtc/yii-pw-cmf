<?php
namespace pw\seo\migrations;
use pw\core\db\Migration;

class m190523_083252_init_redirect extends Migration
{
    public function up()
    {
        $this->createTable('{{%pw_redirects}}', [
            'id'            => $this->primaryKey(),
            'url_from'      => $this->string(1024),
            'url_to'        => $this->string(1024),
            'status'        => $this->integer(),
            'response_code' => $this->integer(),
            'times_used'    => $this->integer(),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%pw_redirects}}');
    }

}
