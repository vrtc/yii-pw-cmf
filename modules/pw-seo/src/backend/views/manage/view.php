<?php

use pw\ui\grid\GridView;

/* @var $this \pw\web\View */
/* @var $searchModel \pw\seo\backend\models\SeoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $selection \pw\seo\forms\SelectionForm */

$js = <<<JS

    $('#change-seo').on('submit', function (e) {
    
    var array_id = [];
    
    $('[name ="selection[]"]:checked').each(function () { 
        array_id.push($(this).attr('value'));
    });
    if($('[name ="selection_all"]:checked')){
        $('#selection_all').attr('value', $('[name ="selection_all"]:checked').attr('value'));
    }
    $('#selection_id').attr('value', JSON.stringify(array_id));
    
    });

    
    $('#check-all').on('change', function(e) {
        if(this.checked){
            $('.selection').prop('checked', true);
        }else{
            $('.selection').prop('checked', false);
        }
    });
JS;
$this->registerJs($js, \yii\web\View::POS_END);
?>

<?php $form = \pw\ui\form\ActiveForm::begin([
    'model' => $selection,
    'options' => ['id' => 'change-seo'],
]); ?>
<?php $form->beginTab('general', 'Основное') ?>
<?= $form->field($selection, 'title')->textInput() ?>
<?= $form->field($selection, 'keywords')->textInput() ?>
<?= $form->field($selection, 'description')->textarea() ?>
<?= $form->field($selection, "no_index")->checkbox() ?>
<?= $form->field($selection, "no_follow")->checkbox() ?>
<?= $form->field($selection, "in_sitemap")->checkbox() ?>
<?= $form->field($selection, 'selection')->hiddenInput(['id' => 'selection_id'])->label(false) ?>
<?= $form->field($selection, 'action')->dropDownList($selection::getActions()) ?>
<?php $form->endTab() ?>

<?php \pw\ui\form\ActiveForm::end(); ?>

<?= GridView::widget([
    'id' => 'seo',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'title' => $route->name,
    'columns' => [
        [
            'class' => \pw\ui\grid\CheckboxColumn::class,
            'header' => \yii\helpers\Html::checkbox('selection_all', false, ['value' => 1, 'id' => 'check-all']),
            'content' => function ($model) use ($form, $selection) {
                return \yii\helpers\Html::checkbox('selection[]', false, ['value' => $model->model_id, 'class' => 'selection']);
            },
        ],
        [
            'attribute' => 'modelName',
            'label' => 'Название',
        ],
        'title',
        'keywords',
        [
            'class' => \pw\ui\grid\ActionColumn::class,
            'visibleButtons' => [
                'view' => false,
            ],
        ],
    ],
]);
?>

