<?php
/* @var $this yii\web\View */
/* @var $model \pw\seo\models\Seo */

/* @var $form pw\ui\form\ActiveForm */

use yii\helpers\Html;
use pw\ui\form\ActiveForm;

?>

<?php $form = ActiveForm::begin(['model' => $model]); ?>
<?= $form->field($model, 'title')->textInput() ?>
<?= $form->field($model, 'h1')->textInput() ?>
<?= $form->field($model, 'description')->textarea() ?>
<?= $form->field($model, 'keywords')->textarea() ?>
<?= $form->field($model, 'no_index')->checkbox([
    'class'         => 'make-switch',
    'data-on-text'  => '<i class="fa fa-check"></i>',
    'data-off-text' => '<i class="fa fa-times"></i>',
    'data-on-color' => 'success',
]) ?>
<?= $form->field($model, 'no_follow')->checkbox([
    'class'         => 'make-switch',
    'data-on-text'  => '<i class="fa fa-check"></i>',
    'data-off-text' => '<i class="fa fa-times"></i>',
    'data-on-color' => 'success',
]) ?>
<?= $form->field($model, 'in_sitemap')->checkbox([
    'class'         => 'make-switch',
    'data-on-text'  => '<i class="fa fa-check"></i>',
    'data-off-text' => '<i class="fa fa-times"></i>',
    'data-on-color' => 'success',
]) ?>
<?php ActiveForm::end(); ?>