<?php

use pw\ui\grid\GridView;
use yii\helpers\Html;

/* @var $this \pw\web\View */
/* @var $searchModel \pw\mailer\backend\models\TemplateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>

<?= GridView::widget([
    'id' => 'seo',
    //'simple' => true,
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,

    'columns' => [
        [
            'header' => Yii::t('seo', 'Категория'),
            'attribute' => 'name'
        ],
        [
            'class' => \pw\ui\grid\ActionColumn::class,
            'visibleButtons' => [
                'delete' => false,
                'view' => function ($model, $key, $index) {
                    return !empty($model->model);
                },
                'update' => function ($model, $key, $index) {
                    return empty($model->model);
                }
            ],
            'buttons' => [
                'view' => function ($url, $model) {
                    return Html::a('<i class="fas fa-eye"></i>', ['manage/view', 'id' => $model->model], [
                        'title' => 'Просмотр', 'class' => 'btn-secondary btn-sm'
                    ]);
                },

                'update' => function ($url, $model) {
                    return Html::a('<i class="fas fa-edit"></i>', ['manage/update', 'id' => $model->route, 'route' => true], [
                        'title' => 'Изменить', 'class' => 'btn-primary btn-sm'
                    ]);
                },

            ],
        ],
    ],
]);
