<?php

use pw\ui\grid\GridView;
use yii\helpers\Html;
use ct\catalog\models\Brand;
use ct\catalog\Module;

/* @var $this \pw\web\View */
/* @var $searchModel \ct\catalog\backend\models\BrandSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Редиректы';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'model' => $searchModel,
    'title' => Module::t('seo', 'Редиректы'),
    'icon' => 'users',
    'actions' => [
        [
            'url' => ['create'],
            'label' => Module::t('seo', 'Добавить редирект')
        ]
    ],
    'columns' => [
        [
            'attribute' => 'status',
            'value' => function(\pw\seo\models\Redirect $model){
                return $model->status == \pw\seo\models\Redirect::STATUS_ACTIVE ? 'Активен' : 'Не активен';
            },
            'filter' => \pw\seo\models\Redirect::getStatuses(),
        ],
        'url_from',
        'url_to',
        [
            'attribute' => 'response_code',
            'value' => function(\pw\seo\models\Redirect $model){
                return \pw\seo\models\Redirect::getResponseCodes()[$model->response_code];
            },
            'filter' => \pw\seo\models\Redirect::getResponseCodes(),
        ],
        'times_used',
        [
            'class' => \pw\ui\grid\ActionColumn::class,
            'visibleButtons' => [
                'view' => false
            ]
        ],
    ],
]); ?>
