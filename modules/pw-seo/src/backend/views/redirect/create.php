<?php

use pw\ui\form\ActiveForm;
use pw\seo\models\Redirect;

/* @var $this \pw\web\View */
/* @var $model \pw\seo\models\Redirect */
/* @var $form ActiveForm */
?>

<?php $form = ActiveForm::begin([
    'model' => $model,
]); ?>
<?= $form->field($model, 'url_from')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'url_to')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'response_code')->dropDownList(Redirect::getResponseCodes()) ?>
<?= $form->field($model, 'status')->checkbox() ?>
<?php ActiveForm::end(); ?>

