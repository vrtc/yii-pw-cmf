<?php
/* @var $this pw\web\View */
/* @var $model \pw\seo\backend\models\SettingsForm */
/* @var $form pw\ui\form\ActiveForm */

use pw\ui\form\ActiveForm;

?>
<?php $form = ActiveForm::begin([
   'model'=>$model
]); ?>
<legend>Общее</legend>
<?= $form->field($model, 'scriptHeader')->textarea() ?>
<?= $form->field($model, 'scriptFooter')->textarea() ?>
<?= $form->field($model, 'robots')->textarea() ?>
<legend>Главная страница</legend>
<?= $form->field($model, 'titleHome')->textInput() ?>
<?= $form->field($model, 'descriptionHome')->textarea() ?>
<?= $form->field($model, 'keywordsHome')->textarea() ?>

<?php ActiveForm::end(); ?>
