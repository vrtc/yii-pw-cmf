<?php

namespace pw\seo\backend\models;

use pw\seo\models\Redirect;
use pw\seo\models\Seo;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;


class RedirectSearch extends \pw\core\Model
{

    public $status;
    public $response_code;
    public $times_used;
    public $id;
    public $url_from;
    public $url_to;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'response_code', 'times_used', 'id'], 'integer'],
            [['url_from', 'url_to'], 'string']
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Redirect::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC],
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }


        if ($this->status != null) {
            $query->andWhere(['status' => $this->status]);
        }

        if ($this->response_code != null) {
            $query->andWhere(['response_code' => $this->response_code]);
        }

        if ($this->url_from != null) {
            $query->andWhere(['like', 'url_from', $this->url_from]);
        }

        if ($this->url_to != null) {
            $query->andWhere(['like', 'url_to', $this->url_to]);
        }

        if ($this->id != null) {
            $query->andWhere(['id' => $this->id]);
        }

        return $dataProvider;
    }
}
