<?php

namespace pw\seo\backend\models;

use ct\catalog\models\product\Product;
use pw\seo\models\Seo;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;


class SeoSearch extends \pw\core\Model
{

    public $model_class;
    public $model_id;
    public $no_index;
    public $in_sitemap;
    public $no_follow;
    public $id;
    public $keywords;
    public $title;
    public $modelName;
    public $modelTitle;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_index', 'no_follow', 'in_sitemap'], 'integer'],
            [['model_id', 'model_class', 'id'], 'safe'],
            [['keywords', 'title', 'modelName', 'modelTitle'], 'string'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Seo::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->select([
            'b.*',
            'modelName' => $this->getModelName(),
            'modelTitle' => $this->getModelTitle(),
        ]);

        if ($this->model_class != null) {
            $query->andWhere(['model_class' => $this->model_class]);
        }

        if ($this->no_index != null) {
            $query->andWhere(['no_index' => $this->no_index]);
        }

        if ($this->title != null) {
            $query->andWhere(['like', 'title', $this->title]);
        }

        if ($this->keywords != null) {
            $query->andWhere(['like', 'keywords', $this->keywords]);
        }

        if ($this->modelName != null) {
            $query->having(['like', 'modelName', $this->modelName]);
        }

        if ($this->modelTitle != null) {
            $query->andHaving(['like', 'modelTitle', $this->modelTitle]);
        }

        return $dataProvider;
    }

    public function getModelName()
    {
        if ($this->model_class != null) {

            return $this->model_class::find()->where(['id' => new Expression('`b`.`model_id`')])->select('name');
        }
        return null;
    }

    public function getModelTitle()
    {
        if ($this->model_class != null) {

            return $this->model_class::find()->where(['id' => new Expression('`b`.`model_id`')])->select('title');
        }
        return null;
    }
}
