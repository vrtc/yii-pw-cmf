<?php
namespace pw\seo\backend\models;

use DeepCopyTest\Matcher\Y;
use pw\modules\SettingsModel;
use Yii;


class SettingsForm extends SettingsModel
{

    public $scriptHeader;
    public $scriptFooter;
    public $titleHome;
    public $descriptionHome;
    public $keywordsHome;
    public $robots;

    public function rules()
    {
        return [
            [['scriptHeader', 'scriptFooter', 'keywordsHome', 'descriptionHome', 'titleHome', 'robots'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'scriptHeader' => Yii::t('seo', 'Скрипты в начале страницы'),
            'scriptFooter' => Yii::t('seo', 'Скрипты в конце страницы'),
            'titleHome' => Yii::t('seo', 'Titile главной страницы'),
            'descriptionHome' => Yii::t('seo', 'Мета-описание главной страницы'),
            'keywordsHome' => Yii::t('seo', 'Ключевые слова главной страницы'),
            'robots' => Yii::t('seo', 'Robots.txt'),
        ];
    }
}
