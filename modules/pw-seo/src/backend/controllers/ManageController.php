<?php

namespace pw\seo\backend\controllers;

use pw\seo\backend\models\SeoSearch;
use pw\seo\forms\SelectionForm;
use pw\seo\models\Seo;
use Yii;
use pw\web\Controller;
use yii\data\ArrayDataProvider;
use yii\helpers\Json;

class ManageController extends Controller
{

    public function actionIndex()
    {
        $classes = [];
        $seoData = [];
        foreach (Yii::$app->getUrlManager()->getRules() as $rule) {
            if (\property_exists($rule, 'internal') && $rule->internal === false) {
                $classes [] = $rule;
            }
        }

        // var_dump($classes);
        $dataProvider = new ArrayDataProvider([
            'allModels' => $classes,
            'pagination' => [
                'pageSize' => 16,
            ],
        ]);
        $searchModel = new SeoSearch();
        $this->addBreadcrumb(Yii::t('seo', 'SEO'));
        $this->setPageTitle(Yii::t('seo', 'SEO'));
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $route = $this->findRoute($id);
        $selection = new SelectionForm();
        $searchModel = new SeoSearch();
        if (Yii::$app->request->isGet) {

            $params = Yii::$app->request->queryParams;
        }
        $params['SeoSearch']['model_class'] = $route->model;
        $dataProvider = $searchModel->search($params);
        if (Yii::$app->request->isPost) {
            if ($selection->load(Yii::$app->request->post()) && $selection->validate()) {
                if ($selection->action == SelectionForm::ALL) {
                    if ($selection->action == SelectionForm::NOT_SELECTED) {

                        return $this->render('view', [
                            'route' => $route,
                            'selection' => $selection,
                            'searchModel' => $searchModel,
                            'dataProvider' => $dataProvider,
                        ]);
                    }
                    $selection->selection = null;
                    foreach ($dataProvider->query->each() as $model) {
                        $selection->selection[] = $model->model_id;
                    }
                } else {
                    $selection->selection = Json::decode($selection->selection);
                }
                $selection->changeSeo();
                return $this->redirect(['view', 'id' => $id]);
            }
        }

        $this->addBreadcrumb(Yii::t('seo', 'SEO'), ['index']);
        $this->addBreadcrumb($route->name);
        $this->setPageTitle(Yii::t('seo', 'SEO'));
        return $this->render('view', [
            'route' => $route,
            'selection' => $selection,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate($id, $route = false)
    {
        if($route){
            $model = Seo::findOne(['route' => $id]);
        }else{
            $model = Seo::findOne(['id' => $id]);
        }
        if(!$model){
            $model = new Seo();
            if($route){
                $model->route = $id;
                $model->save(false);
            }
        }


        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {

            Yii::$app->session->setFlash('success', Yii::t('seo', 'Изменения сохранены'));
            return $this->redirect(['update', 'id' => $id, 'route' => $route]);
        }

       // $class = $model->model_class::findOne(['id' => $model->model_id]);
        $this->addBreadcrumb(Yii::t('seo', 'SEO'), ['index']);
        $this->addBreadcrumb(Yii::t('seo', 'Редактирование {route}', ['route' => $id]));
     //   $this->addBreadcrumb(Yii::t('seo', '{name}', ['name' => isset($class->name) && ($class->name != '') ? $class->name : $class->title]));
        $this->setPageTitle(Yii::t('seo', 'SEO'));

        return $this->render('form', [
            'model' => $model,
        ]);
    }

    protected function findRoute($route)
    {
        foreach (Yii::$app->getUrlManager()->getRules() as $rule) {
            if (\property_exists($rule, 'internal') && $rule->internal === false && $rule->model === $route) {
                return $rule;
            }
        }
        return null;
    }

}
