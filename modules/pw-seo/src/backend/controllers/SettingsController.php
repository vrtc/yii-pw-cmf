<?php
namespace pw\seo\backend\controllers;

use Yii;
use pw\web\Controller;
use pw\seo\backend\models\SettingsForm;

class SettingsController extends Controller
{

    public function actionIndex()
    {
        $form = new SettingsForm();

        if ($form->load(Yii::$app->request->post()) && $form->save()) {
            Yii::$app->session->setFlash('success', Yii::t('seo', 'Изменения сохранены'));
            return $this->redirect(['index']);
        }
        $this->setPageTitle(Yii::t('seo', 'Найтроки SEO'));
        $this->addBreadcrumb(Yii::t('seo', 'Найтроки SEO'));
        return $this->render('index', ['model' => $form]);
    }
}
