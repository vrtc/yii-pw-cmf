<?php
namespace pw\seo\backend\controllers;

use pw\seo\backend\models\RedirectSearch;
use pw\seo\models\Redirect;
use Yii;
use pw\web\Controller;
use yii\web\NotFoundHttpException;

class RedirectController extends Controller
{
    public function actionIndex()
    {
        $searchModel = new RedirectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new Redirect();
        if($model->load(Yii::$app->request->post()) && $model->validate()){
            $model->save();
            return $this->redirect(['index']);
        }
        return $this->render('create', ['model' => $model]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if($model->load(Yii::$app->request->post()) && $model->validate()){
            $model->save();
            return $this->redirect(['index']);
        }
        return $this->render('create', ['model' => $model]);
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if($model->delete()){
            return $this->redirect(['index']);
        }
    }

    protected function findModel($id)
    {
        $model = Redirect::findOne(['id' => $id]);
        if(!$model){
            throw new NotFoundHttpException('Такой страницы не существует');
        }else{
            return $model;
        }
    }
}
