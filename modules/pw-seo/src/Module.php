<?php

namespace pw\seo;

use pw\seo\handlers\SeoHandlers;
use pw\seo\handlers\RedirectHandlers;
use yii\base\Application;
use yii\base\BootstrapInterface;
use pw\web\View;
use pw\core\db\ActiveRecord;

class Module extends \pw\core\Module implements BootstrapInterface
{

    public $scriptHeader;
    public $scriptFooter;
    public $titleHome;
    public $descriptionHome;
    public $keywordsHome;
    public $robots;

    public function handlers(Application $app): array
    {
        $handlers = [
            [
                'class'    => View::class,
                'event'    => View::EVENT_BEGIN_PAGE,
                'callback' => [SeoHandlers::class, 'registerCode']
            ],
            [
                'class'    => View::class,
                'event'    => View::EVENT_BEGIN_PAGE,
                'callback' => [RedirectHandlers::class, 'registerCode']
            ]
        ];

        foreach ($app->getUrlManager()->getRules() as $rule) {
            if (isset($rule->model) && $rule->model) {
                $handlers[] = [
                    'class'    => $rule->model,
                    'event'    => ActiveRecord::EVENT_INIT,
                    'callback' => function ($event) {
                        $event->sender->attachBehavior('seo', SeoModelBehavior::class);
                    }
                ];
            }
        }

        return $handlers;
    }

    public function getNavigation(): array
    {
        return [
            'content' => [
                [
                    'label' => 'SEO',
                    'url'   => ['/pw-seo/manage/index'],
                    'icon'  => 'award'
                ]
            ],
            'settings' => [
                [
                    'label' => 'SEO настройки',
                    'url'   => ['/pw-seo/settings/index'],
                    'icon'  => 'award'
                ],
                [
                    'label' => 'Редиректы',
                    'url'   => ['/pw-seo/redirect/index'],
                    'icon'  => 'award'
                ]
            ]
        ];
    }

    public function routes(Application $app): array
    {
        return [
            [
                'pattern' => '/robots.txt',
                'route'   => '/pw-seo/default/robots',
                'name'    => self::t('route', 'Robots.txt'),
                'internal' => true
            ],
            [
                'pattern' => '/sitemap.xml',
                'route'   => '/pw-seo/default/sitemap',
                'name'    => 'Яндекс Турбо',
                'internal' => true
            ],
        ];
    }
}
