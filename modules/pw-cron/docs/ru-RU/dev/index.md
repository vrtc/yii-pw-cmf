# Модуль Pw-Cron

Модуль служит для централизованного управления фоновыми заданиями.

## Основные методы компонента
**Получить все задания**
```php
<?php
  Yii::$app->cron->all();
 ?>
```
**Получить все активные задания**
```php
<?php
  Yii::$app->cron->enabled();
 ?>
```
**Получить все задания в ожидании выполнения**
```php
<?php
  Yii::$app->cron->inProgress();
 ?>
```
**Принудительно выполнить команду**
```php
<?php
  Yii::$app->cron->run('module/controller/action');
 ?>
```
**Добавить новую команду в планировщик**
```php
<?php
   Yii::$app->cron->add('module/controller/action',
    [
           'name' => 'My module command',
           'description' => ''
           'expression' => '*/1 * * * * *', //@daily, @monthly
    ]);
?>
```

**Добавление нового задания в планировщик:**
```php
<?php
   Yii::$app->cron->add([
           'user_id' => 1,
           'command' => 'module/controller',
           'name' => 'Hello',
           'description' => '',
           'expression' => '*/1 * * * * *', //@daily, @monthly
           'status' => pw\cron\models\CronJobs::STATUS_ENABLED,
           'progress' => pw\cron\models\CronJobs::PROGRESS_WAITING
    ]);
?>
```
**Удаление задания из планировщика:** (метод также очистит лог удалеяемого задания)

```php
```

**Включить задание:**
```php
<?php
  Yii::$app->cron->enable('module/controller/action');
 ?>
```
**Выключить задание:**
```php
<?php
  Yii::$app->cron->disable('module/controller/action');
 ?>
```
