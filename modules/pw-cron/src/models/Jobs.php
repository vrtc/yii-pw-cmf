<?php
/**
 * @package pw.cron
 * @author Pw team <info@pw-cms.ru>, Kindeev Dmitriy
 * @link http://pw-cms.ru/
 * @copyright Copyright &copy; 2015 PwCMS team
 * @license http://pw-cms.ru/license
 */
namespace pw\cron\models;

use Yii;
use pw\core\db\ActiveRecord;
use Cron\CronExpression;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%pw_cron_jobs}}".
 *
 * @property integer $id
 * @property string $command
 * @property string $expression
 * @property string $name
 * @property string $description
 * @property int $progress_status
 * @property int $is_active
 * @property int $fail_tally
 * @property string $start_time
 * @property string $end_time
 *
 */
class Jobs extends ActiveRecord
{
    /**
     * status success
     */
    const STATUS_DISABLED = 0;
    /**
     * status failed
     */
    const STATUS_ENABLED = 1;

    /**
     * Task ready waiting running
     */
    const PROGRESS_WAITING = 1;

    /**
     * Task in progress
     */
    const PROGRESS_PROCESS = 2;

    /**
     * The task fails
     */
    const PROGRESS_FAILED = 3;

    private $_startTime;

    public static function tableName()
    {
        return '{{%pw_cron_jobs}}';
    }

    public function rules()
    {
        return [
            [['command', 'name', 'description'], 'string'],
            [['expression'], function ($attribute) {
                if (!CronExpression::isValidExpression($this->$attribute)) {
                    $this->addError($attribute, 'This expression is not valid.');
                }
            }],
            [['is_active','progress_status'], 'integer'],
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'start_time',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'end_time',
                ],
                'value' => function () {
                    return new Expression('NOW()');
                }
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('cron', 'ID'),
            'command' => Yii::t('cron', 'Команда'),
            'name' => Yii::t('cron', 'Название'),
            'description' => Yii::t('cron', 'Описание'),
            'expression' => Yii::t('cron', 'Периодичность'),
            'progress_status' => Yii::t('cron', 'Прогресс'),
            'start_time' => Yii::t('cron', 'Время старта'),
            'end_time' => Yii::t('cron', 'Время остановки'),
            'count_fail' => Yii::t('cron', 'Количество отказов'),
            'is_active' => Yii::t('cron', 'Активна'),
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->is_active=== self::STATUS_ENABLED) {
                $this->fail_tally = 0;
            }
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isDue()
    {
        $cron = CronExpression::factory($this->expression);
        $currentDate = new \DateTime();
        $nextDate = new \DateTime($cron->getNextRunDate($this->start_time)->format('Y-m-d H:i:s'));
        return $currentDate >= $nextDate;
    }

    public static function getNameProgress($progress)
    {
        static $array = [
            self::PROGRESS_WAITING => 'Waiting start',
            self::PROGRESS_PROCESS => 'In process',
            self::PROGRESS_FAILED => 'Failed',
        ];

        return $array[$progress] ?? 'unknown';
    }

    /**
     * @return JobsQuery
     */
    public static function find()
    {
        return new JobsQuery(get_called_class());
    }
}
