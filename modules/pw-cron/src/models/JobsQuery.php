<?php
namespace pw\cron\models;

use yii\db\ActiveQuery;

class JobsQuery extends ActiveQuery
{

    public function enabled(bool $state = true)
    {
        return $this->andOnCondition(['is_active' => $state ? Jobs::STATUS_ENABLED : Jobs::STATUS_DISABLED]);
    }

    public function inProgress(bool $state = true)
    {
        if ($state) {
            return $this->andOnCondition(['progress_status' => Jobs::PROGRESS_PROCESS]);
        }
        return $this->andOnCondition(['!=', 'progress_status', Jobs::PROGRESS_PROCESS]);
    }

    public function failed()
    {
        return $this->andOnCondition(['progress_status' => Jobs::PROGRESS_FAILED]);
    }
}
