<?php
/**
 * @package pw.cron
 * @author Pw team <info@pw-cms.ru>, Kindeev Dmitriy
 * @link http://pw-cms.ru/
 * @copyright Copyright &copy; 2015 PwCMS team
 * @license http://pw-cms.ru/license
 */
namespace pw\cron;

use pw\core\Pw;
use pw\core\traits\GetModule;
use Yii;
use pw\cron\models\Jobs;
use pw\core\Component;
use yii\base\InvalidConfigException;
use yii\base\InvalidArgumentException;
use yii\db\Expression;

class CronManager extends Component
{
    use GetModule;

    private $_jobs = [];

    /**
     * @param array $params
     * @return array | \pw\cron\models\Jobs[]
     */
    public function all()
    {
        return Jobs::find()->asArray()->all();
    }

    /**
     * @param bool $state
     * @return \pw\cron\models\Jobs[]
     */
    public function enabled(bool $state = true): array
    {
        return Jobs::find()->enabled($state)->asArray()->all();
    }

    /**
     * @param bool $state
     * @return \pw\cron\models\Jobs[]
     */
    public function inProgress(bool $state = true): array
    {
        return Jobs::find()->inProgress($state)->asArray()->all();
    }

    /**
     * Run cron jobs or $command
     * @param string|null $command
     */
    public function run(string $command = null)
    {
        $jobs = Jobs::find()->enabled()->inProgress(false);

        if ($command) {
            $jobs->andWhere(['command' => $command]);
        }

        foreach ($jobs->each(1) as $job) {
            if ($job->isDue()) {
                try {
                    $job->progress_status = Jobs::PROGRESS_PROCESS;
                    $job->start_time = new Expression('NOW()');
                    $job->update(false, ['progress_status', 'start_time']);
                    Yii::$app->runAction($job->command);
                    $job->progress_status = Jobs::PROGRESS_WAITING;
                    $job->end_time = new Expression('NOW()');
                    $job->update(false, ['progress_status', 'end_time']);
                } catch (\Exception $e) {
                    $job->progress_status = Jobs::PROGRESS_FAILED;
                    $job->fail_tally++;
                    if ($job->fail_tally >= $this->getModule()->maxFails) {
                        $job->is_active = Jobs::STATUS_DISABLED;
                    }
                    $job->save();
                    \Yii::error($e, self::class);
                }
            }
        }
    }

    /**
     * @Example:
     * <code>
     * $cron->add('module/controller/action',
     *  [
     *      'name' => 'Hello',
     *      'description' => '',
     *      'expression' => '* * * * *', //@daily, @monthly
     * ]);
     * </code>
     * @param string $command
     * @param array $params
     * @throws InvalidParamException
     */
    public function add(string $command, array $params)
    {
        if (empty($params['name'])) {
            throw new InvalidArgumentException("Name required for cron job $command ");
        }
        if (empty($params['expression'])) {
            throw new InvalidArgumentException("Expression required for cron job $command ");
        }
        $job = Jobs::find()->where(['command' => $command])->one();
        if (!$job) {
            $job = new Jobs();
            $job->expression = $params['expression'];
            $job->is_active = Jobs::STATUS_ENABLED;
            $job->progress_status = Jobs::PROGRESS_WAITING;
        }
        $job->command = $command;
        $job->name = $params['name'];
        $job->description = $params['description'] ?? '';

        if($cron = $job->save()) {
            return $cron;
        }else{
            return $job->errors;
        }
    }

    /**
     * @param string $command
     * @return bool
     */
    public function disable(string $command): bool
    {
        $job = Jobs::find()->where(['command' => $command])->one();
        if (!$job) {
            throw new InvalidArgumentException("Cron job $command not found");
        }
        $job->is_active = Jobs::STATUS_DISABLED;
        return $job->save(false, ['is_active']);
    }

    /**
     * @param string $command
     * @return bool
     */
    public function enable(string $command): bool
    {
        $job = Jobs::find()->where(['command' => $command])->one();
        if (!$job) {
            throw new InvalidArgumentException("Cron job $command not found");
        }
        $job->is_active = Jobs::STATUS_ENABLED;
        return $job->save(false, ['is_active']);
    }

    /**
     * @param string $command
     * @return bool
     * @throws \yii\base\InvalidParamException
     */
    public function remove(string $command): bool
    {
        $job = Jobs::find()->where(['command' => $command])->one();
        if (!$job) {
            throw new InvalidArgumentException("Cron job $command not found");
        }
        return $job->delete();
    }
}
