<?php
namespace pw\cron\migrations;

use pw\core\db\Migration;

class M170730102809Init extends Migration
{
    public function up()
    {
        $this->createTable('{{%pw_cron_jobs}}', [
            'id'            => $this->bigPrimaryKey()->unsigned(),
            'command'         => $this->string()->notNull(),
            'name'            => $this->string()->notNull(),
            'description'     => $this->string(),
            'expression'      => $this->string()->notNull(),
            'start_time'      => $this->timestamp()->defaultValue(null),
            'end_time'        => $this->timestamp()->defaultValue(null),
            'progress_status' => $this->smallInteger()->notNull(),
            'is_active'       => $this->smallInteger()->notNull(),
            'fail_tally'      => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx_command', '{{%pw_cron_jobs}}', 'command');
        $this->createIndex('idx_status', '{{%pw_cron_jobs}}', ['is_active', 'progress_status']);
    }

    public function down()
    {
        echo "M170730102809Init cannot be reverted.\n";

        return false;
    }

}
