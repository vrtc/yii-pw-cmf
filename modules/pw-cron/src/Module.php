<?php
/**
 * @package   pw.cron
 * @author    Pw team <info@pw-cms.ru>, Kindeev Dmitriy
 * @link      http://pw-cms.ru/
 * @copyright Copyright &copy; 2015 PwCMS team
 * @license   http://pw-cms.ru/license
 */

namespace pw\cron;

use yii\base\Application;
use pw\cron\commands\CronJobsCommands;
use yii\base\BootstrapInterface;

class Module extends \pw\core\Module implements BootstrapInterface
{

    public $maxFails = 10;

    public function components(Application $app): array
    {
        return [
            'cron' => CronManager::class
        ];
    }

    public function commands(Application $app): array
    {
        return [
            'cronjobs' => CronJobsCommands::class
        ];
    }

    public function getNavigation(): array
    {
        return [
            'system' => [
                [
                    'label' => 'Планировщик задач',
                    'url'   => ['/pw-cron/jobs/index'],
                    'icon'  => 'clock'
                ]
            ]
        ];
    }
}
