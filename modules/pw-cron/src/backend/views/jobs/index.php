<?php
/**
 * @var $this \pw\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $searchModel \pw\cron\backend\models\JobsSearch
 */
use pw\ui\grid\GridView;
use \pw\cron\models\Jobs;
use Cron\CronExpression;

?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'model' => $searchModel,
    'columns' => [
        'command',
        'name',
        'description',
        [
            'attribute' => 'expression',
            'content' => function ($model) {
                $cron = CronExpression::factory($model->expression);
                return $model->expression . ' <br> ' . Yii::t('cron', 'Следующий запуск: <br>') . $cron->getNextRunDate($model->start_time)->format('Y-m-d H:i:s');
            }
        ],
        [
            'attribute' => 'progress_status',
            'content' => function ($model) {
                return Jobs::getNameProgress($model->progress_status);
            }
        ],
        'start_time:datetime',
        'end_time:datetime',
        'fail_tally',
        [
            'class' => \pw\ui\grid\ToggleColumn::class,
            'attribute' => 'is_active',
            'afterToggle' => 'function(success,data){
                        if(success){
                            var msg = "' . Yii::t('i18n', 'Язык по умолчанию изменен') . '";
                            toastr.success(msg);
                            jQuery("#currency .toggle[name!=\'"+$self.attr("name")+"\']").bootstrapSwitch("state",false,true);
                        }
                        else
                            toastr.error("Network error");
                    }'
        ],
    ],
]); ?>

<div class="note note-info">
    <h4 class="block">Добавьте эту команду в ваш планировщик заданий (cron):</h4>
    <pre>php <?= Yii::$app->getBasePath() ?>/pw cronjobs/run</pre>
</div>
