<?php
namespace pw\cron\backend\models;

use pw\cron\models\Jobs;
use yii\data\ActiveDataProvider;

class JobsSearch extends Jobs
{
    public function search($params)
    {

        $query = Jobs::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['start_time' => SORT_DESC]]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }


        return $dataProvider;
    }
}
