<?php
namespace pw\cron\backend\controllers;

use pw\cron\backend\models\JobsSearch;
use Yii;
use pw\web\Controller;
use yii\web\NotFoundHttpException;
use pw\ui\grid\actions\ToggleAction;
use pw\cron\models\Jobs;

class JobsController extends Controller
{


        public function actions()
        {
            return [
                'toggle' => [
                    'class' => ToggleAction::class,
                    'modelClass' => Jobs::class,
                ]
            ];
        }

    public function actionIndex()
    {
        $this->view->title = Yii::t('cron', 'Планировщик заданий');
        $searchModel = new JobsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $this->view->title = Yii::t('mailer', 'Редактирование задачи') . ' ' . $model->command;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('mailer', 'Изменения сохранены'));
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('form', [
            'model' => $model,
        ]);
    }

    public function actionToggle(){

    }

    public function findModel($id)
    {
        if (($model = Jobs::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
