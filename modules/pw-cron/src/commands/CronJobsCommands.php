<?php
/**
 * @package pw.cron
 * @author Pw team <info@pw-cms.ru>, Kindeev Dmitriy
 * @link http://pw-cms.ru/
 * @copyright Copyright &copy; 2015 PwCMS team
 * @license http://pw-cms.ru/license
 */
namespace pw\cron\commands;

use Yii;
use yii\console\Controller;

class CronJobsCommands extends Controller
{

    public function actionRun($command = null)
    {
        Yii::$app->cron->run($command);
    }
}
