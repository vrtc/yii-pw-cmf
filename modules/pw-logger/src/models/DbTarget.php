<?php
/**
 * Created by PhpStorm.
 * User: vrtc
 * Date: 30.11.15
 * Time: 12:37
 */
namespace pw\logger\models;
use Yii;
use yii\db\Connection;
use yii\base\InvalidConfigException;
use yii\di\Instance;
use yii\helpers\VarDumper;
use yii\log\Target;

class DbTarget extends \yii\log\DbTarget
{
    /**
     * @var Connection|array|string the DB connection object or the application component ID of the DB connection.
     * After the DbTarget object is created, if you want to change this property, you should only assign it
     * with a DB connection object.
     * Starting from version 2.0.2, this can also be a configuration array for creating the object.
     */
    public $db = 'db';
    /**
     * @var string name of the DB table to store cache content. Defaults to "log".
     */
    public $logTable = '{{%pw_logs}}';

    /*
     * @var string name module
     */
    public $module = 'unknown';

    /*
     * @var string name module
     */
    public $route = 'unknown';


    /**
     * Initializes the DbTarget component.
     * This method will initialize the [[db]] property to make sure it refers to a valid DB connection.
     * @throws InvalidConfigException if [[db]] is invalid.
     */
    public function init()
    {
        parent::init();
        $this->db = Instance::ensure($this->db, Connection::className());
    }

    /**
     * Stores log messages to DB.
     */
    public function export()
    {

        $tableName = $this->db->quoteTableName($this->logTable);

        $sql = "INSERT INTO $tableName ([[level]], [[category]], [[log_time]], [[prefix]], [[message]], [[module]], [[route]])
                VALUES (:level, :category, :log_time, :prefix, :message, :module, :route)";

        $command = $this->db->createCommand($sql);
        foreach ($this->messages as $message) {
            list($text, $level, $category, $timestamp) = $message;
            if (!is_string($text)) {
                // exceptions may not be serializable if in the call stack somewhere is a Closure
                if ($text instanceof \Exception) {
                    $text = (string) $text;
                } else {
                    $text = VarDumper::export($text);
                }
            }
            //var_dump($this->getMessagePrefix($message)); exit;
            try {
                $command->bindValues([
                    ':level' => $level,
                    ':category' => $category,
                    ':log_time' => $timestamp,
                    ':prefix' => $this->getMessagePrefix($message),
                    ':message' => $text,
                    ':module' => \Yii::$app->controller->module->id,
                    ':route' => $this->route,
                ])->execute();
            }catch (\Exception $e){
                var_dump($e->getMessage());
            }

        }
    }
}
