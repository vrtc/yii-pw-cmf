<?php
namespace pw\logger\models;

use Yii;
use pw\core\db\ActiveRecord;


class Logs extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%pw_logs}}';
    }

    public function rules()
    {
        return [
            [['level'], 'integer'],
            [['message'], 'string'],
            [['prefix'], 'string'],
            [['route'], 'string'],
        ];
    }
}