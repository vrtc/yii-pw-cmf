<?php
use yii\helpers\Html;
use yii\helpers\VarDumper;
use yii\log\Logger;
use pw\ui\grid\GridView;
$categories = \pw\logger\models\Logs::find()->groupBy('category')->indexBy('category')->select('category')->asArray()->all();
$categories = \yii\helpers\ArrayHelper::map($categories, 'category', 'category');

?>
<?= GridView::widget([
    'actions' => [
        [
            'url' => ['/pw-logger/manage/clear-all'],
            'label' => 'Очистить лог'
        ]
    ],
    'id'=>'logger',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'tableOptions'=>['class'=>'table table-striped table-bordered table-hover '],
    'columns' => [
        'id',
        [
            'attribute' => 'level',
            'content' => function ($model) {
                return Logger::getLevelName($model->level);
            },
            'filter' => [
                Logger::LEVEL_TRACE => ' Trace ',
                Logger::LEVEL_INFO => ' Info ',
                Logger::LEVEL_WARNING => ' Warning ',
                Logger::LEVEL_ERROR => ' Error ',
            ],
        ],
        [
            'attribute' => 'category',
            'filter' => $categories,
        ],
        'log_time:datetime',
        'prefix',
        'module',
        [
            'attribute' => 'route',
            'filter' => [
                'frontend' => 'Frontend',
                'backend' => 'Backend',
            ],
        ],
        [
            'class' => \pw\ui\grid\ActionColumn::class,
            'visibleButtons' => ['update' => false],
        ],
    ],
]); ?>
