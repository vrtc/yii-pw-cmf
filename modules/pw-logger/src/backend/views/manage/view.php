<?php
/**
 * Created by new_trisport.
 * User: Dmitriy Kindeev
 * Email: dkindeev@mail.ru
 * Date: 22.07.2019
 * Time: 14:04
 */

use yii\helpers\Html;
use yii\widgets\DetailView; ?>
<?php echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        'level',
        'category',
        [
            'attribute' => 'log_time',
            'format' => 'datetime',
            'value' => (int)$model->log_time,
        ],
        'prefix:ntext',
        [
            'attribute' => 'message',
            'format' => 'raw',
            'value' => Html::tag('pre', $model->message, ['style' => 'white-space: pre-wrap']),
        ],
    ],
]) ?>
