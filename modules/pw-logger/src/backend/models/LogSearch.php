<?php
namespace pw\logger\backend\models;

use pw\logger\models\Logs;
use yii\data\ActiveDataProvider;

class LogSearch extends Logs
{
    public function rules()
    {
        return [
            [['category', 'prefix', 'level', 'message', 'route'], 'string']
        ];
    }

    public function search($params)
    {

        $query = Logs::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['log_time' => SORT_DESC]]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'level' => $this->level,
        ]);
        $query->andFilterWhere([
            'category' => $this->category,
        ]);
        $query->andFilterWhere([
            'like', 'message', $this->message
        ]);
        $query->andFilterWhere([
            'like', 'prefix', $this->prefix
        ]);
        $query->andFilterWhere([
            'like', 'route', $this->route
        ]);


        return $dataProvider;
    }
}
