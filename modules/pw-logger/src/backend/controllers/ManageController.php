<?php

namespace pw\logger\backend\controllers;

use pw\logger\backend\models\LogSearch;
use pw\logger\models\Logs;
use pw\menu\models\Menu;
use Yii;
use pw\web\Controller;
use yii\web\NotFoundHttpException;

class ManageController extends Controller
{

    public function actionIndex()
    {
        $this->addBreadcrumb(Yii::t('mailer', 'Логгирование'));
        $this->setPageTitle(Yii::t('mailer', 'Логгирование'));
        $searchModel = new LogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {

        $model = $this->findModel($id);
        $this->addBreadcrumb(Yii::t('mailer', 'Логгирование'), ['index']);
        $this->addBreadcrumb(Yii::t('mailer/view', 'Лог ID: ' . $id));
        $this->setPageTitle(Yii::t('mailer', 'Лог ID: ' . $id));

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model->delete()) {
            Yii::$app->session->setFlash('success', Yii::t('menu', 'Изменения сохранены'));
            return $this->redirect(['index']);
        }
        return $this->redirect(['index']);
    }

    public function actionClearAll()
    {
        $model = Logs::deleteAll();
        Yii::$app->session->setFlash('success', 'Удалено записей: ' . $model);
        $this->redirect(['manage/index']);
    }
    protected function findModel($id)
    {
        if (($model = Logs::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
