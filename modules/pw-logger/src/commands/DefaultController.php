<?php
/**
 * Created by new_trisport.
 * User: Dmitriy Kindeev
 * Email: dkindeev@mail.ru
 * Date: 23.07.2019
 * Time: 11:54
 */
namespace pw\logger\commands;
use pw\console\Controller;
use pw\logger\models\Logs;
use yii\helpers\Console;

class DefaultController extends Controller
{

    public function actionClearOld($day = 10){

        $result =  Logs::deleteAll('FROM_UNIXTIME(log_time) <= DATE_SUB(NOW(), INTERVAL ' . (int)$day . ' DAY);');
        Console::output('Удалено: ' . $result);
    }
}
