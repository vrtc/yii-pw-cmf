<?php

namespace pw\logger;

use pw\core\traits\Settings;
use pw\logger\models\DbTarget;
use pw\logger\models\EmailTarget;
use yii\base\Application;
use yii\base\BootstrapInterface;
use yii\helpers\Json;
use yii\log\FileTarget;

class Module extends \pw\core\Module implements BootstrapInterface
{
    public $scriptHeader;
    public $scriptFooter;
    public $levels = ["error", "warning"];
    public $logVars;
    public $emailTarget = false;

    /**
     * @return array
     */
    public function getNavigation(): array
    {
        return [
            'system' => [
                [
                    'label' => 'Логи',
                    'url' => ['/pw-logger/manage/index'],
                    'icon' => 'toolbox'
                ]
            ]
        ];
    }

    /**
     * @param Application $app
     * @return array
     */
    public function bootstraps(Application $app): array
    {
        if (YII_DEBUG && !\Yii::$app->request->isConsoleRequest) {
            return [
                'log',
                'debug'
            ];
        }
        return [
            'log'
        ];
    }

    /**
     * @param Application $app
     * @return array
     */
    public function modules(Application $app): array
    {
        if (YII_DEBUG && !\Yii::$app->request->isConsoleRequest) {
            if(env('DEBUG_ALLOWED')) {
                $allowed = explode(',', env('DEBUG_ALLOWED'));
            }else{
                $allowed = ['127.0.0.1', '::1', '192.168.33.1', '172.17.42.1', '172.17.0.1', '192.168.99.1'];
            }
            return [
                'debug' => [
                    'class' => \yii\debug\Module::class,
                    'traceLine' => '<a href="phpstorm://open?url=file://{file}&line={line}">{file}:{line}</a>',
                    'allowedIPs' => $allowed,
                ]

            ];
        }
        return [];
    }

    /**
     * @param Application $app
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function components(Application $app): array
    {
        $conf = \Yii::$app->getModulesManager()->getSettings('pw-logger');
        if($conf) {
            return [
                'log' => [
                    'class' => 'yii\log\Dispatcher',
                    'traceLevel' => YII_DEBUG ? 3 : 0,
                    'targets' => [
                        [
                            'class' => DbTarget::class,
                            'enabled' => true,
                            'levels' => $conf['levels'],
                            'except' => [
                               // 'yii\db\*',
                                'yii\web\HttpException:*',
                                'yii\i18n\I18N\*',
                                'yii\debug\*',
                            //    'yii\log\*',
                            ],
                            'route' => !\Yii::$app->request->isConsoleRequest ? sprintf('[%s][%s]', \Yii::$app->id, \Yii::$app->request->getUrl()) : null,
                            'prefix' => function () {
                                $url = !\Yii::$app->request->isConsoleRequest ? \Yii::$app->request->getUrl() : \Yii::$app->controller->module->id;
                                return sprintf('[%s][%s]', \Yii::$app->id, $url);
                            },
                           // 'logVars' => $conf['logVars'] != null ? $conf['logVars'] : '',
                        ],
                        [
                            'class' => FileTarget::class,
                            'enabled' => env('DEBUG'),
                            'levels' => ["error", "warning", 'info'],
                            /*'except' => [
                                'yii\db\*',
                                'yii\web\HttpException:*',
                                'yii\i18n\I18N\*'
                            ],*/
                            'prefix' => function () {
                                $url = !\Yii::$app->request->isConsoleRequest ? \Yii::$app->request->getUrl() : null;
                                return sprintf('[%s][%s]', \Yii::$app->id, $url);
                            },
                        ],
                        [
                            'class' => EmailTarget::class,
                            'enabled' => false,
                            'levels' => $conf['levels'],
                            'except' => ['yii\web\HttpException:*', 'yii\i18n\I18N\*'],
                            'message' => [
                                'to' => 'i@yar.pw',
                            ],
                            'prefix' => '',
                            'logVars' => $conf['logVars'] != null ? $conf['logVars'] : '',
                        ],

                    ],
                ]
            ];
        }else{
            return [];
        }
    }
}
