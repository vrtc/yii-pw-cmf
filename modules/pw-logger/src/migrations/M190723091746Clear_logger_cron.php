<?php

namespace pw\logger\migrations;

use pw\core\db\Migration;
use Yii;

class M190723091746Clear_logger_cron extends Migration
{
    public function up()
    {
        Yii::$app->cron->add('pw-logger/default/clear-old',
            [
                'name' => 'Logs clear',
                'description' => '',
                'expression' => '@daily', //@daily, @monthly
            ]);
    }

    public function down()
    {
        echo "M190723091746Clear_logger_cron cannot be reverted.\n";

        return false;
    }

}
