<?php
namespace pw\logger\migrations;
use pw\i18n\db\Migration;

class m151130_121057_add_logs_table extends Migration
{
    public function up()
    {
        $this->createTable('{{%pw_logs}}', [
            'id' => $this->bigPrimaryKey(),
            'level' => $this->integer(),
            'category' => $this->string(),
            'log_time' => $this->double(),
            'prefix' => $this->text(),
            'message' => $this->text(),
            'module' => $this->text(),
            'route' => $this->text(),
        ], $this->tableOptions);

        $this->createIndex('idx_log_level', '{{%pw_logs}}', 'level');
        $this->createIndex('idx_log_category', '{{%pw_logs}}', 'category');
    }

    public function down()
    {
        $this->dropTable('{{%pw_logs}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
