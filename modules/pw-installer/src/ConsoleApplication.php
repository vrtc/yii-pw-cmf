<?php
namespace pw\installer;

use pw\rbac\AuthManager;
use pw\console\commands\MigrateController;
use pw\cron\CronManager;
use pw\modules\ModulesManager;
use pw\installer\commands\InstallController;
use pw\web\UrlManager;
use yii\console\controllers\ServeController;
use yii\web\User;
use Yii;

class ConsoleApplication extends \pw\console\Application
{

    public function coreCommands()
    {
        return [
            'installer' => InstallController::class,
            'migrate'   => [
                'class'               => MigrateController::class,
                'migrationNamespaces' => $this->_migrationNamespaces
            ],
            'serve' => ServeController::class,
        ];
    }

    public function coreComponents()
    {
        return array_merge(
            parent::coreComponents(),
            [
                'authManager' => ['class' => AuthManager::class],
                'modulesManager' => ['class' => ModulesManager::class],
                'urlManager' => ['class' => UrlManager::class],
                'user' => ['class' => User::class],
                'cron' => ['class' => CronManager::class],
            ]
        );
    }

    /**
     * Returns the modules manager component.
     * @return \pw\modules\Manager the modules manager.
     */
    public function getModulesManager()
    {
        return $this->get('modulesManager');
    }

    public function getCron()
    {
        return $this->get('cron');
    }
}
