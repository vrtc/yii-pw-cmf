<?php
namespace pw\installer;

use yii\base\Application;
use yii\base\BootstrapInterface;
use pw\installer\commands\InstallController;

class Module extends \pw\core\Module implements BootstrapInterface
{
    public function commands(Application $app):array
    {
        return [
            'installer' => InstallController::class
        ];
    }
}
