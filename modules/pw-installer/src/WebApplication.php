<?php
namespace pw\installer;

use Yii;
use pw\core\models\Modules;
use pw\core\models\I18NLanguages;
use yii\helpers\FileHelper;
use yii\helpers\Url;

class WebApplication extends \pw\web\Application
{
    public $id = 'web-installer';

    public $name = 'Pw Installer';

    public $layout = '//main';

    public $theme;

    public $errorAction;

    public $baseUrl;

    protected $events = [];


    /**
     * @inheritdoc
     */
    protected function bootstrap()
    {
        $this->bootstrap[] = ['class' => \pw\installer\Bootstrap::class];
        $modules = [];
        foreach (Modules::find()->enabled()->asArray()->all() as $module) {
            if (!empty($module['class']))
                $modules[$module['key']]['class'] = $module['class'];
            if ($module['bootstrap']) {
                $this->bootstrap[] = ['class' => $module['bootstrap']];
            }
        }
        $this->setModules([
            'installer' => [
                'class' => \pw\installer\Module::class
            ]
        ]);
        parent::bootstrap();

    }

    public function init()
    {
        parent::init();

        $config = Yii::$app->settings->get($this->id);
        Yii::configure($this, $config);

        $this->view->theme = $this->theme;
        $this->errorHandler->errorAction = $this->errorAction;

        $request = $this->getRequest();

        Yii::setAlias('@webroot', dirname($request->getScriptFile()));
        Yii::setAlias('@web', $request->getBaseUrl());
        Yii::setAlias('@theme', $this->view->theme->getBasePath());
        Yii::setAlias('@themes', realpath(Yii::getAlias('@webroot') . '/../themes'));
    }

    /**
     * @inheritdoc
     */
    public function coreComponents()
    {
        return array_merge(parent::coreComponents(), [
            'view' => ['class' => 'pw\web\View'],
            'response' => ['class' => 'yii\web\Response'],
            'session' => ['class' => 'yii\web\DbSession', 'sessionTable' => '{{%Sessions}}'],
            'user' => ['class' => 'yii\web\User'],
            'authManager' => ['class' => 'pw\auth\AuthManager'],
            'errorHandler' => ['class' => 'yii\web\ErrorHandler'],
        ]);
    }

    /**
     * Returns the modules manager component.
     * @return \pw\modules\Manager the modules manager.
     * @throws \yii\base\InvalidConfigException
     */
    public function getModulesManager()
    {
        return $this->get('modulesManager');
    }

    /**
     * Returns the modules manager component.
     * @return \pw\themes\Manager the modules manager.
     * @throws \yii\base\InvalidConfigException
     */
    public function getThemesManager()
    {
        return $this->get('themesManager');
    }

    /**
     * @inheritdoc
     */
    public function trigger($name, \yii\base\Event $event = null)
    {
        if ($event === null && $name instanceof \yii\base\Event && $name->name) {
            $event = $name;
            $name = $event->name;
        }
        parent::trigger($name, $event);
    }
}
