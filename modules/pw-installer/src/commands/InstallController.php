<?php

namespace pw\installer\commands;

use pw\core\Pw;
use pw\i18n\db\Connection;
use pw\modules\ModulesManager;
use pw\modules\models\Modules;
use pw\web\models\SettingsModel;
use Yii;
use yii\console\ExitCode;
use yii\db\Exception;
use yii\helpers\Console;
use pw\console\Controller;
use pw\modules\BaseInstaller;
use pw\modules\InstallerException;
use yii\helpers\VarDumper;

class InstallController extends Controller
{

    public $defaultAction = 'install';

    protected $coreModules = [
        'pw/i18n',
        'pw/console',
        'pw/menu',
        'pw/cron',
        'pw/modules',
        'pw/mailer',
        'pw/rbac',
        'pw/web',
        'pw/reference',
        'pw/filemanager',
        'pw/users'
    ];

    private $_installers = [];

    /**
     * Install system
     * @throws \pw\modules\InstallerException
     */
    public function actionInstall($module = null)
    {
        //установка обязательных модулей
        $this->installCore();

        //установка других модулей
        $this->runModulesInstaller(BaseInstaller::COMMAND_INSTALL, $module);

        if ($module === null) {
            Yii::$app->getModulesManager()->load();
            $settingsModel = new SettingsModel();

            $themes = Yii::$app->getModulesManager()->themes()->installed();
            $backend = $frontend = [];
            foreach ($themes as $theme) {
                if ($theme['type'] === Modules::TYPE_THEME_BACKEND) {
                    $backend[] = $theme;
                } elseif ($theme['type'] === Modules::TYPE_THEME_FRONTEND) {
                    $frontend[] = $theme;
                }
            }
            if ($backend) {
                $this->stdout('Установленные темы для backend:' . PHP_EOL);
                $i = 1;
                foreach ($backend as $theme) {
                    $this->stdout($i . ') - ' . $theme['name'] . PHP_EOL);
                    $i++;
                }
                $selected = (int)$this->select('Выберите тему', \array_fill(1, \count($backend), true));
                $settingsModel->backendTheme = $backend[$selected - 1]['key'];
            }
            if ($frontend) {
                $this->stdout('Установленные темы для frontend:' . PHP_EOL);
                $i = 1;
                foreach ($frontend as $theme) {
                    $this->stdout($i . ') - ' . $theme['name'] . PHP_EOL);
                    $i++;
                }
                $selected = (int)$this->select('Выберите тему', \array_fill(1, \count($frontend), true));
                $settingsModel->frontendTheme = $frontend[$selected - 1]['key'];
            }
            $settingsModel->save();
            file_put_contents(Yii::getAlias('@app/.env'), "INSTALLED=true\n", FILE_APPEND);
        }
    }

    /**
     * Reinstall system
     * @throws \pw\modules\InstallerException
     */
    public function actionReInstall($module)
    {
        //Yii::$app->runAction('migrate/init');
        $this->installCore();

        $this->runModulesInstaller(BaseInstaller::COMMAND_REINSTALL, $module);

        if ($module === null) {
            Yii::$app->getModulesManager()->load();
            $settingsModel = new SettingsModel();

            $themes = Yii::$app->getModulesManager()->themes()->installed();
            $backend = $frontend = [];
            foreach ($themes as $theme) {
                if ($theme['type'] === Modules::TYPE_THEME_BACKEND) {
                    $backend[] = $theme;
                } elseif ($theme['type'] === Modules::TYPE_THEME_FRONTEND) {
                    $frontend[] = $theme;
                }
            }
            if ($backend) {
                $this->stdout('Установленные темы для backend:' . PHP_EOL);
                $i = 1;
                foreach ($backend as $theme) {
                    $this->stdout($i . ') - ' . $theme['name'] . PHP_EOL);
                    $i++;
                }
                $selected = (int)$this->select('Выберите тему', \array_fill(1, \count($backend), true));
                $settingsModel->backendTheme = $backend[$selected - 1]['key'];
            }
            if ($frontend) {
                $this->stdout('Установленные темы для frontend:' . PHP_EOL);
                $i = 1;
                foreach ($frontend as $theme) {
                    $this->stdout($i . ') - ' . $theme['name'] . PHP_EOL);
                    $i++;
                }
                $selected = (int)$this->select('Выберите тему', \array_fill(1, \count($frontend), true));
                $settingsModel->frontendTheme = $frontend[$selected - 1]['key'];
            }
            $settingsModel->save();
            file_put_contents(Yii::getAlias('@app/.env'), "INSTALLED=true\n", FILE_APPEND);
        }
    }

    protected function runModulesInstaller($command, $module = null)
    {

        if ($module !== null) {
            if ($command == BaseInstaller::COMMAND_UPDATE) {
                Yii::$app->getModulesManager()->update($module);
            }
            if ($command == BaseInstaller::COMMAND_INSTALL) {
                Yii::$app->getModulesManager()->install($module);
            }
            if ($command == BaseInstaller::COMMAND_MIGRATE) {
                Yii::$app->getModulesManager()->migrate($module);
            }
            if ($command == BaseInstaller::COMMAND_REINSTALL) {
                Yii::$app->getModulesManager()->reInstall($module);
            }
        } else {
            $modules = Yii::$app->getModulesManager()->notInstalled();
            $cntModules = count($modules);
            if ($cntModules) {
                $prefix = 'Modules installation progress: ';
                Console::startProgress(0, $cntModules, $prefix);
                $i = 0;
              //  var_dump($modules);exit;
                foreach ($modules as $module => $params) {
                        Yii::$app->getModulesManager()->install($module);

                    $i++;
                    Console::updateProgress($i, $cntModules, $prefix . $module);
                }
                Console::endProgress();
            }
        }
    }

    /**
     * Update system
     *
     * @param string|null $extension
     *
     * @return int
     */
    public function actionUpdate($extension = null)
    {
        $this->installCore();
        $this->runModulesInstaller(BaseInstaller::COMMAND_UPDATE, $extension);
    }


    /**
     * Run migrations from extension
     *
     * @param string $extension
     *
     * @return void
     */
    public function actionMigrate($extension)
    {
        if ($extension == null) {
            throw new InstallerException("empty Module");
        }
        $this->runModulesInstaller(BaseInstaller::COMMAND_MIGRATE, $extension);
    }

    private function checkDatabase()
    {
        if (getenv('DB_HOST') && getenv('DB_USER') && getenv('DB_PASS') && getenv('DB_NAME')) {
            /**
             * @var $db \yii\db\Connection;
             */
            $db = Yii::createObject([
                'class' => Connection::class,
                'dsn' => 'mysql:host=' . getenv('DB_HOST') . ';dbname=' . getenv('DB_NAME'),
                'username' => getenv('DB_USER'),
                'password' => getenv('DB_PASS'),
                'charset' => 'utf8',
                'tablePrefix' => getenv('DB_PREFIX'),
            ]);
            $db->open();
            Yii::$app->set('db', $db);

            return true;
        }

        return false;
    }


    private function configureDatabase()
    {
        $config = '';
        $host = $this->prompt('Enter database host:', [
            'required' => true,
            'default' => 'localhost'
        ]);
        $config .= 'DB_HOST=' . $host . "\n";
        putenv('DB_HOST=' . $host);
        $user = $this->prompt('Enter database user:', [
            'required' => true,
        ]);
        $config .= 'DB_USER=' . $user . "\n";
        putenv('DB_USER=' . $user);
        $pass = $this->prompt('Enter password:', [
            'required' => true,
        ]);
        $config .= 'DB_PASS=' . $pass . "\n";
        putenv('DB_PASS=' . $pass);
        $dbname = $this->prompt('Enter database name:', [
            'required' => true
        ]);
        $config .= 'DB_NAME=' . $dbname . "\n";
        putenv('DB_NAME=' . $dbname);
        $prefix = $this->prompt('Enter table prefix:');
        if ($prefix) {
            $prefix = substr($prefix, -1) === '_' ? $prefix : $prefix . '_';
            $config .= 'DB_PREFIX=' . $prefix . "\n";
            putenv('DB_PREFIX=' . $prefix);
        }
        if ($this->checkDatabase()) {
            file_put_contents(Yii::getAlias('@app/.env'), $config);
        }
    }

    private function checkCookieValidationKey()
    {
        return !empty(getenv('COOKIE_VALIDATION_KEY'));
    }


    private function configurureCookieValidationKey()
    {
        $key = Yii::$app->getSecurity()->generateRandomString();
        putenv('COOKIE_VALIDATION_KEY=' . $key);
        file_put_contents(Yii::getAlias('@app/.env'), 'COOKIE_VALIDATION_KEY=' . $key . "\n", FILE_APPEND);
    }

    private function installCore()
    {
        try {
            !$this->checkDatabase() && $this->configureDatabase();
            !$this->checkCookieValidationKey() && $this->configurureCookieValidationKey();
        } catch (Exception $e) {

            echo $this->stdout($e->getMessage());

            return ExitCode::UNSPECIFIED_ERROR;
        }
        if (!getenv('INSTALLED')) {

            $cntCoreModules = count($this->coreModules);
            $prefix = 'Core modules installation progress: ';
            Console::startProgress(0, $cntCoreModules, $prefix);
            $i = 0;

            foreach ($this->coreModules as $coreModule) {
                Yii::$app->getModulesManager()->install($coreModule, true);
                $i++;
                Console::updateProgress($i / 2, $cntCoreModules, $prefix . $coreModule);

            }
            foreach ($this->coreModules as $coreModule) {
                Yii::$app->getModulesManager()->install($coreModule, false, true);
                $i++;
                Console::updateProgress($i / 2, $cntCoreModules, $prefix . $coreModule);
            }
            Console::endProgress();
        }
    }
}
