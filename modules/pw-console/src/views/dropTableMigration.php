<?php
/* @var $className string the new migration class name */
/* @var $table string the name table */
/* @var $fields array the fields */

echo "<?php\n";
if (!empty($namespace)) {
    echo "\nnamespace {$namespace};\n";
}
?>

use pw\core\db\Migration;

class <?= $className ?> extends Migration
{
    public function up()
    {
        $this->dropTable('<?= $table ?>');
    }

    public function down()
    {
        $this->createTable('<?= $table ?>', [
<?php foreach ($fields as $field): ?>
<?php if ($field == end($fields)): ?>
            '<?= $field['property'] ?>' => $this-><?= $field['decorators'] . "\n"?>
<?php else: ?>
            '<?= $field['property'] ?>' => $this-><?= $field['decorators'] . ",\n"?>
<?php endif; ?>
<?php endforeach; ?>
        ]);
    }
}
