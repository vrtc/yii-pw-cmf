<?php

namespace pw\console\commands;

use Yii;
use yii\db\Connection;
use yii\di\Instance;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;

class MigrateController extends \yii\console\controllers\MigrateController
{

    /**
     * @inheritdoc
     */
    public $migrationTable = '{{%migrations}}';
    public $module_id = null;
    /**
     * @inheritdoc
     */
    public $templateFile = '@pw-console/views/migration.php';



    public function options($actionID)
    {
        return array_merge(
            parent::options($actionID),
            ['module_id']
        );
    }


    protected function createMigrationHistoryTable()
    {
        $tableName = $this->migrationTable;
        $this->stdout("Creating migration history table \"$tableName\"...", Console::FG_YELLOW);
        $this->db->createCommand()->createTable($this->migrationTable, [
            'version' => 'varchar(' . static::MAX_NAME_LENGTH . ') NOT NULL PRIMARY KEY',
            'module_id' => 'varchar(255)',
            'apply_time' => 'integer',
        ])->execute();
        $this->db->createCommand()->insert($this->migrationTable, [
            'version' => self::BASE_MIGRATION,
            'apply_time' => time(),
        ])->execute();
        $this->stdout("Done.\n", Console::FG_GREEN);
    }

    protected function getMigrationHistory($limit = null)
    {
        if ($this->db->schema->getTableSchema($this->migrationTable, true) === null) {
            $this->createMigrationHistoryTable();
        }
        if ($this->module_id != null) {

            $query = (new Query())
                ->select(['version', 'apply_time'])
                ->from($this->migrationTable)
                ->where(['module_id' => $this->module_id])
                ->orderBy(['apply_time' => SORT_DESC, 'version' => SORT_DESC]);
        } else {

            $query = (new Query())
                ->select(['version', 'apply_time'])
                ->from($this->migrationTable)
                ->orderBy(['apply_time' => SORT_DESC, 'version' => SORT_DESC]);
        }

        if (empty($this->migrationNamespaces)) {
            $query->limit($limit);
            $rows = $query->all($this->db);
            $history = ArrayHelper::map($rows, 'version', 'apply_time');
            unset($history[self::BASE_MIGRATION]);
            return $history;
        }

        $rows = $query->all($this->db);

        $history = [];
        foreach ($rows as $key => $row) {
            if ($row['version'] === self::BASE_MIGRATION) {
                continue;
            }
            if (preg_match('/m?(\d{6}_?\d{6})(\D.*)?$/is', $row['version'], $matches)) {
                $time = str_replace('_', '', $matches[1]);
                $row['canonicalVersion'] = $time;
            } else {
                $row['canonicalVersion'] = $row['version'];
            }
            $row['apply_time'] = (int)$row['apply_time'];
            $history[] = $row;
        }

        usort($history, function ($a, $b) {
            if ($a['apply_time'] === $b['apply_time']) {
                if (($compareResult = strcasecmp($b['canonicalVersion'], $a['canonicalVersion'])) !== 0) {
                    return $compareResult;
                }

                return strcasecmp($b['version'], $a['version']);
            }

            return ($a['apply_time'] > $b['apply_time']) ? -1 : +1;
        });

        $history = array_slice($history, 0, $limit);

        $history = ArrayHelper::map($history, 'version', 'apply_time');

        return $history;
    }

    protected function addMigrationHistory($version)
    {
        $command = $this->db->createCommand();
        $command->insert($this->migrationTable, [
            'version' => $version,
            'module_id' => $this->module_id,
            'apply_time' => time(),
        ])->execute();
    }

    public function actionDownModule()
    {
        foreach ($this->getMigrationHistory(null) as $name => $time) {
            $migration = $this->createMigration($name);
            if ($migration->down() !== false) {

                $this->removeMigrationHistory($name);
            } else {
                return false;
            }
        }
        return true;
    }
}
