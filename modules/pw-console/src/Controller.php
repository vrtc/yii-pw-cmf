<?php
namespace pw\console;

use Yii;

class Controller extends \yii\console\Controller
{

    public function getModule()
    {
        return Yii::$app->getModule($this->module);
    }
}
