<?php

namespace pw\console;

use pw\console\commands\MigrateController;
use pw\modules\ModulesManager;
use pw\i18n\db\Connection;
use pw\installer\commands\InstallController;
use Yii;
use yii\caching\FileCache;
use yii\helpers\ArrayHelper;

class Application extends \yii\console\Application
{
    protected $models = [];
    private $_vendorPath;
    protected $_migrationNamespaces = [];
    public $settings;

    /**
     * @inheritdoc
     */
    public function __construct($config = [])
    {
        $defaultConfig = [
            'components' => [
                'db' => [
                    'class'               => Connection::class,
                    'dsn'                 => 'mysql:host=' . getenv('DB_HOST') . ';dbname=' . getenv('DB_NAME'),
                    'username'            => env('DB_USER'),
                    'password'            => env('DB_PASS'),
                    'charset'             => 'utf8',
                    'tablePrefix'         => env('DB_PREFIX'),
                    'enableSchemaCache'   => env('DEBUG'),
                    'schemaCacheDuration' => 3600,
                    'schemaCache'         => 'cache',
                    'enableLogging'       => env('DEBUG'),
                    'enableProfiling'     => env('DEBUG'),
                ]
            ]
        ];
        parent::__construct(
            ArrayHelper::merge($config, $defaultConfig)
        );
    }

    public function init()
    {
        $basePath = $this->getBasePath();
        $this->settings = Yii::$app->getModulesManager()->installed();
        Yii::setAlias('@themes', $basePath . DIRECTORY_SEPARATOR . 'themes');
        Yii::setAlias('@modules', Yii::$app->basePath . DIRECTORY_SEPARATOR . 'modules');
        Yii::setAlias('@pw-console', Yii::getAlias('@modules'). DIRECTORY_SEPARATOR . 'pw-console/src');
        $this->getModulesManager()->load();

        foreach ($this->getModulesManager()->installed() as $module) {
            $this->_migrationNamespaces [] = $module['namespace'] . '\migrations';
        }

        parent::init();

        Yii::setAlias('@root', $basePath . DIRECTORY_SEPARATOR );
        Yii::setAlias('@webroot', $basePath . DIRECTORY_SEPARATOR . 'web');

    }


    /**
     * @inheritdoc
     */
    public function coreCommands()
    {
        return array_merge(
            parent::coreCommands(),
            [
                'installer' => InstallController::class,
                'migrate'   => [
                    'class'               => MigrateController::class,
                    'migrationNamespaces' => $this->_migrationNamespaces,
                ]
            ]
        );
    }

    /**
     * @return array
     */
    public function coreComponents()
    {
        return array_merge(
            parent::coreComponents(),
            [
                'modulesManager' => ['class' => ModulesManager::class],
                'cache'             => ['class' => FileCache::class],
                'log'               => [
                    'class'      => 'yii\log\Dispatcher',
                    'traceLevel' => 3,
                    'targets'    => [
                        [
                            'class'  => 'yii\log\FileTarget',
                            'levels' => ['error', 'warning'],
                        ],
                    ],
                ],
            ]
        );
    }

    /**
     * Returns the modules manager component.
     * @return \pw\modules\ModulesManager the modules manager.
     */
    public function getModulesManager()
    {
        return $this->get('modulesManager');
    }

    /**
     * @param $models
     */
    public function registerModels($models)
    {
        $this->models = array_merge($this->models, $models);
    }

    /**
     * @inheritdoc
     */
    public function setVendorPath($path)
    {
        $this->_vendorPath = Yii::getAlias($path);
        Yii::setAlias('@vendor', $this->_vendorPath);
        Yii::setAlias('@bower', $this->_vendorPath . DIRECTORY_SEPARATOR . 'bower-asset');
        Yii::setAlias('@npm', $this->_vendorPath . DIRECTORY_SEPARATOR . 'npm-asset');
    }

    public function isBackend()
    {
        return $this->id === 'backend';
    }

    public function isFrontend()
    {
        return $this->id === 'frontend';
    }
}
