<?php
namespace pw\console;

class Installer extends \pw\modules\ModuleInstaller
{

    public function getVersion(): string
    {
        return '0.1';
    }

    public function getAuthors(): array
    {
        return ['i@yar.pw'];
    }

    public function getLink(): string
    {
        return 'http://yar.pw';
    }

    public function getName(): string
    {
        return 'Console';
    }

    public function getSourceLanguage(): string
    {
        return 'ru-RU';
    }
}
