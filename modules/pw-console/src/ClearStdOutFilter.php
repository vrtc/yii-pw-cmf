<?php
namespace pw\console;

class ClearStdOutFilter extends \php_user_filter
{

    public function filter($in, $out, &$consumed, $closing)
    {
        while ($bucket = stream_bucket_make_writeable($in)) {}
        return PSFS_PASS_ON;
    }
}
