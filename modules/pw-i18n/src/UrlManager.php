<?php

namespace pw\i18n;

use pw\core\traits\GetModule;
use Yii;
use yii\helpers\Url;
use yii\web\Cookie;
use pw\i18n\models\Languages;
use yii\web\Request;

class UrlManager extends \pw\web\UrlManager
{

    use GetModule;

    public $enableLanguageDetection = true;

    public $enableDefaultLanguageUrlCode = false;

    public $languageCookieName = '_lang';

    public $languageCookieDuration = 2592000;

    protected $_request;
    private $_languages = [];
    private $_defaultLanguage;


    public function init()
    {

        $languages = Yii::$app->i18n->getLanguages();
        foreach ($languages as $language) {
            $this->_languages[$language['locale']] = $language['slug'];
            if ($language['default']) {
                $this->_defaultLanguage = $language['locale'];
            }
        }
        if (count($languages) < 2) {
            $this->enableLocaleUrls = false;
        }
        Yii::$app->language = $this->_defaultLanguage;
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function parseRequest($request)
    {
        if ($this->enableLocaleUrls && $this->_languages) {
            $this->processLocaleUrl($request);
        }

        return parent::parseRequest($request);
    }

    /**
     * @param $request Request
     */
    protected function processLocaleUrl($request)
    {
        $this->_request = $request;
        $pathInfo       = $request->getPathInfo();
        $parts          = explode('/', $pathInfo);
        $langUrl        = $parts[0];
        $language       = array_search($langUrl, $this->_languages);
        if ($language) {
            $request->setPathInfo(mb_substr($pathInfo, mb_strlen($langUrl)));
            Yii::$app->language = $language;
            $cookie             = new Cookie([
                'name'     => $this->languageCookieName,
                'httpOnly' => true
            ]);
            $cookie->value      = $language;
            $cookie->expire     = time() + (int)$this->languageCookieDuration;
            Yii::$app->getResponse()->getCookies()->add($cookie);
            if (!$this->enableDefaultLanguageUrlCode && $language === $this->_defaultLanguage) {
                $this->redirectToLanguage();
            }
        } else {
            $cookie = $request->getCookies()->get($this->languageCookieName);
            if ($cookie) {
                $language = $cookie->value;
            }
            if (!$language && $this->enableLanguageDetection) {
                foreach ($request->getAcceptableLanguages() as $acceptable) {
                    if (isset($this->_languages[$acceptable])) {
                        $language = $acceptable;
                        break;
                    }
                }
            }
            if (!$language || $language === $this->_defaultLanguage) {
                Yii::$app->language = $this->_defaultLanguage;
                if (!$this->enableDefaultLanguageUrlCode) {
                    return;
                }
                $language = $this->_defaultLanguage;
            }
            $this->redirectToLanguage($language);
        }
    }

    /**
     * @inheritdoc
     */
    public function createUrl($params)
    {
        $params           = (array)$params;
        $language         = Yii::$app->language;
        $languageRequired = false;
        if (isset($params['language'])) {
            $language = $params['language'];
            unset($params['language']);
            if ($language && $this->enableLocaleUrls && $this->_languages) {
                $languageRequired = true;
            }
        }
        elseif (Yii::$app->language !== $this->_defaultLanguage && $this->enableLocaleUrls && $this->_languages) {
            $languageRequired = true;
        }
        $url = parent::createUrl($params);
        if ($languageRequired || $this->enableDefaultLanguageUrlCode || $language !== Yii::$app->language) {
            $language = $this->_languages[$language] ?? null;
            $baseUrl  = $this->getBaseUrl();
            $length   = strlen($baseUrl);
            if ($language) {
                if ($length) {
                    return substr_replace($url, "{$baseUrl}{$language}/", 0, $length);
                }
                return "/{$language}{$url}";
            }
        }
        return $url;
    }

    protected function redirectToLanguage($language = null)
    {
        $redirectUrl = $this->_request->getBaseUrl();
        if ($language && isset($this->_languages[$language])) {
            $redirectUrl .= '/' . $this->_languages[$language];
        }
        $pathInfo = $this->_request->getPathInfo();
        if ($pathInfo) {
            $redirectUrl .= '/' . $pathInfo;
        }
        if ($redirectUrl === '') {
            $redirectUrl = '/';
        }
        $queryString = $this->_request->getQueryString();
        if ($queryString) {
            $redirectUrl .= '?' . $queryString;
        }
        if (Yii::$app->isBackend()) {
            $redirectUrl = '/' . trim(Yii::$app->getModule('pw-web')->backendUrlPrefix, '/') . $redirectUrl;
        }
        Yii::$app->getResponse()->redirect($redirectUrl);
        Yii::$app->end();
    }
}
