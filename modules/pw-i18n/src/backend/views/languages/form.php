<?php
/* @var $this yii\web\View */
/* @var $model \pw\i18n\models\Languages */
/* @var $form yii\widgets\ActiveForm */
use pw\ui\form\ActiveForm;

?>

<?php $form = ActiveForm::begin(['model' => $model]) ?>

<?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
<?= $form->field($model, 'slug')->textInput(['maxlength' => 255]) ?>
<?= $form->field($model, 'locale')->textInput(['maxlength' => 255]) ?>
<?= $form->field($model, 'is_active')->checkbox([
    'class' => 'make-switch',
    'data-on-text' => '<i class="fa fa-check"></i>',
    'data-off-text' => '<i class="fa fa-times"></i>',
    'data-on-color' => 'success',
]) ?>
<?php ActiveForm::end(); ?>

