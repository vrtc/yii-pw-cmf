<?php

use pw\ui\grid\GridView;


/* @var $this \pw\web\View */
/* @var $searchModel \pw\i18n\backend\models\LanguagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>

<?= GridView::widget([

    'dataProvider' => $dataProvider,
    'model' => $searchModel,
    'filter' => true,
    'actions' => [
        [
            'label' => Yii::t('i18n', 'Добавить язык'),
            'url' => ['create']
        ]
    ],
    'columns' => [
        'name',
        'slug',
        'locale',
        [
            'class' => \pw\ui\grid\ToggleColumn::class,
            'attribute' => 'default',
            'afterToggle' => 'function(success,data){
                        if(success){
                            var msg = "' . Yii::t('i18n', 'Язык по умолчанию изменен') . '";
                            toastr.success(msg);
                            jQuery("#currency .toggle[name!=\'"+$self.attr("name")+"\']").bootstrapSwitch("state",false,true);
                        }
                        else
                            toastr.error("Network error");
                    }'
        ],
        [
            'class' => \pw\ui\grid\ActionColumn::class,
            'buttons' => [
                'view' => false
            ]],
    ],
]);
