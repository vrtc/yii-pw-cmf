<?php
/* @var $this \pw\web\View */
/* @var $searchModel \pw\i18n\backend\models\SourceMessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
use yii\helpers\ArrayHelper;
use pw\ui\grid\GridView;
use dosamigos\editable\Editable;
use pw\i18n\models\SourceMessage;

$row = function ($model, $key, $index, $column) use ($languages) {
    static $lastIndex, $lang_internal;
    if (!$lang_internal)
        $lang_internal = $languages;
    if ($lastIndex !== $index) {
        $lastIndex = $index;
        reset($lang_internal);
    } else
        next($lang_internal);
    $lang = current($lang_internal);
    $content = Editable::widget([
        'type' => 'textarea',
        'name' => $lang['locale'],
        'value' => isset($model->messages[$lang['locale']]) ? $model->messages[$lang['locale']]->translation : '',
        'url' => ['save', 'id' => $model->id],
        'mode' => 'popup',
        'clientOptions' => [
            'value' => isset($model->messages[$lang['locale']]) ? $model->messages[$lang['locale']]->translation : ''
        ]
    ]);
    return $content;
};

$columns = [
    [
        'attribute' => 'category',
        'filter' => ArrayHelper::map(SourceMessage::find()->groupBy('category')->all(), 'category', 'category')
    ],
    'message'
];
foreach ($languages as $lang) {
    $columns[] = ['header' => $lang['name'], 'content' => $row];
}
?>

<?= GridView::widget([
    'id' => 'currency',
    'dataProvider' => $dataProvider,
    'model' => $searchModel,
    'export' => false,
    'columns' => $columns
]);