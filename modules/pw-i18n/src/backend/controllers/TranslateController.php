<?php
namespace pw\i18n\backend\controllers;


use Yii;
use pw\web\Controller;
use pw\i18n\models\Languages;
use pw\i18n\models\Message;
use pw\i18n\models\SourceMessage;
use pw\i18n\backend\models\SourceMessageSearch;
use yii\web\NotFoundHttpException;

class TranslateController extends Controller
{

    public function actionIndex()
    {
        $searchModel = new SourceMessageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $languages = Languages::find()->asArray()->all();
        $this->setPageTitle(Yii::t('i18n', 'Переводы'));
        $this->addBreadcrumb(Yii::t('i18n', 'Переводы'));
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'languages' => $languages
        ]);
    }

    public function actionSave($id)
    {
        $model = $this->findModel($id);
        Yii::$app->db->createCommand()->delete(Message::tableName(), [
            'language' => $_POST['name'],
            'id' => $model->id
        ])->execute();
        return Yii::$app->db->createCommand()->insert(Message::tableName(), [
            'id' => $id,
            'translation' => $_POST['value'],
            'language' => $_POST['name']
        ])->execute();
    }

    protected function findModel($id)
    {
        if (($model = SourceMessage::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
