<?php
namespace pw\i18n\backend\controllers;


use Yii;
use pw\web\Controller;
use pw\ui\grid\actions\ToggleAction;
use pw\i18n\models\Languages;
use pw\i18n\backend\models\LanguagesSearch;
use yii\web\NotFoundHttpException;

class LanguagesController extends Controller
{


    public function actions()
    {
        return [
            'toggle' => [
                'class' => ToggleAction::class,
                'modelClass' => Languages::class,
                'toggleType' => ToggleAction::TOGGLE_UNIQ
            ],
        ];
    }


    public function actionIndex()
    {
        $this->setPageTitle(Yii::t('i18n', 'Языки'));
        $this->addBreadcrumb(Yii::t('i18n', 'Языки'));
        $searchModel = new LanguagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new Languages();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('i18n', 'Язык добавлен'));
            return $this->redirect(['update', 'id' => $model->id]);
        }
        $this->setPageTitle(Yii::t('i18n', 'Добавить новый язык'));
        $this->addBreadcrumb(Yii::t('i18n', 'Языки'), ['index']);
        $this->addBreadcrumb(Yii::t('i18n', 'Добавить новый язык'));
        return $this->render('form', ['model' => $model]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('i18n', 'Изменения сохранены'));
            return $this->redirect(['update', 'id' => $model->id]);
        }

        $this->setPageTitle(Yii::t('i18n', 'Редактирование языка: {name}', ['name' => $model->name]));
        $this->addBreadcrumb(Yii::t('i18n', 'Языки'), ['index']);
        $this->addBreadcrumb(Yii::t('i18n', 'Редактирование языка: {name}', ['name' => $model->name]));
        return $this->render('form', ['model' => $model]);
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model->delete()) {
            Yii::$app->session->setFlash('success', Yii::t('i18n', 'Язык удалён'));
        }
        return $this->redirect(['index']);
    }

    public function findModel($id)
    {
        if (($model = Languages::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
