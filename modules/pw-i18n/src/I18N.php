<?php

namespace pw\i18n;

use Yii;
use yii\i18n\MessageSource;
use yii\base\InvalidConfigException;
use pw\i18n\models\Languages;

class I18N extends \yii\i18n\I18N
{

    public $enableCache = true;

    public $cacheDuration = 3600;

    private $_languages = [];
    private $_defaultLanguage;

    public function init()
    {
        parent::init();
        $languages = Languages::find()->active()->asArray()->all();
        foreach ($languages as $language) {
            if ($language['default']) {
                $this->_defaultLanguage = $language;
            }
            $this->_languages[$language['locale']] = $language;
        }
    }

    /**
     * @inheritdoc
     *
     */
    public function getMessageSource($category)
    {
        if ($category === 'yii' && isset($this->translations[$category])) {
            return $this->translations[$category];
        }

        $source = $this->translations['*'];
        if ($source instanceof MessageSource) {
            return $source;
        }

        return $this->translations['*'] = Yii::createObject($source);
    }

    public function getAllLanguages()
    {
        return Languages::find()->asArray()->all();
    }

    public function getLanguages()
    {
        return $this->_languages;
    }

    public function getDefaultLanguage()
    {
        return $this->_defaultLanguage;
    }
}
