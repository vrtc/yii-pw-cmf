<?php
namespace pw\i18n\db\oci;

use pw\i18n\db\I18NTrait;

class ColumnSchemaBuilder extends \yii\db\oci\ColumnSchemaBuilder
{
    use I18NTrait;
}
