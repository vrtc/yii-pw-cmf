<?php
namespace pw\i18n\db\oci;

class Schema extends \yii\db\oci\Schema
{
    /**
     * @inheritdoc
     */
    public function createColumnSchemaBuilder($type, $length = null)
    {
        return new ColumnSchemaBuilder($type, $length, $this->db);
    }
}
