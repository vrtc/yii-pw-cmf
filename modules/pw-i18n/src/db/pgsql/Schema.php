<?php
namespace pw\i18n\db\pgsql;

class Schema extends \yii\db\pgsql\Schema
{
    /**
     * @inheritdoc
     */
    public function createColumnSchemaBuilder($type, $length = null)
    {
        return new ColumnSchemaBuilder($type, $length, $this->db);
    }
}
