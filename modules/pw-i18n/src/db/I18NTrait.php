<?php
namespace pw\i18n\db;


trait I18NTrait
{
    public $i18n;

    public function i18n()
    {
        $this->i18n = true;
        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getCategoryType()
    {
        return $this->getTypeCategory();
    }
}
