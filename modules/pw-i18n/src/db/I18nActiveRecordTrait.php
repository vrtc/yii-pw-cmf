<?php
namespace pw\i18n\db;

use Yii;
use pw\core\Model;
use yii\base\UnknownClassException;

trait I18nActiveRecordTrait {
    private $_language;
    private $_languages = [];
    private $_i18nModel;
    private $_defaultLanguage;
    private $_i18nAtrributes = [];

    public function init(){
        $languages = Yii::$app->i18n->getLanguages();
        foreach ($languages as $language) {
            $this->_languages[$language['locale']] = $language;
            if ($language['default']) {
                $this->_defaultLanguage = $language['locale'];
            }
        }
        $this->_i18nModel = $this->getI18NModel();

        $i18nModel = new $this->_i18nModel;
        $i18nAttributes = $i18nModel->attributes();
        $i18nAttributes =\array_fill_keys($i18nAttributes,true);
        unset($i18nAttributes['model_id'],$i18nAttributes['language']);
        $this->_i18nAtrributes = $i18nAttributes;
        parent::init();
    }

    public function getI18NLanguage()
    {
        if ($this->_language === null) {
            $this->_language = Yii::$app->language;
        }
        return $this->_language;
    }

    /**
     * Returns the translation model for the specified language.
     * @param string|null $language
     * @return ActiveRecord
     */
    public function translate($language = null)
    {
        return $this->getTranslation($language);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getI18NTranslations()
    {
        return $this->hasMany($this->getI18NModel(), ['model_id' => $this->getPrimaryKeyName()]);
    }

    /**
     * Returns the translation model for the specified language.
     * @param string|null $language
     * @return ActiveRecord
     */
    protected function getTranslation($language = null)
    {
        if ($language === null) {
            $language = $this->getI18NLanguage();
        }
        $this->_language = $language;

        /* @var ActiveRecord[] $translations */
        $translations = $this->i18NTranslations;

        foreach ($translations as $translation) {
            if ($translation->language === $language) {
                return $translation;
            }
        }

        /* @var ActiveRecord $translation */
        $class = $this->getI18NModel();
        $translation = new $class;
        $translation->language = $language;
        $translations[] = $translation;
        $this->populateRelation('i18NTranslations', $translations);
        return $translation;
    }

    /**
     * Returns a value indicating whether the translation model for the specified language exists.
     * @param string|null $language
     * @return boolean
     */
    public function hasTranslation($language = null)
    {
        if ($language === null) {
            $language = $this->getI18NLanguage();
        }
        /* @var ActiveRecord[] $translations */
        $translations = $this->i18NTranslations;

        foreach ($translations as $translation) {
            if ($translation->language === $language) {
                return true;
            }
        }

        return false;
    }

    public function afterLoad(array $data)
    {
        if ($data) {
            $class = $this->getI18NModel();
            $model = new $class;
            $formName = $model->formName();
            $language = $data['language'] ?? Yii::$app->language;
            if (isset($data[$formName])) {
                $data = array_filter($data[$formName]);
            }
            $this->translate($language)->setAttributes($data);
            if(!$this->translate($language)->isNewRecord) {
                $this->translate($language)->save();
            }
        }
        parent::afterLoad($data);
    }

    /**
     * @return void
     */
    public function afterValidate()
    {
        if (!Model::validateMultiple($this->i18NTranslations)) {
            $this->addError('i18NTranslations');
        }
        parent::afterValidate();
    }

    /**
     * @return void
     */
    public function afterSave($insert, $changedAttributes)
    {
        /* @var ActiveRecord $translation */
        $translated = false;
        /* @var ActiveRecord[] $translations */
        $translations = $this->i18NTranslations;
        $i18nAttributes = \array_keys($this->_i18nAtrributes);
        foreach ($translations as $translation) {
            $attributes = $translation->getAttributes($i18nAttributes);
            if (array_filter($attributes)) {
                $translated = $attributes;
                break;
            }
        }
        foreach ($translations as $translation) {
            $attributes = $translation->getAttributes($i18nAttributes);
            if (!array_filter($attributes)) {
                $translation->setAttributes($translated); //set translated attributes for non-translated language
            }
            $this->link('i18NTranslations', $translation);
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @inheritdoc
     */
    public function canGetProperty($name, $checkVars = true, $checkBehaviors = true)
    {
        return isset($this->_i18nAtrributes[$name]) ?: parent::canGetProperty($name, $checkVars,$checkBehaviors);
    }

    /**
     * @inheritdoc
     */
    public function canSetProperty($name, $checkVars = true,$checkBehaviors = true)
    {
        return isset($this->_i18nAtrributes[$name]) ?: parent::canSetProperty($name, $checkVars,$checkBehaviors);
    }

    /**
     * @inheritdoc
     */
    public function __get($name)
    {
        if(isset($this->_i18nAtrributes[$name])) {
            if ($this->hasTranslation()) {
                return $this->getTranslation()->getAttribute($name);
            }

            return $this->getTranslation($this->_defaultLanguage)->getAttribute($name);
        }
        return parent::__get($name);
    }

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidParamException
     */
    public function __set($name, $value)
    {
        if(isset($this->_i18nAtrributes[$name])) {
            $translation = $this->translate();
            $translation->setAttribute($name, $value);
            return;
        }
        parent::__set($name,$value);
    }


    /**
     * @return string
     * @throws UnknownClassException
     */
    private function getI18NModel()
    {
        if ($this->_i18nModel === null) {
            $parents = [
                ActiveRecord::class=>true,
                AdjacencyListActiveRecord::class=>true,
                MaterializedPathActiveRecord::class=>true,
                NestedSetsActiveRecord::class=>true
            ];
            $reflection = new \ReflectionObject($this);
            $name = $reflection->getShortName();
            $namespace = $reflection->getNamespaceName();
            $parent = $reflection->getParentClass();
            while (!isset($parents[$parent->getName()])) {
                $name = $parent->getShortName();
                $namespace = $parent->getNamespaceName();
                $parent = $parent->getParentClass();
            }
            $this->_i18nModel = "$namespace\\i18n\\{$name}";
            if (!class_exists($this->_i18nModel)) {
                throw new UnknownClassException("Class $this->_i18nModel not found");
            }
        }
        return $this->_i18nModel;
    }

    public static function find()
    {
        return new ActiveQuery(static::class);
    }
}
