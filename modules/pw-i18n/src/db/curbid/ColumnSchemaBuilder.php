<?php
namespace pw\i18n\db\curbid;

use pw\i18n\db\I18NTrait;

class ColumnSchemaBuilder extends \yii\db\cubrid\ColumnSchemaBuilder
{
    use I18NTrait;
}
