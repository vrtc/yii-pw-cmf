<?php
namespace pw\i18n\db\curbid;

class Schema extends \yii\db\cubrid\Schema
{

    /**
     * @inheritdoc
     */
    public function createColumnSchemaBuilder($type, $length = null)
    {
        return new ColumnSchemaBuilder($type, $length, $this->db);
    }
}
