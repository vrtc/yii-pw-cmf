<?php
namespace pw\i18n\db;

class ActiveQuery extends \pw\core\db\ActiveQuery {

    public function init()
    {
        $this->alias('b');
        $this->joinWith(['i18NTranslations i18n']);
        parent::init();
    }
}