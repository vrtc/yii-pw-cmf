<?php
namespace pw\i18n\db\sqlite;

use pw\i18n\db\I18NTrait;

class ColumnSchemaBuilder extends \yii\db\sqlite\ColumnSchemaBuilder
{
    use I18NTrait;
}
