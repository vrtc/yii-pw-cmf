<?php
namespace pw\i18n\db\sqlite;

class Schema extends \yii\db\sqlite\Schema
{

    /**
     * @inheritdoc
     */
    public function createColumnSchemaBuilder($type, $length = null)
    {
        return new ColumnSchemaBuilder($type, $length, $this->db);
    }
}
