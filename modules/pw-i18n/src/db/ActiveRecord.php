<?php
namespace pw\i18n\db;


use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;

class ActiveRecord extends \pw\core\db\ActiveRecord {

    use I18nActiveRecordTrait;

    protected static function findByCondition($condition)
    {
        $query = static::find();

        if (!ArrayHelper::isAssociative($condition)) {
            // query by primary key
            $primaryKey = static::primaryKey();
            if (isset($primaryKey[0])) {
                $condition = ["b.$primaryKey[0]" => $condition];
            } else {
                throw new InvalidConfigException('"' . get_called_class() . '" must have a primary key.');
            }
        }

        return $query->andWhere($condition);
    }
}