<?php

namespace pw\i18n\db;

use Yii;
use yii\db\ColumnSchemaBuilder;

class Migration extends \pw\core\db\Migration
{

    protected $i18nColumns = [];


    public function createTable($table, $columns, $options = null)
    {
        $i18nColumns = [];
        $pkName = $pkType = null;
        foreach ($columns as $i => $column) {
            if (is_object($column)) {
                if ($column->getCategoryType() === ColumnSchemaBuilder::CATEGORY_PK) {
                    $pkName = $i;
                    $pkType = $column->getType();
                }
                if ($column->i18n === true) {
                    $i18nColumns[$i] = $column;
                    unset($columns[$i]);
                }
            }
        }
        parent::createTable($table, $columns, $options);
        if (!empty($i18nColumns)) {
            $tableName = str_replace(['}}', '{{%'], '', $table);
            $i18nTable = "{{%$tableName" . '_i18n}}';

            $i18nColumns = array_merge([
                'model_id' => $this->getRelationColumn($pkType)->notNull(),
                'language' => $this->string(12)->notNull(),
            ], $i18nColumns);
            parent::createTable($i18nTable, $i18nColumns, $this->tableOptions);
            $this->addPrimaryKey('idx_primary', $i18nTable, ['model_id', 'language']);
            $this->addForeignKey("fk_$tableName" . '_i18n_model_id', $i18nTable, 'model_id', $table, $pkName, 'cascade', 'cascade');
        }
    }
}
