<?php
namespace pw\i18n\db;

use pw\i18n\db\mysql\Schema;

class Connection extends \yii\db\Connection
{

    public $schemaMap = [
        'mysqli' => Schema::class,
        'mysql' => Schema::class,
    ];
}
