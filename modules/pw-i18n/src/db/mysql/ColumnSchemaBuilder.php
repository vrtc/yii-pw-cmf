<?php
namespace pw\i18n\db\mysql;

use pw\i18n\db\I18NTrait;

class ColumnSchemaBuilder extends \pw\core\db\mysql\ColumnSchemaBuilder
{
    use I18NTrait;
}
