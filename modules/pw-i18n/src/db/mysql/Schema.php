<?php
namespace pw\i18n\db\mysql;


class Schema extends \pw\core\db\mysql\Schema
{
    /**
     * @inheritdoc
     */
    public function createColumnSchemaBuilder($type, $length = null)
    {
        return new ColumnSchemaBuilder($type, $length, $this->db);
    }

}
