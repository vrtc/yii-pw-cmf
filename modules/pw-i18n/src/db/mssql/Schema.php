<?php
namespace pw\i18n\db\mssql;

class Schema extends \yii\db\mssql\Schema
{

    /**
     * @inheritdoc
     */
    public function createColumnSchemaBuilder($type, $length = null)
    {
        return new ColumnSchemaBuilder($type, $length, $this->db);
    }
}
