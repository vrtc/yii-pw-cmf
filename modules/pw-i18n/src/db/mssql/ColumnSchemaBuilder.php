<?php
namespace pw\i18n\db\mssql;

use pw\i18n\db\I18NTrait;

class ColumnSchemaBuilder extends \yii\db\ColumnSchemaBuilder
{
    use I18NTrait;
}
