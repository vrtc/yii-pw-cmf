<?php

namespace pw\i18n;


use Yii;
use yii\base\Application;
use yii\base\BootstrapInterface;
use yii\db\ActiveQuery;
use yii\i18n\DbMessageSource;
use pw\core\db\ActiveRecord;
use pw\web\View;
use pw\i18n\models\Message;
use pw\i18n\models\SourceMessage;
use pw\i18n\handlers\I18NHandlers;

class Module extends \pw\core\Module implements BootstrapInterface
{

    public $enableCache = true;

    public function components(Application $app): array
    {
        return [
            'i18n'       => [
                'class'        => I18N::class,
                'translations' => [
                    'yii' => Yii::createObject([
                        'class'          => 'yii\i18n\PhpMessageSource',
                        'sourceLanguage' => 'en-US',
                        'basePath'       => '@yii/messages',
                    ]),
                    '*'   => Yii::createObject([
                        'class'              => DbMessageSource::class,
                        'sourceMessageTable' => SourceMessage::tableName(),
                        'messageTable'       => Message::tableName(),
                        'sourceLanguage'     => 'key',
                        'enableCaching'      => $this->enableCache,
                        'forceTranslation'   => true,
                    ])
                ]
            ],
            'urlManager' => [
                'class' => UrlManager::class
            ]
        ];
    }

    public function handlers(Application $app): array
    {
        return [
            [
                'class'    => DbMessageSource::class,
                'event'    => DbMessageSource::EVENT_MISSING_TRANSLATION,
                'callback' => [I18NHandlers::class, 'addMissingTranslation']
            ],
            [
                'class'    => View::class,
                'event'    => View::EVENT_BEGIN_PAGE,
                'callback' => [I18NHandlers::class, 'registerLinkTag']
            ],
        ];
    }

    public function getNavigation(): array
    {
        return [
            'content'  => [
                [
                    'label' => 'Переводы',
                    'url'   => ['/pw-i18n/translate/index'],
                    'icon'  => 'language',
                    'render'=> false,
                ],
            ],
            'settings' => [
                [
                    'label' => 'Языки',
                    'url'   => ['/pw-i18n/languages/index'],
                    'icon'  => 'language'
                ],
            ]
        ];
    }
}
