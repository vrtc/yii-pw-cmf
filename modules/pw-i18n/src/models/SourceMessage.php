<?php

namespace pw\i18n\models;

use Yii;
use pw\core\db\ActiveRecord;

/**
 * This is the model class for table "{{%i18n_source_message}}".
 *
 * @property integer $id
 * @property string $category
 * @property string $message
 *
 * @property Message[] $Messages
 */
class SourceMessage extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pw_i18n_source_message}}';
    }

    public static function getModelName()
    {
        return Yii::t('i18n','Переводы');
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message'], 'string'],
            [['category'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' =>'ID',
            'category' => Yii::t('i18n', 'Модуль'),
            'message' => Yii::t('i18n', 'Сообщение'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Message::class, ['id' => 'id'])->indexBy('language');
    }
}
