<?php
namespace pw\i18n\models;

use pw\core\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "{{%i18n_languages}}".
 *
 * @property integer $id
 * @property string $url
 * @property string $locale
 * @property string $name
 * @property integer $default
 * @property integer $is_active
 */
class Languages extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pw_i18n_languages}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['slug', 'locale'], 'required'],
            [['default', 'is_active'], 'boolean'],
            [['slug', 'locale'], 'string', 'max' => 12],
            [['name'], 'string', 'max' => 255],
            [['slug'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('i18n', 'ID'),
            'slug' => Yii::t('i18n', 'URL'),
            'locale' => Yii::t('i18n', 'Локаль'),
            'name' => Yii::t('i18n', 'Название'),
            'default' => Yii::t('i18n', 'По умолчанию'),
            'is_active' => Yii::t('i18n', 'Активен'),
        ];
    }

    /**
     * @inheritdoc
     * @return LanguagesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LanguagesQuery(get_called_class());
    }

    public static function findDefault()
    {
        return self::find()->andWhere('[[default]]=1')->one();
    }
}

/**
 * This is the ActiveQuery class for [[Languages]].
 *
 * @see Languages
 */
class LanguagesQuery extends \yii\db\ActiveQuery
{
    public function init()
    {
        $this->orderBy(['default' => SORT_DESC]);
    }

    public function active()
    {
        $this->andWhere('[[is_active]]=1');
        return $this;
    }
}
