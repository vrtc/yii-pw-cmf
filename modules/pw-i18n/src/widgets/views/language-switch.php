<?php
use yii\helpers\Url;
?>
<div class="btn-group">
    <a href="javascript:;" class="btn btn-default" data-toggle="dropdown">
        <img style="height: 11px !important;"  width="16"  src="<?=Yii::$app->assetManager->publish('@themes/pw-metronic/src/assets/img/flags/'.$currentLanguage['slug'].'.png')[1]?>">
        <i class="fa fa-angle-down"></i>
    </a>
    <ul class="dropdown-menu pull-right">
        <?php foreach($languages as $lang):?>
            <?php if($lang['locale'] == Yii::$app->language) continue;?>
            <li>
                <a  href="#" data-url="<?=Url::current(['language'=>$lang['locale']])?>" class="reload">
                    <img alt="<?= $lang['slug'] ?>" src="<?=Yii::$app->assetManager->publish('@themes/pw-metronic/src/assets/img/flags/'.$lang['slug'].'.png')[1]?>"> <?=htmlspecialchars($lang['name'])?> </a>
            </li>
        <?php endforeach;?>
    </ul>
</div>