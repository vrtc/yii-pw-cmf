<?php
namespace pw\i18n\widgets;

use pw\core\traits\GetModule;
use Yii;
use yii\base\Widget;
use pw\i18n\models\Languages;

class LanguageSwitchWidget extends Widget
{

    public $template = 'language-switch';

    public function run()
    {
        $languages = Yii::$app->i18n->getLanguages();
        if (count($languages) > 1) {
            $currentLanguage = null;
            foreach ($languages as $lang) {
                if ($lang['locale'] === Yii::$app->language) {
                    $currentLanguage = $lang;
                }
            }

            return $this->render($this->template, [
                'languages' => $languages,
                'currentLanguage' => $currentLanguage,
            ]);
        }
        return null;
    }
}
