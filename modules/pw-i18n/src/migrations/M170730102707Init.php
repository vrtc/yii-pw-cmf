<?php
namespace pw\i18n\migrations;

use pw\core\db\Migration;

class M170730102707Init extends Migration
{
    public function up()
    {
        $this->createTable('{{%pw_i18n_languages}}', [
            'id'      => $this->bigPrimaryKey()->unsigned(),
            'slug'       => $this->string(12)->notNull(),
            'locale'    => $this->string(12)->notNull(),
            'name'      => $this->string()->notNull(),
            'default'   => $this->boolean()->notNull(),
            'is_active' => $this->boolean()->notNull()
        ]);

        $this->createIndex('idx_slug', '{{%pw_i18n_languages}}', 'slug', true);
        $this->createIndex('idx_active', '{{%pw_i18n_languages}}', ['is_active']);

        $this->batchInsert('{{%pw_i18n_languages}}', ['slug', 'locale', 'name', 'default', 'is_active'], [
            [ 'ru', 'ru-RU', 'Русский', 1, 1]

        ]);

        $this->createTable('{{%pw_i18n_source_message}}', [
            'id'       => $this->primaryKey(),
            'category' => $this->string(),
            'message'  => $this->text(),
        ]);

        $this->createTable('{{%pw_i18n_message}}', [
            'id'          => $this->integer()->notNull(),
            'language'    => $this->string(12)->notNull(),
            'translation' => $this->text(),
        ]);

        $this->createIndex('idx_id', '{{%pw_i18n_message}}', ['id', 'language']);

        $this->createIndex('idx_category', '{{%pw_i18n_source_message}}', 'category');
        $this->createIndex('idx_language', '{{%pw_i18n_message}}', 'language');
        $this->addForeignKey(
            'fk_i18n_source_message_message',
            '{{%pw_i18n_message}}',
            'id',
            '{{%pw_i18n_source_message}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );
    }

    public function down()
    {
        echo "M170730102707Init cannot be reverted.\n";

        return false;
    }

}
