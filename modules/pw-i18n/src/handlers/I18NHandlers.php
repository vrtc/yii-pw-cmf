<?php
namespace pw\i18n\handlers;

use pw\core\helpers\StringHelper;
use Yii;
use yii\db\Query;
use yii\helpers\Url;
use yii\db\ActiveQuery;
use pw\i18n\TranslateableBehavior;
use yii\i18n\MissingTranslationEvent;
use pw\i18n\interfaces\Translateable;
use pw\i18n\models\SourceMessage;

class I18NHandlers
{

    public static function registerLinkTag($event)
    {
        if (Yii::$app->request->isAjax || !Yii::$app->response->isOk) {
            return null;
        }
        foreach (Yii::$app->i18n->getLanguages() as $lang) {
            if ($lang['locale'] !== Yii::$app->language) {
                $event->sender->registerLinkTag([
                    'rel' => 'alternate',
                    'hreflang' => $lang['locale'],
                    'href' => Url::current(['language' => $lang['slug']], true)
                ]);
            }
        }
    }

    public static function addMissingTranslation(MissingTranslationEvent $event)
    {

        $cache = Yii::$app->cache;
        $key = md5($event->category . $event->message);
        $data = $cache->get($key);
        if ($data === false) {
            $sourceMessage = (new Query())->from(SourceMessage::tableName())
                ->where('category = :category and message = binary :message', [
                    ':category' => $event->category,
                    ':message' => $event->message
                ])
                ->one();
            if (!$sourceMessage) {
                Yii::$app->db->createCommand()->insert(SourceMessage::tableName(), [
                    'category' => $event->category,
                    'message' => $event->message
                ])->execute();
            }
            $data =  $event->message;
            $cache->set($key, $data,  time() + (60 * 60 * 24 * 365));
        }
        $event->translatedMessage = $data;
    }
}