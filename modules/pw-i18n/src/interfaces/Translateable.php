<?php
namespace pw\i18n\interfaces;


interface Translateable
{
    public function i18nAttributes():array;
}
