<?php

namespace pw\sessions;


use yii\base\Application;
use yii\base\BootstrapInterface;
use yii\web\DbSession;

class Module extends \pw\core\Module implements BootstrapInterface
{

    public $sessionTable = '{{%pw_sessions}}';


    public function getNavigation(): array
    {
        return [
            'settings' => [
                [
                    'label' => 'Сессии',
                    'url' => ['/pw-sessions/settings/index'],
                    'icon' => 'user-lock'
                ]
            ]
        ];
    }
}
