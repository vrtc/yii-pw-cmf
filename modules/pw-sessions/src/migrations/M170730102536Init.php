<?php
namespace pw\sessions\migrations;

use pw\core\db\Migration;

class M170730102536Init extends Migration
{
    public function up()
    {
        $this->createTable('{{%pw_sessions}}', [
            'id'     => $this->char(40)->notNull(),
            'expire' => $this->integer(),
            'data'   => $this->binary()
        ]);
        $this->addPrimaryKey('idx_id', '{{%pw_sessions}}', 'id');
    }

    public function down()
    {
        echo "M170730102536Init cannot be reverted.\n";

        return false;
    }

}
