<?php

use yii\helpers\Html;
use pw\ui\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model pw\reference\models\Reference */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin([
    'model' => $model,
    'title' => $model->isNewRecord ? Yii::t('reference', 'Новое значение') : Yii::t('reference', 'Редактирование значения справочника {name}', ['name' => $reference->name]),
    'icon' => 'book'
]); ?>
<?= $form->field($model, 'reference_id')->hiddenInput()->label(false) ?>
<?= $form->field($model, 'key')->textInput(['maxlength' => true]) ?>
<?= $form->field($model->translate(), 'value')->textInput() ?>

<?php ActiveForm::end(); ?>
