<?php

use pw\ui\nestable\NestableView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $models \pw\reference\models\Reference[] */

?>


<?= NestableView::widget([
    'title' =>'Справочники',
    'icon' => 'file-o',
    'models' => $models,
    'nodeMoveUrl' => ['move'],
    'actions' => [
        [
            'label' => Yii::t('menu', 'Добавить новый справочник'),
            'url' => ['create']
        ]
    ],
    'itemLabel' => 'name'
]);