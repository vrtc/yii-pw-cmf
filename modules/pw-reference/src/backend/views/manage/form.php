<?php

use yii\helpers\Html;
use pw\ui\form\ActiveForm;
use pw\ui\grid\GridView;

/* @var $this yii\web\View */
/* @var $model pw\reference\models\Reference */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin([
    'model' => $model,
    'title' => $model->isNewRecord ? Yii::t('reference', 'Новый справочник') : Yii::t('reference', 'Редактирование справочника {key}', ['key' => $model->key]),
    'icon' => 'book'
]); ?>

<?= $form->field($model, 'status')->checkbox() ?>
<?= $form->field($model, 'key')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'description')->textarea(['maxlength' => true]); ?>

<?php ActiveForm::end(); ?>

<?php if (!$model->isNewRecord): ?>
    <?= GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'filter' => false,
        'title' => Yii::t('reference', 'Справочник {name}', ['name' => $model->name]),
        'icon' => 'book',
        'actions' => [
            [
                'url' => ['create-value', 'reference' => $model->id],
                'label' => Yii::t('reference', 'Добавить значение'),
                'icon' => 'plus'
            ]
        ],
        'bulkActions' => [
            'deleteAll' => Yii::t('reference', 'Удалить')
        ],
        'columns' => [
            'key',
            'value',
            [
                'class' => \pw\ui\grid\ActionColumn::class,
                'template' => '{reorder} {update} {delete}',
                'urlCreator' => function ($action, $model) {
                    if ($action === 'update') {
                        return \yii\helpers\Url::to(['update-value', 'id' => $model->id]);
                    }
                    if ($action === 'delete') {
                        return \yii\helpers\Url::to(['delete-value', 'id' => $model->id]);
                    }
                    return null;
                }
            ],
        ],
    ]); ?>
<?php endif; ?>
