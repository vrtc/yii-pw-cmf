<?php

namespace pw\reference\backend\controllers;


use pw\reference\backend\models\SearchReferenceValue;
use pw\reference\models\ReferenceValue;
use Yii;
use yii\filters\VerbFilter;
use pw\web\Controller;
use yii\web\NotFoundHttpException;
use pw\reference\models\Reference;
use pw\reference\backend\models\SearchReference;

/**
 * ManageController implements the CRUD actions for Reference model.
 */
class ManageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
            //        'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Reference models.
     * @return mixed
     */
    public function actionIndex()
    {
        $models = Reference::find()->roots()->all();
        $this->setPageTitle(Yii::t('ui', 'Справочники'));
        $this->addBreadcrumb(Yii::t('ui', 'Справочники'));
        return $this->render('index', [
            'models' => $models,
        ]);
    }

    public function actionCreate($id = null, $parent = null, $position = null)
    {
        if ($parent) {
            $parent = $this->findModel($parent);
        }
        $model = new Reference();
        if ($model->load(Yii::$app->request->post())) {
            if ($parent) {
                $model->appendTo($parent);
            } else {
                $model->makeRoot();
            }
            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('pages', 'Страница добавлена'));

                return $this->redirect(['update', 'id' => $model->id]);
            }
        }
        $this->addBreadcrumb(Yii::t('reference', 'Справочники'), ['index']);
        if ($parent) {
            foreach ($parent->parents as $reference) {
                if (!$reference->isRoot()) {
                    $this->addBreadcrumb($reference->name, ['update', 'id' => $reference->id]);
                }
            }
            $this->addBreadcrumb($parent->name, ['update', 'id' => $parent->id]);
        }

        $this->setPageTitle(Yii::t('reference', 'Новый справочник'));
        $this->addBreadcrumb(Yii::t('reference', 'Новый справочник'));

        return $this->render('form', [
            'model' => $model,
        ]);

    }

    /**
     * Updates an existing Reference model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $searchModel = new SearchReferenceValue();
        $params = Yii::$app->request->queryParams;
        $params['SearchReferenceValue']['reference_id'] = $model->id;
        $dataProvider = $searchModel->search($params);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        }

        $this->setPageTitle(Yii::t('reference', 'Редактирование справочника {key}', ['key' => $model->key]));
        $this->addBreadcrumb(Yii::t('reference', 'Справочники'), ['index']);
        $this->addBreadcrumb(Yii::t('reference', 'Редактирование '.$model->name));

        return $this->render('form', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Creates a new Reference model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\base\InvalidParamException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionCreateValue($reference)
    {
        $reference = $this->findModel($reference);
        $model = new ReferenceValue();
        $model->reference_id = $reference->id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $reference->id]);
        }
        $this->setPageTitle(Yii::t('reference', 'Новое значение'));
        $this->addBreadcrumb(Yii::t('reference', 'Справочники'), ['index']);
        $this->addBreadcrumb($reference->name, ['update', 'id' => $reference->id]);
        $this->addBreadcrumb(Yii::t('reference', 'Новое значение'));

        return $this->render('value-form', [
            'model' => $model,
        ]);

    }

    /**
     * Updates an existing Reference model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionUpdateValue($id)
    {

        $model = $this->findModelValue($id);
        $reference = $model->reference;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            return $this->redirect(['update', 'id' => $reference->id]);
        }

        $this->setPageTitle(Yii::t('reference', 'Редактирование справочника {name}', ['name' => $reference->name]));
        $this->addBreadcrumb(Yii::t('reference', 'Справочники'), ['index']);
        $this->addBreadcrumb($reference->name, ['update', 'id' => $reference->id]);
        $this->addBreadcrumb(Yii::t('reference', 'Редактирование'));
        return $this->render('value-form', [
            'model' => $model,
            'reference' => $reference
        ]);
    }

    /**
     * Deletes an existing ReferenceValue model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteValue($id)
    {
        $model = $this->findModelValue($id);
        $reference = $model->reference;
        $model->delete();

        return $this->redirect(['update', 'id' => $reference->id]);
    }

    /**
     * Deletes an existing Reference model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->deleteWithChildren();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Reference model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Reference the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Reference::findOne(['id' => $id])) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findModelValue($id)
    {
        if (($model = ReferenceValue::find()->andWhere(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
