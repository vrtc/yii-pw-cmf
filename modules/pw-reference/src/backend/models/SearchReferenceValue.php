<?php

namespace pw\reference\backend\models;

use pw\reference\models\ReferenceValue;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use pw\reference\models\Reference;

/**
 * SearchReferenceValue represents the model behind the search form of `app\models\PwReferenceValue`.
 */
class SearchReferenceValue extends ReferenceValue
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id'], 'integer'],
            [['value', 'reference_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReferenceValue::find();
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if (!($this->load($params) && $this->validate())) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->andFilterWhere(['reference_id' => $this->reference_id]);
        $query->andFilterWhere(['like', 'i18n.value', $this->value]);
        return $dataProvider;
    }
}
