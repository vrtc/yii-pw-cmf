<?php
namespace pw\reference\migrations;
use pw\i18n\db\Migration;

class M170730102556Init extends Migration
{
    public function up()
    {

        $this->createAdjacencyListTable('{{%pw_references}}', [
            'id' => $this->bigPrimaryKey()->unsigned(),
            'key' => $this->string()->notNull(),
            'name' => $this->string(),
            'description' => $this->string()->i18n(),
            'model' => $this->string()
        ]);

        $this->createIndex('idx_key', '{{%pw_references}}', 'key');

        $this->createAdjacencyListTable( '{{%pw_references_values}}', [
            'id' => $this->bigPrimaryKey()->unsigned(),
            'reference_id' => $this->bigInteger()->unsigned()->notNull(),
            'key' => $this->string(),
            'value' => $this->string()->i18n()
        ]);
        $this->createIndex('idx_key', '{{%pw_references_values}}', 'key');
    }

    public function down()
    {
        $this->dropTable('{{%pw_references_i18n}}');
        $this->dropTable('{{%pw_references_values_i18n}}');
        $this->dropTable('{{%pw_references_values}}');
        $this->dropTable('{{%pw_references}}');
    }

}
