<?php

namespace pw\reference\migrations;
use pw\core\db\Migration;

class m191030_091128_add_active_to_reference extends Migration
{
    public function up()
    {
        $this->addColumn('{{%pw_references}}', 'status', $this->tinyInteger(3)->notNull()->defaultValue(1));
    }

    public function down()
    {
        $this->dropColumn('{{%pw_references}}', 'status');
    }

}
