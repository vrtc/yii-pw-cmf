<?php

namespace pw\reference;

use Yii;
use yii\base\Application;
use yii\base\BootstrapInterface;

class Module extends \pw\core\Module implements BootstrapInterface
{

    public function models(Application $app): array
    {
        return [
            \pw\reference\models\Reference::class=>Yii::t('referenct','Справочник')
        ];
    }

    public function components(Application $app): array
    {
        return [
            'reference' => Reference::class
        ];
    }

    public function getNavigation(): array
    {
        return [
            'content' => [
                [
                    'label' => 'Справочиники',
                    'url'   => ['/pw-reference/manage/index'],
                    'icon'  => 'book'
                ]
            ]
        ];
    }
}
