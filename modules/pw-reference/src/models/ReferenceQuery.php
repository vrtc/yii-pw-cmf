<?php
namespace pw\reference\models;

use pw\core\db\AdjacencyListQuery;

class ReferenceQuery extends AdjacencyListQuery
{
    /**
     * @param null $alias
     * @return $this
     */
    public function active($alias = null)
    {
        return $this->andWhere([
            ($alias ? $alias . '.' : '') . 'status' => Reference::STATUS_ACTIVE,
        ]);
    }
}