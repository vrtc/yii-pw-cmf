<?php

namespace pw\reference\models;

use Yii;


/**
 * This is the model class for table "pw_references".
 *
 * @property integer $id
 * @property string $key
 * @property string $model
 * @property string $status
 *
 * @property ReferenceValue[] $values
 */
class Reference extends \pw\i18n\db\AdjacencyListActiveRecord
{

    const STATUS_ACTIVE = 1;
    const STATUS_NO_ACTIVE = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pw_references}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key'], 'required'],
            [['status'], 'integer'],
            [['key', 'model', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => Yii::t('reference', 'Ключ'),
            'status' => Yii::t('reference', 'Активен'),
            'name' => Yii::t('reference', 'Название'),
            'description' => Yii::t('reference', 'Описание')
        ];
    }

    public static function find()
    {
        return new ReferenceQuery(static::class);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValues()
    {
        return $this->hasMany(ReferenceValue::class, ['reference_id' => 'id'])->indexBy('key');
    }
}
