<?php

namespace pw\reference\models;

use pw\core\interfaces\Sortable;
use pw\i18n\interfaces\Translateable;
use paulzi\sortable\SortableBehavior;
use paulzi\sortable\SortableTrait;

/**
 * This is the model class for table "pw_references_values".
 *
 * @property integer $id
 * @property integer $reference_id
 * @property integer $parent_id
 * @property string $key
 * @property integer $sort
 *
 * @property Reference $reference
 * @property \pw\reference\models\i18n\ReferenceValue $i18n
 */
class ReferenceValue extends \pw\i18n\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pw_references_values';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reference_id', 'key'], 'required'],
            [['parent_id', 'sort'], 'integer'],
            ['value', 'safe'],
            [['reference_id'], 'exist', 'skipOnError' => true, 'targetClass' => Reference::class, 'targetAttribute' => ['reference_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reference_id' => 'Справочник',
            'parent_id' => 'Родитель',
            'key' => 'Ключ',
            'sort' => 'Сортировка',
        ];
    }

    public function getI18n()
    {
        return $this->hasOne(\pw\reference\models\i18n\ReferenceValue::class, ['model_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReference()
    {
        return $this->hasOne(Reference::class, ['id' => 'reference_id']);
    }

}
