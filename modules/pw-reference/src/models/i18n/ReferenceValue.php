<?php

namespace pw\reference\models\i18n;

use Yii;
use paulzi\jsonBehavior\JsonBehavior;
/**
 * This is the model class for table "crm_pw_references_values_i18n".
 *
 * @property integer $id
 * @property integer $idReferenceValue
 * @property string $language
 * @property string $value
 * @property string $data
 *
 */
class ReferenceValue extends \pw\core\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pw_references_values_i18n';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language'], 'string', 'max' => 16],
            [['data', 'value'],'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'model_id' => 'Id Reference Value',
            'language' => 'Language',
            'value' => 'Значение',
        ];
    }
}
