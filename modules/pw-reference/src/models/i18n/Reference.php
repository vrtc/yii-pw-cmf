<?php

namespace pw\reference\models\i18n;

use Yii;

/**
 * This is the model class for table "crm_pw_references_i18n".
 *
 * @property integer $id
 * @property integer $model_id
 * @property string $language
 * @property string $name
 * @property string $description
 *
 * @property Reference $reference
 */
class Reference extends \pw\core\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pw_references_i18n}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language'], 'string', 'max' => 16],
            [['description'], 'string', 'max' => 255],
        ];
    }
}
