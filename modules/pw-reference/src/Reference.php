<?php

namespace pw\reference;

use pw\reference\models\ReferenceValue;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use pw\reference\models\Reference as ReferenceModel;

class Reference
{

    public function get($key)
    {
        $reference = ReferenceModel::findOne(['key' => $key]);
        if ($reference) {
            return ArrayHelper::map($reference->values, 'key', 'value');
        }

        return [];
    }

    /**
     * @param $key
     * @return array|ReferenceModel[]|\yii\db\ActiveRecord[]
     */
    public function getChildren($key)
    {
        $parent = ReferenceModel::findOne(['key' => $key]);
        if ($parent) {
            $references = ReferenceModel::find()->active()->andWhere(['parent_id' => $parent->id])->all();
            return $references;
        }
        return [];
    }

    public function create($data)
    {
        if (empty($data['key'])) {
            throw new InvalidConfigException('missing key reference');
        }
        if (empty($data['name'])) {
            throw new InvalidConfigException('missing name reference');
        }
        if (empty($data['values'])) {
            throw new InvalidConfigException('missing values reference');
        }
        $reference = new ReferenceModel();
        $reference->key = $data['key'];
        $reference->name = $data['name'];
        if ($reference->save()) {
            foreach ($data['values'] as $value) {
                $refValue = new ReferenceValue();
                $refValue->value = $value;
                $reference->link('values', $refValue);
            }
        }
    }
}
