<?php
namespace pw\rbac\interfaces;

interface Owner
{

    public function getOwnerId();
}
