<?php

namespace pw\rbac;

use pw\rbac\behaviors\AccessBehavior;
use pw\users\models\Users;
use yii\base\Application;
use yii\base\BootstrapInterface;

class Module extends \pw\core\Module implements BootstrapInterface
{

    public $cache = 'cache';

    public function bootstrap($app)
    {
        parent::bootstrap($app);
        $app->attachBehavior('access', AccessBehavior::class);
    }

    public function components(Application $app): array
    {
        return [
            'authManager' => [
                'class' => AuthManager::class,
            ]
        ];
    }


//    public function handlers(Application $app): array
//    {
//
//        return [
//            [
//                'class'    => Users::class,
//                'event'    => Users::EVENT_BEFORE_FORM_RENDER,
//                'callback' => function ($event) {
//                    $event->sender->attachBehavior('seo', RbacUserBehavior::class);
//                }
//            ]
//        ];
//    }

    public function getNavigation(): array
    {
        return [
            'settings' => [
                [
                    'label' => 'Управление правами',
                    'url'   => ['/pw-rbac/manage/index'],
                    'icon' => 'users-cog'
                ]
            ]
        ];
    }
}
