<?php
namespace pw\rbac;

use pw\core\helpers\StringHelper;
use Yii;
use yii\rbac\DbManager;
use yii\db\Query;
use yii\rbac\Assignment;

class AuthManager extends DbManager
{

    public $itemTable = '{{%pw_auth_item}}';

    public $itemChildTable = '{{%pw_auth_item_child}}';

    public $assignmentTable = '{{%pw_auth_assignment}}';

    public $ruleTable = '{{%pw_auth_rule}}';

    public $defaultRoles = ['guest'];

    /**
     * @inheritdoc
     */
    public function getAssignments($userId)
    {
        if (empty($userId)) {
            return $this->getPermissionsByRole('guest');
        }

        $query = (new Query)
            ->from($this->assignmentTable)
            ->where(['user_id' => $userId]);

        $assignments = [];
        foreach ($query->all($this->db) as $row) {
            $assignments[$row['item_name']] = new Assignment([
                'userId' => $row['user_id'],
                'roleName' => $row['item_name'],
                'createdAt' => $row['created_at'],
            ]);
        }
        return $assignments;
    }
}
