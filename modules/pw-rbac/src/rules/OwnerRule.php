<?php
namespace pw\rbac\rules;


use Yii;
use yii\rbac\Rule;
use pw\rbac\interfaces\Owner;

class OwnerRule extends Rule
{

    public $name = 'OwnerRule';

    public function execute($user, $item, $params): bool
    {
        if ($user && isset($params['model']) && $params['model'] instanceof Owner) {
            return $params['model']->getOwnerId() == $user;
        }
    }
}
