<?php

namespace pw\rbac\behaviors;

use Yii;
use yii\base\Action;
use yii\base\ActionFilter;
use yii\web\ForbiddenHttpException;
use yii\web\User;

class AccessBehavior extends ActionFilter
{

    /**
     * This method is invoked right before an action is to be executed (after all possible filters.)
     * You may override this method to do last-minute preparation for the action.
     *
     * @param Action $action the action to be executed.
     *
     * @return boolean whether the action should continue to be executed.
     * @throws \yii\web\ForbiddenHttpException
     */
    public function beforeAction($action): bool
    {
        if (Yii::$app->request->isConsoleRequest) {
            return true;
        }
        $user     = Yii::$app->getUser();

        $roles = Yii::$app->authManager->getRolesByUser($user->id);
        if(!Yii::$app->user->isGuest && array_key_exists('root', $roles)){
            return true;
        }
        if(Yii::$app->id == 'frontend'){
            return true;
        }
        $actionId = '/' . $action->getUniqueId();
        $id       = '/' . Yii::$app->id . $actionId;
        if ($user->can($id) || $user->can($actionId)) {
            return true;
        }
        $this->denyAccess($user);

        return false;
    }

    /**
     * Denies the access of the user.
     * The default implementation will redirect the user to the login page if he is a guest;
     * if the user is already logged, a 403 HTTP exception will be thrown.
     *
     * @param User $user the current user
     *
     * @throws ForbiddenHttpException if the user is already logged in.
     */
    protected function denyAccess(User $user)
    {
        $user->logout();
        if ($user->getIsGuest()) {
            $user->loginRequired();
        } else {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }
}
