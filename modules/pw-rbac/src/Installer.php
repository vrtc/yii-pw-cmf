<?php
namespace pw\rbac;

use pw\rbac\rules\GuestRule;
use pw\rbac\rules\OwnerRule;
use Yii;

class Installer extends \pw\modules\ModuleInstaller
{


    public function getVersion(): string
    {
        return '0.1';
    }

    public function getAuthors(): array
    {
        return ['i@yar.pw'];
    }

    public function getLink(): string
    {
        return 'http://yar.pw';
    }

    public function getName(): string
    {
        return 'Auth';
    }

    public function getSourceLanguage(): string
    {
        return 'ru-RU';
    }
}
