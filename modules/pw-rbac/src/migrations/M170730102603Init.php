<?php
namespace pw\rbac\migrations;

use pw\core\db\Migration;

class M170730102603Init extends Migration
{
    public function up()
    {
        require(\Yii::getAlias('@yii/rbac/migrations/m140506_102106_rbac_init.php'));

        $authManager = \Yii::$app->getAuthManager();

        $authManager->itemTable       = '{{%pw_auth_item}}';
        $authManager->itemChildTable  = '{{%pw_auth_item_child}}';
        $authManager->assignmentTable = '{{%pw_auth_assignment}}';
        $authManager->ruleTable       = '{{%pw_auth_rule}}';

        $migration = new \m140506_102106_rbac_init();
        $migration->up();

        $this->alterColumn($authManager->assignmentTable, 'user_id', $this->bigInteger()->unsigned());

        $rootPermission              = $authManager->createPermission('/');
        $rootPermission->description = 'Вся система';
        $authManager->add($rootPermission);

        $frontPermission              = $authManager->createPermission('/frontend');
        $frontPermission->description = 'Основной сайт';
        $authManager->add($frontPermission);
        $authManager->addChild($rootPermission, $frontPermission);

        $backPermission              = $authManager->createPermission('/backend');
        $backPermission->description = 'Административная часть';
        $authManager->add($backPermission);
        $authManager->addChild($rootPermission, $backPermission);

        $root              = $authManager->createRole('root');
        $root->description = 'Роль с полным доступом ко всей системе';
        $authManager->add($root);

        $ownerRule = new \pw\rbac\rules\OwnerRule();
        $authManager->add($ownerRule);

        $guest              = $authManager->createRole('guest');
        $guest->description = 'Неавторизованный пользователь';
        $authManager->add($guest);

        $user              = $authManager->createRole('user');
        $user->description = 'Авторизованный пользователь';
        $authManager->add($user);

        $owner           = $authManager->createRole('owner');
        $owner->ruleName = $ownerRule->name;
        $authManager->add($owner);

        $authManager->addChild($root, $rootPermission);
        $authManager->addChild($guest,$frontPermission);
        $authManager->addChild($user,$guest);

        $authManager->itemTable       = '{{%pw_auth_item}}';
        $authManager->itemChildTable  = '{{%pw_auth_item_child}}';
        $authManager->assignmentTable = '{{%pw_auth_assignment}}';
        $authManager->ruleTable       = '{{%pw_auth_rule}}';
    }

    public function down()
    {
        echo "M170730102603Init cannot be reverted.\n";

        return false;
    }

}
