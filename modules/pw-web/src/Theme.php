<?php
namespace pw\web;

use pw\core\traits\Settings;
use pw\modules\models\Modules;
use Yii;
use yii\base\Theme as BaseTheme;
use yii\helpers\FileHelper;
use yii\base\InvalidConfigException;

class Theme extends BaseTheme
{

    use Settings;

    const THEME_FRONTEND = Modules::TYPE_THEME_FRONTEND;
    const THEME_BACKEND = Modules::TYPE_THEME_BACKEND;

    public $id;
    public $uuid;

    protected $path = '';

    public function bloks()
    {
        return [
        ];
    }

    public function init()
    {
        parent::init();
        $this->pathMap = [
            '@app/views/layouts' => "$this->basePath/layouts",
            '@app/views/' => "$this->basePath/views",
            '@app/modules/*/src/' . Yii::$app->id . '/views/' => "$this->basePath/modules/*/",
            '@app/modules/*/src/widgets/views/' => "$this->basePath/widgets/*/",
            '@app/modules/*/src/*/views/' => "$this->basePath/modules/*/*/",
        ];
    }

    public function registerBaseAssets($view)
    {

    }


    /**
     * @param string $path
     * @return string
     * @throws InvalidConfigException
     */
    public function applyTo($path)
    {

        $pathMap = $this->pathMap;

        if (empty($pathMap)) {
            if (($basePath = $this->getBasePath()) === null) {
                throw new InvalidConfigException('The "basePath" property must be set.');
            }
            $pathMap = [Yii::$app->getBasePath() => [$basePath]];
        }
        $path = FileHelper::normalizePath($path);

        foreach ($pathMap as $from => $tos) {
            $from = FileHelper::normalizePath(Yii::getAlias($from)) . DIRECTORY_SEPARATOR;
            $tos = FileHelper::normalizePath(Yii::getAlias($tos)) . DIRECTORY_SEPARATOR;

            if (strpos($from, '*') !== false) {
                $files = glob($from);
                if (!$files) {
                    continue;
                }

                $from_array = explode(DIRECTORY_SEPARATOR, $from);
                $from_keys = array_keys($from_array, '*');

                $tos_array = explode(DIRECTORY_SEPARATOR, $tos);
                $tos_keys = array_keys($tos_array, '*');

                foreach ($files as $file) {
                    if (strpos($path, $file) === 0) {
                        $count = 0;
                        $arr = [];
                        foreach ($this->strpos_all($from, '*') as $k => $strpos) {
                            if ($count > 0) {
                                $count = $count - 1;
                            }
                            $d = substr($path, $strpos + $count);
                            $arr[$k] = substr($d, 0, strpos($d, DIRECTORY_SEPARATOR));
                            $count = strlen($arr[$k]);

                                $from_array[$from_keys[$k]] = $arr[$k];
                                $tos_array[$tos_keys[$k]] = $arr[$k];
                        }
                        $from = implode(DIRECTORY_SEPARATOR, $from_array);
                        $tos = implode(DIRECTORY_SEPARATOR, $tos_array);


                    }
                }

            }

            if (strpos($path, $from) === 0) {
                $n = strlen($from);
                foreach ((array)$tos as $to) {
                    $to = FileHelper::normalizePath(Yii::getAlias($to)) . DIRECTORY_SEPARATOR;
                    $file = $to . substr($path, $n);
                    if (is_file($file)) {
                        return $file;
                    }
                }
            }
        }

        return $path;
    }

    /**
     * @param $haystack
     * @param $needle
     * @return array
     */
    protected function strpos_all($haystack, $needle)
    {
        $offset = 0;
        $allpos = array();
        while (($pos = strpos($haystack, $needle, $offset)) !== FALSE) {
            $offset = $pos + 1;
            $allpos[] = $pos;
        }
        return $allpos;
    }


}
