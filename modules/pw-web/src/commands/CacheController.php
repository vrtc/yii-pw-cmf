<?php

namespace pw\web\commands;


use pw\console\Controller;
use pw\seo\models\Seo;
use yii\helpers\Console;
use yii\helpers\FileHelper;
use yii\helpers\VarDumper;

class CacheController extends Controller
{

    /**
     * Очи
     */
    public function actionIndex()
    {

        $clearMinify = \Yii::$app->fileManager->purgeContent(\Yii::getAlias('@webroot') . '/minify/');
        Console::output('Удалено: ' . $clearMinify);
        \Yii::$app->runAction('cache/flush-all');
    }

}
