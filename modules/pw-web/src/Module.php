<?php

namespace pw\web;

use Yii;
use yii\base\Application;
use yii\base\BootstrapInterface;
use yii\caching\FileCache;
use yii\caching\MemCache;

class Module extends \pw\core\Module implements BootstrapInterface
{

    public $defaultRoute = 'default/index';

    public $defaultApplication = 'frontend';

    public $backendUrlPrefix = 'admin';

    public $frontendTheme;

    public $backendTheme;

    public $timeZone;

    public $dateFormat = 'dd.MM.yyyy';

    public $timeFormat = 'medium';

    public function init()
    {
        parent::init();
        Yii::$container->set('yii\i18n\Formatter', [
            'timeFormat' => $this->timeFormat,
            'dateFormat' => $this->dateFormat,
            'timeZone' => $this->timeZone
        ]);
    }

    public function components(Application $app): array
    {

            return [
                'cache' => [
                    'class' => FileCache::class,
                ],
                /*'cache' => [
                    'class' => MemCache::class,
                    'servers' => [
                        [
                            'host' => getenv('CACHE_SERVER') ? getenv('CACHE_SERVER') : 'localhost',
                            'port' => 11211,
                            'weight' => 100,
                        ],
                    ]
                ],*/
               /* 'cache' => [
                    'class' => 'yii\redis\Cache',
                    'redis' => [
                        'hostname' => 'localhost',
                        'port' => 6379,
                        'database' => 0,
                    ]
                ],*/
               /* 'cache' => [
                    'class' => 'yii\caching\DbCache',
                    // 'db' => 'mydb',
                    // 'cacheTable' => 'my_cache',
                ],*/
                'reCaptcha' => [
                    'class' => 'himiklab\yii2\recaptcha\ReCaptchaConfig',
                    'siteKeyV2' => '6Lc-IMgUAAAAAEKfgFD2HLGYlhie0E91dyPPtZfu',
                    'secretV2' => '6Lc-IMgUAAAAAEj5cN7toZmIcN4zT1NVSekMVQQg',
                ],
                'mobileDetect' => [
                    'class' => '\skeeks\yii2\mobiledetect\MobileDetect'
                ]
            ];

    }

    public function routes(Application $app): array
    {
        return [
            [
                'pattern' => '/',
                'route'   => '/pw-web/default/index',
                'name'    => self::t('route', 'Главная страница'),
            ],
            [
                'pattern' => '/contacts',
                'route'   => '/pw-web/default/contacts',
                'name'    => self::t('route', 'Форма обратной связи'),
            ],
            [
                'pattern' => '/html-map',
                'route'   => '/pw-web/default/map',
                'name'    => 'Карта сайта',
            ],
        ];
    }

    public function getNavigation(): array
    {
        return [
            'dashboard' => [
                'label' => 'Рабочий стол',
                'icon' => 'tachometer-alt',
                'url' => ['/pw-web/default/index'],
                'sort' => 100,
            ],
            'marketing' => [
                'label' => 'Маркетинг',
                'icon' => 'bullhorn',
                'sort' => 200
            ],
            'content' => [
                'label' => 'Контент',
                'icon' => 'newspaper',
                'sort' => 300
            ],
            'system' => [
                'label' => 'Система',
                'sort' => 400,
                'icon' => 'cube',
            ],
            'settings' => [
                'label' => 'Настройки',
                'icon' => 'cogs',
                'sort' => 999
            ]
        ];
    }
}
