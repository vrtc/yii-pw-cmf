<?php

namespace pw\web;

use pw\web\models\Widgets as UrlRuleModel;
use Yii;
use pw\core\traits\GetModule;
use yii\caching\Cache;
use yii\helpers\Url;

class UrlManager extends \yii\web\UrlManager
{
    use GetModule;

    protected $toBackend;
    protected $toFrontend;

    public $enableLocaleUrls = true;

    public $enablePrettyUrl = true;

    public $showScriptName = false;

    public $ruleConfig = ['class' => UrlRule::class];

    protected $_backendPrefix;

    public function init()
    {

        if(!isset(\Yii::$app->settings['pw-web']['settings']['backendUrlPrefix'])){
            $prefix = 'admin';
        }else{
            $prefix = \Yii::$app->settings['pw-web']['settings']['backendUrlPrefix'];
        }
        $this->_backendPrefix = \trim($prefix, '/');
        parent::init();
    }

    /**
     * @return array Rules
     */
    public function getRules()
    {
        $rules = [];
       /* foreach ($this->rules as $rule) {
            if (!property_exists($rule, 'rawPattern') || $rule->name !== $rule->rawPattern) {
                $rules[] = $rule;*/
//                    'id'    => $rule->id ?? '',
//                    'name'  => $rule->name,
//                    'route' => $rule->route,
//                    'model' => property_exists($rule, 'model') ? $rule->model : null
//                ];
        /*    }
        }*/

        return $this->rules;
    }

    public function createUrlFromModel($model)
    {
        $class = \get_class($model);
        foreach ($this->rules as $rule) {
            if (isset($rule->model) && $rule->model === $class) {
                $params   = [$rule->route];
                $params[] = $rule->getParamsFromModel($model);

                return $this->createUrl($params);
            }
        }
        throw new \InvalidArgumentException('Url for model not found');
    }

    public function createUrl($params)
    {
        $this->determineApplication($params);
      //  $params = array_map('\strval', $params);
        $url    = parent::createUrl($params);
        $url    = preg_replace('#(\%2F|/)+#i', '/', $url, -1); // BUG
        return $url;
    }

    public function getBaseUrl()
    {
        try {
            $baseUrl = parent::getBaseUrl();
            if ($this->toBackend && $this->_backendPrefix) {
                $baseUrl = "/$this->_backendPrefix/$baseUrl";
            }

            return $baseUrl;
        }catch (\Exception $e){
         //   return env('BASE_URL');
        }
    }

    protected function determineApplication(&$params)
    {
        if (isset($params['toFrontend'])) {
            $this->toFrontend = !$this->toBackend = false;
            unset($params['toFrontend']);
        } elseif (isset($params['toBackend'])) {
            $this->toBackend = !$this->toFrontend = false;
            unset($params['toBackend']);
        } else {
            $this->toFrontend = !$this->toBackend = Yii::$app->isBackend();
        }
    }
}
