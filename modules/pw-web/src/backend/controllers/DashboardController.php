<?php
namespace pw\web\backend\controllers;

use pw\web\Controller;

class DashboardController extends Controller{

    public function actionIndex(){
        return $this->render('index');
    }

}