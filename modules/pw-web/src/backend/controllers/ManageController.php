<?php
namespace pw\web\backend\controllers;

use pw\web\Controller;

class ManageController extends Controller{

    public function actionIndex(){
        return $this->render('index');
    }

    public function actionClearCache()
    {
        \Yii::$app->cache->flush();
        return $this->redirect(['/']);
    }

    public function actionUpdate(){

    }

}