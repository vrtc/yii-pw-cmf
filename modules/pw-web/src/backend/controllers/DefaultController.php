<?php
namespace pw\web\backend\controllers;

use pw\web\Controller;

class DefaultController extends Controller{

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex(){
        return $this->render('index');
    }

}