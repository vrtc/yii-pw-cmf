<?php

namespace pw\web;

use Yii;
use yii\helpers\Url;

class Controller extends \yii\web\Controller
{

    public function addBreadcrumb($label, $url = null)
    {
        $this->view->params['breadcrumbs'][] = ['label' => $label, 'url' => $url];
    }

    public function setPageTitle($title)
    {
        $this->view->title = $title;
    }

    public function setPageDescription($description)
    {
        $this->view->params['description'] = $description;
    }

    public function setPageKeywords($keywords)
    {
        $this->view->params['keywords'] = $keywords;
    }

    public function render($view, $params = [])
    {
        if (Yii::$app->request->getIsAjax()) {
            if($this->view->viewFileExists($view.'-ajax')){
                $view.='-ajax';
            }
            return parent::renderAjax($view, $params);
        }
        return parent::render($view, $params);
    }
}