<?php
namespace pw\web;

use Yii;

class Installer extends \pw\modules\ModuleInstaller
{


    public function getVersion():string
    {
        return '0.1';
    }

    public function getAuthors():array
    {
        return ['i@yar.pw'];
    }

    public function getLink():string
    {
        return 'http://yar.pw';
    }

    public function getName():string
    {
        return 'Web';
    }

    public function getSourceLanguage():string
    {
        return 'ru-RU';
    }
}
