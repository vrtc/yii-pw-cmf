<?php
use yii\helpers\Html;
?>
<div class="site-error">

    <h1><?=Html::encode($title)?></h1>

    <div class="alert alert-danger">
        <?=$message?>
    </div>

</div>
