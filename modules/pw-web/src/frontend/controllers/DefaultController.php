<?php

namespace pw\web\frontend\controllers;

use ct\catalog\models\Category;
use ct\catalog\models\product\Product;
use pw\banners\models\Banners;
use pw\reference\Reference;
use pw\users\models\WishList;
use pw\web\Controller;
use pw\web\models\Contacts;
use yii\filters\PageCache;
use yii\web\Cookie;
use yii\web\Response;

class DefaultController extends Controller
{

    public function behaviors()
    {
        return [
             [
                 'class' => PageCache::class,
                 'only' => ['index'],
                 'duration' => 86400,
                 'enabled' => env('CACHE_GLOBAL'),
                 'variations' => [
                     \Yii::$app->request->getUrl(),
                     \Yii::$app->mobileDetect->isMobile(),
                     \Yii::$app->mobileDetect->isiOS(),
                     get_operating_system()
                 ],
                 /*'dependency' => [
                     'class' => 'yii\caching\DbDependency',
                     'sql' => 'SELECT COUNT(*) FROM post',
                 ],*/
             ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex($key = null)
    {

        $banners = Banners::find()->joinWith('place')->where(['pw_banners_places.alias' => 'MainBanners'])->indexBy('alias')->all();
        return $this->render('index', [
            'banners' => $banners,
        ]);
    }

    public function actionSetCity()
    {
        if (\Yii::$app->request->isAjax) {
            if (\Yii::$app->request->cookies->getValue('city')) {
                \Yii::$app->getResponse()->getCookies()->remove('city');
            }
            \Yii::$app->getResponse()->getCookies()->add(new Cookie([
                'name' => 'city',
                'value' => \Yii::$app->request->get('city_id'),
                'expire' => time() + 86400 * 365,
            ]));

            \Yii::$app->response->format = Response::FORMAT_JSON;
            return [];
        }

    }

    public function actionSetDelivery()
    {
        if (\Yii::$app->request->isAjax) {
            $city = \Yii::$app->getRequest()->getCookies()->getValue('city');
            $reference = new Reference();
            \Yii::$app->response->format = Response::FORMAT_JSON;
            $cookieArr = [];
            foreach ($reference->getChildren('sh-shipping') as $delivery) {
                $component = new $delivery->values['namespace']->value();
                $values = $reference->get($component->getKey());
                $component->setAttributes($values);
                switch ($component->getKey()) {
                    case "moscow_shipping":
                        if ($city == 4400) {
                            $cookieArr[] = [
                                'price' => \Yii::$app->formatter->asCurrency(350),
                                'name' => $delivery->values['namespace']->value::getName(),
                            ];
                        }
                        break;
                    case "pickup":
                        if ($city == 4400) {
                            $cookieArr[] = [
                                'price' => "Бесплатно",
                                'name' => $delivery->values['namespace']->value::getName(),
                            ];
                        }
                        break;
                    default:
                        if ($city != 4400) {
                            $price = $component->calculate($city, [], true, false);
                            $cookieArr[] = [
                                'price' => $price ? \Yii::$app->formatter->asCurrency($price) : 'Недоступно',
                                'name' => $delivery->values['namespace']->value::getName(),
                            ];

                        }
                        break;
                }
            }
            \Yii::$app->getResponse()->getCookies()->remove('delivery');
            \Yii::$app->getResponse()->getCookies()->add(new Cookie([
                'name' => 'delivery',
                'value' => serialize($cookieArr),
                'expire' => time() + 86400 * 365,
            ]));
        }
    }

    /**
     * @param $product_id
     */
    public function actionAddWishlist($product_id)
    {
        if (\Yii::$app->request->isAjax) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            $returnArr = [];
            if (!(\Yii::$app->user->isGuest)) {
                if (!isset(\Yii::$app->user->identity->wishlist[$product_id])) {
                    if (Product::findOne(['id' => $product_id])) {
                        $model = new WishList();
                        $model->product_id = $product_id;
                        $model->user_id = \Yii::$app->user->id;
                        $model->save();
                        $returnArr = [
                            'success' => true,
                        ];
                    }
                    else {
                        $returnArr = [
                            'success' => false,
                            'message' => 'Товар не найден'
                        ];
                    }
                }
                else {
                    $returnArr = [
                        'success' => false,
                        'message' => 'Товар уже добавлен'
                    ];
                }
            }
            else {
                $returnArr = [
                    'success' => false,
                    'guest' => 1,
                    'message' => 'Пользователь не авторизован'
                ];
            }
            return $returnArr;
        }
    }

    /**
     * @return string
     */
    public function actionContacts()
    {
        $model = new Contacts();
        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            \Yii::$app->session->setFlash('success', 'Спасибо за ваще письмо, мы ответим вам в ближайшее время!');
            return $this->redirect('/');
        }
        return $this->render('contacts', [
            'model' => $model,
        ]);
    }

    public function actionMap()
    {
        $models = Category::find()->roots()->active()->orderBy(['sort' => SORT_ASC])->all();

        return $this->render('map', [
            'models' => $models,
        ]);
    }
}
