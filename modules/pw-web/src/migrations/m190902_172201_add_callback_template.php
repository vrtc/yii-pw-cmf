<?php
namespace pw\web\migrations;

use pw\core\db\Migration;

class m190902_172201_add_callback_template extends Migration
{
    public function up()
    {
        $template = new \pw\mailer\models\Template();
        $template->key = 'system.callback';
        $template->placeholders = '{user.email}{text}';
        $template->save();
    }

    public function down()
    {
        echo "m190902_172201_add_callback_template cannot be reverted.\n";

        return false;
    }

}
