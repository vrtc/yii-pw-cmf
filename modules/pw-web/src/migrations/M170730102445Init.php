<?php
namespace pw\web\migrations;

use pw\core\db\Migration;

class M170730102445Init extends Migration
{
    public function up()
    {
        $this->createTable('{{%pw_widgets}}', [
            'widget_id'  => $this->bigPrimaryKey()->unsigned(),
            'theme_id'   => $this->bigInteger()->unsigned(),
            'layout'     => $this->string(),
            'route'    => $this->string(),
            'model_id'   => $this->bigInteger()->unsigned(),
            'place'   => $this->string(),
            'settings'   => $this->text(),
            'order'   =>$this->smallInteger()->unsigned(),
            'is_active'  => $this->boolean()->defaultValue(1)
        ]);

        $this->createIndex('idx_widget', '{{%pw_widgets}}', 'widget_id');
        $this->createIndex('idx_theme', '{{%pw_widgets}}', ['theme_id','layout','route']);
        $this->createIndex('idx_model', '{{%pw_widgets}}', 'model_id');
        $this->createIndex('idx_active', '{{%pw_widgets}}', 'is_active');
        $this->createIndex('idx_order', '{{%pw_widgets}}', 'order');
    }

    public function down()
    {
        echo "M170730102445Init cannot be reverted.\n";

        return false;
    }

}
