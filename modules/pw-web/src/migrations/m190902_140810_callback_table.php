<?php
namespace pw\web\migrations;

use pw\core\db\Migration;

class m190902_140810_callback_table extends Migration
{
    public function up()
    {
        $this->createTable('{{%pw_callback}}', [
            'id' => $this->primaryKey(),
            'email' => $this->string(255)->notNull(),
            'text' => $this->text()->null(),
            'name' => $this->string(255)->notNull(),
            'subject' => $this->string(255)->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%pw_callback}}');
    }
}
