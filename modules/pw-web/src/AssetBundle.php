<?php
namespace pw\web;

use Yii;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class AssetBundle extends \yii\web\AssetBundle
{

    protected $addJS = [];
    protected $addCSS = [];

    public static function registerJS(string $path, array $files)
    {
        $am = Yii::$app->getAssetManager();
        $bundle = $am->getBundle(get_called_class(), false);
        $path = Yii::getAlias($path);
        foreach ($files as $old => $file) {
            if (!file_exists("$path/$file")) {
                throw new InvalidParamException("File: $file not exists in $path");
            }
            $bundle->addJS[$path][] = $file;
        }
    }


    public static function registerCSS()
    {
    }
}
