<?php
namespace pw\web;

interface PageInterface {

    public function getSlug();

    public function getTitle();

    public function getAuthor();
}