<?php

namespace pw\web;

use yii\base\InvalidConfigException;

class UrlRule extends \yii\web\UrlRule
{

    public $id;

    public $model;

    public $rawPattern;

    public $internal = false;


    public function init()
    {
        if ($this->pattern === null) {
            throw new InvalidConfigException('UrlRule::pattern must be set.');
        }
        if ($this->route === null) {
            throw new InvalidConfigException('UrlRule::route must be set.');
        }
        $this->rawPattern = $this->pattern;
        parent::init();
    }

    public function getModelInstance($params)
    {
        $condition = [];
        foreach ($this->getParamsFromModel(new $this->model) as $param){
            if(isset($params[$param])){
                $condition[$param] = $params[$param];
            }
        }
        return $this->model::find()->where($condition)->one();
    }


    public function getParamsFromModel($model)
    {

        $attributes = $model->toArray();
        $params     = [];

        foreach ($this->getParamRules() as $param => $rule) {

            if (isset($attributes[$param])) {
                $params[$param] = $attributes[$param];
            }
        }

        return $params;
    }
}
