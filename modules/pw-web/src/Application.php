<?php

namespace pw\web;

use Yii;
use yii\caching\FileCache;
use yii\helpers\ArrayHelper;
use pw\modules\ModulesManager;
use yii\i18n\Formatter;
use yii\web\DbSession;
use yii\web\ErrorHandler;
use yii\web\User;


/**
 * Class Application
 * @property ModulesManager $modulesManager
 */
class Application extends \yii\web\Application
{

    public $name = 'Pw Application';

    public $layout = '/main';

    public $defaultRoute = 'pw-web/default/index';

    public $theme;

    public $errorAction = 'pw-web/default/error';

    public $baseUrl;

    public $asset;

    public $settings;

    protected $events = [];
    protected $models = [];
    private $_module;
    private $_vendorPath;

    public function __construct($config = [])
    {
        $defaultConfig = [
            'components' => [
                'request' => [
                    'cookieValidationKey' => env('COOKIE_VALIDATION_KEY'),
                    'parsers' => [
                        'application/json' => 'yii\web\JsonParser',
                    ]
                ],
                'db' => [
                    'class' => 'pw\i18n\db\Connection',
                    'dsn' => 'mysql:host=' . env('DB_HOST') . ';dbname=' . env('DB_NAME'),
                    'username' => env('DB_USER'),
                    'password' => env('DB_PASS'),
                    'charset' => 'utf8',
                    'tablePrefix' => env('DB_PREFIX'),
                    'enableSchemaCache' => env('SCHEMA_CACHE'),
                    'schemaCacheDuration' => 3600,
                    'schemaCache' => 'cache',
                    'enableLogging' => env('DEBUG'),
                    'enableProfiling' => env('DEBUG'),
                ],
                'assetManager' => [
                    'appendTimestamp' => true,
                    'linkAssets' => true,
                    'bundles' => [
                        //  'yii\web\JqueryAsset' => false,
                        'yii\bootstrap\BootstrapAsset' => false,
                      //  'yii\bootstrap4\BootstrapAsset' => false,
                        'yii\bootstrap\BootstrapPluginAsset' => false,
                       // 'yii\bootstrap4\BootstrapPluginAsset' => false,
                    ],
                ],
                'formatter' => [
                    'class' => Formatter::class,
                    'dateFormat' => 'php:d.m.Y',
                    'timeFormat' => 'php:H:i:s',
                    'defaultTimeZone' => 'Europe/Moscow',
                    'timeZone' => 'Europe/Moscow',
                    'decimalSeparator' => ',',
                    'numberFormatterOptions' => [
                        \NumberFormatter::MIN_FRACTION_DIGITS => 0,
                        \NumberFormatter::MAX_FRACTION_DIGITS => 0,
                    ],
                    'numberFormatterSymbols' => [
                        \NumberFormatter::CURRENCY_SYMBOL => '₽',
                    ],
                    /*'currencyCode' => 'RUB',*/
                    'nullDisplay' => '-',
                ],
            ]
        ];

        parent::__construct(
            ArrayHelper::merge($defaultConfig, $config)
        );
        Yii::$app = $this;
    }


    /**
     * @inheritdoc
     */
    public function setVendorPath($path)
    {
        $this->_vendorPath = Yii::getAlias($path);
        Yii::setAlias('@vendor', $this->_vendorPath);
        Yii::setAlias('@bower', $this->_vendorPath . DIRECTORY_SEPARATOR . 'bower-asset');
        Yii::setAlias('@npm', $this->_vendorPath . DIRECTORY_SEPARATOR . 'npm-asset');
    }

    public function preInit(&$config)
    {
        parent::preInit($config);
        $basePath = $this->getBasePath();
        Yii::setAlias('@themes', $basePath . DIRECTORY_SEPARATOR . 'themes');
        Yii::setAlias('@modules', $basePath . DIRECTORY_SEPARATOR . 'modules');
        Yii::setAlias('@root', $basePath . DIRECTORY_SEPARATOR );
    }

    public function init()
    {

        $this->getModulesManager()->load();

        $this->settings = Yii::$app->cache->getOrSet('settings_modules', function () {
            return Yii::$app->getModulesManager()->installed();
        });

        $settings = $this->settings['pw-web']['settings'];
        $this->id = $settings['defaultApplication'];
        if (!empty($settings['backendUrlPrefix'])) {
            $parts = explode('/', $this->getRequest()->getPathInfo());
            if ($parts[0] === trim($settings['backendUrlPrefix'])) {
                unset($parts[0]);
                $this->getRequest()->setPathInfo(implode('/', $parts));
                $this->id = 'backend';
            }
        }
        parent::init();
        $this->errorHandler->errorAction = $this->errorAction;
        if ($this->isBackend()) {
            $theme = $settings['backendTheme'];
        } else {
            $theme = $settings['frontendTheme'];
        }

        if ($theme) {
            $theme = $this->getModulesManager()->installed($theme);
            if ($theme) {
                Yii::setAlias('@theme', $theme['path']);

                $this->view->theme = Yii::createObject([
                    'id' => $theme['key'],
                    'uuid' => $theme['id'],
                    'basePath' => $this->basePath . '/' . $theme['path'] . '/src',
                    'class' => $theme['namespace'] . '\Theme',
                ]);
                if($this->isBackend()) {
                    $this->asset = $this->view->theme->registerBaseAssets($this->view);
                }
            }
        }
    }

    public function registerModels($models)
    {
        $this->models = array_merge($this->models, $models);
    }

    public function getModels()
    {
        return $this->models;
    }

    public function getNavigation()
    {
        if ($this->isFrontend()) {
            return [];
        }
        $menu = [];
        foreach ($this->getModules() as $id => $module) {
            if (\is_array($module)) {
                $module = $this->getModule($id);
            }
            if ($module instanceof \pw\core\Module) {
                $navigation = $module->getNavigation();
                foreach ($navigation as $key => $item) {
                    if (!isset($menu[$key])) {
                        $menu[$key] = [];
                    }
                    if (isset($item['label'])) {
                        $menu[$key] = \array_merge_recursive($menu[$key], $item);
                    } else {
                        if (!isset($menu[$key]['items'])) {
                            $menu[$key]['items'] = [];
                        }
                        $menu[$key]['items'] = \array_merge($menu[$key]['items'], $item);
                    }
                }
            }
        }
        \usort($menu, function ($a, $b) {
            if (isset($a['sort'], $b['sort'])) {
                return $a['sort'] - $b['sort'];
            }
            return 0;
        });
        return $menu;
    }

    /**
     * @inheritdoc
     */
    public
    function coreComponents()
    {
        return array_merge(parent::coreComponents(), [
            'modulesManager' => ['class' => ModulesManager::class],
            'view' => [
                'class' => View::class
            ],
            'response' => ['class' => \yii\web\Response::class],
            'user' => ['class' => User::class],
            'errorHandler' => ['class' => ErrorHandler::class],
            'urlManager' => ['class' => UrlManager::class],
            'cache' => ['class' => FileCache::class],
            'session' => ['class' => DbSession::class, 'sessionTable' => '{{%pw_sessions}}']
        ]);
    }

    /**
     * @inheritdoc
     */
    public function trigger($name, \yii\base\Event $event = null)
    {
        if ($event === null && $name instanceof \yii\base\Event && $name->name) {
            $event = $name;
            $name = $event->name;
        }

        parent::trigger($name, $event);
    }


    /**
     * Returns the modules manager component.
     * @return \pw\modules\ModulesManager the modules manager.
     * @throws \yii\base\InvalidConfigException
     */
    public function getModulesManager()
    {
        return $this->get('modulesManager');
    }

    /**
     * @return bool
     */
    public function isBackend()
    {
        return $this->id === 'backend';
    }

    /**
     * @return bool
     */
    public function isFrontend()
    {
        return $this->id === 'frontend';
    }

    /**
     * @param $module
     * @param $param
     * @return mixed|null
     */
    public function getConfig($module, $param = null){
        if(isset($this->settings[$module]['settings'])){
            if($param){
                return $this->settings[$module]['settings'][$param];
            }
            return $this->settings[$module]['settings'];
        }
        return null;
    }
}
