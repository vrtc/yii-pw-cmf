<?php

namespace pw\web\models;

use pw\core\db\ActiveRecord;
use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "crm_pw_widgets".
 *
 * @property string $widget_id
 * @property string $theme_id
 * @property string $layout
 * @property string $route
 * @property string $model_id
 * @property string $place
 * @property string $settings
 * @property integer $order
 * @property boolean $is_active
 */
class Widgets extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pw_widgets}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['widget_id','theme_id','model_id'],'safe'],
            [['route', 'layout', 'place'], 'string', 'max' => 255],
            [['settings'], 'string'],
            [['order'],'integer'],
            [['is_active'],'boolean']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pattern' => 'Pattern',
            'route' => 'Route',
            'name' => 'Name',
            'modelClass' => 'Model Class',
        ];
    }

    public function getWidget(){
        return $this->hasOne(\pw\modules\models\Widgets::class,['id'=>'widget_id']);
    }

    public function run(){
        $class = $this->widget->class;
        $params = Json::decode($this->settings);
        return $class::widget($params);
    }
}
