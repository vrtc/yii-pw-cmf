<?php
namespace pw\web\models;



class SettingsModel extends \pw\modules\SettingsModel
{
    public $defaultApplication;

    public $backendUrlPrefix;

    public $frontendTheme;

    public $backendTheme;

    public $timeZone;

    public $dateFormat;

    public $timeFormat;
}
