<?php
namespace pw\banners\migrations;
use pw\core\db\Migration;

class m190529_080930_add_sort_field extends Migration
{
    public function up()
    {
        $this->addColumn('{{%pw_banners}}', 'sort', 'INTEGER');
    }

    public function down()
    {
        $this->dropColumn('{{%pw_banners}}', 'sort');
    }

}
