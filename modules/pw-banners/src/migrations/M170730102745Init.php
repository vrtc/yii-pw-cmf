<?php
namespace pw\banners\migrations;

use pw\core\db\Migration;

class M170730102745Init extends Migration
{
    public function up()
    {

        $this->createTable('{{%pw_banners_places}}', [
            'id' => $this->bigPrimaryKey()->unsigned(),
            'alias' => $this->string(),
            'name' => $this->string(),
            'is_active' => $this->boolean()
        ]);

        $this->createTable('{{%pw_banners}}', [
            'id' => $this->bigPrimaryKey()->unsigned(),
            'place_id' => $this->bigInteger()->unsigned(),
            'link' => $this->string(),
            'image' => $this->bigInteger()->unsigned(),
            'name' => $this->string(),
            'alias' => $this->string(),
            'description' => $this->text(),
            'start_date' => $this->dateTime(),
            'end_date' => $this->dateTime(),
            'is_active' => $this->boolean()
        ]);

        $this->addForeignKey(
            'fk_banners_places_id',
            '{{%pw_banners}}',
            'place_id',
            '{{%pw_banners_places}}',
            'id',
            'cascade',
            'cascade'
        );

        $this->addForeignKey(
            'fk-banners-image',
            '{{%pw_banners}}',
            'image',
            '{{%pw_filemanager_files}}',
            'id',
            'SET NULL',
            'NO ACTION'
        );

        $this->createIndex('idx_active', '{{%pw_banners}}', 'is_active');
        $this->createIndex('idx_date', '{{%pw_banners}}', ['start_date', 'end_date']);

        $this->createTable('{{%pw_banners_stats}}', [
            'banner_id' => $this->bigPrimaryKey()->unsigned(),
            'views' => $this->bigInteger()->unsigned(),
            'hits' => $this->bigInteger()->unsigned()
        ]);

        $this->addForeignKey(
            'fk_banners_stats_banner_id',
            '{{%pw_banners_stats}}',
            'banner_id',
            '{{%pw_banners}}',
            'id',
            'cascade',
            'cascade'
        );
    }

    public function down()
    {
        $this->dropTable('{{%pw_banners_stats}}');
        $this->dropTable('{{%pw_banners}}');
        $this->dropTable('{{%pw_banners_places}}');
    }

}
