<?php

namespace pw\banners\backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use pw\banners\models\Banners;

/**
 * SearchReference represents the model behind the search form of `app\models\PwReference`.
 */
class SearchBanners extends Banners
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['place_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Banners::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'sort' => SORT_ASC,
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'place_id'=>$this->place_id,
            'link' => $this->link
        ]);


        return $dataProvider;
    }
}
