<?php

namespace pw\banners\backend\controllers;


use pw\banners\backend\models\SearchBanners;
use pw\banners\backend\models\SearchPlaces;
use pw\banners\models\Banners;
use pw\banners\models\Places;
use Yii;
use yii\filters\VerbFilter;
use pw\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * ManageController implements the CRUD actions for Banners model.
 */
class ManageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Places models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new SearchPlaces();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->setPageTitle(Yii::t('banners', 'Баннерные места'));
        $this->addBreadcrumb(Yii::t('banners', 'Баннерные места'));

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Banners models.
     * @return mixed
     */
    public function actionView($id)
    {
        $place                               = $this->findModel($id);
        $searchModel                         = new SearchBanners();
        $params                              = Yii::$app->request->queryParams;
        $params['SearchBanners']['place_id'] = $id;
        $dataProvider                        = $searchModel->search($params);
        $this->setPageTitle(Yii::t('banners', 'Баннеры'));
        $this->addBreadcrumb(Yii::t('banners', 'Баннерные места'), ['index']);
        $this->addBreadcrumb($place->name);

        return $this->render('view', [
            'place'        => $place,
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Places model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionCreate()
    {
        $model = new Places();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('banners', 'Изменения сохранены'));
            return $this->redirect(['manage/view', 'id' => $model->id]);
        }
        $this->setPageTitle(Yii::t('Banners', 'Новое баннерное место'));
        $this->addBreadcrumb(Yii::t('Banners', 'Баннерные места'), ['index']);
        $this->addBreadcrumb(Yii::t('Banners', 'Новое баннерное место'));

        return $this->render('form', [
            'model' => $model,
        ]);

    }

    /**
     * Updates an existing Banners model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('banners', 'Изменения сохранены'));

            return $this->redirect(['index']);
        }

        $this->setPageTitle(Yii::t('Banners', 'Редактирование баннера {name}', ['name' => $model->name]));
        $this->addBreadcrumb(Yii::t('Banners', 'Баннеры'), ['index']);
        $this->addBreadcrumb($model->name, ['view', 'id' => $model->id]);
        $this->addBreadcrumb(Yii::t('Banners', 'Редактирование'));

        return $this->render('form', [
            'model' => $model,
        ]);
    }


    /**
     * Deletes an existing Banners model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Banners model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Banners the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Places::findOne(['id' => $id])) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
