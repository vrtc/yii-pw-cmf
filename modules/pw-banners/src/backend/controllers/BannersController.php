<?php

namespace pw\banners\backend\controllers;


use pw\banners\backend\models\SearchBanners;
use pw\banners\models\Banners;
use pw\banners\models\Places;
use Yii;
use yii\filters\VerbFilter;
use pw\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * ManageController implements the CRUD actions for Banners model.
 */
class BannersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Creates a new Banners model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionCreate($place_id)
    {
        $place = $this->findPlaceModel($place_id);
        $model = new Banners();
        $model->place_id = $place_id;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->checkImage();
            $model->save();
            return $this->redirect(['manage/view', 'id' => $model->place_id]);
        }
        $this->setPageTitle(Yii::t('Banners', 'Новый баннер'));
        $this->addBreadcrumb(Yii::t('banners', 'Баннерные места'),['manage/index']);
        $this->addBreadcrumb($place->name, ['manage/view','id'=>$place->id]);
        $this->addBreadcrumb(Yii::t('Banners', 'Новый баннер'));

        return $this->render('form', [
            'model' => $model,
        ]);

    }


    public function actionDeleteImage($id)
    {
        if(Yii::$app->request->isAjax) {
            $banner = $this->findModel($id);
            $banner->image = null;
            $banner->save();
        }
    }

    /**
     * Updates an existing Banners model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $place = $this->findPlaceModel($model->place_id);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->checkImage();
            $model->save();
            Yii::$app->session->setFlash('success',Yii::t('banners','Изменения сохранены'));
            return $this->redirect(['manage/view', 'id' => $model->place_id]);
        }

        $this->setPageTitle(Yii::t('Banners', 'Редактирование баннера {name}', ['name' => $model->name]));
        $this->addBreadcrumb(Yii::t('banners', 'Баннерные места'),['manage/index']);
        $this->addBreadcrumb($place->name, ['manage/view','id'=>$place->id]);
        $this->addBreadcrumb(Yii::t('Banners', 'Редактирование {name}',['name'=>$model->name]));

        return $this->render('form', [
            'model' => $model,
        ]);
    }


    /**
     * Deletes an existing Banners model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();
        return $this->redirect(['manage/view', 'id' => $model->place_id]);
    }

    /**
     * Finds the Banners model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Banners the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Banners::findOne(['id' => $id])) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findPlaceModel($id)
    {
        if (($model = Places::findOne(['id' => $id])) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
