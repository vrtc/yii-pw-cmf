<?php

use pw\banners\models\Banners;
use pw\ui\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $place \pw\banners\models\Places */

?>
<?= GridView::widget([
    'model' => $searchModel,
    'filterModel' => $searchModel,
    'dataProvider' => $dataProvider,
    'filter' => false,
    'title' => Yii::t('banners', 'Баннеры'),
    'icon' => 'image',
    'actions' => [
        [
            'url' => ['create', 'place_id' => $place->id],
            'label' => Yii::t('banners', 'Добавить баннер'),
            'icon' => 'plus'
        ]
    ],
    'columns' => [
        [
            'attribute' => 'image',
            'content' => function (Banners $model) {
                return $model->pic != null ? $model->pic->getThumbnail(120) : null;
            }
        ],
        'name',
        [
            'attribute' => 'link',
            'content' => function ($model) {
                if ($model->link) {
                    return \yii\helpers\Html::a($model->link, $model->link, ['target' => '_blank']);
                }
            }
        ],
        'description',
        [
            'class' => \pw\ui\grid\ActionColumn::class,
            'visibleButtons' => [
                'view' => false
            ]
        ],
    ],
]);
