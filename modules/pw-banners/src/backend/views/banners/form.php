<?php

use kartik\file\FileInput;
use yii\helpers\Html;
use pw\ui\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model pw\banners\models\Banners */
/* @var $form yii\widgets\ActiveForm */
$file = null;
if ($model->pic) {
    $file = $model->pic->getThumbnail(200, 200, \pw\filemanager\models\File::THUMBNAIL_RESIZE, ['class' => 'img-fluid']);
}
?>

<?php $form = ActiveForm::begin([
    'model' => $model,
    'title' => $model->isNewRecord ? Yii::t('banners', 'Новый баннер') : Yii::t('banners', 'Редактирование баннера {name}', ['name' => $model->name]),
    'icon' => 'book'
]); ?>

<?php if ($model->place->alias == 'MainSlider' || $model->place->alias == 'MainSliderMobile'): ?>


<?= $form->field($model, 'start_date')->textInput(['maxlength' => true, 'class' => 'date-set']) ?>

<?= $form->field($model, 'end_date')->textInput(['maxlength' => true, 'class' => 'date-set']) ?>

<?php endif; ?>

<? echo $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'link')->textInput() ?>
<?= $form->field($model, 'sort')->textInput() ?>
<?= $form->field($model, 'is_active')->checkbox() ?>
<?= $form->field($model, 'image')->widget(FileInput::class, [
    'options' => ['accept' => 'image/*', 'multiple' => false],
    'pluginOptions' => [
        'initialPreview' => [
            $file
        ],
    ],
]) ?>
<? //echo $form->field($model, 'description')->widget(\mihaildev\ckeditor\CKEditor::class) ?>

<?php ActiveForm::end(); ?>
<?php
if (!$model->isNewRecord) {
    $url = \yii\helpers\Url::to(['delete-image', 'id' => $model->id]);
    $js = <<<JS
    $('.close').on('click', function() {
        $.ajax({
            type: 'GET',
            url: '{$url}',
            success: function(data){
                console.log('success');
            },
        });
    });

    $(document).ready(function() {
        jQuery.datetimepicker.setLocale('ru');
        
        jQuery('.date-set').datetimepicker({
            format:'Y-m-d H:i:s',
            inline:false,
            mask:true,
            lazyInit: true,
            allowBlank: true
        });
    });
JS;
    $this->registerJs($js, \pw\web\View::POS_READY);
}
?>
