<?php
/* @var $this yii\web\View */
/* @var $reference pw\banners\models\Reference */

use pw\ui\grid\GridView;

?>
<?= GridView::widget([
    'filterModel' => $searchModel,
    'dataProvider' => $dataProvider,
    'filter' => false,
    'title' => Yii::t('reference', 'Справочник {name}', ['name' => $reference->name]),
    'icon' => 'book',
    'actions' => [
        [
            'url' => ['create-value', 'reference' => $reference->id],
            'label' => Yii::t('reference', 'Добавить значение'),
            'icon' => 'plus'
        ]
    ],
    'bulkActions' => [
        'deleteAll'=>Yii::t('reference', 'Удалить')
    ],
    'columns' => [
        'value',
        [
            'class' => \pw\ui\grid\ActionColumn::class,
            'template' => '{reorder} {update} {delete}',
            'urlCreator' => function ($action, $model) {
                if ($action === 'update') {
                    return \yii\helpers\Url::to(['update-value', 'id' => $model->id]);
                }
                if ($action === 'delete') {
                    return \yii\helpers\Url::to(['delete-value', 'id' => $model->id]);
                }
                return null;
            }
        ],
    ],
]);
