<?php

use pw\banners\models\Banners;
use pw\ui\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<?= GridView::widget([
    'model' => $searchModel,
    'filterModel' => $searchModel,
    'dataProvider' => $dataProvider,
    'filter' => false,
    'title' => Yii::t('banners', 'Баннерные места'),
    'icon' => 'image',
    'columns' => [
        'name',
        'alias',
        [
            'class' => \pw\ui\grid\ActionColumn::class,
            'visibleButtons' => ['update' => false, 'delete' => false],
        ],
    ],
]);
