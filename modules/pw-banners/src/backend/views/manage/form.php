<?php

use yii\helpers\Html;
use pw\ui\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model pw\banners\models\Banners */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin([
    'model' => $model,
    'title' => $model->isNewRecord ?
        Yii::t('banners', 'Новое баннерное место') :
        Yii::t('banners', 'Редактирование места {name}', ['name' => $model->name]),
    'icon' => 'book'
]); ?>

<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'alias')->textInput() ?>
<?php ActiveForm::end(); ?>
