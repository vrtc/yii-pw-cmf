<?php
/* @var $this yii\web\View */

/* @var $place pw\banners\models\Places */

/* @var $dataProvider \yii\data\ActiveDataProvider */

use pw\ui\grid\GridView;

$columns = [
    [
        'attribute' => 'image',
        'content' => function (\pw\banners\models\Banners $model) {
            return $model->pic != null ? $model->pic->getThumbnail(120) : null;
        }
    ],
    'name',
    [
        'attribute' => 'link',
        'content' => function ($model) {
            if ($model->link) {
                return \yii\helpers\Html::a(\pw\ui\icons\Icons::show('external-link-alt'), $model->link, ['target' => '_blank']);
            }
        }
    ],
    'alias',
    'sort',
    [
        'attribute' => 'is_active',
        'value' => function($model){
            return $model->is_active ? "Да" : "Нет";
        }
    ],
];

$visibleButtons = ['view' => false];
if($place->alias != 'MainSlider'){
    $visibleButtons = [
        'view' => false,
        'delete' => false,
    ];
}else{
    if(count($place->banners) == 1){
        $visibleButtons = [
            'view' => false,
            'delete' => false,
        ];
    }
}
$actions = [];
if ($place->alias == 'MainSlider' || $place->alias == 'MainSliderMobile') {
    $columns[] = "start_date";
    $columns[] = "end_date";
    $actions = [
        [
            'url' => ['banners/create', 'place_id' => $place->id],
            'label' => Yii::t('banners', 'Добавить баннер'),
            'icon' => 'plus'
        ]
    ];
}
$columns[] = [
    'class' => \pw\ui\grid\ActionColumn::class,
    'urlCreator' => function ($action, $model) {
        if ($action === 'update') {
            return ['banners/update', 'id' => $model->id];
        }
        if ($action === 'delete') {
            return ['banners/delete', 'id' => $model->id];
        }
    },
    'visibleButtons' => $visibleButtons
];
?>
<?= GridView::widget([
    'model' => $searchModel,
    'filterModel' => $searchModel,
    'dataProvider' => $dataProvider,
    'filter' => false,
    'title' => Yii::t('banners', 'Баннеры'),
    'icon' => 'image',
    'actions' => $actions,
    'columns' => $columns
]);

