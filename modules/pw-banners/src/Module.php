<?php

namespace pw\banners;

use yii\base\Application;
use yii\base\BootstrapInterface;

class Module extends \pw\core\Module implements BootstrapInterface
{

    public function routes(Application $app): array
    {
        return [
            [
                'pattern'  => '/banners/go/<id:.+>',
                'route'    => '/default/go',
                'internal' => true
            ],
        ];
    }

    public function getNavigation(): array
    {
        return [
            'marketing' => [
                [
                    'label' => 'Баннеры',
                    'url'   => ['/pw-banners/manage/index'],
                    'icon'  => 'image'
                ]
            ]
        ];
    }
}
