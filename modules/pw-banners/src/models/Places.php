<?php
namespace pw\banners\models;

use Yii;
/**
 * This is the model class for table "{{%banners_places}}".
 *
 * @property string $id
 * @property string $alias
 * @property string $name
 * @property boolean $is_active
 *
 * @property Banners $banners
 */
class Places extends \pw\core\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pw_banners_places}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_active'], 'boolean'],
            [['name','alias'], 'string', 'max' => 255],
            ['alias', 'unique',
                'message' => Yii::t('banners', 'Системное имя должно быть уникальным')
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('banners', 'ID'),
            'alias' => Yii::t('banners', 'Системное имя'),
            'name' => Yii::t('banners', 'Название'),
        ];
    }

    /**
     * @return BannersQuery
     */
    public function getBanners(){
        return $this->hasMany(Banners::class,['place_id'=>'id']);
    }
}

