<?php

namespace pw\banners\models;


use pw\core\db\ActiveQuery;
use yii\db\Expression;

class BannersQuery extends ActiveQuery
{

    public function active(int $state = 1)
    {
        $this->andWhere(['is_active' => $state]);
        return $this;
    }

    public function displayedNow()
    {
        $this->andWhere([
            'or',
            [
                '<=',
                'start_date',
                date("Y-m-d H:i:s", time())
            ],
            new Expression('isnull(start_date)')
        ])->andWhere([
            'or',
            [
                '>=',
                'end_date',
                date("Y-m-d H:i:s", time())
            ],
            new Expression('isnull(end_date)')
        ])->active();
        return $this;
    }

}