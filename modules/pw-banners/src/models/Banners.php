<?php

namespace pw\banners\models;

use pw\filemanager\FileBehavior;
use pw\filemanager\models\File;
use pw\filemanager\models\Store;
use Yii;
use yii\helpers\Inflector;
use yii\web\UploadedFile;

/**
 * This is the model class for table "{{%banners}}".
 *
 * @property string $id
 * @property string $place_id
 * @property string $name
 * @property string $alias
 * @property string $link
 * @property string $image
 * @property string $description
 * @property string $start_date
 * @property string $end_date
 * @property boolean $is_active
 * @property integer $sort
 *
 * @property File $pic
 * @property Places $place
 */
class Banners extends \pw\core\db\ActiveRecord
{
    public $file; //Костыль

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pw_banners}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['is_active'], 'boolean'],
            [['place_id', 'image'], 'safe'],
            [['sort'], 'integer'],
            [['name', 'link', 'alias'], 'string', 'max' => 255],
            ['description', 'string'],
            [['start_date', 'end_date'], 'datetime', 'format' => 'php:Y-m-d H:i:s'],
            [['start_date', 'end_date', 'image'], 'default', 'value' => null],
        ];
    }


    public function behaviors()
    {
        return [
            'filemanager' => [
                'class' => FileBehavior::class,
                'attributes' => [
                    'image',
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('banners', 'ID'),
            'name' => Yii::t('banners', 'Название'),
            'alias' => Yii::t('banners', 'Системное имя'),
            'link' => Yii::t('banners', 'Ссылка'),
            'image' => Yii::t('banners', 'Изображение'),
            'start_date' => Yii::t('banners', 'Начало показа'),
            'end_date' => Yii::t('banners', 'Показывать до'),
            'is_active' => Yii::t('banners', 'Активен?'),
            'description' => Yii::t('banners', 'Описание'),
            'sort' => Yii::t('banners', 'Порядок сортировки'),
        ];
    }

    public function getPic()
    {
        return $this->hasOne(File::class, ['id' => 'image']);
    }

    public function getPlace()
    {
        return $this->hasOne(Places::class, ['id' => 'place_id']);
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new BannersQuery(get_called_class());
    }

    public function checkImage()
    {
        $old = Banners::findOne(['id' => $this->id]);
        if($this->file = UploadedFile::getInstance($this, 'image')){
            $this->image = $this->Upload();
        }else{
            $this->image = isset($old->image) ? $old->image : null;
        }
    }

    public function Upload()
    {
        $file = $this->file;
        $store = Store::find()->one();
        if (!$file->hasError) {
            $dir = chunk_split(substr(\md5($file->name), 0, 4), 2, '/');
            $path = $dir . Inflector::slug($file->baseName) . '.' . $file->module;
            $stream = fopen($file->tempName, 'r+');
            if ($store->putStream($path, $stream)) {
                $model = new File();
                $model->store_id = $store->id;
                $model->user_id = Yii::$app->user->id;
                $model->filename = $file->name;
                $model->path = $path;
                $model->type = $file->type;
                $model->size = $file->size;
                //  $model->created_time = new \DateTime();
                $model->save();
            }
            fclose($stream);
        }

        return $model->id;
    }
}

