<?php

namespace pw\pages\frontend\controllers;

use pw\web\Controller;
use pw\pages\models\Pages;
use yii\data\ActiveDataProvider;
use yii\filters\HttpCache;
use yii\filters\PageCache;
use yii\web\NotFoundHttpException;

class BlogController extends Controller
{

    public function behaviors()
    {
        return [
            [
                'class' => PageCache::class,
                'only' => ['articles', 'article'],
                'duration' => 86400,
                'enabled' => env('CACHE_GLOBAL'),
                'variations' => [
                    \Yii::$app->request->getUrl(),
                    \Yii::$app->mobileDetect->isMobile()
                ],
                'dependency' => [
                    'class' => 'yii\caching\DbDependency',
                    'sql' => 'SELECT MAX(updated_time) FROM ' . Pages::tableName(),
                ],
            ],
        ];
    }

    public function actionIndex($slug)
    {
        $this->layout = '/news.php';
        $model = Pages::findOne(['slug' => $slug, 'container' => Pages::CONTAINER_TRUE, 'status' => Pages::STATUS_ENABLED]);
        if (!$model) {
            throw new NotFoundHttpException('Страницы не существует');
        }

        $ids = $model->getArticles($model->children);
        $dataProvider = new ActiveDataProvider([
            'query' => Pages::find()
                ->where([
                    'in', 'id', $ids
                ])
                ->andWhere(['container' => Pages::CONTAINER_FALSE])
                ->orderBy(['created_time' => SORT_DESC])
                ->groupBy('slug')]);

        return $this->render('articles', ['dataProvider' => $dataProvider, 'model' => $model]);
    }

    public function actionArticle($slug)
    {
        $this->layout = '/news.php';
        $model = Pages::findOne(['slug' => $slug, 'container' => Pages::CONTAINER_TRUE]);
        if (!$model) {
            $page = Pages::findOne(['slug' => $slug]);
            if (!$page) {
                throw new NotFoundHttpException('Страницы не существует');
            }
            $this->view->params['article'] = $page;
            return $this->render('article', ['page' => $page]);
        }
        $ids = [];
        $ids[] = $model->id;
        foreach ($model->children as $child) {
            $ids[] = $child->id;
            foreach ($child->children as $child2){
                $ids[] = $child2->id;
            }
        }
        if($slug == Pages::ROOT_CATEGORY){
            $dataProvider = new ActiveDataProvider([
                'query' => Pages::find()
                    ->andWhere(['container' => Pages::CONTAINER_FALSE])
                    ->where([
                        '!=', 'parent_id', 259
                    ])
                    ->andWhere(['!=', 'parent_id', 450])
                    ->andWhere(['!=', 'parent_id', 457])
                    ->orderBy(['created_time' => SORT_DESC])
                    ->groupBy('slug')]);

        }else {
            $dataProvider = new ActiveDataProvider([
                'query' => Pages::find()
                    ->where([
                        'in', 'id', $ids
                    ])
                    ->andWhere(['container' => Pages::CONTAINER_FALSE])
                    ->orderBy(['created_time' => SORT_DESC])
                    ->groupBy('slug')]);
        }
        return $this->render('articles', ['dataProvider' => $dataProvider, 'model' => $model]);
    }
}
