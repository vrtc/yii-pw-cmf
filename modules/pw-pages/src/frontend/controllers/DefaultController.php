<?php

namespace pw\pages\frontend\controllers;


use ct\catalog\models\product\Product;
use himiklab\yii2\recaptcha\ReCaptchaValidator2;
use Yii;
use pw\web\Controller;
use pw\pages\models\Pages;
use yii\base\DynamicModel;
use yii\filters\HttpCache;
use yii\filters\PageCache;
use yii\web\NotFoundHttpException;
use kartik\mpdf\Pdf;
use yii\web\Response;
use yii\widgets\FragmentCache;

class DefaultController extends Controller
{

    public function behaviors()
    {
        return [
            [
                'class' => PageCache::class,
                'only' => ['view', 'static', 'system'],
                'duration' => 86400,
                'enabled' => env('CACHE_GLOBAL'),
                'variations' => [
                    \Yii::$app->request->getUrl(),
                    \Yii::$app->mobileDetect->isMobile()
                ],
                'dependency' => [
                    'class' => 'yii\caching\DbDependency',
                    'sql' => 'SELECT MAX(updated_time) FROM ' . Pages::tableName(),
                ],
            ],
        ];
    }

    public function actionView($slug)
    {

        $page = Pages::findBySlug($slug);
        if (!$page) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        //$this->layout = '/base';
        if ($page->parents) {
            foreach ($page->parents as $parent) {
                $this->addBreadcrumb($parent->title, ['/pw-pages/default/view', 'slug' => $parent->slug]);
            }
        }
        $this->addBreadcrumb($page->title);
        return $this->render('view', ['page' => $page]);
    }

    public function actionStatic($slug)
    {
        $page = Pages::findBySlug($slug);

        if (!$page) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        return $this->render('static', ['page' => $page]);
    }

    public function actionSystem($slug)
    {
        $page = Pages::findBySlug($slug);
        if (!$page) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_HTML;
            return $this->renderAjax('system', ['page' => $page]);
        } else {
            switch ($page->id) {
                //страника Условия возврата
                case 276:
                    $model = new DynamicModel([
                        'name', 'phone', 'email', 'message', 'reCaptcha'
                    ]);
                    $model->addRule('name', 'required', ["message" => "Необходимо заполнить «Ваше имя»"])
                        ->addRule('phone', 'required', ["message" => "Необходимо заполнить «Телефон»"])
                        ->addRule('email', 'required', ["message" => "Необходимо заполнить «Ваш E-mail»"])
                        ->addRule(['name'], 'string', ['max' => 255])
                        ->addRule('email', 'email', ["message" => "Значение «Ваш E-mail» не является правильным email адресом."])
                        ->addRule(['reCaptcha'], ReCaptchaValidator2::class, [
                            'uncheckedMessage' => 'Пожалуйста подтвердите, что вы не робот',
                            'secret' => env('RECAPCHA_SECRET')
                        ]);
                    if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
                        \Yii::$app->session->setFlash('success', 'Спасибо за ваще письмо, мы ответим вам в ближайшее время!');

                        $data = \Yii::$app->request->post('DynamicModel');
                        \Yii::$app->mailer->compose('system.vozvrat', [
                            '{user.name}' => $data['name'],
                            '{user.phone}' => $data['phone'],
                            '{user.email}' => $data['email'],
                            '{message}' => $data['message'],
                        ])->setSubject('Форма возврата')->setTo('online@tri-sport.ru')->send();
                        return $this->redirect('/');
                    }
                    return $this->render('usloviya-vozvrata', [
                        'page' => $page,
                        'model' => $model
                    ]);
                    break;
                //страница Оптовикам
                case 279:
                    $model = new DynamicModel([
                        'name', 'phone', 'email', 'organization', 'site', 'reCaptcha'
                    ]);
                    $model->addRule('name', 'required', ["message" => "Необходимо заполнить «Ваше имя»"])
                        ->addRule('phone', 'required', ["message" => "Необходимо заполнить «Телефон»"])
                        ->addRule('email', 'required', ["message" => "Необходимо заполнить «Ваш E-mail»"])
                        ->addRule(['name'], 'string', ['max' => 255])
                        ->addRule('email', 'email', ["message" => "Значение «Ваш E-mail» не является правильным email адресом."])
                        ->addRule(['organization', 'site'], 'string')
                        ->addRule(['reCaptcha'], ReCaptchaValidator2::class, [
                            'uncheckedMessage' => 'Пожалуйста подтвердите, что вы не робот',
                            'secret' => env('RECAPCHA_SECRET')
                        ]);

                    if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
                        \Yii::$app->session->setFlash('success', 'Спасибо за ваще письмо, мы ответим вам в ближайшее время!');

                        $data = \Yii::$app->request->post('DynamicModel');
                        \Yii::$app->mailer->compose('system.optovikam', [
                            '{user.name}' => $data['name'],
                            '{user.phone}' => $data['phone'],
                            '{user.email}' => $data['email'],
                            '{user.organization}' =>  $data['organization'],
                            '{user.site}' => $data['site'],
                        ])->setSubject('Заявка на оптовый прайс-лист')->setTo('online@tri-sport.ru')->send();
                        return $this->redirect('/');
                    }
                    return $this->render('optovikam', [
                        'page' => $page,
                        'model' => $model
                    ]);
                    break;
                default:
                    return $this->render('system_page', ['page' => $page]);
                    break;

            }
        }
    }

    public function actionError()
    {
        return $this->render('error');
    }

    public function actionPdf($url)
    {
        $page = Pages::findBySlug($url);
        if (!$page) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $content = $this->renderPartial('view', ['page' => $page, 'pdf' => true]);
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_DOWNLOAD,
            'content' => $content,
            'options' => ['title' => $page->seo->title, 'ignore_invalid_utf8' => true, 'debug' => true],

        ]);
        return $pdf->render();
    }
}
