<?php
/** @var $this \pw\core\View */
/** @var $page \pw\pages\models\Pages */
use yii\helpers\Html;
?>

<div id="col-main" class="col-md-20 clearfix">
    <h1 class="page-title">
        <?=$page->title ?>
    </h1>
    <div class="page-body"><?=$page->text?></div>
    <?php if($page->id == 4 && !isset($pdf)):?>
        <?=Html::a(Yii::t('pages','Скачать в виде PDF'),['pdf','url'=>$page->url],['class'=>'btn'])?>
    <?php endif;?>
</div>

