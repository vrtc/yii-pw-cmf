<?php
/**
 * Created by PhpStorm.
 * User: vrtc
 * Date: 16.02.16
 * Time: 11:02
 */

namespace pw\pages\widgets;

use yii\base\Widget;
use pw\pages\models\Pages;

class PageContent extends Widget
{
    public $pageId;

    public function run()
    {
        $model = Pages::findOne(['id' => $this->pageId]);
        if ($model) {
            return $this->render('page', [
                'model' => $model
            ]);
        }
    }
}
