<?php
namespace pw\pages\components;

use kartik\date\DatePicker;
use kartik\file\FileInput;
use pw\filemanager\models\File;
use pw\pages\models\PagesGroup;
use trntv\aceeditor\AceEditor;

class JsonForm extends \pw\ui\form\ActiveForm
{
    public $properties;

    public function renderProperties($model)
    {
        $return = [];
        foreach ($model->group->properties as $property) {
            if($property['type'] == PagesGroup::TYPE_STRING){
                echo $this->field($this->model, 'properties['.$property['name'].']')->textInput()->label($property['name']);
            }elseif($property['type'] == PagesGroup::TYPE_TEXT){
                echo $this->field($this->model, 'properties['.$property['name'].']')->wysiwyg()->label($property['name']);
            }elseif($property['type'] == PagesGroup::TYPE_TIMESTAMP){
                echo $this->field($this->model, 'properties['.$property['name'].']')->widget(DatePicker::class, [
                    'options' => ['placeholder' => 'Выберите дату'],
                    'pluginOptions' => [
                        'autoclose' => true
                    ]
                ])->label($property['name']);
            }elseif($property['type'] == PagesGroup::TYPE_HTML){
                echo $this->field($this->model, 'properties['.$property['name'].']')->widget(AceEditor::class, ['mode' => 'html'])->label($property['name']);
            }elseif($property['type'] == PagesGroup::TYPE_IMAGE){
                $file = null;
                if(isset($this->model->properties[$property['name']]) && $this->model->properties[$property['name']] != null){
                    $file = File::findOne(['id' => $this->model->properties[$property['name']]])->getThumbnail(200, 200);
                }
                echo $this->field($this->model, 'properties['.$property['name'].']')->widget(FileInput::class, [
                    'options' => ['accept' => 'image/*', 'multiple' => false],
                    'pluginOptions' => [
                        'initialPreview'=>[
                            $file
                        ],
                    ],
                ])->label($property['name']);
            }
        }
    }
}