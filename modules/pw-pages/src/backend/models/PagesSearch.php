<?php

namespace pw\pages\backend\models;

use ct\catalog\models\product\Product;
use pw\core\Model;
use pw\filemanager\models\File;
use pw\pages\models\Pages;
use pw\pages\models\PagesGroup;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class PagesSearch extends Model
{
    public $name;
    public $group_id;
    public function rules(): array
    {
        return [
            ['name', 'string'],
            ['group_id', 'safe'],
        ];
    }

    private function getIds($id)
    {
        $group = PagesGroup::findOne(['id' => $id]);
        $return = [];
        $return = array_merge($return, ArrayHelper::getColumn($group->children, 'id'));
        $return[] = $group->id;
        return $return;
    }

    public function search(array $params, $group_id = null)
    {
        $query = Pages::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if(!$this->validate()){
            return $dataProvider;
        }
        if($this->group_id != null){
            $query->andWhere(['group_id' => $this->group_id]);
        }elseif($group_id){
                $query->andWhere(['in', 'group_id', $this->getIds($group_id)]);
        }
        if($this->name != null){
            $query->andWhere(['like','JSON_EXTRACT(data, "$.Заголовок")', $this->name]);
        }
        return $dataProvider;
    }
}