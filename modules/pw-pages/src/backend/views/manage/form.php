<?php
/* @var $this yii\web\View */
/* @var $model \pw\pages\models\Pages */

/* @var $form \pw\pages\components\JsonForm */

use kartik\file\FileInput;
use pw\ui\form\ActiveForm;
use dosamigos\tinymce\TinyMce;
use yii\web\JsExpression;
$file = null;
if($model->pic) {
    $file = $model->pic->getThumbnail(200, 200, \pw\filemanager\models\File::THUMBNAIL_RESIZE, ['class' => 'img-fluid']);
}
?>

<?php $form = ActiveForm::begin([
    'model' => $model,
    'title' => $model->getIsNewRecord() ? Yii::t('pages', 'Добавить новую страницу') : Yii::t('pages', 'Редакировать страницу'),
    'icon' => 'file'
]); ?>
<?/*= $form->field($model, 'group_id')->dropDownList($model->getPossibleGroups());*/?>
<?= $form->field($model->translate(), 'name')->textInput() ?>
<?= $form->field($model, 'slug')->textInput() ?>
<?= $form->field($model, 'introtext')->widget(\dosamigos\ckeditor\CKEditor::class, [
    'options' => [],
    'preset' => 'custom',
    'clientOptions' => [
        'extraPlugins' => '',
        'height' => 350,

        //Here you give the action who will handle the image upload
        'filebrowserUploadUrl' => \yii\helpers\Url::to(['/pw-filemanager/manage/ckeditor_image_upload', 'name' => 'upload']),
        'filebrowserUploadMethod' => 'form',

        /* 'toolbarGroups' => [
             ['name' => 'undo'],
             ['name' => 'basicstyles', 'groups' => ['basicstyles', 'cleanup']],
             ['name' => 'paragraph', 'groups' => ['list', 'indent', 'blocks', 'align', 'bidi' ]],
             ['name' => 'styles'],
             ['name' => 'links', 'groups' => ['links', 'insert']]
         ]*/

    ]
]) ?>
<?= $form->field($model, 'description')->widget(\dosamigos\ckeditor\CKEditor::class, [
    'options' => [],
    'preset' => 'custom',
    'clientOptions' => [
        'extraPlugins' => 'iframe',
        'height' => 350,

        //Here you give the action who will handle the image upload
        'filebrowserUploadUrl' => \yii\helpers\Url::to(['/pw-filemanager/manage/ckeditor_image_upload', 'name' => 'upload']),
        'filebrowserUploadMethod' => 'form',

        /* 'toolbarGroups' => [
             ['name' => 'undo'],
             ['name' => 'basicstyles', 'groups' => ['basicstyles', 'cleanup']],
             ['name' => 'paragraph', 'groups' => ['list', 'indent', 'blocks', 'align', 'bidi' ]],
             ['name' => 'styles'],
             ['name' => 'links', 'groups' => ['links', 'insert']]
         ]*/

    ]
]) ?>


<?= $form->field($model, 'image')->widget(FileInput::class, [
    'options' => ['accept' => 'image/*', 'multiple' => false],
    'pluginOptions' => [
        'initialPreview'=>[
            $file
        ],
    ],
]) ?>
<?= $form->field($model, 'status')->checkbox([
    'class' => 'make-switch',
    'data-on-text' => '<i class="fa fa-check"></i>',
    'data-off-text' => '<i class="fa fa-times"></i>',
    'data-on-color' => 'success',
]) ?>
<?= $form->field($model, 'container')->checkbox([
    'class' => 'make-switch',
    'data-on-text' => '<i class="fa fa-check"></i>',
    'data-off-text' => '<i class="fa fa-times"></i>',
    'data-on-color' => 'success',
]) ?>
<?php ActiveForm::end(); ?>

<?php
if(!$model->isNewRecord) {
    $url = \yii\helpers\Url::to(['delete-image', 'id' => $model->id]);
    $js = <<<JS
    $('.close').on('click', function() {
        $.ajax({
            type: 'GET',
            url: '{$url}',
            success: function(data){
                console.log('success');
            },
        });
    })
JS;
    $this->registerJs($js, \pw\web\View::POS_READY);
}
?>
