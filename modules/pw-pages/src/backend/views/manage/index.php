<?php
use pw\ui\nestable\NestableView;
?>
<?= NestableView::widget([
    'title' =>'Страницы',
    'icon' => 'file-o',
    'models' => $models,
    'nodeMoveUrl' => ['node-move'],
    'actions' => [
        [
            'label' => Yii::t('menu', 'Добавить новую страницу'),
            'url' => ['create']
        ]
    ],
    'itemLabel' => 'name'
]);