<?php

namespace pw\pages\backend\controllers;

use pw\filemanager\models\File;
use Yii;
use yii\web\Response;
use pw\web\Controller;
use yii\web\NotFoundHttpException;
use pw\pages\models\Pages;
use yii\web\UploadedFile;

class ManageController extends Controller
{
    public function actionIndex()
    {
        $this->setPageTitle(Yii::t('pages', 'Страницы'));
        $this->addBreadcrumb(Yii::t('pages', 'Страницы'));
        $models = Pages::find()->roots()->all();
        return $this->render('index', [
            'models' => $models ?: []
        ]);
    }

    public function actionCreate($parent = null)
    {
        if ($parent) {
            $parent = $this->findModel($parent);
        }
        $model = new Pages();
        if ($model->load(Yii::$app->request->post())) {
            $model->checkImage();
            if ($parent) {
                $model->appendTo($parent);
            } else {
                $model->makeRoot();
            }
            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('pages', 'Страница добавлена'));
                return $this->redirect(['index']);
            }
        }
        $this->addBreadcrumb(Yii::t('pages', 'Добавить новую страницу'));
        $this->setPageTitle(Yii::t('pages', 'Добавить новую страницу'));
        return $this->render('form', [
            'model' => $model
        ]);
    }

    public function actionDeleteImage($id)
    {
        if(Yii::$app->request->isAjax) {
            $banner = $this->findModel($id);
            $banner->image = null;
            $banner->save();
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->checkImage();
            $model->save();
            Yii::$app->session->setFlash('success', Yii::t('pages', 'Изменения сохранены'));
            return $this->redirect(['update', 'id' => $model->id]);
        }
        $this->addBreadcrumb(Yii::t('pages', 'Страницы'), ['index']);
        $this->addBreadcrumb(Yii::t('pages', 'Редактирование страницы {title}',['title'=>$model->name]));
        $this->setPageTitle(Yii::t('pages', 'Редактирование страницы {title}',['title'=>$model->name]));
        return $this->render('form', [
            'model' => $model
        ]);
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model->deleteWithChildren()) {
            Yii::$app->session->setFlash('success', Yii::t('pages', 'Страница удалена'));
            return $this->redirect(['group/index']);
        }
        Yii::$app->session->setFlash('danger', Yii::t('pages', 'Ошибка '));
        return $this->redirect(['group/index']);
    }

    public function actionChildren($id)
    {
        if (!$id) {
            $id = 1;
        }
        $parent = $this->findModel($id);
        $nodes = $parent->children(1)->all();
        $result = [];
        foreach ($nodes as $node) {
            $result[] = [
                'id' => $node->id,
                'name' => $node->title,
                'level' => $node->level,
                'type' => 'default'
            ];
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['nodes' => $result];
    }

    public function actionMove($id)
    {
        $model = $this->findModel($id);
        $related = $this->findModel(Yii::$app->request->post('related'));
        $position = Yii::$app->request->post('position');
        if ($position === 'after') {
            $model->insertAfter($related);
        } elseif ($position === 'before') {
            $model->insertBefore($related);
        } elseif ($position === 'lastChild') {
            $model->appendTo($related);
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'id' => $model->id,
            'name' => $model->name,
            'level' => $model->level,
            'type' => $model->type
        ];
    }

    protected function findModel($id)
    {
        if (($model = Pages::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
