<?php
namespace pw\pages;

use Yii;
use yii\base\Application;
use pw\pages\models\Pages;
use pw\pages\widgets\PageContent;
use yii\base\BootstrapInterface;

class Module extends \pw\core\Module implements BootstrapInterface
{
    public function models(Application $app):array
    {
        return [
            Pages::class=>Yii::t('pages','Страница')
        ];
    }


    public function widgets(Application $app):array
    {
        return [
            PageContent::class
        ];
    }

    public function getNavigation(): array
    {
        return [
            'content' => [
                [
                    'label' => 'Страницы',
                    'url'   => ['/pw-pages/manage/index'],
                    'icon'  => 'file'
                ],
            ]
        ];
    }

    public function routes(Application $app): array
    {
        return [
            [
                'pattern' => '/system/<slug:.+>',
                'route'   => '/pw-pages/default/system',
                'model'   => Pages::class,
                'name'    => Module::t('model', 'Системные страницы'),
                'suffix' => '.html'
            ],
            [
                'pattern' => '/static/<slug:.+>',
                'route'   => '/pw-pages/default/static',
                'model'   => Pages::class,
                'name'    => Module::t('model', 'Статичные страницы'),
            ],
            [
                'pattern' => '/articles/page:<page:.+>',
                'route'   => '/pw-pages/blog/index',
                'model'   => Pages::class,
                'defaults' => ['slug' => 'articles'],
                'name'    => Module::t('category', 'Статьи блога'),
            ],
            [
                'pattern' => '/articles',
                'route'   => '/pw-pages/blog/index',
                'model'   => Pages::class,
                'defaults' => ['slug' => 'articles'],
                'name'    => Module::t('category', 'Статьи блога'),
            ],
            [
                'pattern' => '/articles/<slug:.+>/page:<page:.+>',
                'route'   => '/pw-pages/blog/article',
                'model'   => Pages::class,
                'name'    => Module::t('category', 'Статьи блога'),
            ],
            [
                'pattern' => '/articles/<slug:.+>',
                'route'   => '/pw-pages/blog/article',
                'model'   => Pages::class,
                'name'    => Module::t('category', 'Статьи блога'),
            ],

        ];
    }
}
