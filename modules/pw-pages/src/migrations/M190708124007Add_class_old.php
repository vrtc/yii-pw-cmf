<?php

namespace pw\pages\migrations;

use pw\core\db\Migration;
use pw\pages\models\Pages;

class M190708124007Add_class_old extends Migration
{
    public function up()
    {
        $this->addColumn(Pages::tableName(), 'old_class', $this->string()->after('old_id'));
    }

    public function down()
    {
        echo "M190708124007Add_class_old cannot be reverted.\n";

        return false;
    }

}
