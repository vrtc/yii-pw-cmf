<?php

namespace pw\pages\migrations;

use pw\core\db\Migration;
use pw\pages\models\Pages;

class M190708074955Add_container_field extends Migration
{
    public function up()
    {
        $this->addColumn(Pages::tableName(), 'container', $this->integer()->defaultValue(Pages::CONTAINER_FALSE));
        $this->addColumn(\pw\pages\models\i18n\Pages::tableName(), 'description', $this->text());
    }

    public function down()
    {
        echo "M190708074955Add_container_field cannot be reverted.\n";

        return false;
    }

}
