<?php
namespace pw\pages\migrations;

use pw\core\db\Migration;
use pw\pages\models\Pages;

class m300320_432531_voucher_page extends Migration
{
    public function safeUp()
    {

        $parent = Pages::findOne(['slug' => 'sistemnye-stranicy']);

        if($parent) {
            $name = 'Подарочный сертификат';
            $slug = 'voucher';

            $model = ($model = Pages::findOne(['slug' => $slug])) ? $model : new Pages();
            $model->name = $name;
            $model->slug = $slug;
            $model->introtext = '<p class="p-banner__text">Сделайте подарок своим друзьям! Подарите им карту с номиналом от 500 до 50 000 ₽. Отправка карты осуществляется по электронной почте.</p>';
            $model->container = Pages::CONTAINER_FALSE;
            $model->status = Pages::STATUS_ENABLED;
            $model->appendTo($parent)->save(false);
        }

    }

}
