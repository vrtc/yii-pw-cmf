<?php


namespace pw\pages\migrations;

use pw\core\db\Migration;
use pw\pages\models\Pages;

class M720708125001_add_pay_delivery_pages extends Migration
{
    public function safeUp()
    {
        $name = 'Оплата и доставка';
        $slug = $this->translit($name);

        $model = ($model = Pages::findOne(['slug' => $slug])) ? $model : new Pages();
        $model->name = 'Оплата';
        $model->slug = 'pay';
        $model->container = Pages::CONTAINER_TRUE;
        $model->status = Pages::STATUS_DISABLED;
        $model->makeRoot()->save(false);


        $slug = 'delivery';

        $model = ($model = Pages::findOne(['slug' => $slug])) ? $model : new Pages();
        $model->name = 'Доставка';
        $model->slug = $slug;
        $model->container = Pages::CONTAINER_TRUE;
        $model->status = Pages::STATUS_DISABLED;
        $model->makeRoot()->save(false);

    }

    private function translit($s)
    {//Генерация чпу
        $s = (string)$s;
        $s = strip_tags($s);
        $s = str_replace(array("\n", "\r"), " ", $s);
        $s = preg_replace("/\s+/", ' ', $s);
        $s = trim($s);
        $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s);
        $s = strtr($s, array('а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'e', 'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shch', 'ы' => 'y', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya', 'ъ' => '', 'ь' => ''));
        $s = preg_replace("/[^0-9a-z-_ ]/i", "", $s);
        $s = str_replace(" ", "-", $s);
        return $s;
    }
}