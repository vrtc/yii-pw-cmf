<?php
namespace pw\pages\migrations;

use pw\i18n\db\Migration;

class M170730102622Init extends Migration
{
    public function up()
    {
        $this->createAdjacencyListTable('{{%pw_pages}}',[
            'id'            => $this->bigPrimaryKey()->unsigned(),
            'slug'          => $this->string()->notNull(),
            'author'        => $this->bigInteger()->unsigned(),
            'name'          => $this->string()->i18n(),
            'data'          => $this->json(),
            'group_id'      => $this->bigInteger()->unsigned(),
            'allow_comments'=> $this->integer(),
            'created_time'  => $this->timestamp()->notNull(),
            'updated_time'  => $this->timestamp()->defaultValue(null)->null(),
            'status'        => $this->smallInteger(),
            'old_id' => $this->integer(),
        ]);

        $this->createIndex('idx_slug', '{{%pw_pages}}', 'slug', true);
        $this->createIndex('idx_status', '{{%pw_pages}}', 'status');
    }

    public function down()
    {
        $this->dropTable('{{%pw_pages_i18n}}');
        $this->dropTable('{{%pw_pages}}');
    }

}
