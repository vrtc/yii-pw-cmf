<?php

namespace pw\pages\migrations;

use pw\core\db\Migration;
use pw\filemanager\models\File;
use pw\pages\models\i18n\Pages;

class M190708143509Add_contents_fields extends Migration
{
    public function up()
    {
        $this->addColumn(Pages::tableName(), 'introtext', $this->text());
        $this->addColumn(\pw\pages\models\Pages::tableName(), 'image', $this->bigInteger()->unsigned());
        $this->addColumn(\pw\pages\models\Pages::tableName(), 'date_active', $this->timestamp()->null());
        $this->addForeignKey('pw-page-image', \pw\pages\models\Pages::tableName(), 'image', File::tableName(), 'id', 'SET NULL', 'NO ACTION');
    }

    public function down()
    {
        echo "M190708143509Add_contents_fields cannot be reverted.\n";

        return false;
    }

}
