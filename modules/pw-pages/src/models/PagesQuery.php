<?php
namespace pw\pages\models;

use pw\core\db\AdjacencyListQuery;

class PagesQuery extends AdjacencyListQuery
{

    public function enabled()
    {
        $this->andWhere(['status' => Pages::STATUS_ENABLED]);
        return $this;
    }

    public function disabled()
    {
        $this->andWhere(['status' => Pages::STATUS_DISABLED]);
        return $this;
    }
}