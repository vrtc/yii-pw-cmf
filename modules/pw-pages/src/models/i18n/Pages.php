<?php

namespace pw\pages\models\i18n;

use klisl\behaviors\JsonBehavior;
use Yii;

/**
 * This is the model class for table "{{%pages_i18n}}".
 *
 * @property integer $id
 * @property integer $model_id
 * @property string $language
 *
 * @property Pages $page
 */
class Pages extends \yii\db\ActiveRecord
{
    public $properties;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pw_pages_i18n}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['model_id'], 'safe'],
            [['model_id', 'name', 'description', 'introtext'], 'string'],
            [['language'], 'string', 'max' => 16],
            [['model_id'], 'exist', 'skipOnError' => true, 'targetClass' => \pw\pages\models\Pages::class, 'targetAttribute' => ['model_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('pages', 'ID'),
            'model_id' => Yii::t('pages', 'Id Page'),
            'language' => Yii::t('pages', 'Language'),
            'name' => Yii::t('pages', 'Системное имя'),
        ];
    }
}
