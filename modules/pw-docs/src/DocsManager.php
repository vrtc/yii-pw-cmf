<?php
namespace pw\docs;

use pw\core\Component;
use yii\helpers\FileHelper;
use pw\docs\DocsMarkdown;

class DocsManager extends Component
{
    /**
     * default module name
     */
    const MODULE = 'pw-core';
    /**
     * default type documentation (dev or user)
     */
    const TYPE = 'dev';
    /**
     * default page
     */
    const PAGE = 'index';

    /**
     * @param string $module
     * @param string $page
     * @return bool|string
     */
    public function getDocDev($module = self::MODULE, $page = self::PAGE)
    {
        return $this->getDocFile($module, $page, 'dev');
    }

    /**
     * @param string $module
     * @param string $page
     * @return bool|string
     */
    public function getDocUser($module = self::MODULE, $page = self::PAGE)
    {
        return $this->getDocFile($module, $page, 'user');
    }

    /**
     * @param $type
     * @return array
     */
    public function getTreeDocs($module = null, $type = self::TYPE): array
    {
        $tree = \Yii::$app->cache->get('docMenu');
        if ($tree === false) {
            $tree = [];
            $modules = \Yii::$app->getModulesManager()->installed($module);

            foreach ($modules as $key => $module) {

                $path = \Yii::getAlias('@app') . $module['path'] . DIRECTORY_SEPARATOR . 'docs' . DIRECTORY_SEPARATOR . \Yii::$app->language .DIRECTORY_SEPARATOR. $type ;
                $url = $docFile = [];
                if (is_dir($path)) {
                    $url = [
                        'url' => [
                            'manage/index',
                            'module' => $key,
                            'page' => self::PAGE
                        ]
                    ];
                    $files = FileHelper::findFiles($path, [
                        'filter' => function ($path) {
                            $file = pathinfo($path);
                            if ($file['module'] === 'md') {
                                return true;
                            }
                            return false;
                        }
                    ]);

                    foreach ($files as $doc) {

                        $pathInfo = pathinfo($doc);
                        $headers = DocsMarkdown::getItemsMenu($doc, $pathInfo['filename']);
                        if ($headers) {

                            $ancors = [];
                            if (!empty($subItems = $headers[$pathInfo['filename']]['subItems'])) {
                                foreach ($subItems as $subItem) {
                                    $ancors['items'][] = [
                                        'label' => $subItem,
                                        'url' => [
                                            'manage/index',
                                            'module' => $key,
                                            'page' => $pathInfo['filename'],
                                            '#' => $subItem

                                        ]
                                    ];
                                }
                            }

                            $docFile['items'][] = [
                                    'label' => $headers[$pathInfo['filename']]['item'],
                                    'url' => [
                                        'manage/index',
                                        'module' => $key,
                                        'page' => $pathInfo['filename']
                                    ]
                                ] + $ancors;

                        } else {
                            $docFile['items'][] = [
                                'label' => $pathInfo['filename'],
                                'url' => [
                                    'manage/index',
                                    'module' => $key,
                                    'page' => $pathInfo['filename']
                                ]
                            ];
                        }


                    }
                }

                $tree['items'][] = [
                        'label' => $module['name']

                    ] + $url + $docFile;

                $tree['options'] = [
                    'class' => 'nav nav-pills nav-stacked'
                ];

            }
            \Yii::$app->cache->set('docMenu', $tree);
            return $tree;
        }

        return $tree;
    }


    /**
     * @param string $module
     * @param string $page
     * @param string $type
     * @return bool|string
     */
    protected function getDocFile($module, $page, $type)
    {
        $module = \Yii::$app->getModulesManager()->installed($module);
        if ($module) {
            $path = \Yii::getAlias('@app') . $module['path'];
            $file = $path . DIRECTORY_SEPARATOR . 'docs' . DIRECTORY_SEPARATOR . \Yii::$app->language. DIRECTORY_SEPARATOR .$type . DIRECTORY_SEPARATOR . $page . '.md';
            if (file_exists($file)) {
                return $file;
            }

        }

        return false;
    }

}
