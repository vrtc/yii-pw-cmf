<?php
namespace pw\docs;

use yii\base\Application;
use yii\base\BootstrapInterface;

class Module extends \pw\core\Module implements BootstrapInterface
{

    public function components(Application $app): array
    {
        return [
            'docs' => DocsManager::class
        ];
    }


    public function getNavigation(): array
    {
        return [
            'system' => [
                [
                    'label' => 'Документация',
                    'url' => ['/pw-docs/manage/index'],
                    'icon' => 'book'
                ]
            ]
        ];
    }
}