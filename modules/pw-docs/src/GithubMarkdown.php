<?php
namespace pw\docs;

use cebe\markdown\Markdown;

class GithubMarkdown extends Markdown
{
    protected function renderAbsy($blocks)
    {
        $head = [];
        foreach ($blocks as $block) {
            if ($block[0] === 'headline') {
                $head[] = [
                    'content' => $block['content'][0],
                    'level' => $block['level']
                ];
            }
        }
        return $head;
    }
}