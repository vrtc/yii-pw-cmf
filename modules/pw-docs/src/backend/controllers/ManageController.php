<?php
namespace pw\docs\backend\controllers;

use pw\web\Controller;
use yii\helpers\Markdown;
use yii\web\NotFoundHttpException;
use Yii;
use pw\docs\DocsManager;

class ManageController extends Controller
{

    public function actionIndex($module = DocsManager::MODULE, $page = DocsManager::PAGE)
    {
        $this->view->title = Yii::t('cron', 'Документация');

        $file = \Yii::$app->docs->getDocDev($module, $page);
        if ($file) {
            return $this->render('index', [
                'html' => Markdown::process(file_get_contents($file), 'gfm'),
                'menu' => \Yii::$app->docs->getTreeDocs()
            ]);
        } else {
            throw new NotFoundHttpException(Yii::t('docs', 'Документ не найден'));
        }
    }
}
