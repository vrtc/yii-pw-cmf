<?php
use yii\widgets\Menu;

?>
<div class="row">
    <div class="col-sm-3">
        <div class="side-menu">
            <div class="box box-solid">
                <div class="box-body no-padding">
                    <?= Menu::widget($menu); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-9"><?= $html ?></div>
</div>