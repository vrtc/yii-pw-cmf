<?php
namespace pw\docs;

use pw\modules\ModuleInstaller;
use Yii;

class Installer extends ModuleInstaller
{

    public function getVersion(): string
    {
        return '0.1';
    }

    public function getAuthors(): array
    {
        return ['dkindeev@mail.ru'];
    }

    public function getLink(): string
    {
        return 'http://yar.pw';
    }

    public function getName(): string
    {
        return 'Documentation';
    }

    public function getSourceLanguage(): string
    {
        return 'ru-RU';
    }
}
