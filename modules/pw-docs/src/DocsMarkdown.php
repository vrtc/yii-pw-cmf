<?php
namespace pw\docs;

use yii\helpers\Markdown;

class DocsMarkdown extends Markdown
{
    public static function getItemsMenu($docFile, $fileName)
    {
        self::$flavors = [
            'custom' => [
                'class' => 'pw\docs\GithubMarkdown',
                'html5' => true,
            ]
        ];

        $headers = self::process(file_get_contents($docFile), 'custom');

        if (is_array($headers)) {
            $items = $subItems = [];
            foreach ($headers as $header) {
                if ($header['level'] === 2) {
                    $subItems[] = $header['content'][1];

                }
            }
            foreach ($headers as $header) {
                if ($header['level'] === 1) {
                    $items[$fileName] = [
                        'item' => $header['content'][1],
                        'subItems' => $subItems
                    ];
                    break;
                }
            }
            return $items;
        }

        return false;
    }
}