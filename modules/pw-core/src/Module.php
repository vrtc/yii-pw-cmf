<?php

namespace pw\core;

use pw\core\traits\Settings;
use Yii;
use yii\base\BootstrapInterface;
use yii\base\Application;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;

class Module extends \yii\base\Module
{
    use Settings;
    private $_namespace;
    protected $_id;

    /**
     * @param $app Application
     * @return array of components
     */
    public function components(Application $app): array
    {
        return [];
    }

    /**
     * @param $app Application
     * @return array of modules
     */
    public function modules(Application $app): array
    {
        return [];
    }

    /**
     * @param $app Application
     * @return array of models
     */
    public function models(Application $app): array
    {
        return [];
    }

    /**
     * @param $app Application
     * @return array of bootstrap
     */
    public function bootstraps(Application $app): array
    {
        return [];
    }


    /**
     * @param $app Application
     * @return array of commands
     */
    public function commands(Application $app): array
    {
        return [];
    }

    /**
     * @param $app Application
     * @return array of events
     */
    public function events(Application $app): array
    {
        return [];
    }


    /**
     * @param $app Application
     * @return array of event handlers
     */
    public function handlers(Application $app): array
    {
        return [];
    }


    /**
     * @param $app Application
     * @return array of widgets
     */
    public function widgets(Application $app): array
    {
        return [];
    }


    public function routes(Application $app):array{
        return [];
    }

    public function init()
    {
        $reflection = new \ReflectionObject($this);
        $parts = explode('\\', $reflection->getNamespaceName(), 1);
        $namespace = $this->_namespace = $parts[0];
        $alias = '@' . str_replace('\\', '-', $namespace);
        Yii::setAlias($alias, $this->getBasePath());
        if (Yii::$app instanceof \pw\web\Application) {
            $application = Yii::$app->id;
            if (is_dir($this->getBasePath() . '/controllers')) {
                $this->controllerNamespace = "$namespace\\controllers";
                $this->setViewPath($this->getBasePath() . '/views');
            } else {
                $this->controllerNamespace = "$namespace\\$application\\controllers";
                $this->setViewPath($this->getBasePath() . "/$application/views");
            }
        } else {
            $this->controllerNamespace = "$namespace\\commands";
        }
        parent::init();
    }

    public function setUuid(int $value)
    {
        $this->_id = $value;
    }

    public function getUUID($asString = true)
    {
        return (int)$this->_id;
    }

    public static function t($category, $message, $params = [], $language = null): string
    {
        return \Yii::$app->cache->getOrSet(md5($message.$category), function () use($category, $message, $params, $language) {
            $instance = self::getInstance();
            if (!$instance) {
                $p = [];
                foreach ((array)$params as $name => $value) {
                    $p['{' . $name . '}'] = $value;
                }

                return ($p === []) ? $message : strtr($message, $p);
            }
            return Yii::t($instance->id . '/' . $category, $message, $params, $language);
        });
    }

    /**
     * @return string
     */
    public function getNameSpace(): string
    {
        return $this->_namespace;
    }

    /**
     * @param $app Application
     * @throws \yii\base\InvalidConfigException
     */
    public function bootstrap($app)
    {

        $this->registerComponents($app);
        $this->registerModules($app);
        $this->registerBootstraps($app);

        if ($app instanceof \yii\console\Application) {
            $this->registerCommands($app);
            $this->registerRoutes($app);
        } else {
            $this->registerRoutes($app);
        }
        $this->registerHandlers($app);
        $this->registerModels($app);
    }

    public function getNavigation(): array{
        return [];
    }

    /**
     * @param $app Application
     * @throws \yii\base\InvalidConfigException
     */
    private function registerComponents(Application $app)
    {
        $components = $this->components($app);
        if ($components && is_array($components)) {
            foreach ($components as $component => $params) {
                $app->set($component, $params);
            }
        }
    }


    /**
     * @param $app Application
     */
    private function registerModules(Application $app)
    {
        $modules = $this->modules($app);
        if ($modules && is_array($modules)) {
            foreach ($modules as $module => $params) {
                $app->setModule($module, $params);
            }
        }
    }

    /**
     * @param $app Application
     */
    private function registerModels(Application $app)
    {
        $models = $this->models($app);
        if ($models && is_array($models)) {
            $app->registerModels($models);
        }
    }

    /**
     * @param $app Application
     */
    private function registerRoutes(Application $app)
    {
        $routes = $this->routes($app);
        Yii::$app->getUrlManager()->addRules($routes);
    }

    /**
     * @param $app Application
     * @throws \yii\base\InvalidConfigException
     */
    private function registerBootstraps(Application $app)
    {
        $bootstraps = $this->bootstraps($app);
        foreach ($bootstraps as $class) {
            $component = null;
            if (is_string($class)) {
                if ($app->has($class)) {
                    $component = $app->get($class);
                } elseif ($app->hasModule($class)) {
                    $component = $app->getModule($class);
                } elseif (strpos($class, '\\') === false) {
                    throw new InvalidConfigException("Unknown bootstrapping component ID: $class");
                }
            }
            if ($component === null) {
                $component = Yii::createObject($class);
            }
            if ($component instanceof BootstrapInterface) {
                Yii::debug('Bootstrap with ' . get_class($component) . '::bootstrap()', __METHOD__);
                $component->bootstrap($app);
            } else {
                Yii::debug('Bootstrap with ' . get_class($component), __METHOD__);
            }
        }
    }

    /**
     * @param $app Application
     */
    private function registerCommands(Application $app)
    {
        $commands = $this->commands($app);
        if ($commands && is_array($commands)) {
            foreach ($commands as $id => $command) {
                if (!isset($app->controllerMap[$id])) {
                    $app->controllerMap[$id] = $command;
                }
            }
        }
    }

    /**
     * @param $app Application
     * @throws \yii\base\InvalidConfigException
     */
    private function registerHandlers(Application $app)
    {
        $handlers = $this->handlers($app);
        if ($handlers && is_array($handlers)) {
            foreach ($handlers as $handler) {
                if (isset($handler['class'])) {
                    Event::on($handler['class'], $handler['event'], $handler['callback']);
                } else {
                    Event::on($handler[0], $handler[1], $handler[2]);
                }
            }
        }
    }
}
