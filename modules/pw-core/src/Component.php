<?php
namespace pw\core;

use pw\core\traits\GetModule;
use yii\base\Event;

class Component extends \yii\base\Component
{

    use GetModule;

    /**
     * @inheritdoc
     */
    public function trigger($name, Event $event = null)
    {
        if ($event === null && $name instanceof Event && $name->name) {
            $event = $name;
            $name = $event->name;
        }
        parent::trigger($name, $event);
    }
}
