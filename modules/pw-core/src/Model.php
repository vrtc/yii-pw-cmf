<?php
namespace pw\core;

use pw\core\events\AfterLoadEvent;

class Model extends \yii\base\Model
{

    use \pw\core\traits\GetModule;
    use \pw\core\traits\CreateMultiple;
    use \pw\core\traits\BehaviorAttached;
    use \pw\core\traits\ViewEvents;

    const EVENT_AFTER_LOAD = 'afterLoad';
    const EVENT_BEFORE_FORM_RENDER = 'beforeFormRender';
    const EVENT_BEFORE_DETAIL_RENDER = 'beforeDetailRender';
    const EVENT_BEFORE_GRID_RENDER = 'beforeGridRender';

    /**
     * @inheritdoc
     */
    public function load($data, $formName = null)
    {
        if (parent::load($data, $formName)) {
            $this->afterLoad($data);
            return true;
        }
        return false;
    }

    public function afterLoad($data)
    {
        $event = new AfterLoadEvent(['load' => $data]);
        $this->trigger(self::EVENT_AFTER_LOAD, $event);
    }



}
