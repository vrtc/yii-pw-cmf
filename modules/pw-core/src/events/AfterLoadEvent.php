<?php
namespace pw\core\events;

use yii\base\Event;

class AfterLoadEvent extends Event
{

    public $load;
}
