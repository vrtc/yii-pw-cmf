<?php
namespace pw\core\events;

use yii\base\Event;

class AfterInitEvent extends Event
{

    public $model;
}
