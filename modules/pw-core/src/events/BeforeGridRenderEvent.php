<?php
namespace pw\core\events;

use yii\base\Event;

class BeforeGridRenderEvent extends Event
{

    public $grid;
    public $model;
}
