<?php
namespace pw\core\events;

use yii\base\Event;

class BeforeFormRenderEvent extends Event
{

    public $form;
    public $model;
}
