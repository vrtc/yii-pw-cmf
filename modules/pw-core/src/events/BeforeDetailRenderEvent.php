<?php
namespace pw\core\events;

use yii\base\Event;

class BeforeDetailRenderEvent extends Event
{

    public $detail;
    public $model;
}
