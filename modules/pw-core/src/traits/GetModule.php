<?php
namespace pw\core\traits;

use Yii;

trait GetModule
{

    private static $_module = null;

    public static function getModule($class = null)
    {
        if (self::$_module === null) {
            self::$_module = self::findModule($class ?: \get_called_class());
        }
        if(isset(self::$_module->id)) {
            foreach (\Yii::$app->getModulesManager()->getSettings(self::$_module->id) as $key => $value){
                self::$_module->{$key} = $value;
            }
        }
        return self::$_module;
    }

    public static function findModule($id)
    {
        if (($pos = \strrpos($id, '\\')) !== false) {
            $id = \substr($id, 0, $pos);
            $class = $id . '\\Module';
            if (\class_exists($class)) {
                foreach (Yii::$app->getModules() as $idModule => $module) {
                    if ($module instanceof $class || (\is_array($module) && $module['class'] === $class)) {
                        return Yii::$app->getModule($idModule);
                    }
                }
            } else {
                return self::findModule($id);
            }
        }
        return false;
    }
}
