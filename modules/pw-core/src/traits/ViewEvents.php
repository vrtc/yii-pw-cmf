<?php
namespace pw\core\traits;

use pw\core\events\BeforeDetailRenderEvent;
use pw\core\events\BeforeFormRenderEvent;
use pw\core\events\BeforeGridRenderEvent;

trait ViewEvents{

    public function beforeFormRender($form)
    {
        $event = new BeforeFormRenderEvent([
            'form' => $form,
            'model' => $this,
        ]);
        $this->trigger(self::EVENT_BEFORE_FORM_RENDER, $event);
    }

    public function beforeDetailRender($detail)
    {
        $event = new BeforeDetailRenderEvent([
            'detail' => $detail,
            'model' => $this,
        ]);
        $this->trigger(self::EVENT_BEFORE_DETAIL_RENDER, $event);
    }

    public function beforeGridRender($grid)
    {
        $event = new BeforeGridRenderEvent([
            'grid' => $grid,
            'model' => $this,
        ]);
        $this->trigger(self::EVENT_BEFORE_GRID_RENDER, $event);
    }
}