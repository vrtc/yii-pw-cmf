<?php
namespace pw\core\traits;

trait Settings
{

    public static function getDefaultSettings()
    {

        $reflection = new \ReflectionClass(static::class);
        $defaults = $reflection->getDefaultProperties();
        unset(
            $defaults['id'],
            $defaults['pathMap'],
            $defaults['uuid'],
            $defaults['params'],
            $defaults['module'],
            $defaults['controllerMap'],
            $defaults['controllerNamespace'],
            $defaults['layout']
        );
        $privateProperties = $reflection->getProperties(\ReflectionProperty::IS_PRIVATE | \ReflectionProperty::IS_PROTECTED);
        foreach ($privateProperties as $prop) {
            $name = $prop->getName();
            if (array_key_exists($name, $defaults)) {
                unset($defaults[$name]);
            }
        }
        return $defaults;
    }

    public function getSettings()
    {
        $class = new \ReflectionClass($this);
        $settings = [];
        foreach ($class->getProperties(\ReflectionProperty::IS_PUBLIC) as $property) {
            if (!$property->isStatic()) {
                $settings[$property->getName()] = $property->getValue($this);
            }
        }
        return $settings;
    }
}
