<?php
namespace pw\core\traits;

use paulzi\sortable\SortableBehavior;
use yii\base\InvalidConfigException;
use pw\core\db\ActiveRecord;

trait Sortable
{
    private $_sortableBehavior;

    /**
     * @return SortableBehavior
     * @throws InvalidConfigException
     */
    private function getSortableBehavior(): SortableBehavior
    {
        if ($this->_sortableBehavior === null) {
            /** @var \yii\base\Component|self $this */
            foreach ($this->getBehaviors() as $behavior) {
                if ($behavior instanceof SortableBehavior) {
                    $this->_sortableBehavior = $behavior;
                    break;
                }
            }
            if ($this->_sortableBehavior === null) {
                throw new InvalidConfigException('SortableBehavior is not attached to model');
            }
        }
        return $this->_sortableBehavior;
    }

    /**
     * @return int
     * @throws \Exception
     */
    public function getSortablePosition(): int
    {
        return (int)$this->getSortableBehavior()->getSortablePosition();
    }

    /**
     * @return ActiveRecord
     * @throws \Exception
     */
    public function moveFirst(): ActiveRecord
    {
        return $this->getSortableBehavior()->moveFirst();
    }

    /**
     * @return ActiveRecord
     * @throws \Exception
     */
    public function moveLast(): ActiveRecord
    {
        return $this->getSortableBehavior()->moveLast();
    }

    /**
     * @param int $position
     * @param bool $forward
     * @return ActiveRecord
     * @throws \Exception
     */
    public function moveTo(int $position, bool $forward = true): ActiveRecord
    {
        return $this->getSortableBehavior()->moveTo($position, $forward);
    }

    /**
     * @param ActiveRecord $model
     * @return \pw\core\db\ActiveRecord
     * @throws \Exception
     */
    public function moveBefore(ActiveRecord $model): ActiveRecord
    {
        return $this->getSortableBehavior()->moveBefore($model);
    }

    /**
     * @param ActiveRecord $model
     * @return ActiveRecord
     * @throws \Exception
     */
    public function moveAfter(ActiveRecord $model): ActiveRecord
    {
        return $this->getSortableBehavior()->moveAfter($model);
    }

    /**
     * @param bool $middle
     * @return int
     * @throws \Exception
     */
    public function reorder(bool $middle = true): int
    {
        return $this->getSortableBehavior()->reorder($middle);
    }
}
