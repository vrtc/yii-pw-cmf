<?php
namespace pw\core\traits;

trait BehaviorAttached
{

    public function behaviorAttached($classname)
    {
        foreach ($this->getBehaviors() as $behavior) {
            if ($behavior instanceof $classname) {
                return true;
            }
        }
        return false;
    }
}
