<?php
namespace pw\core\traits;

use yii\validators\{
    BooleanValidator,
    DateValidator,
    StringValidator,
    NumberValidator
};

trait Typecast
{

    protected $attributeTypes = [];

    protected function detectAttributeTypes()
    {
        foreach ($this->getValidators() as $validator) {
            $type = null;
            if ($validator instanceof BooleanValidator) {
                $type = 'bool';
            } elseif ($validator instanceof NumberValidator) {
                $type = $validator->integerOnly ? 'integer' : 'float';
            } elseif ($validator instanceof DateValidator) {
                $type = 'date';
            } elseif ($validator instanceof StringValidator) {
                $type = 'string';
            }

            if ($type !== null) {
                foreach ((array)$validator->attributes as $attribute) {
                    $attributeTypes[$attribute] = $type;
                }
            }
        }
    }

    public function typecastAttributes()
    {
        foreach ($this->attributeTypes as $attribute => $type) {
            $value = $this->{$attribute};
            if ($value === null) {
                continue;
            }
            $this->{$attribute} = $this->typecastValue($value, $type);
        }
    }

    protected function typecastValue($value, $type)
    {
        if (is_scalar($type)) {
            if (is_object($value) && method_exists($value, '__toString')) {
                $value = $value->__toString();
            }
            switch ($type) {
                case 'integer':
                    return (int)$value;
                case 'float':
                    return (float)$value;
                case 'boolean':
                    return (bool)$value;
                case 'date':
                    return  new \DateTime(strtotime($value));
                case 'string':
                    return (string)$value;
            }
        }
        return $value;
    }
}
