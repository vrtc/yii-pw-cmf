<?php
namespace pw\core\interfaces;

use pw\core\db\ActiveRecord;

interface Sortable
{

    public function getSortablePosition(): int;

    public function moveFirst():ActiveRecord;

    public function moveLast():ActiveRecord;

    public function moveTo(int $position, bool $forward = true):ActiveRecord;

    public function moveBefore(ActiveRecord $model):ActiveRecord;

    public function moveAfter(ActiveRecord $model):ActiveRecord;

    public function reorder(bool $middle = true):int;
}
