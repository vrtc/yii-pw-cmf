<?php
namespace pw\core\interfaces;


interface TreeQuery
{
    public function roots();
}
