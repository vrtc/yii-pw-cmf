<?php
namespace pw\core\interfaces;

use yii\db\ActiveQueryInterface;
use yii\db\ActiveRecordInterface;

interface Tree
{
    public function getParents(int $depth = null);

    public function getParent();

    public function getRoot();

    public function getDescendants(int $depth = null, bool $andSelf = false);

    public function getChildren();

    public function getLeaves(int $depth = null);

    public function getPrev();

    public function getNext();

    public function populateTree(int $depth = null);

    public function isRoot();

    public function isChildOf(ActiveRecordInterface $node);

    public function isLeaf();

    public function makeRoot();

    public function prependTo(ActiveRecordInterface $node);

    public function appendTo(ActiveRecordInterface $node);

    public function insertBefore(ActiveRecordInterface $node);

    public function insertAfter(ActiveRecordInterface $node);

    public function deleteWithChildren();

    public static function find();
}
