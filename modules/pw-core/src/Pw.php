<?php

namespace pw\core;

use yii\base\Application;

class Pw extends \yii\BaseYii
{
    /**
     * @var \pw\console\Application|\pw\web\Application|BaseApplication|WebApplication|ConsoleApplication the application instance
     */
    public static $app;

    public static function getVersion()
    {
        return '0.1';
    }


    public static function powered()
    {
        return \Yii::t('yii', 'Powered by {pw}', [
            'pw' => '<a href="https://yar.pw/" rel="external">' . Yii::t('yii', 'Pw CMF') . '</a>'
        ]);
    }

    public static function t($category, $message, $params = [], $language = null)
    {
        if (static::$app !== null) {
            var_dump(debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, 1));
            exit;
            return static::$app->getI18n()->translate($category, $message, $params, $language ?: static::$app->language);
        }

        $placeholders = [];
        foreach ((array)$params as $name => $value) {
            $placeholders['{' . $name . '}'] = $value;
        }

        return ($placeholders === []) ? $message : strtr($message, $placeholders);
    }
}

/**
 * Class BaseApplication
 * Used for properties that are identical for both WebApplication and ConsoleApplication
 * @property \pw\cron\CronManager $cron The user component. This property is read-only. Extended component.
 */
abstract class BaseApplication extends Application
{
}

/**
 * Class WebApplication
 * Include only Web application related components here
 * @property \pw\cron\CronManager $cron The user component. This property is read-only. Extended component.
 */
class WebApplication extends \yii\web\Application
{
}

/**
 * Class ConsoleApplication
 * Include only Console application related components here
 *
 * @property \pw\cron\CronManager $cron The user component. This property is read-only. Extended component.
 */
class ConsoleApplication extends \yii\console\Application
{
}