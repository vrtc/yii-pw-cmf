<?php
namespace pw\core\db;

use pw\core\interfaces\TreeQuery;
use paulzi\nestedsets\NestedSetsQueryTrait;
use yii\db\ActiveQuery;

class NestedSetsQuery extends ActiveQuery implements TreeQuery
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function roots()
    {
        /** @var \yii\db\ActiveQuery $this */
        return $this->andWhere(['lft' => 1]);
    }
}
