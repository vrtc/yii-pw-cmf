<?php
namespace pw\core\db\mysql;

use pw\core\db\ColumnSchema;

class Schema extends \yii\db\mysql\Schema
{
    public $columnSchemaClass = ColumnSchema::class;

    /**
     * @inheritdoc
     */
    public function createColumnSchemaBuilder($type, $length = null)
    {
        return new ColumnSchemaBuilder($type, $length, $this->db);
    }

    public function createQueryBuilder()
    {
        return new QueryBuilder($this->db);
    }

}
