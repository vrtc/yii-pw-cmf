<?php

namespace pw\core\db;

use pw\core\events\AfterLoadEvent;
use pw\core\traits\Typecast;
use pw\ui\detail\DetailView;
use pw\ui\form\ActiveForm;
use pw\ui\grid\GridView;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class ActiveRecord extends \yii\db\ActiveRecord
{

    use \pw\core\traits\GetModule;
    use \pw\core\traits\CreateMultiple;
    use \pw\core\traits\BehaviorAttached;
    use \mootensai\relation\RelationTrait;
    use \pw\core\traits\ViewEvents;
    use Typecast;

    const EVENT_AFTER_LOAD = 'afterLoad';
    const EVENT_BEFORE_FORM_RENDER = 'beforeFormRender';
    const EVENT_BEFORE_DETAIL_RENDER = 'beforeDetailRender';
    const EVENT_BEFORE_GRID_RENDER = 'beforeGridRender';


    public function init()
    {
        parent::init();

    }

    public function beforeSave($insert)
    {
        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function load($data, $formName = null)
    {
        if (parent::load($data, $formName)) {
            $this->afterLoad($data);

            return true;
        }

        return false;
    }

    public function afterLoad(array $data)
    {
        $event = new AfterLoadEvent(['load' => $data]);
        $this->trigger(self::EVENT_AFTER_LOAD, $event);
    }

    public function fields()
    {
        $fields = [];
        foreach ($this->attributes as $key => $value) {
            if ($value instanceof UuidInterface) {
                $fields[$key] = function ($model, $field) {
                    if ($model->$field instanceof UuidInterface) {
                        return $model->$field->toString();
                    }

                    return $model->$field;
                };
            } else {
                $fields[$key] = $key;
            };
        }

        return $fields;
    }

    public function getPrimaryKeyName($asArray = false)
    {
        $keys = static::primaryKey();
        if (!$asArray && count($keys) === 1) {
            return $keys[0];
        }
        return $keys;
    }

    /**
     * @return ActiveQuery
     */
    public static function find()
    {
        return new ActiveQuery(\get_called_class());
    }
}
