<?php

namespace pw\core\db;

use pw\core\interfaces\Tree;
use pw\core\interfaces\TreeQuery;
use paulzi\materializedPath\MaterializedPathBehavior;
use yii\base\InvalidConfigException;

class MaterializedPathActiveRecord extends ActiveRecord implements Tree
{
    use TreeActiveRecordTrait;

    const MULTIPLE = false;

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $behavior = [
            'class'          => MaterializedPathBehavior::class,
            'pathAttribute'  => 'path',
            'depthAttribute' => 'depth',
            'sortable'       => []
        ];
        if (self::MULTIPLE) {
            $behavior['treeAttribute'] = 'tree';
        }
        $this->attachBehavior('tree', $behavior);
    }

    /**
     * @return array
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @return TreeQuery
     * @throws \yii\base\InvalidConfigException
     */
    public static function find()
    {
        return new MaterializedPathQuery(get_called_class());
    }
}
