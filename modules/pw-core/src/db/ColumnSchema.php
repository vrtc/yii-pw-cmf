<?php
namespace pw\core\db;

use pw\core\helpers\StringHelper;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class ColumnSchema extends \yii\db\ColumnSchema
{

    /**
     * @inheritdoc
     */
    public function phpTypecast($value)
    {
        if ($value && $this->phpType === 'uuid') {
            if (!StringHelper::uuidIsValid($value) && strlen($value) === 16) {
                $value = StringHelper::uuidFromBytes($value)->toString();
            }
            return $value;
        }
        if ($this->dbType === Schema::TYPE_TIMESTAMP || $this->dbType === Schema::TYPE_DATETIME || $this->dbType === Schema::TYPE_DATE) {
            $date = new \DateTime();
            $date->setTimestamp(\strtotime($value));
        }
        return $this->typecast($value);
    }

    /**
     * @inheritdoc
     */
    public function dbTypecast($value)
    {

        if ($this->phpType === 'uuid') {
            if ($value instanceof UuidInterface) {
                return $value->getBytes();
            }
            if (is_string($value) && StringHelper::uuidIsValid($value)) {
                return StringHelper::uuidFromString($value)->getBytes();
            }
        }
        if ($value instanceof \DateTime) {
            if ($this->dbType === Schema::TYPE_TIMESTAMP) {
                return $value->format('U');
            }
            if ($this->dbType === Schema::TYPE_DATETIME) {
                return $value->format('Y-m-d H:i:s');
            }
            if ($this->dbType === Schema::TYPE_DATE) {
                return $value->format('Y-m-d');
            }
        }
        if($value instanceof  \Ramsey\Uuid\Uuid){
            \var_dump($this,$value);exit;
        }
        return $this->typecast($value);
    }
}
