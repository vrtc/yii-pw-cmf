<?php
namespace pw\core\db;
use yii\db\ColumnSchemaBuilder;

trait UuidTypesTrait
{
    /**
     * @return \yii\db\Connection the database connection to be used for schema building.
     */
    protected abstract function getDb();

    /**
     * Creates a uuid column.
     * @return ColumnSchemaBuilder the column instance which can be further customized.
     */
    public function uuid()
    {
        return $this->getDb()->getSchema()->createColumnSchemaBuilder(Schema::TYPE_UUID);
    }

    /**
     * Creates a uuid primary key column.
     * @return ColumnSchemaBuilder the column instance which can be further customized.
     */
    public function uuidPrimaryKey()
    {
        return $this->getDb()->getSchema()->createColumnSchemaBuilder(Schema::TYPE_UUIDPK);
    }
}