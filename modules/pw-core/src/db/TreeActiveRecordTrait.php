<?php
namespace pw\core\db;


use paulzi\adjacencyList\AdjacencyListBehavior;
use paulzi\materializedPath\MaterializedPathBehavior;
use paulzi\nestedsets\NestedSetsBehavior;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecordInterface;

trait TreeActiveRecordTrait {


    /**
     * @var
     */
    private $_treeBehavior;

    /**
     * @return null|AdjacencyListBehavior|MaterializedPathBehavior|NestedSetsBehavior|\yii\base\Behavior
     * @throws InvalidConfigException
     */
    private function getTreeBehavior()
    {
        if ($this->_treeBehavior === null) {
            if (isset($this->behaviors['tree']) && (
                    $this->behaviors['tree'] instanceof AdjacencyListBehavior
                    || $this->behaviors['tree'] instanceof MaterializedPathBehavior
                    || $this->behaviors['tree'] instanceof NestedSetsBehavior
                )
            ) {
                $this->_treeBehavior = $this->behaviors['tree'];
            } else {
                throw new InvalidConfigException('TreeBehavior is not attached to model');
            }
        }
        return $this->_treeBehavior;
    }


    /**
     * @param int|null $depth
     * @return ActiveQueryInterface
     * @throws \yii\base\InvalidConfigException
     */
    public function getParents(int $depth = null)
    {
        return $this->getTreeBehavior()->getParents($depth);
    }

    /**
     * @return ActiveQueryInterface
     * @throws \yii\base\InvalidConfigException
     */
    public function getParent()
    {
        return $this->getTreeBehavior()->getParent();
    }

    /**
     * @return ActiveQueryInterface
     * @throws \yii\base\InvalidConfigException
     */
    public function getRoot()
    {
        return $this->getTreeBehavior()->getRoot();
    }

    /**
     * @param int|null $depth
     * @param bool $andSelf
     * @throws \yii\base\InvalidConfigException
     * @return ActiveQueryInterface
     */
    public function getDescendants(int $depth = null, bool $andSelf = false)
    {
        return $this->getTreeBehavior()->getDescendants($depth, $andSelf);
    }

    /**
     * @return ActiveQueryInterface
     * @throws \yii\base\InvalidConfigException
     */
    public function getChildren()
    {
        return $this->getTreeBehavior()->getChildren();
    }

    /**
     * @param int|null $depth
     * @throws \yii\base\InvalidConfigException
     * @return ActiveQueryInterface
     */
    public function getLeaves(int $depth = null)
    {
        return $this->getTreeBehavior()->getLeaves($depth);
    }

    /**
     * @return ActiveQueryInterface
     * @throws \yii\base\InvalidConfigException
     */
    public function getPrev()
    {
        return $this->getTreeBehavior()->getPrev();
    }

    /**
     * @return ActiveQueryInterface
     * @throws \yii\base\InvalidConfigException
     */
    public function getNext()
    {
        return $this->getTreeBehavior()->getNext();
    }

    /**
     * @param int|null $depth
     * @throws \yii\base\InvalidConfigException
     * @return Tree
     */
    public function populateTree(int $depth = null)
    {
        return $this->getTreeBehavior()->populateTree($depth);
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function isRoot()
    {
        return $this->getTreeBehavior()->isRoot();
    }

    /**
     * @param ActiveRecordInterface $node
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function isChildOf(ActiveRecordInterface $node)
    {
        return $this->getTreeBehavior()->isChildOf($node);
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function isLeaf()
    {
        return $this->getTreeBehavior()->isLeaf();
    }

    /**
     * @return TreeActiveRecord
     * @throws \yii\base\InvalidConfigException
     */
    public function makeRoot()
    {
        return $this->getTreeBehavior()->makeRoot();
    }

    /**
     * @param ActiveRecordInterface $node
     * @throws \yii\base\InvalidConfigException
     * @return Tree
     */
    public function prependTo(ActiveRecordInterface $node)
    {
        return $this->getTreeBehavior()->prependTo($node);
    }

    /**
     * @param ActiveRecordInterface $node
     * @throws \yii\base\InvalidConfigException
     * @return Tree
     */
    public function appendTo(ActiveRecordInterface $node)
    {
        return $this->getTreeBehavior()->appendTo($node);
    }

    /**
     * @param ActiveRecordInterface $node
     * @throws \yii\base\InvalidConfigException
     * @return Tree
     */
    public function insertBefore(ActiveRecordInterface $node)
    {
        return $this->getTreeBehavior()->insertBefore($node);
    }

    /**
     * @param ActiveRecordInterface $node
     * @throws \yii\base\InvalidConfigException
     * @return Tree
     */
    public function insertAfter(ActiveRecordInterface $node)
    {
        return $this->getTreeBehavior()->insertAfter($node);
    }

    /**
     * @return int
     * @throws \yii\base\InvalidConfigException
     */
    public function deleteWithChildren()
    {
        return $this->getTreeBehavior()->deleteWithChildren();
    }

}