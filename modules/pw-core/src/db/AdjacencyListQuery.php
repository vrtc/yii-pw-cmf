<?php
namespace pw\core\db;

use pw\core\interfaces\TreeQuery;
use yii\db\ActiveQuery;

class AdjacencyListQuery extends ActiveQuery implements TreeQuery
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function roots()
    {
        return $this->andWhere(['parent_id' => null]);
    }
}
