<?php
namespace pw\core\db;


abstract class Schema extends \yii\db\Schema
{
    public $columnSchemaClass = ColumnSchema::class;


    /**
     * @inheritdoc
     */
    public function createQueryBuilder()
    {
        return new QueryBuilder($this->db);
    }
}
