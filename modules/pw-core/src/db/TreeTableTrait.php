<?php
namespace  pw\core\db;
use yii\db\ColumnSchemaBuilder;
use yii\base\InvalidParamException;

trait TreeTableTrait {

    private function getPrimaryKeyType(array $columns){
        foreach ($columns as $i => $column) {
            if (is_object($column)) {
                if ($column->getCategoryType() === ColumnSchemaBuilder::CATEGORY_PK) {
                    return  $column->getType();
                }
            }
        }
        return false;
    }

    public function createAdjacencyListTable(string $table, array $columns, string $options = null){

        $pkType = $this->getPrimaryKeyType($columns);
        if($pkType === false){
            throw new InvalidParamException("Primary key not found in table $table");
        }
        $columns['parent_id'] = $this->getRelationColumn($pkType);
        $columns['sort'] = $this->integer();
        $this->createTable($table, $columns, $options);
        $this->createIndex('parent_sort', $table, ['parent_id', 'sort']);
    }

    public function createMultipleNestedSetsTable(string $table,array $columns,string $options = null){
        $pkType = $this->getPrimaryKeyType($columns);
        if($pkType === false){
            throw new InvalidParamException("Primary key not found in table $table");
        }
        $columns['tree'] = $this->getRelationColumn($pkType);
        $this->createNestedSetsTable($table,$columns,$options);
    }

    public function createNestedSetsTable(string $table, array $columns, string $options = null){
        $pkType = $this->getPrimaryKeyType($columns);
        if($pkType === false){
            throw new InvalidParamException("Primary key not found in table $table");
        }
        $columns['parent_id'] = $this->getRelationColumn($pkType);
        $columns['lft'] = $this->integer()->notNull();
        $columns['rgt'] = $this->integer()->notNull();
        $columns['depth'] = $this->integer()->notNull();

        $this->createTable($table, $columns, $options);

        if(isset($columns['tree'])) {
            $this->createIndex('lft', $table, ['tree', 'lft', 'rgt']);
            $this->createIndex('rgt', $table, ['tree', 'rgt']);
        }
        else {
            $this->createIndex('lft', $table, ['lft', 'rgt']);
            $this->createIndex('rgt', $table, ['rgt']);
        }
    }

    public function createMaterializedPathTable(string $table, array $columns, string $options = null){
        $pkType = $this->getPrimaryKeyType($columns);
        if($pkType === false){
            throw new InvalidParamException("Primary key not found in table $table");
        }
        if (isset($options['multiple'])) {
            $columns['tree'] = $this->getRelationColumn($pkType);
            unset($options['multiple']);
        }
        $columns['path'] = $this->string();
        $columns['depth'] = $this->integer()->notNull();
        $columns['sort'] = $this->integer();

        $this->createTable($table, $columns, $options);

        if(isset($columns['tree'])) {
            $this->createIndex('path', $table, ['tree', 'path']);
        }
        else {
            $this->createIndex('path', $table, ['path']);
        }
    }

    protected function getRelationColumn($pkType)
    {
        if ($pkType === Schema::TYPE_PK) {
            $model_field = $this->integer();
        } elseif ($pkType === Schema::TYPE_UPK) {
            $model_field = $this->integer()->unsigned();
        } elseif ($pkType === Schema::TYPE_BIGPK) {
            $model_field = $this->bigInteger();
        } elseif ($pkType === Schema::TYPE_UBIGPK) {
            $model_field = $this->bigInteger()->unsigned();
        }
        if (!isset($model_field)) {
            throw new Exception('Primary key not found');
        }
        return $model_field;
    }
}