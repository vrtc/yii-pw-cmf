<?php

namespace pw\core\db;

use Yii;
use yii\base\Exception;
use yii\db\ColumnSchemaBuilder;

class Migration extends \yii\db\Migration
{
    use TextTypesTrait;
    use TreeTableTrait;

    /**
     * @var string
     */
    protected $tableOptions = '';

    protected $driver;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->driver = Yii::$app->db->driverName;
        switch ($this->driver) {
            case 'mysql':
                $this->tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
                break;
            case 'pgsql':
                $this->tableOptions = '';
                break;
            default:
                throw new \RuntimeException('Your database is not supported!');
        }
    }

    public function createTable($table, $columns, $options = null)
    {
        if ($options === null) {
            $options = $this->tableOptions;
        }
        parent::createTable($table, $columns, $options);
    }
}