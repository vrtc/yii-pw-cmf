<?php
namespace pw\core\db;

use pw\core\interfaces\TreeQuery;
use paulzi\materializedPath\MaterializedPathQueryTrait;
use yii\db\ActiveQuery;

class MaterializedPathQuery extends ActiveQuery implements TreeQuery
{

    use MaterializedPathQueryTrait;
}
