<?php
namespace pw\core\db;

use pw\core\interfaces\Tree;
use pw\core\interfaces\TreeQuery;
use paulzi\nestedsets\NestedSetsBehavior;
use yii\base\InvalidConfigException;
use yii\base\NotSupportedException;
use yii\db\ActiveRecordInterface;
use yii\db\Exception;
use yii\db\Expression;

class NestedSetsActiveRecord extends ActiveRecord implements Tree
{
    //use TreeActiveRecordTrait;

    const OPERATION_MAKE_ROOT       = 1;
    const OPERATION_PREPEND_TO      = 2;
    const OPERATION_APPEND_TO       = 3;
    const OPERATION_INSERT_BEFORE   = 4;
    const OPERATION_INSERT_AFTER    = 5;
    const OPERATION_DELETE_ALL      = 6;

    const MULTIPLE = false;



    /**
     * @var string|null
     */
    protected $_operation;

    /**
     * @var ActiveRecord|self|null
     */
    protected $_node;

    /**
     * @var string
     */
    protected $_treeChange;



    /**
     * @param int|null $depth
     * @return \yii\db\ActiveQuery
     */
    public function getParents(int $depth = null)
    {
        $tableName = static::tableName();
        $condition = [
            'and',
            ['<', "{$tableName}.[[lft]]",  $this->lft],
            ['>', "{$tableName}.[[rgt]]", $this->rgt],
        ];
        if ($depth !== null) {
            $condition[] = ['>=', "{$tableName}.[[depth]]", $this->depth - $depth];
        }

        $query = static::find()
            ->andWhere($condition)
            ->andWhere($this->treeCondition())
            ->addOrderBy(["{$tableName}.[[lft]]" => SORT_ASC]);
        $query->multiple = true;

        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        $tableName = static::tableName();
        $query = $this->getParents(1)
            ->orderBy(["{$tableName}.[[lft]]" => SORT_DESC])
            ->limit(1);
        $query->multiple = false;
        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoot()
    {
        $tableName = static::tableName();
        $query = static::find()
            ->andWhere(["{$tableName}.[[lft]]" => 1])
            ->andWhere($this->treeCondition())
            ->limit(1);
        $query->multiple = false;
        return $query;
    }

    /**
     * @param int|null $depth
     * @param bool $andSelf
     * @param bool $backOrder
     * @return \yii\db\ActiveQuery
     */
    public function getDescendants(int $depth = null,bool $andSelf = false, $backOrder = false)
    {
        $tableName = static::tableName();
        $attribute = $backOrder ? 'rgt' : 'lft';
        $condition = [
            'and',
            [$andSelf ? '>=' : '>', "{$tableName}.[[{$attribute}]]",  $this->lft],
            [$andSelf ? '<=' : '<', "{$tableName}.[[{$attribute}]]",  $this->rgt],
        ];

        if ($depth !== null) {
            $condition[] = ['<=', "{$tableName}.[[depth]]", $this->depth + $depth];
        }

        $query = static::find()
            ->andWhere($condition)
            ->andWhere($this->treeCondition())
            ->addOrderBy(["{$tableName}.[[{$attribute}]]" => $backOrder ? SORT_DESC : SORT_ASC]);
        $query->multiple = true;

        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->getDescendants(1);
    }

    /**
     * @param int|null $depth
     * @return \yii\db\ActiveQuery
     */
    public function getLeaves(int $depth = null)
    {
        $tableName = static::tableName();
        $query = $this->getDescendants($depth)
            ->andWhere(["{$tableName}.[[lft]]" => new Expression("{$tableName}.[[rgt]] - 1")]);
        $query->multiple = true;
        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrev()
    {
        $tableName = static::tableName();
        $query = static::find()
            ->andWhere(["{$tableName}.[[rgt]]" => $this->lft - 1])
            ->andWhere($this->treeCondition())
            ->limit(1);
        $query->multiple = false;
        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNext()
    {
        $tableName = static::tableName();
        $query = static::find()
            ->andWhere(["{$tableName}.[[lft]]" => $this->rgt + 1])
            ->andWhere($this->treeCondition())
            ->limit(1);
        $query->multiple = false;
        return $query;
    }

    /**
     * Populate children relations for self and all descendants
     * @param int $depth = null
     * @return static
     */
    public function populateTree(int $depth = null)
    {
        /** @var ActiveRecord[]|static[] $nodes */
        $nodes = $this->getDescendants($depth)->all();

        $key = $this->lft;
        $relates = [];
        $parents = [$key];
        $prev = $this->depth;
        foreach($nodes as $node)
        {
            $level = $node->depth;
            if ($level <= $prev) {
                $parents = array_slice($parents, 0, $level - $prev - 1);
            }

            $key = end($parents);
            if (!isset($relates[$key])) {
                $relates[$key] = [];
            }
            $relates[$key][] = $node;

            $parents[] = $node->lft;
            $prev = $level;
        }

        $ownerDepth = $this->depth;
        $nodes[] = $this;
        foreach ($nodes as $node) {
            $key = $node->lft;
            if (isset($relates[$key])) {
                $node->populateRelation('children', $relates[$key]);
            } elseif ($depth === null || $ownerDepth + $depth > $node->depth) {
                $node->populateRelation('children', []);
            }
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isRoot()
    {
        return $this->lft === 1;
    }

    /**
     * @param ActiveRecord $node
     * @return bool
     */
    public function isChildOf(ActiveRecordInterface $node)
    {
        $result = $this->lft > $node->lft
            && $this->rgt < $node->rgt;

        if ($result && static::MULTIPLE !== false) {
            $result = $this->tree === $node->tree;
        }

        return $result;
    }

    /**
     * @return bool
     */
    public function isLeaf()
    {
        return $this->rgt - $this->lft === 1;
    }

    /**
     * @return ActiveRecord
     */
    public function makeRoot()
    {
        $this->_operation = self::OPERATION_MAKE_ROOT;
        return $this;
    }

    /**
     * @param ActiveRecord $node
     * @return ActiveRecord
     */
    public function prependTo(ActiveRecordInterface $node)
    {
        $this->_operation = self::OPERATION_PREPEND_TO;
        $this->_node = $node;
        return $this;
    }

    /**
     * @param ActiveRecord $node
     * @return ActiveRecord
     */
    public function appendTo(ActiveRecordInterface $node)
    {
        $this->_operation = self::OPERATION_APPEND_TO;
        $this->_node = $node;
        return $this;
    }

    /**
     * @param ActiveRecord $node
     * @return ActiveRecord
     */
    public function insertBefore(ActiveRecordInterface $node)
    {
        $this->_operation = self::OPERATION_INSERT_BEFORE;
        $this->_node = $node;
        return $this;
    }

    /**
     * @param ActiveRecord $node
     * @return ActiveRecord
     */
    public function insertAfter(ActiveRecordInterface $node)
    {
        $this->_operation = self::OPERATION_INSERT_AFTER;
        $this->_node = $node;
        return $this;
    }

    /**
     * Need for paulzi/auto-tree
     */
    public function preDeleteWithChildren()
    {
        $this->_operation = self::OPERATION_DELETE_ALL;
    }

    /**
     * @return bool|int
     * @throws \Exception
     * @throws \yii\db\Exception
     */
    public function deleteWithChildren()
    {
        $this->_operation = self::OPERATION_DELETE_ALL;
        if (!$this->isTransactional(ActiveRecord::OP_DELETE)) {
            $transaction = static::getDb()->beginTransaction();
            try {
                $result = $this->deleteWithChildrenInternal();
                if ($result === false) {
                    $transaction->rollBack();
                } else {
                    $transaction->commit();
                }
                return $result;
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }
        } else {
            $result = $this->deleteWithChildrenInternal();
        }
        return $result;
    }

    /**
     * @throws Exception
     * @throws NotSupportedException
     */
    public function beforeInsert()
    {
        if ($this->_node !== null && !$this->_node->getIsNewRecord()) {
            $this->_node->refresh();
        }
        switch ($this->_operation) {
            case self::OPERATION_MAKE_ROOT:
                $condition = array_merge(['lft' => 1], $this->treeCondition());
                if (static::findOne($condition) !== null) {
                    throw new Exception('Can not create more than one root.');
                }
                $this->lft = 1;
                $this->rgt = 2;
                $this->depth = 0;
                break;

            case self::OPERATION_PREPEND_TO:
                $this->insertNode($this->_node->lft + 1, 1);
                break;

            case self::OPERATION_APPEND_TO:
                $this->insertNode($this->_node->rgt, 1);
                break;

            case self::OPERATION_INSERT_BEFORE:
                $this->insertNode($this->_node->lft, 0);
                break;

            case self::OPERATION_INSERT_AFTER:
                $this->insertNode($this->_node->rgt + 1, 0);
                break;

            default:
                throw new NotSupportedException('Method "'. static::class . '::insert" is not supported for inserting new nodes.');
        }
    }

    /**
     * @throws Exception
     */
    public function afterInsert()
    {
        if ($this->_operation === self::OPERATION_MAKE_ROOT && static::MULTIPLE !== false && $this->tree === null) {
            $id = $this->getPrimaryKey();
            $this->tree = $id;

            $primaryKey = static::primaryKey();
            if (!isset($primaryKey[0])) {
                throw new Exception('"' . static::class . '" must have a primary key.');
            }

            static::updateAll(['tree' => $id], [$primaryKey[0] => $id]);
        }
        $this->_operation = null;
        $this->_node      = null;
    }

    /**
     * @throws Exception
     */
    public function beforeUpdate()
    {
        if ($this->_node !== null && !$this->_node->getIsNewRecord()) {
            $this->_node->refresh();
        }

        switch ($this->_operation) {
            case self::OPERATION_MAKE_ROOT:
                if ('tree' === null) {
                    throw new Exception('Can not move a node as the root when "treeAttribute" is not set.');
                }
                if ($this->getOldAttribute('tree') !== $this->tree) {
                    $this->_treeChange = $this->tree;
                    $this->setAttribute('tree', $this->getOldAttribute('tree'));
                }
                break;

            case self::OPERATION_INSERT_BEFORE:
            case self::OPERATION_INSERT_AFTER:
                if ($this->_node->isRoot()) {
                    throw new Exception('Can not move a node before/after root.');
                }

            case self::OPERATION_PREPEND_TO:
            case self::OPERATION_APPEND_TO:
                if ($this->_node->getIsNewRecord()) {
                    throw new Exception('Can not move a node when the target node is new record.');
                }

                if ($this->equals($this->_node)) {
                    throw new Exception('Can not move a node when the target node is same.');
                }

                if ($this->_node->isChildOf($this)) {
                    throw new Exception('Can not move a node when the target node is child.');
                }
        }
    }

    public function beforeSave($insert)
    {
        if($insert) {
            $this->beforeInsert();
        }
        else {
            $this->beforeUpdate();
        }
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if($insert){
            $this->afterInsert();
        }
        else {
            $this->afterUpdate();
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     *
     */
    public function afterUpdate()
    {
        switch ($this->_operation) {
            case self::OPERATION_MAKE_ROOT:
                $this->moveNodeAsRoot();
                break;

            case self::OPERATION_PREPEND_TO:
                $this->moveNode($this->_node->lft + 1, 1);
                break;

            case self::OPERATION_APPEND_TO:
                $this->moveNode($this->_node->rgt, 1);
                break;

            case self::OPERATION_INSERT_BEFORE:
                $this->moveNode($this->_node->lft, 0);
                break;

            case self::OPERATION_INSERT_AFTER:
                $this->moveNode($this->_node->rgt + 1, 0);
                break;
        }
        $this->_operation  = null;
        $this->_node       = null;
        $this->_treeChange = null;
    }

    /**
     * @throws Exception
     */
    public function beforeDelete()
    {
        if ($this->getIsNewRecord()) {
            throw new Exception('Can not delete a node when it is new record.');
        }
        if ($this->isRoot() && $this->_operation !== self::OPERATION_DELETE_ALL) {
            throw new Exception('Method "'. static::class. '::delete" is not supported for deleting root nodes.');
        }
        $this->refresh();
        return parent::beforeDelete();
    }

    /**
     *
     */
    public function afterDelete()
    {
        $left  = $this->lft;
        $right = $this->rgt;
        if ($this->_operation === static::OPERATION_DELETE_ALL || $this->isLeaf()) {
            $this->shift($right + 1, null, $left - $right - 1);
        } else {
            static::updateAll(
                [
                    'lft'  => new Expression("[[lft]] - 1"),
                    'rgt' => new Expression("[[rgt]] - 1"),
                    'depth' => new Expression("[[depth]] - 1"),
                ],
                $this->getDescendants()->where
            );
            $this->shift($right + 1, null, -2);
        }
        $this->_operation = null;
        $this->_node      = null;
        parent::afterDelete();
    }

    /**
     * @return int
     */
    protected function deleteWithChildrenInternal()
    {
        if (!$this->beforeDelete()) {
            return false;
        }
        $result = static::deleteAll($this->getDescendants(null, true)->where);
        $this->setOldAttributes(null);
        $this->afterDelete();
        return $result;
    }

    /**
     * @param int $to
     * @param int $depth
     * @throws Exception
     */
    protected function insertNode($to, $depth = 0)
    {
        if ($this->_node->getIsNewRecord()) {
            throw new Exception('Can not create a node when the target node is new record.');
        }

        if ($depth === 0 && $this->_node->isRoot()) {
            throw new Exception('Can not insert a node before/after root.');
        }
        $this->lft = $to;
        $this->rgt = $to+1;
        $this->depth = $this->_node->depth + $depth;
        
        if (static::MULTIPLE !== false) {
            $this->tree = $this->_node->tree;
        }
        $this->shift($to, null, 2);
    }

    /**
     * @param int $to
     * @param int $depth
     * @throws Exception
     */
    protected function moveNode($to, $depth = 0)
    {
        $left  = $this->lft;
        $right = $this->rgt;
        $depth = $this->depth - $this->_node->depth - $depth;
        if (static::MULTIPLE === false || $this->tree === $this->_node->tree) {
            // same root
            static::updateAll(
                ['depth' => new Expression("-[[depth]]" . sprintf('%+d', $depth))],
                $this->getDescendants(null, true)->where
            );
            $delta = $right - $left + 1;
            if ($left >= $to) {
                $this->shift($to, $left - 1, $delta);
                $delta = $to - $left;
            } else {
                $this->shift($right + 1, $to - 1, -$delta);
                $delta = $to - $right - 1;
            }
            static::updateAll(
                [
                    'lft'  => new Expression("[[lft]]"  . sprintf('%+d', $delta)),
                    'rgt' => new Expression("[[rgt]]" . sprintf('%+d', $delta)),
                    'depth' => new Expression("-[[depth]]"),
                ],
                [
                    'and',
                    $this->getDescendants(null, true)->where,
                    ['<', 'depth', 0],
                ]
            );
        } else {
            // move from other root
            $tree = $this->_node->tree;
            $this->shift($to, null, $right - $left + 1, $tree);
            $delta = $to - $left;
            static::updateAll(
                [
                    'lft'  => new Expression("[[lft]]"  . sprintf('%+d', $delta)),
                    'rgt' => new Expression("[[rgt]]" . sprintf('%+d', $delta)),
                    'depth' => new Expression("[[depth]]" . sprintf('%+d', -$depth)),
                    'tree'  => $tree,
                ],
                $this->getDescendants(null, true)->where
            );
            $this->shift($right + 1, null, $left - $right - 1);
        }
    }

    /**
     *
     */
    protected function moveNodeAsRoot()
    {
        $left   = $this->lft;
        $right  = $this->rgt;
        $depth  = $this->depth;
        $tree   = $this->_treeChange ? $this->_treeChange : $this->getPrimaryKey();

        static::updateAll(
            [
                'lft'  => new Expression("[[lft]]"  . sprintf('%+d', 1 - $left)),
                'rgt' => new Expression("[[rgt]]" . sprintf('%+d', 1 - $left)),
                'depth' => new Expression("[[depth]]" . sprintf('%+d', -$depth)),
                'tree'  => $tree,
            ],
            $this->getDescendants(null, true)->where
        );
        $this->shift($right + 1, null, $left - $right - 1);
    }



    /**
     * @param int $from
     * @param int $to
     * @param int $delta
     * @param int|null $tree
     */
    protected function shift($from, $to, $delta, $tree = null)
    {
        if ($delta !== 0 && ($to === null || $to >= $from)) {
            if (static::MULTIPLE !== false && $tree === null) {
                $tree = $this->tree;
            }
            foreach (['lft', 'rgt'] as $i => $attribute) {
                static::updateAll(
                    [$attribute => new Expression("[[{$attribute}]]" . sprintf('%+d', $delta))],
                    [
                        'and',
                        $to === null ? ['>=', $attribute, $from] : ['between', $attribute, $from, $to],
                        'tree' !== null ? ['tree' => $tree] : [],
                    ]
                );
            }
        }
    }

    /**
     * @return array
     */
    protected function treeCondition()
    {
        if (static::MULTIPLE === false) {
            return [];
        }
        $tableName = static::tableName();
        return ["{$tableName}.[[tree]]" => $this->tree];
    }

    /**
     * @return array
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @return TreeQuery
     * @throws \yii\base\InvalidConfigException
     */
    public static function find()
    {
        return new NestedSetsQuery(get_called_class());
    }
}
