<?php

namespace pw\core\db;

use Ramsey\Uuid\UuidInterface;
use yii\db\Expression;

trait QueryBuilderTrait
{

    private $_columnSchemas = [];

    /**
     * @inheritdoc
     */
    public function build($query, $params = [])
    {

        $columnSchemas = [];
        if ($query->from) {
            foreach ($query->from as $table) {
                if (is_string($table) && ($tableSchema = $this->db->getTableSchema($table)) !== null) {
                    $columnSchemas = $tableSchema->columns;
                }
            }
        } elseif ($query->modelClass) {
            $table       = $query->modelClass::tablename();
            $tableSchema = $this->db->getTableSchema($table);
            if ($tableSchema !== null) {
                $columnSchemas = $tableSchema->columns;
            }

        }
        $this->_columnSchemas = $columnSchemas;

        return parent::build($query, $params);
    }

    public function buildInCondition($operator, $operands, &$params)
    {
        list($column, $values) = $operands;
        if (!empty($column) && is_array($column)) {
            $column = reset($column);
            if (!is_array($values) && !$values instanceof \Traversable) {
                $values = (array)$values;
            }
            foreach ($values as $i => $value) {
                if (isset($this->_columnSchemas[$column])) {
                }
                $values[$i] = isset($this->_columnSchemas[$column]) ? $this->_columnSchemas[$column]->dbTypecast($value) : $value;
            }
            $operands[1] = $values;
        }

        return parent::buildInCondition($operator, $operands, $params);
    }

/*    public function buildCondition($condition, &$params)
    {
        if (\is_array($condition)) {
            foreach ($condition as $key => $value) {
                if ($value instanceof UuidInterface) {
                    $condition[$key] = $value->getBytes();
                } elseif (\is_string($value) && StringHelper::uuidIsValid($value)) {
                    $condition[$key] = StringHelper::uuidFromString($value)->getBytes();
                }
            }
        }
        foreach ($params as $key => $value) {
            if ($value instanceof UuidInterface) {
                $params[$key] = $value->getBytes();
            }
            if (\is_string($value) && StringHelper::uuidIsValid($value)) {
                $params[$key] = StringHelper::uuidFromString($value)->getBytes();
            }
        }

        return parent::buildCondition($condition, $params);
    }*/
}
