<?php
namespace pw\core\db;

class Connection extends \yii\db\Connection
{
    public $schemaMap = [
        'mysql' => 'pw\core\db\mysql\Schema'
    ];
}
