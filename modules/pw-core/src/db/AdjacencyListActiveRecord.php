<?php

namespace pw\core\db;

use pw\core\interfaces\Tree;
use pw\core\interfaces\TreeQuery;
use yii\db\ActiveRecordInterface;
use yii\db\Query;
use yii\base\NotSupportedException;
use yii\db\Exception;

class AdjacencyListActiveRecord extends ActiveRecord implements Tree
{
    const OPERATION_FIRST = 1;
    const OPERATION_LAST = 2;
    const OPERATION_POSITION_BACKWARD = 3;
    const OPERATION_POSITION_FORWARD = 4;

    const OPERATION_MAKE_ROOT = 5;
    const OPERATION_PREPEND_TO = 6;
    const OPERATION_APPEND_TO = 7;
    const OPERATION_INSERT_BEFORE = 8;
    const OPERATION_INSERT_AFTER = 9;
    const OPERATION_DELETE_ALL = 10;


    protected $checkLoop = false;

    protected $parentsJoinLevels = 3;

    protected $childrenJoinLevels = 3;

    /**
     * @var bool
     */
    protected $operation;

    /**
     * @var ActiveRecord|self|null
     */
    protected $node;

    /**
     * @var ActiveRecord[]
     */
    private $_parentsOrdered;

    /**
     * @var array
     */
    private $_parentsIds;

    /**
     * @var array
     */
    private $_childrenIds;

    public $sort_field = 'sort';


    /**
     * @return array
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public static function find()
    {
        return new AdjacencyListQuery(get_called_class());
    }

    public function getParents(int $depth = null):AdjacencyListQuery
    {
        $tableName       = static::tableName();
        $ids             = $this->getParentsIds($depth);
        $query           = static::find()->andWhere(["{$tableName}.[[{$this->getPrimaryKeyName()}]]" => $ids]);
        $query->multiple = true;

        return $query;
    }

    /**
     * @return ActiveRecord[]
     * @throws Exception
     */
    public function getParentsOrdered():array
    {
        if ($this->_parentsOrdered !== null) {
            return $this->_parentsOrdered;
        }
        $parents    = $this->getParents()->all();
        $ids        = array_flip($this->getParentsIds());
        $primaryKey = $this->getPrimaryKeyName();
        usort($parents, function ($a, $b) use ($ids, $primaryKey) {
            $aIdx = $ids[$a->$primaryKey];
            $bIdx = $ids[$b->$primaryKey];
            if ($aIdx === $bIdx) {
                return 0;
            }
            return $aIdx > $bIdx ? -1 : 1;
        });

        return $this->_parentsOrdered = $parents;
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws Exception
     */
    public function getParent()
    {
        return $this->hasOne(static::class, [$this->getPrimaryKeyName() => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoot()
    {
        $tableName       = static::tableName();
        $id              = $this->getParentsIds();
        $id              = $id ? $id[count($id) - 1] : $this->primaryKey;
        $query           = static::find()->andWhere(["{$tableName}.[[{$this->getPrimaryKeyName()}]]" => $id]);
        $query->multiple = false;

        return $query;
    }

    /**
     * @param int|null $depth
     * @param bool     $andSelf
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDescendants(int $depth = null, bool $andSelf = false)
    {
        $tableName = static::tableName();
        $ids       = $this->getDescendantsIds($depth, true);
        if ($andSelf) {
            $ids[] = $this->getPrimaryKey();
        }
        $query           = static::find()->andWhere(["{$tableName}.[[{$this->getPrimaryKeyName()}]]" => $ids]);
        $query->multiple = true;
        return $query;
    }

    /**
     * @return ActiveRecord[]
     * @throws Exception
     */
    public function getDescendantsOrdered():array
    {
        $descendants = $this->descendants;
        $ids         = array_flip($this->getDescendantsIds(null, true));
        $primaryKey  = $this->getPrimaryKeyName();
        usort($descendants, function ($a, $b) use ($ids, $primaryKey) {
            $aIdx = $ids[$a->$primaryKey];
            $bIdx = $ids[$b->$primaryKey];
            if ($aIdx === $bIdx) {
                return 0;
            }
            return $aIdx > $bIdx ? 1 : -1;
        });
        return $descendants;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->hasMany(static::class, ['parent_id' => $this->getPrimaryKeyName()])->orderBy([$this->sort_field => SORT_ASC]);
    }

    /**
     * @param int|null $depth
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLeaves(int $depth = null)
    {
        $tableName       = static::tableName();
        $query           = $this->getDescendants($depth)
            ->joinWith([
                'children' => function ($query) use ($tableName) {
                    /** @var \yii\db\ActiveQuery $query */
                    $query
                        ->from("$tableName children")
                        ->orderBy(null);
                }
            ])
            ->andWhere(['children.[[parent_id]]' => null]);
        $query->multiple = true;

        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws NotSupportedException
     */
    public function getPrev()
    {
        $tableName       = static::tableName();
        $query           = static::find()
            ->andWhere([
                'and',
                ["{$tableName}.[[parent_id]]" => $this->parent_id],
                ['<', "{$tableName}.[[{$this->sort_field}]]", $this->sort],
            ])
            ->orderBy(["{$tableName}.[[{$this->sort_field}]]" => SORT_DESC])
            ->limit(1);
        $query->multiple = false;

        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws NotSupportedException
     */
    public function getNext()
    {
        $tableName       = static::tableName();
        $query           = static::find()
            ->andWhere([
                'and',
                ["{$tableName}.[[parent_id]]" => $this->parent_id],
                ['>', "{$tableName}.[[{$this->sort_field}]]", $this->sort],
            ])
            ->orderBy(["{$tableName}.[[{$this->sort_field}]]" => SORT_ASC])
            ->limit(1);
        $query->multiple = false;

        return $query;
    }

    /**
     * @param int|null $depth
     * @param bool     $cache
     *
     * @return array
     */
    public function getParentsIds($depth = null, $cache = true):array
    {
        if ($cache && $this->_parentsIds !== null) {
            return $depth === null ? $this->_parentsIds : \array_slice($this->_parentsIds, 0, $depth);
        }

        $parentId = $this->parent_id;
        if ($parentId === null) {
            if ($cache) {
                $this->_parentsIds = [];
            }

            return [];
        }
        $result     = [(int)$parentId];
        $tableName  = static::tableName();
        $primaryKey = $this->getPrimaryKeyName();
        $depthCur   = 1;
        while ($parentId !== null && ($depth === null || $depthCur < $depth)) {
            $query = (new Query())
                ->select(['lvl0.[[parent_id]] AS lvl0'])
                ->from("{$tableName} lvl0")
                ->where(["lvl0.[[{$primaryKey}]]" => $parentId]);
            for ($i = 0; $i < $this->parentsJoinLevels && ($depth === null || $i + $depthCur + 1 < $depth); $i++) {
                $j = $i + 1;
                $query
                    ->addSelect(["lvl{$j}.[[parent_id]] as lvl{$j}"])
                    ->leftJoin("{$tableName} lvl{$j}", "lvl{$j}.[[{$primaryKey}]] = lvl{$i}.[[{$primaryKey}]]");
            }
            if ($parentIds = $query->one(static::getDb())) {
                foreach ($parentIds as $parentId) {
                    $depthCur++;
                    if ($parentId === null) {
                        break;
                    }
                    $result[] = $parentId;
                }
            } else {
                $parentId = null;
            }
        }
        if ($cache && $depth === null) {
            $this->_parentsIds = $result;
        }

        return $result;
    }

    /**
     * @param int|null $depth
     * @param bool     $flat
     * @param bool     $cache
     *
     * @return array
     */
    public function getDescendantsIds($depth = null, $flat = false, $cache = true):array
    {
        if ($cache && $this->_childrenIds !== null) {
            $result = $depth === null ? $this->_childrenIds : \array_slice($this->_childrenIds, 0, $depth);

            return $flat && !empty($result) ? \array_merge(...$result) : $result;
        }

        $result       = [];
        $tableName    = static::tableName();
        $primaryKey   = $this->getPrimaryKeyName();
        $depthCur     = 0;
        $schema = static::getTableSchema();
        $column = $schema->getColumn($primaryKey);
        $lastLevelIds = [$this->primaryKey];
        while (!empty($lastLevelIds) && ($depth === null || $depthCur < $depth)) {
            $levels = 1;
            $depthCur++;
            $query = (new Query())
                ->select(["lvl0.[[{$primaryKey}]] AS lvl0"])
                ->from("{$tableName} lvl0")
                ->where(['lvl0.[[parent_id]]' => $lastLevelIds]);
            $query->orderBy(["lvl0.[[{$this->sort_field}]]" => SORT_ASC]);
            for ($i = 0; $i < $this->childrenJoinLevels && ($depth === null || $i + $depthCur + 1 < $depth); $i++) {
                $depthCur++;
                $levels++;
                $j = $i + 1;
                $query
                    ->addSelect(["lvl{$j}.[[{$primaryKey}]] as lvl{$j}"])
                    ->leftJoin("{$tableName} lvl{$j}", [
                        'and',
                        "lvl{$j}.[[parent_id]] = lvl{$i}.[[{$primaryKey}]]",
                        ['is not', "lvl{$i}.[[{$primaryKey}]]", null],
                    ]);
                $query->addOrderBy(["lvl{$j}.[[{$this->sort_field}]]" => SORT_ASC]);
            }
            if ($this->childrenJoinLevels) {
                $columns = [];
                foreach ($query->all(static::getDb()) as $row) {
                    $level = 0;
                    foreach ($row as $id) {
                        if ($id !== null) {

                            $columns[$level][$id] = true;
                        }
                        $level++;
                    }
                }
                for ($i = 0; $i < $levels; $i++) {
                    if (isset($columns[$i])) {
                        $lastLevelIds = \array_keys($columns[$i]);
                        $result[]     = $lastLevelIds;
                    } else {
                        $lastLevelIds = [];
                        break;
                    }
                }
            } else {
                $lastLevelIds = $query->column(static::getDb());
                if ($lastLevelIds) {
                    $result[] = $lastLevelIds;
                }
            }
        }
        if ($cache && $depth === null) {
            $this->_childrenIds = $result;
        }

        return $flat && !empty($result) ? \array_merge(...$result) : $result;
    }

    /**
     * Populate children relations for self and all descendants
     *
     * @param int $depth = null
     *
     * @return static
     */
    public function populateTree(int $depth = null)
    {
        /** @var ActiveRecord[]|static[] $nodes */
        $primaryKey = $this->getPrimaryKeyName();
        $depths = [$primaryKey=> 0];
        $schema = static::getTableSchema();
        $column = $schema->getColumn($primaryKey);
        if ($depth === null) {
            $nodes = $this->descendantsOrdered;
        } else {
            $data = $this->getDescendantsIds($depth);
            foreach ($data as $i => $ids) {
                foreach ($ids as $id) {
                    $depths[$id] = $i + 1;
                }
            }
            $nodes = $this->getDescendants($depth)
                ->orderBy([$this->sort_field => SORT_ASC])
                ->all();
        }

        $relates = [];
        foreach ($nodes as $node) {
            $key = $node->parent_id;
            if (!isset($relates[$key])) {
                $relates[$key] = [];
            }
            $relates[$key][] = $node;
        }

        $nodes[] = $this;
        foreach ($nodes as $node) {
            $key = $node->getPrimaryKey();
            if (isset($relates[$key])) {
                $node->populateRelation('children', $relates[$key]);
            } elseif ($depth === null || (isset($depths[$node->getPrimaryKey()]) && $depths[$node->getPrimaryKey()] < $depth)) {
                $node->populateRelation('children', []);
            }
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isRoot():bool
    {
        return $this->parent_id === null;
    }

    /**
     * @param ActiveRecord $node
     *
     * @return bool
     */
    public function isChildOf(ActiveRecordInterface $node):bool
    {
        $ids = $this->getParentsIds();

        return \in_array($node->primaryKey, $ids,true);
    }

    /**
     * @return bool
     */
    public function isLeaf():bool
    {
        return \count($this->children) === 0;
    }


    public function makeRoot():ActiveRecordInterface
    {
        $this->operation = self::OPERATION_MAKE_ROOT;

        return $this;
    }

    public function prependTo(ActiveRecordInterface $node):ActiveRecordInterface
    {
        $this->operation = self::OPERATION_PREPEND_TO;
        $this->node      = $node;

        return $this;
    }

    public function appendTo(ActiveRecordInterface $node):ActiveRecordInterface
    {
        $this->operation = self::OPERATION_APPEND_TO;
        $this->node      = $node;

        return $this;
    }

    public function insertBefore(ActiveRecordInterface $node):ActiveRecordInterface
    {
        $this->operation = self::OPERATION_INSERT_BEFORE;
        $this->node      = $node;

        return $this;
    }

    public function insertAfter(ActiveRecordInterface $node):ActiveRecordInterface
    {
        $this->operation = self::OPERATION_INSERT_AFTER;
        $this->node      = $node;

        return $this;
    }
    /**
     * @return bool|int
     * @throws \Exception
     * @throws \yii\db\Exception
     */
    public function deleteWithChildren()
    {
        $this->operation = self::OPERATION_DELETE_ALL;
        if (!$this->isTransactional(ActiveRecord::OP_DELETE)) {
            $transaction = $this->getDb()->beginTransaction();
            try {
                $result = $this->deleteWithChildrenInternal();
                if ($result === false) {
                    $transaction->rollBack();
                } else {
                    $transaction->commit();
                }

                return $result;
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }
        } else {
            $result = $this->deleteWithChildrenInternal();
        }

        return $result;
    }

    public function reorderChildren(bool $middle = true):int
    {
        $item = $this->children[0];
        if ($item) {
            return $item->reorder($middle);
        }
        return 0;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($this->getIsNewRecord() && $this->operation === null) {
            $this->operation = self::OPERATION_LAST;
        }
        if ($this->node !== null && !$this->node->getIsNewRecord()) {
            $this->node->refresh();
        }
        switch ($this->operation) {
            case self::OPERATION_MAKE_ROOT:
                $this->parent_id = null;
                $this->sort      = 0;
                break;

            case self::OPERATION_PREPEND_TO:
                $this->insertIntoInternal(false);
                break;

            case self::OPERATION_APPEND_TO:
                $this->insertIntoInternal(true);
                break;

            case self::OPERATION_INSERT_BEFORE:
                $this->insertNearInternal(false);
                break;

            case self::OPERATION_INSERT_AFTER:
                $this->insertNearInternal(true);
                break;

            default:
                if ($this->getIsNewRecord()) {
                    throw new NotSupportedException('Method "' . static::class . '::insert" is not supported for inserting new nodes.');
                }
        }

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert,$changedAttributes)
    {
        $this->operation = null;
        $this->node      = null;
        parent::afterSave($insert,$changedAttributes);
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if ($this->getIsNewRecord()) {
            throw new Exception('Can not delete a node when it is new record.');
        }
        if ($this->operation !== self::OPERATION_DELETE_ALL && $this->isRoot()) {
            throw new Exception('Method "' . static::class . '::delete" is not supported for deleting root nodes.');
        }
        $this->refresh();

        return parent::beforeDelete();
    }

    /**
     *
     */
    public function afterDelete()
    {
        if ($this->operation !== static::OPERATION_DELETE_ALL) {
            static::updateAll(
                ['parent_id' => $this->parent_id],
                ['parent_id' => $this->primaryKey]
            );
        }
        $this->operation = null;
    }

    /**
     * @param bool $forInsertNear
     *
     * @throws Exception
     */
    protected function checkNode($forInsertNear = false)
    {
        if ($forInsertNear && $this->node->isRoot()) {
            throw new Exception('Can not move a node before/after root.');
        }
        if ($this->node->getIsNewRecord()) {
            throw new Exception('Can not move a node when the target node is new record.');
        }

        if ($this->equals($this->node)) {
            throw new Exception('Can not move a node when the target node is same.');
        }

        if ($this->checkLoop && $this->node->isChildOf($this)) {
            throw new Exception('Can not move a node when the target node is child.');
        }
    }

    /**
     * Append to operation internal handler
     *
     * @param bool $append
     *
     * @throws Exception
     */
    protected function insertIntoInternal($append)
    {
        $this->checkNode(false);
        $this->parent_id = $this->node->primaryKey;
        if ($append) {
            $this->moveLast();
        } else {
            $this->moveFirst();
        }
    }

    /**
     * Insert operation internal handler
     *
     * @param bool $forward
     *
     * @throws Exception
     */
    protected function insertNearInternal($forward)
    {
        $this->checkNode(true);
        $this->parent_id = $this->node->parent_id;
        if ($forward) {
            $this->moveAfter($this->node);
        } else {
            $this->moveBefore($this->node);
        }
    }

    /**
     * @return int
     */
    protected function deleteWithChildrenInternal()
    {
        if (!$this->beforeDelete()) {
            return false;
        }
        $ids    = $this->getDescendantsIds(null, true, false);

        foreach ($ids as $id){
            $idsx[] =  $id;

        }
        $idsx[]  = $this->primaryKey;
        $result = self::deleteAll([$this->getPrimaryKeyName() => $idsx]);

        $this->setOldAttributes(null);
        $this->afterDelete();

        return $result;
    }

    /**
     * @return integer
     */
    public function getSortablePosition()
    {
        return $this->sort;
    }

    /**
     * @return ActiveRecord
     */
    public function moveFirst()
    {
        $this->operation = self::OPERATION_FIRST;

        return $this;
    }

    /**
     * @return ActiveRecord
     */
    public function moveLast()
    {
        $this->operation = self::OPERATION_LAST;

        return $this;
    }

    public function moveTo(int $position,bool $forward = true):AdjacencyListActiveRecord
    {
        $this->operation = $forward ? self::OPERATION_POSITION_FORWARD : self::OPERATION_POSITION_BACKWARD;
        $this->sort  = $position;

        return $this;
    }

    public function moveBefore(AdjacencyListActiveRecord $model):AdjacencyListActiveRecord
    {
        return $this->moveTo($model->sort - 1, false);
    }


    public function moveAfter(AdjacencyListActiveRecord $model):AdjacencyListActiveRecord
    {
        return $this->moveTo($model->sort + 1, true);
    }

    /**
     * Reorders items with values of sortAttribute begin from zero.
     *
     * @param bool $middle
     *
     * @return integer
     * @throws \Exception
     */
    public function reorder(bool $middle = true):int
    {
        $result = 0;
        \Yii::$app->getDb()->transaction(function () use (&$result, $middle) {
            $list = $this->getQueryInternal()
                ->select($this->getPrimaryKeyName())
                ->orderBy([$this->sort_field => SORT_ASC])
                ->asArray()
                ->all();
            $from = $middle ? count($list) >> 1 : 0;
            foreach ($list as $i => $item) {
                $result += static::updateAll([$this->sort_field => ($i - $from) * 100], $item);
            }
        });

        return $result;
    }
}
