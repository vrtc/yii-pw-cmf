<?php
namespace pw\core;

use pw\core\traits\GetModule;
use pw\core\traits\Settings;

class Widget extends \yii\base\Widget
{
    use GetModule;
    use Settings;
}
