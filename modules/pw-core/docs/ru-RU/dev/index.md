# Минимальные системные требования
> Данные системные требования актуальны только для системных (ядерных) модулей. Для дополнительных модулей могут быть дополнительные минимальные системные требования (см. документацию к этим модулям)

* ОЗУ 128 МБ
* Диск не менее 100 МБ
* PHP  =< 7.1
* MySQL =< 5.5


# Установка Pw
## Быстрая установка из консоли

### c помощью composer

Перейдите в директорию, в которую хотите установить Pw и выполните:

```
# composer create-project --stability=dev --repository-url=http://app.yar.pw pw/pw-cms pw-cms

# cd pw-cms

# run  php pw installer/install
```

### в ручном режиме

```
# wget http://app.yar.pw/latest.zip
```

```
# unzip latest.zip
```

```
# cd pw-cms
# run  php pw installer/install
```
