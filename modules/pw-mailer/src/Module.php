<?php

namespace pw\mailer;

use pw\mailer\components\Sms;
use pw\mailer\models\Template;
use yii\base\Application;
use yii\base\BootstrapInterface;
use yii\helpers\ArrayHelper;
use pw\mailer\components\Mailer;
use pw\mailer\commands\QueueController;

class Module extends \pw\core\Module implements BootstrapInterface
{

    public $maxAttempts = 3;

    public $mailsAtTime = 10;

    public $attemptIntervals = [0, 'PT10M', 'PT1H', 'PT6H'];

    public $fromEmail;

    /**
     * @param Application $app
     * @return array
     */
    public function components(Application $app): array
    {
        return [
            'mailer' => Mailer::class,
            'sms' => [
                'class' => \alexeevdv\sms\ru\Client::class,
                'api_id' => env('SMS_API'),
            ]

        ];
    }

    public function commands(Application $app): array
    {
        return [
            'mailer' => QueueController::class
        ];
    }

    public function routes(Application $app): array
    {
        return [
            [

                'pattern'=>'mail/<key:.+>',
                'route'=>'default/view',
                'internal'=>true

            ],
        ];
    }

    public function getNavigation(): array
    {
        return [
            'system'=>[
                 [
                    'label' => 'Шаблоны писем',
                    'url'   => ['/pw-mailer/templates/index'],
                    'icon'  => 'envelope'
                ],
                [
                    'label' => 'Очередь рассылки',
                    'url'   => ['/pw-mailer/messages/index'],
                    'icon'  => 'paper-plane'
                ],
            ],
            'settings' => [
                [
                    'label'  => 'Почта',
                    'icon'   => 'envelope',
                    'url'   => ['/pw-mailer/settings/index'],
                ]
            ]
        ];
    }
}
