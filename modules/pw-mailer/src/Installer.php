<?php

namespace pw\mailer;

use pw\mailer\models\Senders;

class Installer extends \pw\modules\ModuleInstaller
{

    public function getVersion(): string
    {
        return '0.1';
    }

    public function getAuthors(): array
    {
        return ['i@yar.pw'];
    }

    public function getLink(): string
    {
        return 'http://yar.pw';
    }

    public function getName(): string
    {
        return 'Mailer';
    }

    public function getSourceLanguage(): string
    {
        return 'ru-RU';
    }

    public function getCronJobs(): array
    {
        return [
            'mailer/send' => [
                'name'        => 'pw-mailer',
                'description' => 'send all emails from queue',
                'expression'  => '*/1 * * * * *', //@daily, @monthly
            ]
        ];
    }

    public function afterInstall()
    {
        $sender = new Senders();
        $sender->email = 'no-reply@example.com';
        $sender->method = Senders::METHOD_MAIL;
        $sender->translate('ru-RU')->name = 'example.com';
        $sender->save(false);
    }
}
