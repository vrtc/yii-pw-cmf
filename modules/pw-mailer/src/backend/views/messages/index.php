<?php

use yii\helpers\Html;
use pw\ui\grid\GridView;

/* @var $this \pw\web\View */
/* @var $searchModel \pw\mailer\backend\models\MessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>

<?= GridView::widget([
    'pageSize'     => 100,
    'id'           => 'letters',
    'dataProvider' => $dataProvider,
    'filterModel'  => $searchModel,
    'title'        => $this->title,
    'icon'         => 'envelope',
    'tableOptions' => ['class' => 'table table-striped table-bordered table-hover '],
    'columns'      => [
        'to',
        [
            'label' => 'Тема',
            'content' => function($model){
                return $model->getData()['setSubject'][key($model->getData()['setSubject'])][0];
            }
        ],
        'created_time:datetime',
        [
            'attribute' => 'priority',
            'content'   => function ($model) {
                return $model->getPriority();
            },
            'filter'    => \pw\mailer\models\Message::getPriorities()
        ],
        [
            'attribute' => 'status',
            'content'   => function ($model) {
                return $model->getStatus();
            },
            'filter'    => \pw\mailer\models\Message::getStatuses()
        ],
        'attempts',
        'error',
        'send_time:datetime',
        [
            'class'    => \pw\ui\grid\ActionColumn::class,
            'template' => '{view} {reset} {delete}',
            'buttons'  => [
                'view'  => function ($url, $model) {
                    return Html::a('<i class="fa fa-search"></i>',
                        ['/pw-mailer/messages/view', 'id' => $model->id, 'frontend' => true],
                        ['class' => 'btn btn-success', 'target' => '_blank']);
                },
                'reset' => function ($url, $model) {
                    return Html::a('<i class="fa fa-refresh"></i>', ['/pw-mailer/messages/reset', 'id' => $model->id],
                        ['class' => 'btn btn-warning']);
                }
            ]
        ],
    ],
]); ?>
