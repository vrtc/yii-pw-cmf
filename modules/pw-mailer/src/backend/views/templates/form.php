<?php
/* @var $this yii\web\View */
/* @var $model \pw\mailer\models\Template */

/* @var $form pw\ui\form\ActiveForm */

use dosamigos\tinymce\TinyMce;
use yii\helpers\Html;
use pw\ui\form\ActiveForm;
?>

<?php $form = ActiveForm::begin(['model' => $model]); ?>
<?php if ($model->getPlaceholders()): ?>
    <div class="alert alert-info">
        <?php foreach ($model->getPlaceholders() as $placeholder): ?>
            <p><b><?= $placeholder ?></b> - <?= Yii::t('mailer', $placeholder) ?></p>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
<?= $form->field($model, 'subject')->textInput() ?>
<?= $form->field($model, 'html_body')->widget(TinyMce::class, [
    'options' => ['rows' => 25],
    'language' => 'ru',
    'clientOptions' => [
        'plugins' => [
            "advlist autolink lists link charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ],
        'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
    ]
]);?>
<?= $form->field($model, 'text_body')->textarea() ?>
<?= $form->field($model, 'sender_id')->dropDownList($senders,
    ['prompt' => Yii::t('admin', 'Выбрать'), 'class' => 'bs-select']) ?>
<?= $form->field($model, 'theme')->dropDownList($themes,
    ['prompt' => Yii::t('admin', 'Выбрать'), 'class' => 'bs-select']) ?>
<?= $form->field($model, 'is_active')->checkbox([
    'class'         => 'make-switch',
    'data-on-text'  => '<i class="fa fa-check"></i>',
    'data-off-text' => '<i class="fa fa-times"></i>',
    'data-on-color' => 'success',
]) ?>
<?php ActiveForm::end(); ?>
