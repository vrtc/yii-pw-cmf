<?php

use pw\ui\grid\GridView;

/* @var $this \pw\web\View */
/* @var $searchModel \pw\mailer\backend\models\TemplateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>

<?= GridView::widget([
    'id' => 'letters',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'actions' => [
        [
            'label' => Yii::t('pages', 'Добавить шаблон'),
            'url' => ['create']
        ]
    ],
    'columns' => [
        'key',
        'subject',
        [
            'class' => \pw\ui\grid\ActionColumn::class,
            'visibleButtons' => [
                'view' => false,
                'delete' => false
            ]
        ],
    ],
]);
