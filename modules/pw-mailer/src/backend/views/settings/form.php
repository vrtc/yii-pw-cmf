<?php
/* @var $this pw\web\View */
/* @var $model pw\mailer\models\Senders */
/* @var $form pw\ui\form\ActiveForm */


use pw\ui\form\ActiveForm;
use pw\mailer\models\Senders;

$isSmtp = $model->method === Senders::METHOD_SMTP;

$this->registerJs('$("#mailersenders-method").on("change",function(e){
        $type = $(this).val();
        if($type == ' . Senders::METHOD_SMTP . '){
            $(".smtp-form").removeClass("hidden");
        }
        else
            $(".smtp-form").addClass("hidden");
    });');
?>
<?php $form = ActiveForm::begin([
    'model' => $model,
    'title' => Yii::t('mailer', 'Новый отправитель'),
    'icon' => \pw\ui\icons\Icons::show('send')
]); ?>
<?= $form->field($model, 'name')->textInput() ?>
<?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>
<?= $form->field($model, 'method')->dropDownList($methods, ['id' => 'mailersenders-method']) ?>

<?= $form->field($model->smtp, 'host', ['options' => ['class' => 'smtp-form form-group ' . ($isSmtp ? '' : 'hidden')]])->textInput(['maxlength' => 255]) ?>
<?= $form->field($model->smtp, 'port', ['options' => ['class' => 'smtp-form form-group ' . ($isSmtp ? '' : 'hidden')]])->textInput(['maxlength' => 255]) ?>
<?= $form->field($model->smtp, 'encryption', ['options' => ['class' => 'smtp-form form-group ' . ($isSmtp ? '' : 'hidden')]])->dropDownList($encryptions) ?>
<?= $form->field($model->smtp, 'authentication', ['options' => ['class' => 'smtp-form form-group ' . ($isSmtp ? '' : 'hidden')]])->checkbox() ?>
<?= $form->field($model->smtp, 'username', ['options' => ['class' => 'smtp-form form-group ' . ($isSmtp ? '' : 'hidden')]])->textInput(['maxlength' => 255]) ?>
<?= $form->field($model->smtp, 'password', ['options' => ['class' => 'smtp-form form-group ' . ($isSmtp ? '' : 'hidden')]])->textInput(['maxlength' => 255]) ?>
<?php ActiveForm::end(); ?>

