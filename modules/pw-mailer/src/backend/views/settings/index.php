<?php
/* @var $this pw\web\View */
/* @var $form pw\ui\form\ActiveForm */
/* @var $searchModel \pw\mailer\backend\models\SendersSearch */

use pw\ui\form\ActiveForm;
use pw\ui\grid\GridView;

?>
<?php $form = ActiveForm::begin([
    'model' => $model,
    'title' => Yii::t('mailer', 'Настройки отправки писем'),
    'icon' => 'gear'
]); ?>
<?php $form->beginTab('general', 'Параметры отправки') ?>
<?= $form->field($model, 'maxAttempts')->textInput(['maxlength' => 255]) ?>
<?= $form->field($model, 'mailsAtTime')->textInput(['maxlength' => 255]) ?>
<?php $form->endTab() ?>
<?php ActiveForm::end(); ?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'title' => Yii::t('mailer', 'Отправители'),
    'icon' => 'send',
    'actions' => [
        [
            'url' => ['create'],
            'label' => Yii::t('mailer', 'Добавить')
        ]
    ],
    'columns' => [
        'email',
        'name',
        [
            'attribute' => 'method',
            'value' => function ($model) {
                return $model->getMethod();
            },
        ],
        [
            'class' => \pw\ui\grid\ActionColumn::class,
            'template' => '{update}{delete}',
        ],
    ],
]);
