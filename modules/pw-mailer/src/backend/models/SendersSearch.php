<?php
namespace pw\mailer\backend\models;

use pw\mailer\models\Senders;
use pw\mailer\models\SendersI18N;
use yii\data\ActiveDataProvider;
use yii\base\Model;

class SendersSearch extends Senders
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['email', 'name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Senders::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'key' => $this->key,
        ]);

        $query->andFilterWhere(['like', 'i18n.subject', $this->subject]);

        return $dataProvider;
    }
}
