<?php
namespace pw\mailer\backend\models;

use pw\mailer\models\Letters;
use pw\mailer\models\LettersI18N;
use pw\mailer\models\Message;
use pw\mailer\models\Queue;
use yii\data\ActiveDataProvider;
use yii\base\Model;

class MessageSearch extends Message {

public $subject;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attempts','status'],'integer'],
            [['subject'], 'safe'],
            [['to'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Message::find();

        $query->orderBy(['id'=>SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'attempts'=>$this->attempts,
            'status'=>$this->status
        ]);
        $query->andFilterWhere([
            'to'=>$this->to
        ]);

        $query->andFilterWhere(['like','subject',$this->subject]);

        return $dataProvider;
    }
}
