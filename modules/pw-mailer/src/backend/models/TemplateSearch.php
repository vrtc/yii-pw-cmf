<?php
namespace pw\mailer\backend\models;

use pw\mailer\models\Template;
use yii\data\ActiveDataProvider;
use yii\base\Model;

class TemplateSearch extends Template
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'key'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Template::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'key' => $this->key,
        ]);

        return $dataProvider;
    }
}