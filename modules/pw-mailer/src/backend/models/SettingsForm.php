<?php
namespace pw\mailer\backend\models;


use Yii;
use pw\modules\SettingsModel;

class SettingsForm extends SettingsModel
{

    public $fromEmail;
    public $maxAttempts;
    public $mailsAtTime;

    public function rules()
    {
        return [
            [['maxAttempts', 'mailsAtTime'], 'integer'],
            [['fromEmail'], 'email']
        ];
    }

    public function attributeLabels()
    {
        return [
            'fromEmail' => Yii::t('mailer', 'Email адрес отправителя'),
            'maxAttempts' => Yii::t('mailer', 'Кол-во попыток отправить письмо'),
            'mailsAtTime' => Yii::t('mailer', 'Кол-во отправляемых писем за раз'),
        ];
    }
}
