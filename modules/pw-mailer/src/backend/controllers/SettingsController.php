<?php
namespace pw\mailer\backend\controllers;

use pw\mailer\backend\models\SendersSearch;
use Yii;
use pw\web\Controller;
use yii\web\NotFoundHttpException;
use pw\mailer\models\Senders;
use pw\mailer\models\SmtpConfig;
use pw\mailer\backend\models\SettingsForm;

class SettingsController extends Controller
{

    public function actionIndex()
    {
        $form = new SettingsForm();
        if ($form->load(Yii::$app->request->post()) && $form->save()) {
            Yii::$app->session->setFlash('success', Yii::t('mailer', 'Изменения сохранены'));
            return $this->redirect(['index']);
        }
        $searchModel = new SendersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->setPageTitle(Yii::t('mailer', 'Наcтройки почты'));
        $this->addBreadcrumb(Yii::t('mailer', 'Настройки почты'));
        return $this->render('index', [
            'model' => $form,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionCreate()
    {
        $model = new Senders();

        $model->smtp = new SmtpConfig();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->method === Senders::METHOD_SMTP) {
                $model->smtp->load(Yii::$app->request->post());
            }
            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('mailer', 'Изменения сохранены'));
                return $this->redirect(['index']);
            }
        }
        $methods = Senders::getMethods();
        $encryptions = SmtpConfig::getEncryptions();
        $this->setPageTitle(Yii::t('mailer', 'Новый отправитель'));
        $this->addBreadcrumb(Yii::t('mailer', 'Настройки почты'), ['index']);
        $this->addBreadcrumb(Yii::t('mailer', 'Новый отправитель'));
        return $this->render('form', [
            'model' => $model,
            'methods' => $methods,
            'encryptions' => $encryptions
        ]);
    }

    public function actionUpdate($id)
    {
        $model = Senders::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException('');
        }
        $model->smtp = new SmtpConfig();
        if ((int)$model->method === Senders::METHOD_SMTP) {
            $model->smtp->setAttributes(json_decode($model->settings, true), false);
        }
        if ($model->load(Yii::$app->request->post())) {
            if ((int)$model->method === Senders::METHOD_SMTP) {
                $model->smtp->load(Yii::$app->request->post());
            }
            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('mailer', 'Изменения сохранены'));
                return $this->redirect(['index']);
            }
        }
        $methods = Senders::getMethods();
        $encryptions = SmtpConfig::getEncryptions();
        $this->setPageTitle(Yii::t('mailer', 'Редактирование отправителя {name}', ['name' => $model->name]));
        $this->addBreadcrumb(Yii::t('mailer', 'Настройки почты'), ['index']);
        $this->addBreadcrumb(Yii::t('mailer', 'Редактирование отправителя {name}', ['name' => $model->name]));
        return $this->render('form', [
            'model' => $model,
            'methods' => $methods,
            'encryptions' => $encryptions
        ]);
    }
}

