<?php
namespace pw\mailer\backend\controllers;

use pw\modules\models\Modules;
use Yii;
use pw\web\Controller;
use pw\mailer\models\Senders;
use pw\mailer\models\Template;
use pw\mailer\backend\models\TemplateSearch;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class TemplatesController extends Controller
{


    public function actionIndex()
    {
        $searchModel = new TemplateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->addBreadcrumb(Yii::t('mailer', 'Шаблоны писем'));
        $this->setPageTitle(Yii::t('mailer', 'Шаблоны писем'));
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new Template();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('mailer', 'Изменения сохранены'));
            return $this->redirect(['update', 'id' => $model->id]);
        }
        $senders = ArrayHelper::map(Senders::find()->all(), 'id', function ($model) {
            return $model->name . ' <' . $model->email . '>';
        });

        $themes = [];
        foreach(Yii::$app->getModulesManager()->themes()->installed() as $theme){

            if($theme['type'] === Modules::TYPE_THEME_FRONTEND){
                $themeFiles = [];
                $files=\yii\helpers\FileHelper::findFiles(Yii::getAlias('@root') . $theme['path'] . '/src/mail/', ['recursive' => false]);
                foreach ($files as $file){
                    $fileName = str_replace('.php', '', basename($file));
                    $themeFiles[$fileName] = $fileName;
                }
                $themes[$theme['key']] = $themeFiles;
            }
        }

        $this->addBreadcrumb(Yii::t('mailer', 'Шаблоны писем'), ['index']);

        $this->addBreadcrumb(Yii::t('mailer', 'Новый шаблон'));
        $this->setPageTitle(Yii::t('mailer', 'Новый шаблон'));

        return $this->render('form', [
            'model' => $model,
            'senders' => $senders,
            'themes' => $themes
        ]);
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('mailer', 'Изменения сохранены'));
            return $this->redirect(['update', 'id' => $model->id]);
        }
        $senders = ArrayHelper::map(Senders::find()->all(), 'id', function ($model) {
            return $model->name . ' <' . $model->email . '>';
        });


        $themes = [];
        foreach(Yii::$app->getModulesManager()->themes()->installed() as $theme){

            if($theme['type'] === Modules::TYPE_THEME_FRONTEND){
                $themeFiles = [];
                $files=\yii\helpers\FileHelper::findFiles(Yii::getAlias('@root') . $theme['path'] . '/src/mail/', ['recursive' => false]);
                foreach ($files as $file){
                    $fileName = str_replace('.php', '', basename($file));
                    $themeFiles[$fileName] = $fileName;
                }
                $themes[$theme['key']] = $themeFiles;
            }
        }
        $this->addBreadcrumb(Yii::t('mailer', 'Шаблоны писем'), ['index']);
        $this->addBreadcrumb(Yii::t('mailer', 'Редактирование шаблона {key}', ['key' => $model->key]));
        $this->setPageTitle(Yii::t('mailer', 'Редактирование шаблона {key}', ['key' => $model->key]));

        return $this->render('form', [
            'model' => $model,
            'senders' => $senders,
            'themes' => $themes
        ]);
    }

    public function findModel($id)
    {
        if (($model = Template::find()->where(['b.id'=>$id])->one()) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
