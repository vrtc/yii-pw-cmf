<?php

namespace pw\mailer\backend\controllers;

use Yii;
use pw\mailer\models\Message;
use pw\web\Controller;
use yii\web\NotFoundHttpException;
use pw\mailer\backend\models\MessageSearch;

class MessagesController extends Controller
{


    public function actionIndex()
    {
        $searchModel       = new MessageSearch();
        $dataProvider      = $searchModel->search(Yii::$app->request->queryParams);

        $this->setPageTitle(Yii::t('mailer', 'Очередь соощений'));
        $this->addBreadcrumb(Yii::t('mailer', 'Очередь соощений'));
        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model->delete()) {
            Yii::$app->session->setFlash('success', Yii::t('mailer', 'Письмо удалено'));
        }
        return $this->redirect(['index']);
    }

    public function actionReset($id)
    {
        $model           = $this->findModel($id);
        $model->status   = Message::STATUS_QUEUE;
        $model->attempts = 0;
        if ($model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('mailer', 'Письмо поставлено в очередь на отправку'));
        }

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     */
    public function actionView($id){
        $model = $this->findModel($id);
        echo $model->getData()['setHtmlBody'][key($model->getData()['setHtmlBody'])][0]; exit;
    }

    public function findModel($id)
    {
        if (($model = Message::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

}