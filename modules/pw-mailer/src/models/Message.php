<?php

namespace pw\mailer\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Json;

/**
 * This is the model class for table "{{%pw_mailer_messages}}".
 *
 * @property string $id
 * @property string $template_id
 * @property string $to
 * @property string $data
 * @property string $errorM
 * @property integer $attempts
 * @property integer $priority
 * @property string $created_time
 * @property string $updated_time
 * @property string $send_time

 * @property integer $status
 */
class Message extends \pw\core\db\ActiveRecord
{

    const STATUS_QUEUE = 0;
    const STATUS_SUCCESS = 1;
    const STATUS_ERROR = 2;
    const STATUS_SENDING = 3;

    protected static $statuses;
    private $_data;
    public $subject;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pw_mailer_messages}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data', 'error'], 'string'],
            [['attempts', 'priority'], 'integer'],
            [['created_time', 'updated_time', 'send_time'], 'safe'],
            [['to'], 'email']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('mailer', 'ID'),
            'attempts' => Yii::t('mailer', 'Попыток'),
            'created_time' => Yii::t('mailer', 'Создано'),
            'error' => Yii::t('mailer', 'Ошибка'),
            'send_time' => Yii::t('mailer', 'Время отправки'),
            'priority' => Yii::t('mailer', 'Приоритет'),
            'to' => Yii::t('mailer', 'Email'),
            'status'=>Yii::t('mailer','Статус')
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_time',
                'updatedAtAttribute' => 'updated_time',
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    public function setData($key, $value)
    {
        if(!isset($this->_data[$key])){
            $this->_data[$key] = [];
        }
        $hash  = md5($key.Json::encode($value));
        $this->_data[$key][$hash] = $value;
        //$this->_data[$key] = $value;
    }

    public function getData()
    {
        return $this->_data;
    }

    public function beforeSave($insert)
    {
        if($insert){
            $this->status = self::STATUS_QUEUE;
        }
        $this->data = Json::encode($this->_data);
        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->_data = Json::decode($this->data);
        parent::afterFind();
    }

    public function toMessage()
    {
        return (new \pw\mailer\components\Message())->initFromModel($this);
    }

    public function getPriority()
    {
        return self::getPriorities()[$this->priority];
    }

    public static function getPriorities()
    {
        return [
            \pw\mailer\components\Message::LOW_PRIORITY => 'Низкий',
            \pw\mailer\components\Message::NORMAL_PRIORITY => 'Нормальный',
            \pw\mailer\components\Message::HIGH_PRIORITY => 'Высокий'
        ];
    }

    /**
     * @param $interval specification to delay of sending the mail message, see http://php.net/manual/en/dateinterval.construct.php
     */
    public function applyDelay($interval)
    {
        if ($interval) {
            $this->send_time = (new \DateTime)->add(new \DateInterval($interval))->format('Y-m-d H:i:s');
        }
    }

    public function getStatus(){
        return self::getStatuses()[(int)$this->status];
    }

    public static function getStatuses(){
        if(!self::$statuses){
            self::$statuses = [
                self::STATUS_QUEUE =>Yii::t('mailer','Ожидает оптравки'),
                self::STATUS_ERROR => Yii::t('mailer','Ошибка'),
                self::STATUS_SUCCESS => Yii::t('mailer','Отправлено'),
                self::STATUS_SENDING => Yii::t('mailer','Отправляется')
            ];
        }
        return self::$statuses;
    }

}
