<?php

namespace pw\mailer\models;

use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "{{%mailer_senders}}".
 *
 * @property integer $id
 * @property string $email
 * @property integer $method
 * @property string $settings
 * @property integer $status
 *
 */
class Senders extends \pw\i18n\db\ActiveRecord
{

    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 2;

    const METHOD_MAIL = 1;
    const METHOD_SENDMAIL = 2;
    const METHOD_SMTP = 3;

    public $smtp;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pw_mailer_senders}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['method'], 'integer'],
            [['settings'], 'string'],
            [['is_active'],'boolean'],
            [['email'], 'unique', 'targetClass' => Senders::class, 'filter' => ['<>', 'email', $this->email]],
            [['email'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('mailer', 'ID'),
            'email' => Yii::t('mailer', 'Email сайта'),
            'name' => Yii::t('mailer', 'Имя'),
            'method' => Yii::t('mailer', 'Способ отправки'),
            'settings' => Yii::t('mailer', 'Settings'),
            'is_active' => Yii::t('mailer', 'Активен'),
        ];
    }

    public static function getMethods()
    {
        return [
            self::METHOD_MAIL => Yii::t('mailer', 'PHP Mail'),
            self::METHOD_SENDMAIL => Yii::t('mailer', 'Sendmail'),
            self::METHOD_SMTP => Yii::t('mailer', 'SMTP'),
        ];
    }

    public function getMethod()
    {
        return self::getMethods()[$this->method];
    }

    public function afterFind()
    {
        parent::afterFind();
        if ((int)$this->method === self::METHOD_SMTP) {
            $this->smtp = new SmtpConfig();
            $this->smtp->setAttributes(Json::decode($this->settings), false);
        }
    }

    public function beforeValidate()
    {
        if(parent::beforeValidate()){
            if ((int)$this->method === self::METHOD_SMTP) {
                if (!$this->smtp->validate()) {
                    return false;
                }
                $this->settings = Json::encode($this->smtp);
            }
        }
        return true;
    }
}
