<?php
namespace pw\mailer\models;

use pw\core\Model;
use Yii;

use pw\modules\SettingsModel;
use yii\helpers\Json;

class SmtpConfig extends Model
{

    public $host;
    public $port;
    public $encryption;
    public $authentication = false;
    public $username;
    public $password;

    const ENCRYPTION_NONE = 0;
    const ENCRYPTION_SSL = 1;
    const ENCRYPTION_TLS = 2;

    public function rules()
    {
        return [
            //[['host', 'port'], 'required'],
            [['port'], 'integer'],
            [['host', 'username', 'password'], 'string'],
            ['authentication', 'boolean'],
            ['encryption', 'in', 'range' => [self::ENCRYPTION_NONE, self::ENCRYPTION_SSL, self::ENCRYPTION_TLS]]
        ];
    }

    public function attributeLabels()
    {
        return [
            'host' => Yii::t('mailer', 'Сервер'),
            'port' => Yii::t('mailer', 'Порт'),
            'encryption' => Yii::t('mailer', 'Шифрование'),
            'authentication' => Yii::t('mailer', 'Авторизация на SMTP сервере'),
            'username' => Yii::t('mailer', 'Имя пользователя'),
            'password' => Yii::t('mailer', 'Пароль'),
        ];
    }

    public static function getEncryptions()
    {
        return [
            self::ENCRYPTION_NONE => Yii::t('mailer', 'Нет'),
            self::ENCRYPTION_SSL => Yii::t('mailer', 'SSL'),
            self::ENCRYPTION_TLS => Yii::t('mailer', 'TLS')
        ];
    }

    public function getEncryption()
    {
        if ($this->encryption !== self::ENCRYPTION_NONE) {
            if ($this->encryption === self::ENCRYPTION_SSL) {
                return 'ssl';
            }
            return 'tls';
        }
        return null;
    }

}
