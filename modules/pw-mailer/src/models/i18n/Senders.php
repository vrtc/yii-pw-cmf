<?php

namespace pw\mailer\models\i18n;

use Yii;

/**
 * This is the model class for table "{{%mailer_senders_i18n}}".
 *
 * @property integer $id
 * @property integer $model_id
 * @property string $language
 * @property string $name
 *
 */
class Senders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pw_mailer_senders_i18n}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language'], 'string', 'max' => 16],
            [['name'], 'string', 'max' => 255],
            [['model_id'], 'exist', 'skipOnError' => true, 'targetClass' => \pw\mailer\models\Senders::class, 'targetAttribute' => ['model_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('mailer', 'ID'),
            'sender_id' => Yii::t('mailer', 'Id Sender'),
            'language' => Yii::t('mailer', 'Language'),
            'name' => Yii::t('mailer', 'Отправитель письма'),
        ];
    }
}
