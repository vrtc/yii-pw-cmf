<?php

namespace pw\mailer\models\i18n;

use Yii;

/**
 * This is the model class for table "{{%pw_mailer_templates_i18n}}".
 *
 * @property integer $id
 * @property integer $model_id
 * @property string $language
 * @property string $subject
 * @property string $html_body
 * @property string $text_body
 * @property string $help
 *
 */
class Template extends \pw\core\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pw_mailer_templates_i18n';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['html_body', 'text_body', 'help'], 'string'],
            [['language'], 'string', 'max' => 16],
            [['subject'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('mailer', 'ID'),
            'model_id' => Yii::t('mailer', 'Id Letter'),
            'language' => Yii::t('mailer', 'Language'),
            'subject' => Yii::t('mailer', 'Тема'),
            'html_body' => Yii::t('mailer', 'Текст (HTML)'),
            'text_body' => Yii::t('mailer', 'Текст (TXT'),
            'help' => Yii::t('mailer', 'Помощь'),
        ];
    }
}
