<?php

namespace pw\mailer\models;

use Yii;
use pw\i18n\db\ActiveRecord;

/**
 * This is the model class for table "{{%mailer_templates}}".
 *
 * @property integer $id
 * @property string $key
 * @property string $theme
 * @properrt integer $sender_id
 * @property string $placeholders
 * @property integer $is_active
 *
 */
class Template extends ActiveRecord
{
    const ACTIVE = 1;
    const ACTIVE_NON = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pw_mailer_templates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_active'], 'boolean'],
            [['key', 'theme'], 'string', 'max' => 64],
            [['sender_id'],'safe'],
            [['placeholders'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('mailer', 'ID'),
            'sender_id' => Yii::t('mailer', 'Отправитель'),
            'theme' => Yii::t('mailer', 'Шаблон'),
            'key' => Yii::t('mailer', 'Key'),
            'subject' => Yii::t('mailer', 'Тема'),
            'placeholders' => Yii::t('mailer', 'Placeholders'),
            'is_active' => Yii::t('mailer', 'Активен'),
        ];
    }

    public function getPlaceholders()
    {
        return explode(PHP_EOL, $this->placeholders);
    }

    public function setPlaceholders($placeholders)
    {
        $this->placeholders = implode(PHP_EOL, $placeholders);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSender()
    {
        return $this->hasOne(Senders::class, ['id' => 'sender_id']);
    }
}
