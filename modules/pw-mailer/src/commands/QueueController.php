<?php

namespace pw\mailer\commands;

use Yii;
use yii\console\Controller;

class QueueController extends Controller
{

    /**
     * send all emails from queue
     * @return bool
     */
    public function actionSend()
    {
        $sent = Yii::$app->mailer->sending();
        echo sprintf('sent %s emails', $sent);
    }

    /**
     * remove older messages
     * @return bool
     */
    public function actionOlder()
    {
        $remove = Yii::$app->mailer->cleanMessages();
        echo sprintf('remove %s messages', $remove);
    }

}
