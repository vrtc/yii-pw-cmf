<?php

namespace pw\mailer\components;

use Yii;
use yii\db\Expression;
use yii\helpers\StringHelper;
use yii\mail\MailerInterface;
use yii\mail\MessageInterface;

class Message extends \yii\swiftmailer\Message implements MessageInterface
{
    const LOWEST_PRIORITY = 5;
    const LOW_PRIORITY = 4;
    const NORMAL_PRIORITY = 3;
    const HIGH_PRIORITY = 2;
    const HIGHEST_PRIORITY = 1;

    private $_model;
    private $_swiftMessage;
    protected $priority = Message::NORMAL_PRIORITY;

    /**
     * @return \pw\mailer\models\Message
     */
    public function getModel()
    {
        if ($this->_model === null) {
            $this->_model = new \pw\mailer\models\Message();
            $this->_model->send_time = new Expression('NOW()');
            $this->_model->attempts = 0;
        }
        return $this->_model;
    }

    /**
     *
     * @param \pw\mailer\models\Message $model
     * @return $this
     */
    public function initFromModel(\pw\mailer\models\Message $model)
    {
        $this->_model = $model;
        return $this;
    }


    public function getId()
    {
        return $this->getModel()->id;
    }

    /**
     * @param int $priority
     */
    public function setPriority($priority)
    {
        $this->getModel()->priority = $priority;
        if($this->_swiftMessage){
            parent::setPriority($priority);
        }
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function delay($interval)
    {
        $this->getModel()->applyDelay($interval);
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function send(MailerInterface $mailer = null)
    {
        return $this->getModel()->save();
    }

    /**
     * @inheritdoc
     */
    public function setCharset($charset)
    {
        $this->getModel()->setData('setCharset', [$charset]);
        if($this->_swiftMessage){
            parent::setCharset($charset);
        }
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setFrom($from)
    {
        $this->getModel()->setData('setFrom', [$from]);
        if($this->_swiftMessage){
            parent::setFrom($from);
        }
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setReplyTo($replyTo)
    {
        $this->getModel()->setData('setReplyTo', [$replyTo]);
        if($this->_swiftMessage){
            parent::setReplyTo($replyTo);
        }
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setTo($to)
    {
        $model = $this->getModel();
        $model->setData('setTo', [$to]);
        $model->to = $to;
        if($this->_swiftMessage){
            parent::setTo($to);
        }
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setCc($cc)
    {
        $this->getModel()->setData('setCc', [$cc]);
        if($this->_swiftMessage){
            parent::setCc($cc);
        }
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setBcc($bcc)
    {
        $this->getModel()->setData('setBcc', [$bcc]);
        if($this->_swiftMessage){
            parent::setBcc($bcc);
        }
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setSubject($subject)
    {
        $this->getModel()->setData('setSubject', [$subject]);
        $this->getModel()->subject = $subject;
        if($this->_swiftMessage){
            parent::setSubject($subject);
        }
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setTextBody($text)
    {
        $this->getModel()->setData('setTextBody', [$text]);
        if($this->_swiftMessage){
            parent::setTextBody($text);
        }
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setHtmlBody($html)
    {
        $this->getModel()->setData('setHtmlBody', [$html]);
        if($this->_swiftMessage){
            parent::setHtmlBody($html);
        }
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function attach($fileName, array $options = [])
    {
        $this->getModel()->setData('attach', [$fileName, $options]);
        if($this->_swiftMessage){
            parent::attach($fileName,$options);
        }
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function attachContent($content, array $options = [])
    {
        $this->getModel()->setData('attachContent', [$content, $options]);
        if($this->_swiftMessage){
            parent::attachContent($content,$options);
        }
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function addHeader($name, $value)
    {
        $this->getModel()->setData('addHeader', [$name, $value]);
        if($this->_swiftMessage){
            parent::addHeader($name,$value);
        }
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setHeader($name, $value)
    {
        $this->getModel()->setData('setHeader', [$name, $value]);
        if($this->_swiftMessage){
            parent::setHeader($name,$value);
        }
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setReturnPath($address)
    {
        $this->getModel()->setData('setReturnPath', [$address]);
        if($this->_swiftMessage){
            parent::setReturnPath($address);
        }
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setReadReceiptTo($addresses)
    {
        $this->getModel()->setData('setReadReceiptTo', [$addresses]);
        if($this->_swiftMessage){
            parent::setReadReceiptTo($addresses);
        }
        return $this;
    }


    public function preview()
    {
        $parser = new \PhpMimeMailParser\Parser();
        $parser->setText($this->toString());
        return $parser->getMessageBody('htmlEmbedded');
    }

    public function getSwiftMessage()
    {
        if ($this->_swiftMessage === null) {
            $this->_swiftMessage = parent::getSwiftMessage();
            parent::setPriority($this->getModel()->priority);
            parent::setSubject($this->getModel()->subject);
            $data = $this->getModel()->getData();
            foreach ($data as $method => $values) {
                foreach($values as $params) {
                    call_user_func_array([$this, $method], $params);
                }
            }
        }
        return $this->_swiftMessage;
    }


    public function embed($fileName, array $options = [])
    {
        $embedFile = \Swift_EmbeddedFile::fromPath($fileName);
        if (!empty($options['fileName'])) {
            $embedFile->setFilename($options['fileName']);
        }
        if (!empty($options['contentType'])) {
            $embedFile->setContentType($options['contentType']);
        }
        if (!empty($options['id'])) {
            $embedFile->setId($options['id']);
            $cid = $options['id'];
        }
        else {
            $cid = $embedFile->getId();
        }
        $options['id'] = $cid;
        $this->getModel()->setData('embed', [$fileName, $options]);
        if($this->_swiftMessage){
            $this->getSwiftMessage()->embed($embedFile);
        }
        return 'cid:'.$cid;
    }
}
