<?php

namespace pw\mailer\components;

use pw\mailer\models\Senders;
use Yii;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use pw\mailer\models\Template;
use TijsVerkoyen\CssToInlineStyles\CssToInlineStyles;
use pw\mailer\models\Message;
use yii\db\Expression;
use pw\mailer\models\SmtpConfig;


class Mailer extends \yii\swiftmailer\Mailer
{
    const EVENT_FAILED = 'failed';

    private $_transports = [];
    private $_letters = [];
    private $_cssInliner;
    private $_css;
    private $_messages = [];

    public $messageConfig = [
        'class' => \pw\mailer\components\Message::class
    ];

    private function findTemplate($key)
    {
        if (!isset($this->_letters[$key])) {
            $letter = Template::findOne(['key' => $key]);
            if (!$letter) {
                throw new NotFoundHttpException($key);
            }
            $this->_letters[$key] = $letter;
        }
        return $this->_letters[$key];
    }

    public function compose($template = null, array $params = [], $language = null)
    {
        $template = $this->findTemplate($template);
        $subject = strtr($template->translate($language)->subject, $params);
        $message = $this->createMessage();
        $message->setFrom([$template->sender->email => $template->sender->name]);
        $message->setPriority(\pw\mailer\components\Message::HIGH_PRIORITY);
        $message->setSubject($subject);
        $html = strtr($template->translate($language)->html_body, $params);
        $text = strtr($template->translate($language)->text_body, $params);
        if (!empty($html)) {
            $tpl = $template->theme ? $template->theme : 'default';
            Yii::setAlias('@theme', getenv('ABSOLUTE_PATH'));
            $html = $this->render('@theme/src/mail/' . $tpl, [
                'title' => $subject,
                'content' => $html,
                'language' => $language,
                'message' => $message,
                'webLink' => Url::to(['/mailer/default/view', 'key' => $message->getId(), 'language' => $language], true)
            ], false);
            $html = $this->inlineCss($html, $tpl);
            $message->setHtmlBody($html);
        }
        if (!empty($text)) {
            $message->setTextBody($text);
        } elseif (isset($html)) {
            if (preg_match('~<body[^>]*>(.*?)</body>~is', $html, $match)) {
                $html = $match[1];
            }
            // remove style and script
            $html = preg_replace('~<((style|script))[^>]*>(.*?)</\1>~is', '', $html);
            // strip all HTML tags and decoded HTML entities
            $text = html_entity_decode(strip_tags($html), ENT_QUOTES | ENT_HTML5, Yii::$app ? Yii::$app->charset : 'UTF-8');
            // improve whitespace
            $text = preg_replace("~^[ \t]+~m", '', trim($text));
            $text = preg_replace('~\R\R+~mu', "\n\n", $text);
            $message->setTextBody($text);
        }
        return $message;
    }


    public function sending()
    {
        $settings = Yii::$app->settings['pw-mailer']['settings'];
        $i = 0;
        $messages = Message::find()
            ->where(['and',
                ['status' => [Message::STATUS_QUEUE, Message::STATUS_ERROR]],
                ['<=', 'attempts', $settings['maxAttempts']],
                ['<=', 'send_time', new Expression('now()')]
            ])
            ->orderBy(['priority' => SORT_ASC, 'id' => SORT_DESC])->limit($settings['mailsAtTime'])
            ->all();

        $emails = ArrayHelper::map($messages, 'id', 'to');
        Message::updateAll(['status' => Message::STATUS_SENDING, 'updated_time' => new Expression('NOW()')], ['id' => array_keys($emails)]);

        if (!empty($messages)) {
            foreach ($messages as $model) {
                $message = $model->toMessage();

                try {
                    $model->attempts++;
                    $message = $model->toMessage();
                    $transport = $this->getTransportForMessage($message->getFrom());
                    if ($transport->send($message->getSwiftMessage(), $failedRecipients)) {
                        $i++;
                        $model->status = Message::STATUS_SUCCESS;
                        $model->save();
                    } else {
                        throw new \Swift_SwiftException('The email was not sent.');
                    }
                } catch (\Exception $ex) {
                    $model->status = Message::STATUS_ERROR;
                    $model->error = $ex->getMessage();
                    $model->applyDelay($this->getAttemptInterval($model->attempts));
                    $model->save();

                   // $event = new FailedEvent(['email' => $model->to, 'error' => $ex->getMessage()]);
                 //   $this->trigger(self::EVENT_FAILED, $event);
                }
            }
        }
        return $i;
    }

    public function cleanMessages($day = 30)
    {
        return Message::deleteAll('send_time <= DATE_SUB(NOW(), INTERVAL ' . (int)$day . ' DAY);');
    }

    /**
     * @param $email
     * @return \Swift_Transport
     */
    protected function getTransportForMessage($email)
    {

        if (is_array($email)) {
            $email = key($email);
        }
        if (!isset($this->_transports[$email])) {
            $sender = Senders::findOne(['email' => $email]);
            switch ($sender->method) {
                case Senders::METHOD_SMTP:
                    $transport = new \Swift_SmtpTransport();
                    $transport->setHost($sender->smtp->host);
                    $transport->setPort($sender->smtp->port);
                    if ($sender->smtp->encryption != SmtpConfig::ENCRYPTION_NONE) {
                        $transport->setEncryption($sender->smtp->getEncryption());
                    }
                    if ($sender->smtp->authentication) {
                        $transport->setUsername($sender->smtp->username);
                        $transport->setPassword($sender->smtp->password);
                    }
                    break;
                case Senders::METHOD_SENDMAIL:
                    $transport = new \Swift_SendmailTransport();
                    break;
                default:
                    $transport = new \Swift_MailTransport();
                    break;
            }
            $this->_transports[$email] = $transport;
        }
        if (!$this->_transports[$email]->isStarted()) {
            $this->_transports[$email]->start();
        }
        return $this->_transports[$email];
    }

    /**
     * Get time interval of an attempt
     *
     * @param $attempt number of the attempt
     * @return interval specifications, see http://php.net/manual/en/dateinterval.construct.php
     */
    public function getAttemptInterval($attempt)
    {
        $settings = Yii::$app->getModule('pw-mailer');

        if (!$settings->attemptIntervals) {
            return 0;
        }
        $index = $attempt - 1;
        if ($index <= 0) {
            $index = 0;
        } elseif ($index >= count($settings->attemptIntervals)) {
            $index = count($settings->attemptIntervals) - 1;
        }
        return $settings->attemptIntervals[$index];
    }

    /**
     * @param $html
     * @return string
     */
    protected function inlineCss($html, $template)
    {
        if (file_exists(Yii::getAlias('@theme/src/mail/css/' . $template . '_style.css'))) {
            $cssPath = '@theme/src/mail/css/' . $template . '_style.css';
        } else {
            $cssPath = '@theme/src/mail/css/html_style.css';
        }

        if ($this->_cssInliner === null) {
            if (!file_exists(Yii::getAlias($cssPath))) {
                $this->_cssInliner = false;
                return $html;
            }
            $this->_cssInliner = new CssToInlineStyles();
            $this->_css = file_get_contents(Yii::getAlias($cssPath));
        }
        if ($this->_cssInliner !== false) {
            $html = $this->_cssInliner->convert($html, $this->_css);
        }
        return $html;
    }
}
