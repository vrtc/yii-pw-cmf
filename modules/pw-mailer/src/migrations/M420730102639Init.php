<?php
namespace pw\mailer\migrations;
use pw\i18n\db\Migration;
use pw\mailer\models\Template;

class M420730102639Init extends Migration
{
    public function safeUp()
    {

        $model = ($model = Template::findOne(['key' => 'system.optovikam'])) ? $model : new Template();
        $model->sender_id = 2;
        $model->subject = 'Заявка на оптовый прайс-лис';
        $model->key = 'system.optovikam';
        $model->theme = 'default';
        $model->placeholders = str_replace('}{', '}'. PHP_EOL . '{', '{user.email}{user.name}{user.phone}{user.site}{user.organization}');
        $model->html_body = '<p>E-mail пользователя: {user.email}</p> <p>Имя пользователя: {user.name}</p> <p>Телефон: {user.phone}</p><p>Сайт: {user.site}</p><p>Организация: {user.organization}</p>';
        $model->save(false);

        $model = ($model = Template::findOne(['key' => 'system.vozvrat'])) ? $model : new Template();
        $model->sender_id = 2;
        $model->subject = 'Форма возврата';
        $model->key = 'system.vozvrat';
        $model->theme = 'default';
        $model->placeholders = str_replace('}{', '}'. PHP_EOL . '{', '{user.email}{user.name}{message}');
        $model->html_body = '<p>E-mail пользователя: {user.email}</p> <p>Имя пользователя: {user.name}</p> <p>Сообщение: {message}</p>';
        $model->save(false);


    }
}
