<?php

namespace pw\mailer\migrations;

use pw\core\db\Migration;
use Yii;

class M190722064350Create_cron extends Migration
{
    public function up()
    {
        Yii::$app->cron->add('pw-mailer/queue/send',
            [
                'name' => 'Mail send',
                'description' => '',
                'expression' => '*/1 * * * *', //@daily, @monthly
            ]);

        Yii::$app->cron->add('pw-mailer/queue/older',
            [
                'name' => 'Mail remove older',
                'description' => '',
                'expression' => '@daily', //@daily, @monthly
            ]);
    }

    public function down()
    {
        echo "M190722064350Create_cron cannot be reverted.\n";

        return false;
    }

}
