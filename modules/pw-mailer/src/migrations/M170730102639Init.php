<?php
namespace pw\mailer\migrations;
use pw\i18n\db\Migration;

class M170730102639Init extends Migration
{
    public function up()
    {

        $this->createTable('{{%pw_mailer_senders}}', [
            'id'        => $this->bigPrimaryKey()->unsigned(),
            'email'     => $this->string()->notNull(),
            'method'    => $this->smallInteger()->notNull(),
            'name'      => $this->string()->i18n(),
            'settings'  => $this->text(),
            'is_active' => $this->boolean()
        ]);

        $this->createTable('{{%pw_mailer_templates}}', [
            'id'            => $this->bigPrimaryKey()->unsigned(),
            'sender_id'     => $this->bigInteger()->unsigned(),
            'key'           => $this->string(64),
            'theme'         => $this->string(),
            'placeholders'  => $this->string(),
            'subject'       => $this->string()->i18n(),
            'html_body'     => $this->longText()->i18n(),
            'text_body'     => $this->longText()->i18n(),
            'help'          => $this->text()->i18n(),
            'is_active'        => $this->smallInteger()
        ]);

        $this->createIndex('idx_key', '{{%pw_mailer_templates}}', 'key');

        $this->createTable('{{%pw_mailer_messages}}', [
            'id'           => $this->bigPrimaryKey()->unsigned(),
            'template_id'  => $this->bigInteger()->unsigned(),
            'to'           => $this->string()->notNull(),
            'data'         => $this->longText()->notNull(),
            'attempts'     => $this->smallInteger()->notNull(),
            'error'        => $this->string(),
            'priority'     => $this->integer(),
            'created_time' => $this->dateTime(),
            'updated_time' => $this->dateTime(),
            'send_time'    => $this->dateTime(),
            'status'       => $this->smallInteger()
        ]);

        $this->createIndex('idx_template', '{{%pw_mailer_messages}}', 'template_id');
        $this->createIndex('idx_send_time', '{{%pw_mailer_messages}}', ['attempts', 'send_time']);
    }

    public function down()
    {
        echo "M170730102639Init cannot be reverted.\n";

        return false;
    }

}
