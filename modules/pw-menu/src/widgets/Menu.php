<?php

namespace pw\menu\widgets;

use pw\core\traits\Settings;
use pw\ui\icons\Icons;
use Yii;
use yii\base\InvalidConfigException;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\Dropdown;
use pw\menu\models\Menu as MenuModel;
use yii\helpers\Url;


class Menu extends \yii\widgets\Menu
{
    use Settings;

    public $key;

    /**
     * @inherit
     */
    public $activateParents = true;


    public $itemOptions = [];

    /**
     * @var string the CSS class to be appended to the active menu item.
     */
    public $activeCssClass = 'active';


    public function init()
    {
        parent::init();
        if (!$this->key && empty($this->items)) {
            throw new InvalidConfigException('The $key or $items property must be set');
        }
        if ($this->key) {
            $this->items = MenuModel::findMenu($this->key);
        }
    }

    /**
     * @inheritdoc
     */
    protected function renderItems($items)
    {
        $n     = count($items);
        $lines = [];
        foreach ($items as $i => $item) {
            $options = array_merge($this->itemOptions, ArrayHelper::getValue($item, 'options', []));
            $tag     = ArrayHelper::remove($options, 'tag', 'li');
            $class   = [];
            if ($item['active']) {
                $this->view->params['breadcrumbs'][] = ['label' => $item['label'], 'url' => $item['url']];
                $class[]                             = $this->activeCssClass;
            }
            if ($i === 0 && $this->firstItemCssClass !== null) {
                $class[] = $this->firstItemCssClass;
            }
            if ($i === $n - 1 && $this->lastItemCssClass !== null) {
                $class[] = $this->lastItemCssClass;
            }
            if (!empty($class)) {
                if (empty($options['class'])) {
                    $options['class'] = implode(' ', $class);
                } else {
                    $options['class'] .= ' ' . implode(' ', $class);
                }
            }

            $menu = $this->renderItem($item);
            if (!empty($item['items'])) {
                $submenuTemplate = ArrayHelper::getValue($item, 'submenuTemplate', $this->submenuTemplate);
                $menu            .= strtr($submenuTemplate, [
                    '{items}' => $this->renderItems($item['items']),
                ]);
            }
            $lines[] = Html::tag($tag, $menu, $options);
        }

        return implode("\n", $lines);
    }

    /**
     * @inheritdoc
     */
    protected function renderItem($item)
    {
        if(!isset($item['render']) || $item['render'] !== false) {
            if (isset($item['url'])) {
                $template = ArrayHelper::getValue($item, 'template', $this->linkTemplate);

                return strtr($template, [
                    '{url}' => Url::to($item['url'], true),
                    '{label}' => $item['label'],
                    '{icon}' => isset($item['icon']) ? Icons::show($item['icon'], ['class' => 'nav-icon']) : ''
                ]);
            }
            $template = ArrayHelper::getValue($item, 'template', $this->labelTemplate);

            return strtr($template, [
                '{url}' => '',
                '{label}' => $item['label'],
                '{icon}' => isset($item['icon']) ? Icons::show($item['icon'], ['class' => 'nav-icon']) : ''
            ]);
        }
        return null;
    }

    /**
     * @inheritdoc
     */
    protected function isItemActive($item)
    {
        if (isset($item['url']) && is_array($item['url']) && isset($item['url'][0])) {
            $route = Yii::getAlias($item['url'][0]);
            if ($route[0] !== '/' && Yii::$app->controller) {
                $route = Yii::$app->controller->module->getUniqueId() . '/' . $route;
            }
            $route = \ltrim($route, '/');
            if ($route === $this->route) {
                unset($item['url']['#']);
                if (count($item['url']) > 1) {
                    $params = $item['url'];
                    unset($params[0], $params['toBackend'], $params['toFrontend']);
                    foreach ($params as $name => $value) {
                        if ($value !== null && (!isset($this->params[$name]) || $this->params[$name] !== $value)) {
                            return false;
                        }
                    }
                }

                return true;
            } else {
                $lpos = \strrpos($this->route, '/');
                if (\substr($route, 0, $lpos) === \substr($this->route, 0, $lpos)) {
                    return true;
                }
            }

            return false;
        }

        return false;
    }
}