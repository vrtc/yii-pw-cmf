<?php

namespace pw\menu\widgets;

use pw\ui\icons\Icons;
use Yii;
use yii\base\InvalidConfigException;
use yii\bootstrap\Nav;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\Dropdown;
use pw\menu\models\Menu as MenuModel;
use yii\helpers\Url;

class BootstrapMenu extends Nav
{

    public $key;

    /**
     * @inherit
     */
    public $activateParents = true;

    public $dropDownOptions = [];

    public $itemOptions = [];

    /**
     * @var string the CSS class to be appended to the active menu item.
     */
    public $activeCssClass = 'active';


    public function init()
    {
        parent::init();
        if (!$this->key) {
            throw new InvalidConfigException('The "key" property must be set');
        }
        $menu = MenuModel::findMenu($this->key);
        if ($menu) {
            $stack = [];
            $items = [];
            foreach ($menu as $row) {
                $url = $row->getLink();
                $item = [
                    'label' => $row->name,
                    'url' => $url,
                    'level' => $row->level,
                    'items' => null,
                    'icon' => Icons::show($row->icon),
                    'image' => $row->image
                ];
                if ($row->class) {
                    $item['options'] = ['class' => $row->class];
                }
                $level = count($stack);
                while ($level > 0 && $stack[$level - 1]['level'] >= $row->level) {
                    array_pop($stack);
                    $level--;
                }
                if ($level === 0) {
                    $i = count($items);
                    $items[$i] = $item;
                    $stack[] = &$items[$i];
                } else {
                    $i = count($stack[$level - 1]['items']);
                    $stack[$level - 1]['items'][$i] = $item;
                    $stack[] = &$stack[$level - 1]['items'][$i];
                }
            }
            $this->items = $items;
        }
    }


    /**
     * @inheritdoc
     */
    public function renderItem($item)
    {
        if (is_string($item)) {
            return $item;
        }
        if (!isset($item['label'])) {
            throw new InvalidConfigException("The 'label' option is required.");
        }
        $encodeLabel = isset($item['encode']) ? $item['encode'] : $this->encodeLabels;
        $label = $encodeLabel ? Html::encode($item['label']) : $item['label'];
        $options = ArrayHelper::merge(ArrayHelper::getValue($item, 'options', []), $this->itemOptions);
        $items = ArrayHelper::getValue($item, 'items');
        $url = ArrayHelper::getValue($item, 'url', '#');

        $linkOptions = ArrayHelper::getValue($item, 'linkOptions', []);

        if (isset($item['active'])) {
            $active = ArrayHelper::remove($item, 'active', false);
        } else {
            $active = $this->isItemActive($item);
        }

        if ($items !== null) {
            //$linkOptions['data-toggle'] = 'dropdown';
            Html::addCssClass($options, 'dropdown');
            //Html::addCssClass($linkOptions, 'dropdown-toggle');
            if ($this->dropDownCaret !== '') {
                $label .= ' ' . $this->dropDownCaret;
            }
            if (is_array($items)) {
                if ($this->activateItems) {
                    $items = $this->isChildActive($items, $active);
                }
                $items = $this->renderDropdown($items, $item);
            }
        }
        if (is_array($url)) {
            $url = Url::to($url, true);
        }
        if ($this->activateItems && $active) {
            Html::addCssClass($options, $this->activeCssClass);
        }

        return Html::tag('li', Html::a($label, $url, $linkOptions) . $items, $options);
    }

    /**
     * @inheritdoc
     */
    protected function renderDropdown($items, $parentItem)
    {
        return Dropdown::widget([
            'items' => $items,
            'encodeLabels' => $this->encodeLabels,
            'options' => $this->dropDownOptions,
            'clientOptions' => false,
            'view' => $this->getView(),
        ]);
    }

    /**
     * @inheritdoc
     */
    protected function isChildActive($items, &$active)
    {
        foreach ($items as $i => $child) {
            if (ArrayHelper::remove($items[$i], 'active', false) || $this->isItemActive($child)) {
                Html::addCssClass($items[$i]['options'], $this->activeCssClass);
                if ($this->activateParents) {
                    $active = true;
                }
            }
        }
        return $items;
    }

    /**
     * @inheritdoc
     */
    protected function isItemActive($item)
    {
        if (isset($item['url']) && is_array($item['url']) && isset($item['url'][0])) {
            $route = $item['url'][0];
            if ($route[0] !== '/' && Yii::$app->controller) {
                $route = Yii::$app->controller->module->getUniqueId() . '/' . $route;
            }
            if (ltrim($route, '/') !== $this->route && !strpos($route, Yii::$app->controller->getUniqueId())) {
                return false;
            }
            unset($item['url']['#']);
            if (count($item['url']) > 1) {
                $params = $item['url'];
                unset($params[0]);
                foreach ($params as $name => $value) {
                    if ($value !== null && (!isset($this->params[$name]) || $this->params[$name] != $value)) {
                        return false;
                    }
                }
            }

            return true;
        }

        return false;
    }
}
