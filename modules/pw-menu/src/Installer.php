<?php
namespace pw\menu;

use pw\menu\models\Menu;

class Installer extends \pw\modules\ModuleInstaller
{


    public function getVersion(): string
    {
        return '0.1';
    }

    public function getAuthors(): array
    {
        return ['i@yar.pw'];
    }

    public function getLink(): string
    {
        return 'http://yar.pw';
    }

    public function getName(): string
    {
        return 'Menu';
    }

    public function getSourceLanguage(): string
    {
        return 'ru-RU';
    }

    public function afterMigrationUp()
    {
        if (!Menu::find()->exists()) {
            $frontendMenu = new \pw\menu\models\Menu();
            $frontendMenu->key = 'frontend';
            $frontendMenu->translate('ru-RU')->name = 'Главное меню';
            $frontendMenu->makeRoot()->save(false);
        }
    }

    public function getWidgets(): array
    {
        return [
          \pw\menu\widgets\Menu::class
        ];
    }


}
