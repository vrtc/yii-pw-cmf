<?php
namespace pw\menu\migrations;

use pw\i18n\db\Migration;

class M170730102635Init extends Migration
{
    public function up()
    {
        $this->createAdjacencyListTable('{{%pw_menu}}', [
            'id'      => $this->bigPrimaryKey()->unsigned(),
            'key'     => $this->string()->notNull(),
            'route'      => $this->string(),
            'name'       => $this->string()->i18n(),
            'class'      => $this->string(),
            'icon'      => $this->string(),
            'is_visible' => $this->string(),
            'is_active' =>$this->boolean()
        ]);
    }

    public function down()
    {
        echo "M170730102635Init cannot be reverted.\n";

        return false;
    }

}
