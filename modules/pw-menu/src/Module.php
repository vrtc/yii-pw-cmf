<?php

namespace pw\menu;

use Yii;
use pw\menu\widgets\Menu;
use yii\base\Application;

class Module extends \pw\core\Module
{

    public function models(Application $app): array
    {
        return [
            \pw\menu\models\Menu::class=>Yii::t('menu','Меню')
        ];
    }

    public function getNavigation(): array
    {
        return [
            'content' => [
                [
                    'label' => 'Меню',
                    'url'   => ['/pw-menu/manage/index'],
                    'icon'  => 'bars'
                ]
            ]
        ];
    }

    public function widgets(Application $app): array
    {
        return [
            Menu::class
        ];
    }
}
