<?php
namespace pw\menu\models;

use pw\core\db\AdjacencyListQuery;

class MenuQuery extends AdjacencyListQuery
{

    public function active($state = true)
    {
        $this->andWhere(['is_active' => $state]);
        return $this;
    }


    public function key($key)
    {
        $this->andWhere(['key' => $key]);
        return $this;
    }
}
