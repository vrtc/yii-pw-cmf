<?php

namespace pw\menu\models;

use Yii;
use yii\helpers\Json;
use pw\menu\Module;
use pw\i18n\db\AdjacencyListActiveRecord;

/**
 * This is the model class for table "{{%menu}}".
 *
 * @property integer    $id
 * @property integer    $parent_id
 * @property string     $key
 * @property string     $route
 * @property string     $class
 * @property string     $icon
 * @property string     $is_visible
 * @property integer    $sort
 * @property integer    $is_active
 *
 */
class Menu extends AdjacencyListActiveRecord
{
    public $level;
    public $application;
    public $action;
    public $link;
    public $model_id;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pw_menu}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                'key',
                'required',
                'when' => function ($model) {
                    return $model->parent_id === null;
                }
            ],
//            [
//                'route',
//                'required',
//                'when' => function ($model) {
//                    return !$model->isRoot() || empty($model->link);
//                }
//            ],
            [['is_active'], 'boolean'],
            [['application', 'action'], 'string'],
            [['key', 'route', 'class', 'icon','link'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'        => Module::t('menu', 'ID'),
            'key'       => Module::t('menu', ' Ключ'),
            'parent_id' => Module::t('menu', ' Ключ'),
            'name' => Module::t('menu', ' Название'),
            'link'     => Module::t('menu', 'Ссылка'),
            'route'     => Module::t('menu', 'Внутренняя страница'),
            'class'     => Module::t('menu', 'CSS класс'),
            'icon'      => Module::t('menu', 'Иконка'),
            'sort'      => Module::t('menu', 'Sort'),
            'is_active'    => Module::t('menu', 'Активно'),
        ];
    }


    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new MenuQuery(static::class);
    }

    public static function findMenu($key)
    {
        $menu  = self::find()->roots()->key($key)->active()->one();
        $items = [];
        if($menu) {
            $items = $menu->getTreeItems();
        }
        return $items;
    }

    protected function getTreeItems($parent = null,$level = 1)
    {
        if ($parent === null) {
            $parent = $this;
        }
        $items = [];
        foreach ($parent->children as $child) {
            $url  = $child->getLink();
            $item = [
                'label' => $child->name,
                'url'   => $url,
                'level' => $level,
                'items' => null,
                'icon'  => $child->icon
            ];
            if ($child->class) {
                $item['options'] = ['class' => $child->class];
            }
            if (!$child->isLeaf()) {
                $item['items'] = $this->getTreeItems($child,$level+1);
            }
            $items [] = $item;
        }

        return $items;
    }


    public function beforeValidate()
    {
        $this->route = Json::encode([
            'action'      => $this->action,
            'model_id'     => $this->model_id,
            'link'=>$this->link
        ]);

        return parent::beforeValidate();
    }

    public function afterFind()
    {
        parent::afterFind();
        if ($this->route) {
            $route             = Json::decode($this->route);
            if(!empty($route['link'])){
                $this->link = $route['link'];
            }
            else {
                $this->action = $route['action'];
                if (isset($route['model_id'])) {
                    $this->model_id = $route['model_id'];
                }
            }
        }
    }

    public function getLink()
    {
        if ($this->route) {
            $route = Json::decode($this->route);
            if ($route['action']) {
                if (\is_string($route['action'])) {
                    $params = ['/' . trim($route['action'], '/')];
                } else {
                    $params = $route['action'];
                }
                $params['toFrontend'] = true;
                foreach (Yii::$app->getUrlManager()->rules as $rule) {
                    if ($rule->route === $route['action'] && $rule->model) {
                        $modelClass = $rule->model;
                        $model      = $modelClass::findOne($route['model_id']);
                        if ($model) {
                            $params += $rule->getParamsFromModel($model);
                        }
                    }
                }
                return $params;
            }
            elseif($route['link']) {
                return $route['link'];
            }
        }
        return null;
    }
}
