<?php

namespace pw\menu\models\i18n;

use pw\core\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "{{%Menu_i18n}}".
 *
 * @property integer $id
 * @property integer $model_id
 * @property string $language
 * @property string $name
 * @property string $image
 *
 */
class Menu extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pw_menu_i18n}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language'], 'string', 'max' => 16],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('menu', 'ID'),
            'menu_id' => Yii::t('menu', 'Id Menu'),
            'language' => Yii::t('menu', 'Language'),
            'name' => Yii::t('menu', 'Название'),
        ];
    }
}
