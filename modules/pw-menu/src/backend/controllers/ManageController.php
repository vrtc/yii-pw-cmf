<?php
namespace pw\menu\backend\controllers;

use pw\menu\backend\models\SearchMenu;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use pw\web\Controller;
use pw\menu\models\Menu;

class ManageController extends Controller
{

    public function actionIndex()
    {
        $searchModel = new SearchMenu();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->setPageTitle(Yii::t('menu', 'Меню'));
        $this->addBreadcrumb(Yii::t('menu', 'Меню'));
        return $this->render('index', [
            'searchModel'=>$searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionView($id)
    {
        $menu = $this->findModel($id);

        $this->setPageTitle(Yii::t('menu', 'Меню: {name}', ['name' => $menu->name]));
        $this->addBreadcrumb(Yii::t('menu', 'Меню'), ['index']);
        $this->addBreadcrumb($menu->name);
        return $this->render('view', [
            'menu'=>$menu,
            'models' => $menu->children ? $menu->children: []
        ]);
    }

    public function actionChildren($id)
    {
        if ($id > 0) {
            $parent = $this->findModel($id);
            $nodes = $parent->getChildren(1)->all();
        } else {
            $nodes = Menu::find()->roots()->all();
        }
        $result = [];
        foreach ($nodes as $node) {
            $result[] = [
                'id' => $node->id,
                'name' => $node->name,
                'level' => $node->level,
                'type' => 'default'
            ];
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['nodes' => $result];
    }

    public function actionCreate($id = null, $parent = null, $position = null)
    {
        $model = new Menu();
        if ($parent !== null) {
            $model->parent_id = $parent;
        }
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if ($parent === null ) {
                    if($model->makeRoot()->save()){
                        Yii::$app->session->setFlash('success', Yii::t('menu', 'Меню создано'));
                        return $this->redirect(['index']);
                    }
                } else {
                    $related = $this->findModel($parent);
                    if($model->appendTo($related)->save()){
                        Yii::$app->session->setFlash('success', Yii::t('menu', 'Пункт добавлен'));
                        return $this->redirect(['view','id'=>$parent]);
                    }
                }
            }
        }
        $this->addBreadcrumb(Yii::t('menu', 'Меню'), ['index']);
        if ($model->isRoot()) {
            $this->setPageTitle(Yii::t('menu', 'Добавить меню'));
            $this->addBreadcrumb(Yii::t('menu', 'Добавить меню'));
        } else {
            $this->setPageTitle(Yii::t('menu', 'Добавить новый пункт'));
            $this->addBreadcrumb($model->parent->name, ['view', 'id' => $model->parent->id]);
            $this->addBreadcrumb(Yii::t('menu', 'Добавить новый пункт'));
        }
        $this->addBreadcrumb($model->name);

        $routes = [];
        foreach (Yii::$app->getUrlManager()->getRules() as $rule) {
            if(\property_exists($rule,'internal') && $rule->internal === false) {
                $routes[$rule->route] = $rule->name;
            }
        }
        return $this->render('form', [
            'model' => $model,
            'routes'=>$routes
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('menu', 'Изменения сохранены'));
                if($model->isRoot()) {
                    return $this->redirect(['index']);
                }
                return $this->redirect(['view','id'=>$model->parent_id]);
        }
        $this->addBreadcrumb(Yii::t('menu', 'Меню'), ['index']);
        $this->setPageTitle(Yii::t('menu', 'Редактирование меню'));

        if(!$model->isRoot()){
            foreach($model->parents as $parent) {
                $this->addBreadcrumb($parent->name, ['view', 'id' => $parent->id]);
            }
        }

        $this->addBreadcrumb(Yii::t('menu', 'Редактирование меню'));


        $routes = [];
        foreach (Yii::$app->getUrlManager()->getRules() as $rule) {
            $routes[] = [
                'id' => $rule->route, 'name' => $rule->name,
                'model' => isset($rule->model)
            ];
        }
        return $this->render('form', [
            'model' => $model,
            'routes'=>$routes
        ]);
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model->delete()) {
            Yii::$app->session->setFlash('success', Yii::t('menu', 'Изменения сохранены'));
            return $this->redirect(['index']);
        }
        return $this->redirect(['index']);
    }

    public function actionNodeMove($id = 0, $lft = 0, $rgt = 0, $par = 0)
    {

        Yii::$app->response->format = Response::FORMAT_JSON;

        /* Locate the supplied model, left, right and parent models */
        $model = Menu::find()->where(['id' => $id])->one();
        $lft = Menu::find()->where(['id' => $lft])->one();
        $rgt = Menu::find()->where(['id' => $rgt])->one();
        $par = Menu::find()->where(['id' => $par])->one();

        if ($lft) {
            $model->insertAfter($lft);
        } elseif ($rgt) {
            $model->insertBefore($rgt);
        } elseif ($par) {
            $model->appendTo($par);
        }

        $res = $model->save();
        /* report new position */
        return ['updated' => [
            'res' => $res,
            'id' => $model->id,
            'depth' => $model->level,
            'lft' => $model->lft,
            'rgt' => $model->rgt,
        ]];
    }

    protected function findModel($id)
    {
        if (($model = Menu::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionRoutes()
    {
        $out = [];
        $parents = Yii::$app->request->post('depdrop_parents', false);
        $selected = Yii::$app->request->post('depdrop_all_params', false);
        $selected = isset($selected['input-action']) ? $selected['input-action'] : '';
        if ($parents !== false) {
            $app = $parents[0];
            if ($app === 'frontend') {
                /** @var  $urlManager FrontendUrlManager */
                $urlManager = Yii::$app->getFrontendUrlManager();
            } elseif ($app === 'backend') {
                /** @var  $urlManager BackendUrlManager */
                $urlManager = Yii::$app->getBackendUrlManager();
            }
            $out = [];
            foreach ($urlManager->getRules() as $rule) {
                $out[] = [
                    'id' => $rule['route'], 'name' => $rule['name'],
                    'model' => isset($rule['model'])
                ];
            }
            return $this->asJson(['output' => $out, 'selected' => $selected]);
        }
        return $this->asJson(['output' => '', 'selected' => '']);
    }

    public function actionModels()
    {
        $out = [];
        $parents = Yii::$app->request->post('depdrop_parents', false);
        $selected = Yii::$app->request->post('depdrop_all_params', false);
        $selected = isset($selected['input-idModel']) ? $selected['input-idModel'] : '';
        if ($parents !== false && isset($parents[1])) {
            $app = $parents[0];
            $route = $parents[1];
            if ($app === 'frontend') {
                /** @var  $urlManager FrontendUrlManager */
                $urlManager = Yii::$app->getFrontendUrlManager();
            } elseif ($app === 'backend') {
                /** @var  $urlManager BackendUrlManager */
                $urlManager = Yii::$app->getBackendUrlManager();
            }
            $out = [];
            foreach ($urlManager->getRules() as $rule) {
                if ($rule['route'] == $route && isset($rule['model'])) {
                    $models = $rule['model']::find();
                    foreach ($models->each(30) as $model) {
                        $out[] = [
                            'id' => $model->id, 'name' => $model->getName()
                        ];
                    }
                }
            }

            return $this->asJson(['output' => $out, 'selected' => $selected]);
        }
        return $this->asJson(['output' => '', 'selected' => '']);
    }
}
