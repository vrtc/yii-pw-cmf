<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */


use pw\ui\grid\GridView;
use pw\ui\icons\Icons;

?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'model' => $searchModel,
    'title' => Yii::t('menu', 'Меню'),
    'icon' => Icons::show('bars'),
    'actions' => [
        [
            'url' => ['create'],
            'label' => Yii::t('reference', 'Добавить меню'),
            'icon' => 'plus'
        ]
    ],
    'columns' => [
        'name',
        'key',
        [
            'class' => \pw\ui\grid\ActionColumn::class,
        ],
    ],
]);

