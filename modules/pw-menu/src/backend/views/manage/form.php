<?php
/* @var $this yii\web\View */
/* @var $model pw\menu\models\Menu */
/* @var $form yii\widgets\ActiveForm */
use yii\helpers\Html;
use yii\helpers\Url;
use pw\ui\form\ActiveForm;
use kartik\depdrop\DepDrop;

?>
<?php $form = ActiveForm::begin([
    'model' => $model,
    'title' =>  $model->getIsNewRecord() ? 'Новый пункт меню' : 'Редактирование'
]); ?>
<?= $form->field($model, 'name')->textInput() ?>
<?php if ($model->isRoot()): ?>
    <?= $form->field($model, 'key')->textInput(['maxlength' => 255]) ?>
<?php else: ?>
    <?= $form->field($model, 'link')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'route')->dropDownList([$routes], ['id' => 'route-id', 'prompt' => 'Выбрать']); ?>

    <?= Html::hiddenInput('input-action', $model->action, ['id' => 'input-action']);?>
    <?= Html::hiddenInput('input-model_id', $model->model_id, ['id' => 'input-model_id']); ?>

    <?= $form->field($model, 'model_id')->widget(DepDrop::class, [
        'options' => ['id' => 'model-id'],
        'type' => 2,
        'pluginOptions' => [
            'depends' => ['route-id'],
            'placeholder' => 'Выбрать',
            'url' => Url::to(['models']),
            'params' => ['input-model_id']
        ]
    ])->label(false); ?>
<?php endif; ?>
<?= $form->field($model, 'class')->textInput() ?>
<?= $form->field($model, 'is_active')->checkbox([
    'class' => 'make-switch',
    'data-on-text' => '<i class="fa fa-check"></i>',
    'data-off-text' => '<i class="fa fa-times"></i>',
    'data-on-color' => 'success',
]) ?>
<?php ActiveForm::end(); ?>
