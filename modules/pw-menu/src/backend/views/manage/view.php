<?php
use pw\ui\nestable\NestableView;

?>
<?= NestableView::widget([
    'title' => $menu->name,
    'icon' => 'bar',
    'models' => $models,
    'nodeMoveUrl' => ['node-move'],
    'actions' => [
        [
            'label' => Yii::t('menu', 'Добавить новый пункт'),
            'url' => ['create', 'parent' => $menu->id]
        ]
    ],
    'itemLabel' => function ($model) {
        return $model->name;
    }
]);