<?php

namespace pw\ui\nestable;


use pw\ui\assets\Nestable2Asset;
use Yii;
use pw\ui\traits\Button;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use pw\core\Widget;
use yii\helpers\Json;
use yii\helpers\Url;

class NestableView extends Widget
{

    use Button;

    public $title;

    public $icon;

    public $actions = [];
    /**
     * @var string, the handle label, this is not HTML encoded
     */
    public $handleLabel = '<div class="dd-handle dd3-handle">&nbsp;</div>';

    /**
     * @var array the HTML attributes to be applied to list.
     * This will be overridden by the [[options]] property within [[$items]].
     */
    public $listOptions = [];

    /**
     * @var array the HTML attributes to be applied to all items.
     * This will be overridden by the [[options]] property within [[$items]].
     */
    public $itemOptions = [];

    /**
     * @var array the sortable items configuration for rendering elements within the sortable
     * list / grid. You can set the following properties:
     * - id: integer, the id of the item. This will get returned on change
     * - content: string, the list item content (this is not HTML encoded)
     * - disabled: bool, whether the list item is disabled
     * - options: array, the HTML attributes for the list item.
     * - contentOptions: array, the HTML attributes for the content
     * - children: array, with item children
     */
    public $items = [];

    public $models = [];

    public $itemLabel = null;

    public $itemDescription = null;

    public $itemElements = [];

    public $itemTemplate = '{label} <span>{description}</span><div class="pull-right">{elements}</div><div class="clearfix"></div>';

    public $nodeMoveUrl = null;

    public $showPanel = true;

    public function run()
    {
        if ($this->models !== null) {
            $items = [];
            $this->items = $this->prepareItems($this->models);
        }
        $this->registerAsset();
        $items = '';
        if (count($this->items)) {
            $items = Html::beginTag('ol', ['class' => 'dd-list']);
            $items .= $this->renderItems();
            $items .= Html::endTag('ol');
            $this->initDefaultActions();
        }
        echo $this->render('tree', [
            'id' => $this->id,
            'title' => $this->title,
            'icon' => $this->icon,
            'actions' => $this->actions,
            'items' => $items,
            'showPanel' => $this->showPanel
        ]);
    }

    /**
     * Render the list items for the sortable widget
     *
     * @return string
     */
    protected function renderItems($_items = NULL)
    {
        $_items = is_null($_items) ? $this->items : $_items;
        $items = '';
        $dataid = 0;
        foreach ($_items as $item) {
            $options = ArrayHelper::getValue($item, 'options', ['class' => 'dd-item dd3-item']);
            $options = ArrayHelper::merge($this->itemOptions, $options);
            $dataId = ArrayHelper::getValue($item, 'id', $dataid++);
            $options = ArrayHelper::merge($options, ['data-id' => $dataId]);

            $contentOptions = ArrayHelper::getValue($item, 'contentOptions', ['class' => 'dd3-content']);
            $content = $this->handleLabel;
            $content .= Html::tag('div', ArrayHelper::getValue($item, 'content', ''), $contentOptions);

            $children = ArrayHelper::getValue($item, 'children', []);
            if (!empty($children)) {
                // recursive rendering children items
                $content .= Html::beginTag('ol', ['class' => 'dd-list']);
                $content .= $this->renderItems($children);
                $content .= Html::endTag('ol');
            }

            $items .= Html::tag('li', $content, $options) . PHP_EOL;
        }
        return $items;
    }

    /**
     * Prepare $items array from $model
     *
     * @param $model \pw\core\db\ActiveRecord
     * @return array
     */
    protected function prepareItems($models)
    {
        $items = [];
        $this->initDefaultElements();
        $getLabel = $this->itemLabel;
        $getDescription = $this->itemDescription;
        foreach ($models as $child) {
            $elements = [];
            $label = $description = '';
            if (is_string($getLabel)) {
                $label = $child->{$getLabel};
            } elseif (is_callable($getLabel)) {
                $label = $getLabel($child);
            }
            if (is_string($getDescription)) {
                $description = $child->{$getDescription};
            } elseif (is_callable($getDescription)) {
                $description = $getDescription($child);
            }

            if (!empty($this->itemElements)) {
                foreach ($this->itemElements as $id => $element) {
                    if (is_callable($element)) {
                        $elements[] = $element($child);
                    } elseif (is_array($element)) {

                        if (!isset($element['id'])) {
                            $element['id'] = $id;
                        }
                        $elements [$id] = $this->generateActionButton($element);
                    }
                }
            }

            $elements = $this->generateActionButton($elements);

            $content = strtr($this->itemTemplate, [
                '{label}' => $label,
                '{description}' => $description,
                '{elements}' => implode("\n", $elements)
            ]);
            $item = [
                'id' => $child->getPrimaryKey(),
                'content' => $content,
                'children' => [],
            ];
            if (!$child->isLeaf()) {
                $item['children'] = $this->prepareItems($child->children);
            }
            $items [] = $item;
        }
        return $items;
    }

    public function generateActionButton($elements)
    {
        foreach ($elements as $element) {
            $result[] = Html::a($element['label'] . $element['icon'], $element['options']['href']);
        }

        return $result;
    }

    public function registerAsset()
    {
        Nestable2Asset::register($this->view);
        $pluginOptions = [
            'url' => Url::to($this->nodeMoveUrl, true),
            'group' => 1
        ];
        $this->view->registerJs("
            $('#{$this->id}').nestable(" . Json::encode($pluginOptions) . ").nestable('collapseAll');
             $('#collapse-all-{$this->id}').click(function(e){
                $('#{$this->id}').nestable('collapseAll');
             });
             $('#expand-all-{$this->id}').click(function(e){
                $('#{$this->id}').nestable('expandAll');
             });
        ");
    }


    protected function initDefaultActions()
    {
        $this->actions['collapse-all-' . $this->id] = [
            'icon' => 'minus-square'
        ];
        $this->actions['expand-all-' . $this->id] = [
            'icon' => 'plus-square'
        ];
    }

    protected function initDefaultElements()
    {
        if (!isset($this->itemElements['view'])) {
            $this->itemElements['view'] = function ($model) {
                return $this->generateButton([
                    'icon' => 'plus',
                    'url' => ['create', 'parent' => $model->getPrimaryKey()],
                    'title' => Yii::t('ui', 'Добавить эелемент'),
                    'data-pjax' => '0',
                    'class' => 'btn btn-sm default'
                ]);
            };
        }
        if (!isset($this->itemElements['update'])) {
            $this->itemElements['update'] = function ($model) {
                return $this->generateButton([
                    'icon' => 'edit',
                    'url' => ['update', 'id' => $model->getPrimaryKey()],
                    'title' => Yii::t('ui', 'Редактировать'),
                    'data-pjax' => '0',
                    'class' => 'btn btn-sm btn-warning '
                ]);
            };
        }
        if (!isset($this->itemElements['delete'])) {
            $this->itemElements['delete'] = function ($model) {
                return $this->generateButton([
                    'icon' => 'trash',
                    'url' => ['delete', 'id' => $model->getPrimaryKey()],
                    'data-confirm' => Yii::t('ui', 'Вы действительно хотите удалить эту запись?'),
                    'data-method' => 'post',
                    'data-pjax' => '0',
                    'class' => 'red',
                ]);
            };
        }
    }
}