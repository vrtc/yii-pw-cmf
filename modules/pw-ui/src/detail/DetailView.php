<?php

namespace pw\ui\detail;

use Yii;
use yii\helpers\Html;


class DetailView extends \yii\widgets\DetailView
{

    const EVENT_RENDER_DETAIL = 'renderDetail';

    public $title = null;
    public $icon = null;
    public $actions = [];
    public $use_default_actions = true;

    private $_tabs = [];

    /**
     * @inheritdoc
     */
    public function run()
    {
        if ($this->use_default_actions) {
            $this->initDefaultActions();
        }

        ob_start();
        ob_implicit_flush(false);
        parent::run();

        $content = ob_get_clean();
        if (empty($this->_tabs) || (!isset($this->_tabs['general']) && $content)) {
            $this->_tabs = ['general' => ['key' => 'general', 'label' => Yii::t('ui', 'Основные данные'), 'content' => $content, 'icon' => $this->icon]] + $this->_tabs;
            $content = '';
        }
        if (!isset($this->_tabs['general']) && count($this->_tabs) === 1) {
            $tab = current($this->_tabs);
            $this->_tabs['general'] = &$this->_tabs[$tab['key']];
        }
        $this->model->beforeDetailRender($this);
        if (count($this->_tabs) === 1) {
            $tab = current($this->_tabs);
            $content = $tab['content'];
            $this->_tabs = [];
        } elseif (isset($this->_tabs['general']) && $this->_tabs['general']['key'] !== 'general') {
            unset($this->_tabs['general']);
        }

        echo $this->render('detail', [
            'icon' => $this->icon,
            'title' => $this->title,
            'actions' => $this->actions,
            'content' => $content,
            'tabs' => array_values($this->_tabs)
        ]);
    }

    protected function initDefaultActions()
    {
        if (!isset($this->actions['update'])) {
            $this->actions['update'] = [
                'url' => ['update', 'id' => $this->model->getPrimaryKey()],
                'htmlOptions' => [
                    'class' => 'btn btn-primary',
                ],
                'label' => Yii::t('ui', 'Изменить')
            ];
        }
        if (!isset($this->actions['delete'])) {
            $this->actions['delete'] = [
                'url' => ['delete', 'id' => $this->model->getPrimaryKey()],
                'label' => Yii::t('ui', 'Удалить'),
                'htmlOptions' => [
                    'class' => 'btn btn-danger',
                ],
                'data' => [
                    'confirm' => Yii::t('contacts', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ];
        }
    }

    public function beginTab($key, $label)
    {
        $this->_tabs[$key] = ['key' => $key, 'label' => $label, 'content' => ''];
        ob_start();
        ob_implicit_flush(false);
    }

    public function endTab()
    {
        $tab = end($this->_tabs);
        $this->_tabs[$tab['key']]['content'] .= ob_get_clean();
    }
}