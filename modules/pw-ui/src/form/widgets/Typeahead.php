<?php
namespace pw\ui\form\widgets;

use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\JsExpression;
use yii\widgets\InputWidget;

class Typeahead extends InputWidget {

    public $clientOptions = [];
    public $bloodhoundOptions = [];
    public $events = [];
    public $options = ['class' => 'form-control'];


    /**
     * @inheritdoc
     */
    public function run()
    {
        if ($this->hasModel()) {
            echo Html::activeTextInput($this->model, $this->attribute, $this->options);
        } else {
            echo Html::textInput($this->name, $this->value, $this->options);
        }
        $this->registerClientScript();
    }

    /**
     * Initializes bloodhound options
     */
    protected function initBloodhoundOptions()
    {
        $bloodhound = isset($this->bloodhoundOptions) ? $this->bloodhoundOptions : [];
        foreach ($bloodhound as $key => $value) {
            if (in_array($key, ['datumTokenizer', 'queryTokenizer']) && !$value instanceof JsExpression) {
                $bloodhound[$key] = new JsExpression($value);
            }
        }
        if (empty($bloodhound['datumTokenizer'])) {
            $bloodhound['datumTokenizer'] = new JsExpression("Bloodhound.tokenizers.obj.whitespace('value')");
        }
        if (empty($bloodhound['queryTokenizer'])) {
            $bloodhound['queryTokenizer'] = new JsExpression("Bloodhound.tokenizers.whitespace");
        }
        $this->bloodhoundOptions = $bloodhound;
    }
    /**
     * Initializes client options
     */
    protected function initClientOptions()
    {
        $this->initBloodhoundOptions();
        $options = $this->clientOptions;
        foreach ($options as $key => $value) {
            if (in_array($key, ['name', 'source']) && !$value instanceof JsExpression) {
                $options[$key] = new JsExpression($value);
            }
        }
        $this->clientOptions = $options;
    }


    public function registerClientScript()
    {
        $js = '';
        $view = $this->getView();
        $this->initClientOptions();
        $encBloodhoundOptions = empty($this->bloodhoundOptions) ? '{}' : Json::encode($this->bloodhoundOptions);
        $id = 'typeahead_bloodhound_'.$this->id;
        $view->registerJs("var {$id} = new Bloodhound({$encBloodhoundOptions});\n");
        $view->registerJs("{$id}.initialize();\n");

        if (empty($this->clientOptions['source'])) {
            $this->clientOptions['source'] = new JsExpression("{$id}.ttAdapter()");
        }
        $encOptions = empty($this->clientOptions) ? '{}' : Json::encode($this->clientOptions);
        $events = '';
        foreach ($this->events as $event => $handler) {
            $events .= ".bind('{$event}', {$handler})";
        }
        $js .= '$("#' . $this->id . '")' . ".typeahead(null, {$encOptions}){$events};\n";
        TypeaheadAsset::register($view);
        $view->registerJs($js);
    }
}