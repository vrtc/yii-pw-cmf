<?php

namespace pw\ui\form;

use pw\filemanager\FileManagerAsset;
use pw\filemanager\models\File;
use pw\ui\assets\FancyBoxAsset;
use pw\ui\assets\JsTreeAsset;
use pw\web\View;
use Yii;
use pw\ui\assets\Select2Asset;
use pw\ui\wysiwyg\Redactor;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\helpers\Html;
use pw\ui\icons\Icons;

class ActiveField extends \yii\bootstrap\ActiveField
{

    public function wysiwyg($options = [])
    {
        return $this->widget(Redactor::class, [
            'editorOptions' => [
                'filebrowserBrowseUrl' => Url::to(['/pw-filemanager/manage/index']),
                'allowedContent' => 'script'
            ]
        ]);
    }

    public function dateInput($options)
    {
        $options = array_merge($this->inputOptions, $options);
        $this->addAriaAttributes($options);
        $this->adjustLabelFor($options);
        $this->parts['{input}'] = \kartik\date\DatePicker::widget([
            'model' => $this->model,
            'attribute' => $this->attribute
        ]);

        return $this;
    }

    public function dateTimeInput()
    {
    }

    public function colorPicker()
    {
    }

    public function tagsInput()
    {
    }

    public function captcha($params = [])
    {
        $params['siteKey'] = '6Le94joUAAAAAN1KA0KGACjsoFgH4-ISPFwUtkJT';

        return $this->widget(\himiklab\yii2\recaptcha\ReCaptcha::class, ['siteKey' => $params['siteKey']]);
    }

    public function treeInput($model, $checked = [])
    {
        JsTreeAsset::register($this->form->view);
        $this->parts['{label}'] = '';
        $this->parts['{input}'] = '<div id="tree-input-' . $this->getInputId() . '"></div>';
        $js = '
        var checked = ' . Json::encode($checked) . '
        $("#tree-input-' . $this->getInputId() . '").jstree({
            "plugins": [ "checkbox", "types"],
            "checkbox" : {
                "three_state" : false
            },
            "core": {
                "themes" : {
                    "responsive": false
                },
                "data":{
                    "url" : function (node) {
                        return "' . Url::current(['treeInput' => 'true', 'model' => $model], true) . '";
                    },
                    "success":function(categories){
                        var data = [];
                        $.each(categories,function(i,row){
                            row.state  =  [];
                            if(checked[row.id]!= undefined){
                                row.state["selected"] = true;
                            }
                            data.push(row);
                        });
                        return data;
                    },
                    "data" : function (node) {
                        return {"parent_id" : node.id };
                    }
                }
            },
            "types" : {
                "default" : {
                    "icon" : "fa fa-folder icon-state-warning icon-lg"
                },
                "file" : {
                    "icon" : "fa fa-file icon-state-warning icon-lg"
                }
            }
        });
';
        $this->form->view->registerJs($js);

        return $this;
    }

    /**
     * @param       $models
     * @param array $options
     *
     * @return $this
     */
    public function autoComplete($models, $options = [])
    {
        if (is_string($models)) {
            $models = (array)$models;
        }
        if (empty($options['id'])) {
            $options['id'] = $this->getInputId();
        }
        $id = $options['id'];
        $delay = ArrayHelper::getValue($options, 'delay', 250);
        $minLength = ArrayHelper::getValue($options, 'minLength', 2);
        $select2Options = [
            'minimumInputLength' => $minLength,
            'ajax' => [
                'url' => Url::to([
                    '/pw-search/default/autocomplete',
                    'models' => implode(',', $models)
                ], true),
                'dataType' => 'json',
                'delay' => $delay,
                'data' => new JsExpression('function (params) {
                    return {
                        query: params.term, // search term
                        page: params.page
                    };
                }'),
                'processResults' => new JsExpression('function (data, params) {
                  params.page = params.page || 1;
                  return {
                        results: data.items,
                        pagination: {
                        more: (params.page * 30) < data.total_count
                        }
                   };
            }'),
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(city) { return city.short_name; }'),
                'templateSelection' => new JsExpression('function (city) { return city.short_name; }'),
                'cache' => true
            ]
        ];

        return $this->dropDownList([], ['clientOptions' => $select2Options]);
    }

    public function dropDownList($items, $options = [], $useSelect2 = true)
    {
        if($useSelect2) {
            Select2Asset::register(Yii::$app->view);
            if (empty($options['id'])) {
                $options['id'] = $this->getInputId();
            }
            $id = $options['id'];
            $clientOption = ArrayHelper::getValue($options, 'clientOptions', []);
            if (!empty($options['prompt'])) {
                $clientOption['placeholder'] = $options['prompt'];
            }
            $clientOption = Json::encode($clientOption);
            $widget_id = 'select2_' . hash('crc32', $clientOption);
            Yii::$app->view->registerJs("var $widget_id = $clientOption;", View::POS_HEAD);
            Yii::$app->view->registerJs("$('#$id').select2($widget_id);");
            $options['data-widget-id'] = $widget_id;
        }
        return parent::dropDownList($items, $options);
    }

    public function ajaxDropDownList($model, $options = [])
    {
        $delay = ArrayHelper::getValue($options, 'delay', 250);
        $minLength = ArrayHelper::getValue($options, 'minLength', 0);
        $allowClear = ArrayHelper::getValue($options, 'allowClear', true);
        $escapeMarkup = ArrayHelper::getValue(
            $options,
            'escapeMarkup',
            new JsExpression('function (markup) { console.log(markup);return markup; }')
        );
        $templateResult = ArrayHelper::getValue(
            $options,
            'templateResult',
            new JsExpression('function(item) { return item.text; }')
        );
        $templateSelection = ArrayHelper::getValue(
            $options,
            'templateSelection',
            new JsExpression('function(item) { return item.text; }')
        );
        $select2Options = [
            'minimumInputLength' => $minLength,
            'allowClear' => $allowClear,
            'ajax' => [
                'url' => Url::to(['/pw-search/default/autocomplete', 'model' => $model], true),
                'dataType' => 'json',
                'delay' => $delay,
                'data' => new JsExpression('function (params) {
                    return {
                        query: params.term,
                        page: params.page
                    };
                }'),
                'processResults' => new JsExpression('function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                }'),
                'cache' => true
            ],
            'escapeMarkup' => $escapeMarkup,
            'templateResult' => $templateResult,
            'templateSelection' => $templateSelection,
        ];

        return $this->dropDownList([], ['clientOptions' => $select2Options]);
    }


    public function fileInput($options = [])
    {
        $id = $this->getInputId();
        $attributeName = Html::getAttributeName($this->attribute);
        $file = $this->model->{$attributeName};
        $field = '<div class="file-manager-input">';
        //$this->options['id'] = $id;
        if (\is_array($file)) {
            $field .= '<table class="file-preview m-1 table table-active table-responsive ">';
            foreach ($file as $item) {
                $item = File::findOne(['id' => $item]);
                $hideClass = $file === null ? 'hide' : '';
                $size = Yii::$app->formatter->asShortSize($item->size);
                $trash = Icons::show('trash');
                $search = Icons::show('search');
                $edit = Icons::show('edit');
                //<td>{$item->filename} ({$size})</td>
                $field .= <<<HTML

<tr>
    <td class="col-3">{$item->getThumbnail(128, 128)} </td>
    <td class="col-6"><b>Alt: </b><span class="alt_{$item->id}">{$item->alt}</span><br>
    <b>Title: </b><span class="title_{$item->id}">{$item->title}</span>
    </td>
    
    
    <td class="col-3">
     <a href='#' class='m-1 input-group-addon btn btn-primary delete-selected-image {$hideClass}' data-input-id='{$item->id}' >{$trash}</a>

     <a href='/files/{$item->getPath()}' class='m-1 input-group-addon btn btn-primary' data-fancybox='group-{$id}' data-input-id='{$id}'>
                {$search}</a>

        <a href='#' class='m-1 input-group-addon btn btn-primary edit-click' data-input-id='{$item->id}'>
                {$edit}
                </a>
    </td>
</tr>

HTML;

               /* $field .= '<table class="file-preview m-1">';
                $field .= '<tr class="file-preview-frame"><div class="file-preview-content">';
                $field .= 'Alt: <span class="alt_'.$item->id.'">' .$item->alt . '</span><br>';
                $field .= 'Title: <span class="title_'.$item->id.'"> ' .$item->title. '</span>';
                $field .= $item->getThumbnail(128, 128);
                $hideClass = $file === null ? 'hide' : '';
                $field .= '</div><div class="file-preview-meta">';
                //$field .= Html::encode($item->filename);
                if ($item->size) {
                    $field .= ' (' . Yii::$app->formatter->asShortSize($item->size) . ')';
                }
                $field .= '</div>';
                $field .= "<a href='#' class='m-1 input-group-addon btn btn-primary delete-selected-image $hideClass' data-input-id='$item->id' >";
                $field .= Icons::show('trash');
                $field .= '</a>';

                $field .= "<a href='/files/{$item->getPath()}' class='m-1 input-group-addon btn btn-primary' data-fancybox='group-$id' data-input-id='$id'>";
                $field .= Icons::show('search');
                $field .= '</a>';

                $field .= "<a href='#' class='m-1 input-group-addon btn btn-primary edit-click' data-input-id='$item->id'>";
                $field .= Icons::show('edit');
                $field .= '</a>';
                $field .= '</tr></table>';*/
            }
            $field .= '</table>';
        } elseif ($file) {
            $file = File::findOne(['id' => $file]);
            $field .= '<div class="file-preview">';
            $field .= '<div class="file-preview-frame"><div class="file-preview-content">';
            $hideClass = $file === null ? 'hide' : '';
            $field .= $file->getThumbnail(128, 128);

            $field .= '</div><div class="file-preview-meta">';
            $field .= Html::encode($file->filename);
            if ($file->size) {
                $field .= ' (' . Yii::$app->formatter->asShortSize($file->size) . ')';
            }
            $field .= '</div>';
            $field .= "<a href='#' class='input-group-addon btn btn-primary delete-file $hideClass' data-input-id='$id' >";
            $field .= Icons::show('trash');
            $field .= '</a>';

            $field .= "<a href='/files/{$file->getPath()}' class='input-group-addon btn btn-primary' data-fancybox data-input-id='$id'>";
            $field .= Icons::show('search');
            $field .= '</a>';

            $field .= '</div></div>';
        }
        $field .= "<div class='input-group mt-5'>";
        if (is_array($file)) {
            $i = false;
            foreach ($file as $item) {
                if(!$i) {
                    $field .= Html::activeHiddenInput($this->model, $this->attribute, ['id' => $id, 'value' => $item]);
                    $i = true;
                }elseif($i){
                    $field .= Html::activeHiddenInput($this->model, $this->attribute, ['id' => 'other_files', 'value' => $item]);
                }
            }
        } elseif ($file) {
            $field .= Html::activeHiddenInput($this->model, $this->attribute, ['id' => $id, 'value' => $file]);
        }
        $field .= Html::activeHiddenInput($this->model, $this->attribute, ['id' => $id]);
        $field .= Html::textInput($this->attribute . '_name', null, ['readonly' => true, 'class' => 'form-control', 'id' => $id . '_name']);
        $field .= "<a href='#' class='btn btn-primary open-filemanager' data-input-id='$id'>";
        $field .= Icons::show('folder-open') . ' ' . Yii::t('filemanager', 'Выбрать');
        $field .= '</a>';
        $field .= '</div></div>';
        $this->parts['{input}'] = $field;
        FileManagerAsset::register($this->form->view);
        FancyBoxAsset::register($this->form->view);
        $sBaseUrl = Url::to(['/pw-filemanager/manage/index']);
        $config = Json::encode([
            'fileManager' => Yii::t('filemanager', 'Файловое хранилище'),
            'detachWarningMessage' => Yii::t('filemanager', 'Вы действительно хотите удалить файл?'),
        ]);
        $this->form->view->registerJs("fileManagerInput.baseUrl = '$sBaseUrl';");
        $this->form->view->registerJs("fileManagerInput.message = $config;");
        $urlEditFile = Url::to(['/pw-filemanager/manage/edit']);
        $js = <<<JS
            $(document).on('click', '.edit-click', function(e) {
              e.preventDefault();
                var image_id = $(this).data('input-id');
                Swal.fire({
                    html: '<div id="image-content"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>',
                    showConfirmButton: false,
                    showCloseButton: true,
                    onOpen: (comment) => {
                        $.ajax({
                            url: '{$urlEditFile}?id='+image_id,
                            type: 'GET',
                            success: function(res){
                                $('#image-content').html(res);
                            },
                            error: function(){
                                console.log('error');
                            }
                        });
                    }
                });
            });
JS;

        $this->form->view->registerJs($js);

        return $this;
    }
}
