<?php

namespace pw\ui\form;

use pw\core\db\ActiveRecord;
use pw\core\Model;
use pw\ui\traits\Button;
use Yii;

use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveFormAsset;
use yii\base\InvalidConfigException;
use yii\base\InvalidCallException;
use wbraganca\dynamicform\DynamicFormWidget;

class ActiveForm extends \yii\bootstrap4\ActiveForm
{
    use Button;
    const EVENT_RENDER_FORM = 'renderForm';

    public $model;
    public $title;
    public $icon;
    public $isItForm = true;
    public $actions = [];
    public $useDefaultButtons = true;
    public $buttons = [];
    public $showPanel = true;

    public $fieldClass = ActiveField::class;

    private $_tabs = [];
    private $_fields = [];

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidParamException
     */
    public function run()
    {
        if (!empty($this->_fields)) {
            throw new InvalidCallException('Each beginField() should have a matching endField() call.');
        }

        $content = ob_get_clean();

        if ($this->enableClientScript) {
            $options = Json::htmlEncode($this->getClientOptions());
            $attributes = Json::htmlEncode($this->attributes);
            $view = $this->getView();
            ActiveFormAsset::register($view);
            $view->registerJs("jQuery('#$this->id').yiiActiveForm($attributes, $options);");
        }
        if (empty($this->_tabs)) {
            $this->_tabs['general'] = ['key' => 'general', 'label' => Yii::t('ui', 'Основные данные'), 'content' => $content, 'icon' => $this->icon];
            $content = '';
        }
        if (!isset($this->_tabs['general']) && count($this->_tabs) === 1) {
            $tab = current($this->_tabs);
            $this->_tabs['general'] = &$this->_tabs[$tab['key']];
        }
        if ($this->model && ($this->model instanceof ActiveRecord || $this->model instanceof Model)) {
            $this->model->beforeFormRender($this);
        }
        if (count($this->_tabs) === 1) {
            $tab = current($this->_tabs);
            $content = $tab['content'];
            $this->_tabs = [];
        } elseif (isset($this->_tabs['general']) && $this->_tabs['general']['key'] !== 'general') {
            unset($this->_tabs['general']);
        }
        if ($this->useDefaultButtons) {
            $this->initDefaultButtons();
        }
        echo $this->render('form', [
            'action' => $this->action,
            'method' => $this->method,
            'options' => $this->options,
            'title' => $this->title,
            'icon' => $this->icon,
            'actions' => $this->actions,
            'buttons' => $this->buttons,
            'model' => $this->model,
            'tabs' => array_values($this->_tabs),
            'content' => $content,
            'showPanel' => $this->showPanel,
            'isItForm' => $this->isItForm,
        ]);
    }

    public function beginTab($key = 'general', $label = null, $icon = null, $url = null)
    {
        if (!isset($this->_tabs[$key])) {
            if ($label === null) {
                $label = Yii::t('ui', 'Основные данные');
            }
            $this->_tabs[$key] = ['key' => $key, 'label' => $label, 'icon' => $icon, 'content' => '', 'url' => $url];
        }

        ob_start();
        ob_implicit_flush(false);
    }


    public function endTab()
    {
        $tab = end($this->_tabs);
        $this->_tabs[$tab['key']]['content'] .= ob_get_clean();
    }

    public function repeater($options = [])
    {
        if (!isset($options['models'])) {
            throw new InvalidConfigException('missing models array');
        }
        if (empty($options['render']) && empty($options['template'])) {
            throw new InvalidConfigException('missing render callback or template file');
        } elseif (!empty($options['render']) && !is_callable($options['render'])) {
            throw new InvalidConfigException('wrong render callback');
        } elseif (!empty($options['template']) && !file_exists(Yii::getAlias($options['template']))) {
            throw new InvalidConfigException('template ' . $options['template'] . ' not found');
        }
        $model = $options['models'][0];
        $min = ArrayHelper::getValue($options, 'min', 1);
        $max = ArrayHelper::getValue($options, 'max', 10);
        $title = ArrayHelper::getValue($options, 'title', '');
        $icon = ArrayHelper::getValue($options, 'icon', '');
        $fields = ArrayHelper::getValue($options, 'fields', $model->attributes());
        $formName = $model->formName();
        $oldFieldConfig = $this->fieldConfig;
        $this->fieldConfig['horizontalCssClasses'] = [
            'offset' => '',
            'label' => 'col-md-3',
            'wrapper' => 'col-md-12',
            'error' => '',
            'hint' => '',
        ];
        $items = [];
        $templateParams = ArrayHelper::getValue($options, 'templateParams', []);
        $templateParams['form'] = $this;

        foreach ($options['models'] as $i => $model) {
            if (isset($options['template'])) {
                $items[] = $this->render($options['template'], array_merge(['model' => $model, 'i' => $i], $templateParams));
            } elseif (is_callable($options['render'])) {
                $items[] = $options['render']($this, $model, $i, $this->view);
            }
        }
        DynamicFormWidget::begin([
            'widgetContainer' => 'dynamicform_' . $formName,
            'widgetBody' => '.container-items-' . $formName,
            'widgetItem' => '.item-' . $formName,
            'limit' => $max,
            'min' => $min,
            'insertButton' => '.add-item-' . $formName,
            'deleteButton' => '.remove-item-' . $formName,
            'model' => $model,
            'formId' => $this->id,
            'formFields' => $fields,
        ]);

        $this->view->registerJs("$('.dynamicform_$formName').on('afterInsert',function(e,el){
            $(el).find('[data-widget-id]').each(function(i,el){
                var data = $(el).data('widget-id');
                var params = data.match(/(.+)_([a-z0-9]+)/);
                var widget = params[1];
                $(el)[widget](window[data]);
            });
        });");
        echo $this->render('repeater', [
            'title' => $title,
            'icon' => $icon,
            'form' => $this,
            'items' => $items,
            'formName' => $formName,
            'deleteButton' => $this->generateButton([
                'icon' => 'trash',
                'title' => Yii::t('pw-ui', 'Удалить'),
                'htmlOptions' => [
                    'data-repeater-delete' => true,
                    'class' => "remove-item-$formName btn btn-danger"
                ]
            ])
        ]);
        DynamicFormWidget::end();
        $this->fieldConfig = $oldFieldConfig;
    }

    protected function initDefaultButtons()
    {
        if (empty($this->buttons)) {
            $this->buttons['save'] = [
                'type' => 'submit',
                'name' => 'save',
                'label' => Yii::t('ui', 'Сохранить'),
                'class' => 'btn btn-primary'
            ];
        }
    }

    public function addAction($action)
    {
        $this->actions[$action['name']] = $this->generateButton($action);
    }
}
