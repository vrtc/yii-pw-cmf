<?php

namespace pw\ui\form;

use pw\core\interfaces\EventHandler;
use pw\web\Application;
use yii\helpers\Json;
use yii\web\Request;
use yii\web\Response;

class TreeInputHandler
{

    public function actions()
    {
        return [
            [
                [Application::class, Application::EVENT_BEFORE_REQUEST], 'onBeforeRequest'
            ]
        ];
    }

    public static function onBeforeRequest($event)
    {
        $app = $event->sender;
        if ($app->request->isAjax && $app->request->isGET && $app->request->get('treeInput') === 'true') {
            $model = $app->request->get('model', '');
            $parent_id = $app->request->get('parent_id');
            if (empty($model)) {
                return false;
            }
            if($parent_id !== '#'){
                $parent = $model::findOne($parent_id);
                $query = $parent->getChildren();
            }
            else {
                $query = $model::find()->roots();
            }
            $data =[];
            foreach ($query->all() as $item){
                $data [] = [
                    'id'=>$item->id,
                    'text'=>$item->name,
                    'children'=>(bool)$item->getChildren()->count(),
                    'type'=>$item->isRoot() ? 'root' : ''
                ];
            }
            $response = new Response();
            $response->format = Response::FORMAT_JSON;
            $response->data =$data;
            $response->send();
            exit;
        }
    }
}
