<?php
/* @var $this yii\web\View */
/* @var $form pw\ui\ActiveForm */
/* @var $model pw\core\Model */

use pw\ui\panel\Panel;
use yii\helpers\Html;

?>
<?php Panel::begin([
    'panelOptions' => [
        'class' => 'portlet light'
    ],
    'title'=>$title,
    'icon'=>$icon,
    'tools'=>false,
    'actions' => [
        [
            'label' => Yii::t('pw-ui', 'Добавить'),
            'icon' => 'plus',
            'htmlOptions' => [
                'class' => "add-item-$formName red"
            ]
        ]
    ]
]); ?>
    <div class="container-items-<?= $formName ?>">
        <?php foreach ($items as $i => $item): ?>
            <div class="row item-<?= $formName ?>">
                <?= $item ?>
                <div class="col-md-1">
                    <?= $deleteButton ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php Panel::end() ?>