<?php
/* @var $this yii\web\View */
/* @var $form pw\ui\ActiveForm */
/* @var $model pw\core\Model */

use pw\ui\panel\Panel;
use yii\helpers\Html;

$isActiveRecord = $model instanceof \pw\core\db\ActiveRecord;
$actions = array_merge($actions, [
    \pw\i18n\widgets\LanguageSwitchWidget::widget(['template' => 'language-switch'])
]);
?>
<?= Html::beginForm($action, $method, $options); ?>
<?= Html::hiddenInput('language', Yii::$app->language) ?>
<?php if ($showPanel): ?>
    <?php Panel::begin([
        'title' => $title,
        'icon' => $icon,
        'actions' => $actions,
        'panelBodyOptions' => [
            'class' => 'portlet-body form'
        ],
        'isItForm' => true,
        'tabs'=>$tabs
    ]); ?>
<?php endif; ?>
<?= $content ?>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-3 col-md-9">
                <?php foreach ($buttons as $button): ?>
                    <?php if ($button['type'] === 'submit'): ?>
                        <?= Html::submitButton($button['label'],$button) ?>
                    <?php else:?>
                        <?=Html::button($button['label'],$button)?>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php if ($showPanel): ?>
    <?php Panel::end() ?>
<?php endif; ?>
<?= Html::endForm() ?>