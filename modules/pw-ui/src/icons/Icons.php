<?php

namespace pw\ui\icons;

use pw\web\Application;
use Yii;
use pw\core\Component;
use yii\base\InvalidConfigException;

class Icons extends Component
{

    public $default;
    private $_sets = [];
    private $_assets = [];


    public function registerSet($class)
    {
        $set = Yii::createObject($class);
        if (!($set instanceof IconsSetInterface)) {
            throw new InvalidConfigException();
        }
        if(Yii::$app instanceof Application) {
            $set->registerAssets(Yii::$app->view);
        }
        $this->_sets[$set->getName()] = $set;
    }

    public function setDefault($name)
    {
        $this->default = $name;
    }


    public function getIcons()
    {
        $icons = [];
        foreach ($this->_sets as $set) {
            $icon[] = array_merge($icons, $set->getIcons());
        }

        return $icons;
    }

    public function getIcon($name, $params, $set = null)
    {
        if ($set === null) {
            if ($this->default) {
                $set = $this->default;
            } else {
                $set = \key($this->_sets);
            }
        }
        if (!isset($this->_assets[$set])) {
            $this->_sets[$set]->registerAssets(Yii::$app->view);
            $this->_assets[$set] = true;
        }

        return $this->_sets[$set]->getIcon($name, $params);
    }

    public static function show($name, $options = [])
    {
        return Yii::$app->icons->getIcon($name, $options);
    }

    public static function __callStatic($name, $arguments)
    {
        return self::show($name, $arguments);
    }

}