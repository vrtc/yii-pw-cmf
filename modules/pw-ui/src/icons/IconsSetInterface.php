<?php

namespace pw\ui\icons;

use yii\base\View;

interface IconsSetInterface
{

    public function getName(): string;

    public function getIcon(string $name, array $params = []): string;

    public function getIcons(): array;

    public function registerAssets(View $view): void;
}