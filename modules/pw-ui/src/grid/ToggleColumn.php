<?php
namespace pw\ui\grid;

use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\grid\DataColumn;
use yii\helpers\Html;
use yii\helpers\Url;

class ToggleColumn extends DataColumn
{

    /**
     * @var string the label for the toggle button. Defaults to "Check".
     * Note that the label will not be HTML-encoded when rendering.
     */
    public $onLabel = '<i class="fa fa-check"></i>';
    /**
     * @var string the label for the toggle button. Defaults to "Uncheck".
     * Note that the label will not be HTML-encoded when rendering.
     */
    public $offLabel = '<i class="fa fa-times"></i>';


    /**
     * @var string Name of the action to call and toggle values. Defaults to `toggle`.
     * @see [[ToggleAction]] for an easy way to use with your controller
     */
    public $url = ['toggle'];
    /**
     * @var string a javascript function that will be invoked after the toggle ajax call.
     *
     * The function signature is `function(success, data)`.
     *
     * - success:  status of the ajax call, true if the ajax call was successful, false if the ajax call failed.
     * - data: the data returned by the server in case of a successful call or XHR object in case of error.
     *
     * Note that if success is true it does not mean that the delete was successful, it only means that the ajax call
     * was successful.
     *
     * Example:
     *
     * ```
     *  [
     *     'class'=> ToggleColumn::class,
     *     'afterToggle'=>'function(success, data){ if (success) alert("Toggled successfully"); }',
     *  ],
     * ```
     */
    public $afterToggle;
    /**
     * @var string suffix substituted to a name class of the tag <a>
     */
    public $classSuffix;

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        if ($this->url === null) {
            throw new InvalidConfigException("'Url' property must be specified.");
        }
        parent::init();
        $this->registerClientScript();
    }

    /**
     * @inheritdoc
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        $url = $this->url;
        if (null !== $url) {
            if ($model instanceof Model) {
                $keys = $model->primaryKey();
                $key = $keys[0];
            }
            $url = (array)$this->url;
            $url['attribute'] = $this->attribute;
            $url['id'] = ($model instanceof Model)
                ? $model->getPrimaryKey()
                : $key;
            $url = Url::to($url);
        }
        $checkbox = Html::checkbox($model->formName().'['.$model->getPrimaryKey().']['.$this->attribute.']',$this->getDataCellValue($model,$key,$index),[
            //'id'=>$this->attribute.'_toggle',
            'class'=>'switch-input '.$this->attribute.'_toggle',
           /* 'data-on-text'=>$this->onLabel,
            'data-off-text'=>$this->offLabel,*/
            'data-on-color'=>'success',
            'data-size'=>'small',
            'data-url'=>$url
        ]);
        $span = '<span class="switch-slider"></span>';
        return  Html::label($checkbox . $span, null, ['class' => 'switch switch-primary']);

    }

    /**
     * Registers the required scripts for the toggle column to work properly
     */
    protected function registerClientScript()
    {
        $view = $this->grid->getView();
        $grid = $this->grid->id;
        $js = 'jQuery("#'.$grid.' .'.$this->attribute.'_toggle").on("change", function(event, state) {
            var $self = $(this);
            var url = $self.data("url");
            var cb = '.($this->afterToggle?:'function(){}').';
            $.ajax({
                url: url,
                method:"post",
                dataType: "json",
                success: function (data) {
                    $.isFunction(cb) && cb(true, data);
                },
                error:function(xhr){
                    $.isFunction(cb) && cb(true, data);

                }
            });
            return false;
        });';
        $view->registerJs($js);
    }
}