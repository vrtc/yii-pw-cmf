<?php
/** @var $this \pw\web\View */
use yii\helpers\Url;
?>

<div class="btn-group">
    <a class="btn red btn-outline btn-circle" href="javascript:;" data-toggle="dropdown" aria-expanded="false">
        <i class="fa fa-share"></i>
        <span class="hidden-xs"><?=Yii::t('ui', 'Экспорт')?></span>
        <i class="fa fa-angle-down"></i>
    </a>
    <ul class="dropdown-menu pull-right">
        <li>
            <a target="_blank" href="'<?=Url::to(['export','format'=>'excel'])?>"><?=Yii::t('ui', 'Экспорт в Excel')?></a>
        </li>
        <li>
            <a target="_blank" href="<?=Url::to(['export','format'=>'csv'])?>"><?=Yii::t('ui', 'Экспорт в CSV')?></a>
        </li>
        <li>
            <a target="_blank" href="<?=Url::to(['export','format'=>'xml'])?>"><?=Yii::t('ui', 'Экспорт в XML')?></a>
        </li>
        <li>
            <a target="_blank" href="<?=Url::to(['export','format'=>'json'])?>"><?=Yii::t('ui', 'Экспорт в JSON')?></a>
        </li>
    </ul>
</div>