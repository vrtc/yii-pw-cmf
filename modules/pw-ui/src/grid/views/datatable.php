<?php
/** @var $this \pw\web\View */
/** @var $grid \pw\ui\grid\GridView */
/** @var $model \pw\core\Model */
use pw\ui\panel\Panel;

?>
<?php if ($showPanel): ?>
    <?php Panel::begin([
        'title' => $title,
        'icon' => $icon,
        'actions' => $actions,
    ]) ?>
<?php endif; ?>
<div class="table-container">
    <?= $bulkActions?>
    <?= $grid->renderDatatable(); ?>
</div>
<?php if ($showPanel): ?>
    <?php Panel::end() ?>
<?php endif; ?>
