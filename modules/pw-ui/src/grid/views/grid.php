<?php
/** @var $this \pw\web\View */
/** @var $grid \pw\ui\grid\GridView */
/** @var $model \pw\core\Model */
use pw\ui\panel\Panel;

?>
<?php if ($showPanel): ?>
    <?php Panel::begin([
        'title' => $title,
        'icon' => $icon,
        'actions' => $actions,
    ]) ?>
<?php endif; ?>
<div class="table-container">
    <?php if ($grid->showOnEmpty || $grid->dataProvider->getCount() > 0): ?>
        <div class="col-md-8 col-sm-12">
            <?= $grid->renderPager(); ?>
            <div class="grid-info" role="status" aria-live="polite"><span class="seperator">|</span> <?= $grid->renderSummary(); ?></div>
        </div>

        <?= $grid->renderItems(); ?>
    <?php else: ?>
        <div><?= $grid->emptyText ?></div>
    <?php endif; ?>
</div>
<?php if ($showPanel): ?>
    <?php Panel::end() ?>
<?php endif; ?>
