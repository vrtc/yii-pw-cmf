<?php
namespace pw\ui\grid;


use yii\helpers\Html;

class CheckboxColumn extends \yii\grid\CheckboxColumn
{

    public function registerClientScript()
    {
        if (!$this->grid->simple) {
            Html::addCssClass($this->grid->tableOptions, 'table-checkable');
            return null;
        }
        //parent::registerClientScript();
    }
}
