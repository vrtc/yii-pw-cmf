<?php

namespace pw\ui\grid;


use pw\core\db\TreeActiveRecord;
use pw\core\interfaces\Sortable;
use pw\ui\assets\DataTablesHelperAsset;
use pw\ui\assets\GridViewAsset;
use pw\ui\assets\JSTreeGridAsset;
use Yii;
use yii\grid\DataColumn;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\Response;
use yii\widgets\LinkPager;


class GridView extends \yii\grid\GridView
{

    const EVENT_RENDER_GRID = 'renderGrid';

    public $pageSizes = [10, 25, 50, 100];
    public $pageSizesOptions = [
        'class' => 'form-control'
    ];

    public $model;
    public $actions = [];
    public $bulkActions = [];
    public $filter = true;
    public $export = false;
    public $icon;
    public $title;

    public $pageSize = 20;

    public $showPanel = true;

    public $controller = null;

    /**
     * @var bool $simple don`t use jquery DataTable
     */
    public $simple = true;

    /**
     * @var bool $sortable sortable
     */
    public $reorderColumns;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if(Yii::$app->request->getQueryParam('per-page')){
            $this->pageSize = Yii::$app->request->getQueryParam('per-page');
        }
        $this->initPageSize();
        $this->initActions();
        if ($this->filterModel) {
            $this->model = $this->filterModel;
        }
        if ($this->reorderColumns === null && $this->model instanceof Sortable) {
            $this->reorderColumns = true;
        }
        parent::init();
        $this->tableOptions['id'] = $this->options['id'] = $this->getId();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        if ($this->model && method_exists($this->model, 'beforeGridRender')) {
            $this->model->beforeGridRender($this);
        }
        $id            = $this->options['id'];
        $view          = $this->view;
        $clientOptions = [
            'src' => "#$id"
        ];
        $bulkActions   = '';
        if ($this->simple) {
            $type = 'grid';
            GridViewAsset::register($view);
            $clientOptions['yiiGrid'] = $this->getClientOptions();
            $clientOptions            = Json::encode($clientOptions);
            $view->registerJs(
                "var grid_$id = new gridView();
                grid_$id.init($clientOptions);
            ");

        } else {
            if (Yii::$app->request->isAjax) {
                $this->renderJson();
            }

            if ($this->bulkActions) {
                $column = Yii::createObject([
                    'class' => CheckboxColumn::class,
                    'grid'  => $this,
                ]);
                array_unshift($this->columns, $column);
                $bulkActions = '<div class="table-actions-wrapper"><span> </span>';
                $bulkActions .= Html::dropDownList(
                    'bulk-actions',
                    null,
                    $this->bulkActions,
                    [
                        'prompt' => Yii::t('pw-ui', 'Выбрать'),
                        'class'  => 'table-group-action-input form-control input-inline input-small input-sm'
                    ]
                );
                $bulkActions .= '<button class="btn btn-sm green table-group-action-submit">
                <i class="fa fa-check"></i> ' . Yii::t('pw-ui', 'Выполнить') . '</button></div>';
            }
            DataTablesHelperAsset::register($view);
            if ($this->model && $this->filter) {
                $clientOptions['formName']      = $this->model->formName();
                $clientOptions['pageParam']     = $this->dataProvider->getPagination()->pageParam;
                $clientOptions['pageSizeParam'] = $this->dataProvider->getPagination()->pageSizeParam;
            }
            $columns = [];
            $i       = 0;
            foreach ($this->columns as $column) {
                $columns[$i] = [
                    'data'       => $column instanceof DataColumn ? $column->attribute : "col_$i",
                    'orderable'  => $column instanceof DataColumn ? $column->enableSorting : false,
                    'searchable' => $column instanceof DataColumn ? $this->filter : false

                ];
                if ($column instanceof CheckboxColumn) {
                    $columns[$i]['width'] = '5%';
                }
                if ($this->controller && $column instanceof ActionColumn) {
                    $column->controller = $this->controller;
                }
                $i++;
            }
            if ($this->reorderColumns) {
                $columns[$i] = [
                    'data'       => 'reorder',
                    'orderable'  => true,
                    'searchable' => false,
                    'visible'    => false
                ];
            }

            $clientOptions['dataTable']['ajax']['url'] = Url::to($this->filterUrl ?: Yii::$app->request->url);
            $clientOptions['dataTable']['columns']     = $columns;
            if ($this->reorderColumns) {
                $clientOptions['dataTable']['rowReorder'] = [
                    'dataSrc'  => 'reorder',
                    'update'   => true,
                    'selector' => 'a.reorder'
                ];
            }
            $clientOptions = Json::encode($clientOptions);
            $view->registerJs(
                "var drid_$id = new Datatable();
                 drid_$id.init($clientOptions);
                "
            );
            $type = 'datatable';
        }
        Html::addCssClass($this->headerRowOptions, 'heading');
        echo $this->render($type, [
            'dataProvider'=> $this->dataProvider,
            'grid'        => $this,
            'actions'     => $this->actions,
            'bulkActions' => $bulkActions,
            'title'       => $this->title,
            'icon'        => $this->icon,
            'showPanel'   => $this->showPanel
        ]);

        return null;
    }

    protected function initActions()
    {

        if ($this->export) {
            $this->actions[] = $this->render('actionExport');
        }
    }

    protected function initPageSize()
    {
        if (null !== $this->filterSelector) {
            $this->filterSelector .= ', ';
        }
        $this->filterSelector .= '.' . $this->id . '_pagerLength';
        if (isset($this->pageSizesOptions['class'])) {
            $this->pageSizesOptions['class'] .= ' input-xsmall ' . $this->id . '_pagerLength';
        } else {
            $this->pageSizesOptions['class'] = ' input-xsmall ' . $this->id . '_pagerLength';
        }

        /** @var \yii\data\Pagination $pagination */
        $pagination = $this->dataProvider->getPagination();

        /** @var \yii\web\Session $session */
        $pageSize = Yii::$app->request->get($pagination->pageSizeParam, 0);
        if ($pageSize) {
            Yii::$app->response->cookies->add(
                new \yii\web\Cookie([
                    'name'   => $this->id . '_pagerLength',
                    'value'  => $pageSize,
                    'expire' => time() + 60 * 60 * 30
                ]));
        } elseif (isset(Yii::$app->request->cookies[$this->id . '_pagerLength'])) {
            $pageSize = Yii::$app->request->cookies[$this->id . '_pagerLength'];
            $pagination->setPageSize($pageSize->value);
        }
        $pagination->pageSizeLimit = [0, 1000];
        $pagination->pageSize = $this->pageSize;
    }

    public function renderDatatable()
    {
        $tableHeader = $this->showHeader ? $this->renderTableHeader() : false;

        $tableFooter = $this->showFooter ? $this->renderTableFooter() : false;
        $content     = array_filter([
            $tableHeader,
            $tableFooter
        ]);

        return Html::tag('table', implode("\n", $content), $this->tableOptions);
    }

    public function renderTreetable()
    {
        $tableHeader = $this->showHeader ? $this->renderTableHeader() : false;
        $tableFooter = $this->showFooter ? $this->renderTableFooter() : false;
        $content     = array_filter([
            $tableHeader,
            $tableFooter
        ]);

        return Html::tag('table', implode("\n", $content), $this->tableOptions);
    }

    public function renderItems()
    {

        $tableHeader = $this->showHeader ? $this->renderTableHeader() : false;
        $tableBody   = $this->renderTableBody();
        $tableFooter = $this->showFooter ? $this->renderTableFooter() : false;
        $content     = array_filter([
            $tableHeader,
            $tableFooter,
            $tableBody,
        ]);

        return Html::tag('table', implode("\n", $content), $this->tableOptions);
    }

    public function renderPagerLength()
    {
        if ($this->simple) {
            $pagination = $this->dataProvider->getPagination();

            return Html::dropDownList($pagination->pageSizeParam, $pagination->pageSize,
                $this->normalizePageSizes($this->pageSizes), $this->pageSizesOptions);
        }

        return null;
    }

    /* public function renderFilters(){
         if($this->datatable){
             return null;
         }
         return parent::renderFilters();
     }*/

    public function renderPager(array $options = null)
    {
        if ($this->simple) {
            $pagination = $this->dataProvider->getPagination();
            if ($pagination === false || $this->dataProvider->getCount() <= 0) {
                return '';
            }
            /* @var $class LinkPager */
            $pager = $this->pager;
            $class = ArrayHelper::remove($pager, 'class', LinkPager::class);
            $pager['pagination'] = $pagination;
            $pager['view'] = $this->getView();
            if($options){
                $pager = array_merge($pager, $options, $pager);
            }
            return $class::widget($pager);
        }

        return null;
    }

    private function normalizePageSizes($pageSizes)
    {
        $result = [];
        foreach ($pageSizes as $k => $v) {
            if (is_numeric($v)) {
                $k = $v;
            }
            $result[$k] = $v;
        }

        return $result;
    }

    public function renderJson()
    {

        while (@ob_end_clean()) {
            ;
        } // clear all buffers
        Yii::$app->response->clear();
        Yii::$app->response->format = Response::FORMAT_JSON;
        $models                     = array_values($this->dataProvider->getModels());
        $keys                       = $this->dataProvider->getKeys();
        if ($this->bulkActions) {
            $column = Yii::createObject([
                'class' => CheckboxColumn::class,
                'grid'  => $this,
            ]);
            array_unshift($this->columns, $column);
        }
        if ($this->model instanceof TreeActiveRecord && Yii::$app->request->get('grid-view') === 'tree') {
            $rows = [];
            foreach ($models as $index => $model) {
                $cols = [];
                $key  = $keys[$index];
                if ($this->beforeRow !== null) {
                    $row = call_user_func($this->beforeRow, $model, $key, $index, $this);
                    if (!empty($row)) {
                        $cols = $row;
                    }
                }
                $title = '';

                foreach ($this->columns as $i => $column) {
                    $name        = $column instanceof DataColumn ? $column->attribute : "col_$i";
                    $cols[$name] = $column->renderDataCell($model, $key, $index);
                    if ($name === 'name' || $name === 'title') {
                        $title = $cols[$name];
                    }
                }
                $cols['children'] = !$model->isLeaf();
                $rows[]           = ['id' => $model->primaryKey, 'text' => $title, 'data' => $cols];

                if ($this->afterRow !== null) {
                    $row = call_user_func($this->afterRow, $model, $key, $index, $this);
                    if (!empty($row)) {
                        $rows[] = $row;
                    }
                }
            }
            $result = $rows;
        } else {
            $draw = Yii::$app->request->post('draw');
            $rows = [];
            foreach ($models as $index => $model) {
                $key = $keys[$index];
                if ($this->beforeRow !== null) {
                    $row = call_user_func($this->beforeRow, $model, $key, $index, $this);
                    if (!empty($row)) {
                        $rows[] = $row;
                    }
                }
                foreach ($this->columns as $i => $column) {
                    $name        = $column instanceof DataColumn ? $column->attribute : "col_$i";
                    $cols[$name] = $column->renderDataCell($model, $key, $index);
                }
                if ($this->reorderColumns) {
                    $cols['reorder'] = $model->getSortablePosition();
                }
                $rows[] = $cols;

                if ($this->afterRow !== null) {
                    $row = call_user_func($this->afterRow, $model, $key, $index, $this);
                    if (!empty($row)) {
                        $rows[] = $row;
                    }
                }
            }
            $result = [
                'draw'            => (int)$draw,
                'recordsTotal'    => $this->dataProvider->getCount(),
                'recordsFiltered' => $this->dataProvider->getTotalCount(),
                'data'            => $rows,
            ];
        }
        Yii::$app->response->data = $result;
        Yii::$app->end();
    }

}