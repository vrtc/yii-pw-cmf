<?php
namespace pw\ui\grid;

use pw\ui\icons\Icons;
use Yii;
use yii\helpers\Html;

class ActionColumn extends \yii\grid\ActionColumn
{

    public $template = '{reorder} {create} {view} {update} {delete}';
    public $buttonOptions = ['class' => 'btn btn-secondary btn-sm'];

    protected function renderFilterCellContent()
    {

        return '
        <div class="pull-left margin-right-5">
            <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                <i class="fa fa-search"></i> ' . Yii::t('pw-ui', 'Найти') . '
            </button>
        </div>
        <div class="pull-left">
        <button class="btn btn-sm red btn-outline filter-cancel">
            <i class="fa fa-times"></i> ' . Yii::t('pw-ui', 'Сбросить') . '
        </button></div>';

        return Html::a('<i class="fa fa-refresh"></i>', '#', [
            'class' => 'btn btn-info filter-grid',
        ]);
    }

    protected function initDefaultButtons()
    {

        if ($this->grid->reorderColumns && strpos($this->template, '{reorder}')) {
            $buttonOptions = $this->buttonOptions;
            Html::addCssClass($buttonOptions, 'reorder');
            $this->initDefaultButton('reorder', 'arrows', $buttonOptions);
        }
        if($this->grid->model instanceof TreeActiveRecord){
            $this->initDefaultButton('create', 'plus');
        }
        if (strpos($this->template, '{view}') !== false) {
            $this->initDefaultButton('view', 'eye');
        }
        if (strpos($this->template, '{update}') !== false) {
            $this->initDefaultButton('update', 'edit', [
                'class' => 'btn btn-primary btn-sm'
            ]);
        }
        if (strpos($this->template, '{delete}') !== false) {
            $this->initDefaultButton('delete', 'trash', [
                'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                'data-method' => 'post',
                'class' => 'btn btn-danger btn-sm'
            ]);
        }
    }

    protected function initDefaultButton($name, $iconName, $additionalOptions = [])
    {

        if (!isset($this->buttons[$name]) && strpos($this->template, '{' . $name . '}') !== false) {
            $this->buttons[$name] = function ($url, $model, $key) use ($name, $iconName, $additionalOptions) {
                switch ($name) {
                    case 'view':
                        $title = Yii::t('yii', 'View');
                        break;
                    case 'create':
                        $title = Yii::t('yii', 'Create');
                        break;
                    case 'update':
                        $title = Yii::t('yii', 'Update');
                        break;
                    case 'delete':
                        $title = Yii::t('yii', 'Delete');
                        break;
                    default:
                        $title = ucfirst($name);
                }
                $options = array_merge([
                    'title' => $title,
                    'aria-label' => $title,
                    'data-pjax' => '0',
                ], $this->buttonOptions, $additionalOptions);
                $icon = Icons::show($iconName);
                return Html::a($icon, $url, $options);
            };
        }
    }

    protected function renderDataCellContent($model, $key, $index)
    {
        return preg_replace_callback('/\\{([\w\-\/]+)\\}/', function ($matches) use ($model, $key, $index) {
            $name = $matches[1];

            if (isset($this->visibleButtons[$name])) {
                $isVisible = $this->visibleButtons[$name] instanceof \Closure
                    ? \call_user_func($this->visibleButtons[$name], $model, $key, $index)
                    : $this->visibleButtons[$name];
            } else {
                $isVisible = true;
            }

            if ($isVisible && isset($this->buttons[$name]) && $this->buttons[$name] !== false) {
                $url = $this->createUrl($name, $model, $key, $index);

                return \call_user_func($this->buttons[$name], $url, $model, $key);
            }
            return '';
        }, $this->template);
    }
}