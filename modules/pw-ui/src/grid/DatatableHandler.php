<?php

namespace pw\ui\grid;

use pw\core\interfaces\EventHandler;
use pw\web\Application;

class DatatableHandler
{

    public function actions()
    {
        return [
            [
                [Application::class, Application::EVENT_BEFORE_REQUEST], 'onBeforeRequest'
            ]
        ];
    }

    public static function onBeforeRequest($event)
    {
        $app = $event->sender;
        if ($app->request->isAjax && $app->request->get('gridView') === 'datatable') {
            $formName = $app->request->get('formName', '');
            if (empty($formName)) {
                return false;
            }
            $_GET[$formName] = [];

            $pageParam = $app->request->get('pageParam', 'page');
            $pageSizeParam = $app->request->get('pageSizeParam', 'page');
            $start = $app->request->get('start', 0);
            $length = $app->request->get('length', 10);
            $_GET[$pageParam] = $start / $length + 1;
            $_GET[$pageSizeParam] = $length;
            $_GET[$formName] = $app->request->get($formName, []);
            $app->request->setQueryParams($_GET);
        }
    }
}
