<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>
<div <?= Html::renderTagAttributes($panelOptions) ?>>
    <div <?= Html::renderTagAttributes($panelTitleOptions) ?>>
        <ul class="nav nav-tabs">
            <?php foreach ($tabs as $i => $tab): ?>
                <li <?php if (isset($tab['active'])) : ?>
                    class="active"
                    <?php endif; ?>>
                    <?php if(!empty($tab['url'])):?>
                        <a href="<?=Url::to($tab['url'])?>">
                            <?php if (!empty($tab['icon'])) : ?>
                                <?= \pw\ui\icons\Icons::show($tab['icon'], ['class' => 'font-yellow-gold']) ?>
                            <?php endif; ?>
                            <?= $tab['label'] ?>
                        </a>
                    <?php else:?>
                    <a href="#panel-<?= $panelOptions['id'] ?>-tab<?= $i ?>" data-toggle="tab" aria-expanded="true">
                        <?php if (!empty($tab['icon'])) : ?>
                            <?= \pw\ui\icons\Icons::show($tab['icon'], ['class' => 'font-yellow-gold']) ?>
                        <?php endif; ?>
                        <?= $tab['label'] ?>
                    </a>
                    <?php endif?>
                </li>
            <?php endforeach; ?>
        </ul>
        <?php if ($tools) : ?>
            <div class="tools">
                <a class="collapse"
                   title="<?= Yii::t('ui', 'Свернуть') ?>"
                   data-expand-title="<?= Yii::t('ui', 'Развернуть') ?>"
                   data-collapse-title="<?= Yii::t('ui', 'Свернуть') ?>">
                </a>
                <a class="fullscreen" href="#" title="<?= Yii::t('ui', 'На весь экран') ?>"></a>
            </div>
        <?php endif; ?>
        <?php if ($actions) : ?>
            <div class="actions">
                <?= implode("\n", $actions) ?>
            </div>
        <?php endif; ?>
    </div>
    <div <?= Html::renderTagAttributes($panelBodyOptions) ?>>
        <div class="tab-content">
            <?php foreach ($tabs as $i => $tab) : ?>
                <div class="tab-pane <?php if (isset($tab['active'])) : ?>
                                    active
                                <?php endif; ?>"
                     id="panel-<?= $panelOptions['id'] ?>-tab<?= $i ?>">
                    <?= $tab['content'] ?>
                </div>
            <?php endforeach; ?>
        </div>
        <?= $content ?>
    </div>
</div>
