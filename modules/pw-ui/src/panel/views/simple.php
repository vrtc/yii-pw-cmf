<?php

use yii\helpers\Html;

/**
 * @var $tools bool
 * @var $panelOptions array
 * @var $panelTitleOptions array
 * @var $panelBodyOptions array
 * @var $actions array
 * @var $isItForm bool
 */
?>
<?php if ($isItForm): ?>
<div class="row justify-content-center">
    <div class="col-md-8">
        <?php endif; ?>
        <div <?= Html::renderTagAttributes($panelOptions) ?>>
            <div <?= Html::renderTagAttributes($panelTitleOptions) ?>>
                <?php if ($tools): ?>
                    <div class="tools">
                        <a class="collapse"
                           title="<?= Yii::t('ui', 'Свернуть') ?>"
                           data-expand-title="<?= Yii::t('ui', 'Развернуть') ?>"
                           data-collapse-title="<?= Yii::t('ui', 'Свернуть') ?>">
                        </a>
                        <a class="fullscreen" href="#" title="<?= Yii::t('ui', 'На весь экран') ?>"></a>
                    </div>
                <?php endif; ?>
                <?php if ($actions): ?>
                    <div class="actions">
                        <?php foreach ($actions as $action):
                            if ($action !== null) {
                                $action['options']['class'] = 'btn btn-primary';
                                echo Html::tag($action['tag'], $action['icon'] . ' ' . $action['label'], $action['options']);
                            }
                        endforeach; ?>
                    </div>
                <?php endif; ?>

            </div>
            <div <?= Html::renderTagAttributes($panelBodyOptions) ?>>
                <?= $content ?>
            </div>
        </div>
        <?php if ($isItForm): ?>
    </div>
</div>
<?php endif; ?>
