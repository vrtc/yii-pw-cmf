<?php
namespace pw\ui\panel;

use pw\ui\assets\PanelAsset;
use pw\ui\traits\Button;
use yii\helpers\Html;

class Panel extends \yii\bootstrap\Widget
{
    use Button;

    public $title;

    public $panelTitleOptions = [
        'class' => 'portlet-title'
    ];

    public $isItForm = false;

    public $helper;

    public $icon;

    public $actions = [];

    public $panelOptions = [
        'class' => 'portlet light bordered'
    ];


    public $panelBodyOptions = [
        'class' => 'portlet-body'
    ];

    public $tabs = [];

    public $collapsed = false;

    public $tools = false;


    public function init()
    {
        parent::init();
        ob_start();
        ob_implicit_flush(false);
    }

    public function run()
    {
        $content = ob_get_clean();
        $this->registerAsset();
        foreach ($this->actions as $id => $action) {
            if (is_array($action)) {
                if (!isset($action['id'])) {
                    $action['id'] = $id;
                }
                $this->actions[$id] = $this->generateButton($action);
            }
            if($action == ""){
                $this->actions[$id] = null;
            }
        }
        if ($this->collapsed) {
            Html::addCssStyle($this->panelBodyOptions, ['display' => 'hidden']);
        }
        if (strpos($content, '<form') !== false) {
            Html::addCssClass($this->panelBodyOptions, 'form');
        }
        if (empty($this->panelOptions['id'])) {
            $this->panelOptions['id'] = $this->getId();
        }
        $view = 'simple';
        if (!empty($this->tabs)) {
            $view = 'tabs';
            Html::addCssClass($this->panelTitleOptions, 'tabbable-line');
            $this->tabs[0]['active'] = true;
        }
        echo $this->render($view, [
            'tools'=>$this->tools,
            'icon' => $this->icon,
            'title' => $this->title,
            'helper' => $this->helper,
            'actions' => $this->actions,
            'panelOptions' => $this->panelOptions,
            'panelTitleOptions' => $this->panelTitleOptions,
            'panelBodyOptions' => $this->panelBodyOptions,
            'content' => $content,
            'tabs' => $this->tabs,
            'isItForm' => $this->isItForm,
        ]);
    }

    public function registerAsset()
    {
        PanelAsset::register($this->view);
    }
}
