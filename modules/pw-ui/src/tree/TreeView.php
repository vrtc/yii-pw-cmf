<?php

namespace pw\ui\tree;


use pw\core\Widget;
use pw\ui\assets\JsTreeAsset;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;

class TreeView extends Widget
{
    public $jsOptions = [];
    public $containerTag = 'div';
    public $containerOptions = [];

    public $title;

    public $icon;

    public $actions = [];

    public $showPanel = false;

    public function init()
    {
        $view = $this->getView();
        JsTreeAsset::register($view);
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $id                           = ArrayHelper::getValue($this->containerOptions, 'id', $this->getId());
        $this->containerOptions['id'] = $id;
        $jsOptions                    = Json::encode($this->jsOptions);
        $this->getView()->registerJs("$('#{$id}').jstree({$jsOptions});");

        return $this->render('tree', [
            'id'        => $id,
            'icon'      => $this->icon,
            'title'     => $this->title,
            'actions'   => $this->actions,
            'showPanel' => $this->showPanel,
        ]);
    }
}