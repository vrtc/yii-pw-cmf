<?php

namespace pw\ui\traits;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use pw\ui\icons\Icons;
use yii\helpers\Url;

trait Button
{

    public $buttonOptions = [

    ];

    protected function generateButton($button)
    {
        $icon = null;
        $options = [];
        $tag = ArrayHelper::getValue($button, 'tag', 'a');
        $label = '';
        if (!empty($button['label'])) {
            $label = $button['label'];
        }
        if (!empty($button['htmlOptions'])) {
            $options = $button['htmlOptions'];
        }

        if (!empty($button['id']) && !is_numeric($button['id'])) {
            $options['id'] = $button['id'];
        }
        if (!empty($button['icon'])) {
            $options['encodeLabel'] = false;
            $icon = Icons::show($button['icon']);
        }
        if (!empty($button['url'])) {
            $url = Url::to($button['url'], true);
            if ($tag === 'a') {
//                $options['data-fancybox'] = true;
//                $options['data-type'] = 'ajax';
//                $options['data-src'] = $url;
                $options['href'] = $url;
            } else {
                $options['data-url'] = $url;
            }
        }
        if (empty($options['class'])) {
            if (empty($label)) {
                     Html::addCssClass($options, 'btn btn-icon-only btn-primary');
            } else {
                       Html::addCssClass($options, 'btn btn-primary');
            }
        }

        $buttonArray = [
            'tag' => $tag,
            'icon' => $icon,
            'label' => $label,
            'options' => $options,
        ];
        //return Html::tag($tag, $icon . ' ' . $label, $options);
        return $buttonArray;
    }
}

