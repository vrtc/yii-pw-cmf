<?php
namespace pw\ui\assets;

use pw\web\AssetBundle;
use yii\web\JqueryAsset;

class IsLoadingAsset extends AssetBundle
{

    public $sourcePath = '@pw-ui/assets/isloading';

    public $js = [
        'jquery.isloading.min.js'
    ];

    public $depends = [
        JqueryAsset::class
    ];

}