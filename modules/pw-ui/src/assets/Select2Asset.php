<?php
namespace pw\ui\assets;

use Yii;
use pw\web\AssetBundle;
use yii\helpers\FileHelper;

class Select2Asset extends AssetBundle
{

    public $sourcePath = '@pw-ui/assets/select2';

    public $js = [
        'js/select2.full.min.js'
    ];

    public $css = [
        'css/select2.min.css'
    ];

    public function init()
    {

        //FileHelper::localize('@pw-ui/assets/select2/js/i18n/');
        //$this->js[] = 'js/i18n/' . Yii::$app->language . '.js';
        parent::init();
    }

}