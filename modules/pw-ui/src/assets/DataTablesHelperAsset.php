<?php
namespace pw\ui\assets;

use pw\web\AssetBundle;

class DataTablesHelperAsset extends AssetBundle {

    public $sourcePath = '@pw-ui/assets/grid';

    public $js = [
        'datatable.js'
    ];

    public $depends = [
        DataTablesAsset::class
    ];

}