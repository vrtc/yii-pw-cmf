<?php
namespace pw\ui\assets;
use pw\web\AssetBundle;
use yii\web\YiiAsset;
use yii\widgets\ActiveFormAsset;

class PwAsset extends AssetBundle {

    public $sourcePath = '@pw-ui/assets/pw';

    public $js = [
        'pw.js'
    ];

    public $depends = [
        YiiAsset::class,
        ActiveFormAsset::class,
        FancyBoxAsset::class
    ];
}