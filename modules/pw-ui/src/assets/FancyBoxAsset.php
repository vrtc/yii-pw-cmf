<?php
namespace pw\ui\assets;

use yii\web\AssetBundle;

class FancyBoxAsset extends AssetBundle {

    public $sourcePath = '@pw-ui/assets/fancybox';

    public $js = [
        'jquery.fancybox.min.js'
    ];

    public $css = [
        'jquery.fancybox.min.css'
    ];

    public $depends = [
        \yii\web\JqueryAsset::class,
    ];

}