<?php
namespace pw\ui\assets;

use pw\web\AssetBundle;
use yii\web\JqueryAsset;

class DataTablesAsset extends AssetBundle {

    public $sourcePath = '@pw-ui/assets/datatables';

    public $js = [
        'datatables.min.js',
    ];
    public $css = [
        'datatables.min.css'
    ];

    public $depends = [
        JqueryAsset::class,
        IsLoadingAsset::class
    ];

}