<?php
namespace pw\ui\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class JsTreeAsset extends AssetBundle {

    public $sourcePath = '@pw-ui/assets/jstree';

    public $js = [
        'jstree.min.js'
    ];

    public $css = [
        'themes/default/style.min.css'
    ];
}