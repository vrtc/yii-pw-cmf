<?php
namespace pw\ui\assets;

use pw\web\AssetBundle;

class GridViewAsset extends AssetBundle {

    public $sourcePath = '@pw-ui/assets/grid';

    public $js = [
        'gridView.js'
    ];

    public $depends = [
        \yii\grid\GridViewAsset::class
    ];

}