<?php
namespace pw\ui\assets;

use yii\web\AssetBundle;

class PanelAsset extends AssetBundle {

    public $sourcePath = '@pw-ui/assets/panel';

    public $js = [
        'panel.js'
    ];
}