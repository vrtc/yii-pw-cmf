(function (proxied) {
    window.alert = function () {
        var message = arguments[0];
        var text = '';
        if(typeof message === 'object'){
            if(message.text) {
                text = message.text;
            }
        }
        else {
            text = message;
        }
        if($.fancybox) {
            $.fancybox.open(
                '<div class="pw-modal pw-alert">' +
                '<p>' + text + '</p>' +
                '<div class="pw-modal-buttons">' +
                '<button data-fancybox-close class="pw-button-success">OK</button>' +
                '</div>' +
                '</div>', {
                    smallBtn: false,
                    buttons: false,
                    keyboard: false
                }
            );
        }
        else {
            return proxied.apply(this, arguments);
        }
    };
})(window.alert);
(function (proxied) {
    window.confirm = function (message, callback, caption) {
        caption = caption || 'Подтвердите действие';
        if($.fancybox && callback) {
            $.fancybox.open(
                '<div class="pw-modal pw-confirm">' +
                '<h3>' + caption + '</h3>' +
                '<p>' + message + '</p>' +
                '<div class="pw-modal-buttons">' +
                '<a data-value="0" data-fancybox-close class="btn btn-link pw-button-cancel">Отмена</a>' +
                '<button data-value="1" data-fancybox-close class="pw-button-success">OK</button>' +
                '</div>' +
                '</div>', {
                    smallBtn: false,
                    buttons: false,
                    keyboard: false,
                    afterClose: function (instance, current, e) {
                        var button = e ? e.target || e.currentTarget : null;
                        var value = button ? $(button).data('value') : 0;
                        if (callback && typeof(callback) == "function") {
                            callback(value);
                        }
                    }
                }
            );
        }
        else {
            return proxied.apply(this.arguments);
        }
    };
})(window.confirm)