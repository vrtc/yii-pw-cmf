/**
 * Created by karneds on 15.02.17.
 */
var gridView = function(){
    var grid;
    var the;
    var table;
    var tableContainer;

    var countSelectedRecords = function() {
        var selected = $('tbody > tr > td:nth-child(1) input[type="checkbox"]:checked', table).size();
        var text = '_TOTAL_ records selected:  ';
        if (selected > 0) {
            $('.table-group-actions > span', tableWrapper).text(text.replace("_TOTAL_", selected));
        } else {
            $('.table-group-actions > span', tableWrapper).text("");
        }
    };


    return {
        init: function(options){
            options = $.extend(true, {
                src:""
            },options);
            the = this;

            table = $(options.src);
            tableContainer = table.parents(".table-container");
            // handle group checkboxes check/uncheck
            $('.group-checkable', table).change(function() {
                var set = table.find('tbody > tr > td:nth-child(1) input[type="checkbox"]');
                var checked = $(this).prop("checked");
                $(set).each(function() {
                    $(this).prop("checked", checked);
                });
                countSelectedRecords();
            });


            // handle row's checkbox click
            table.on('change', 'tbody > tr > td:nth-child(1) input[type="checkbox"]', function() {
                countSelectedRecords();
            });

            // handle filter submit button click
            table.on('click', '.filter-submit', function(e) {
                e.preventDefault();
                the.submitFilter();
            });

            // handle filter cancel button click
            table.on('click', '.filter-cancel', function(e) {
                e.preventDefault();
                the.resetFilter();
            });
            grid = jQuery(options.src).yiiGridView(options.yiiGrid);
        },
        submitFilter: function() {
            grid.yiiGridView('applyFilter');
        },

        resetFilter: function() {
            table.find('.filters textarea,.filters select,.filters input', table).each(function() {
                $(this).val("");
            });
            table.find('.filters input[type="checkbox"]', table).each(function() {
                $(this).attr("checked", false);
            });
            grid.yiiGridView('applyFilter');
        },

        getSelectedRowsCount: function() {
            return $('tbody > tr > td:nth-child(1) input[type="checkbox"]:checked', table).size();
        },

        getSelectedRows: function() {
            var rows = [];
            $('tbody > tr > td:nth-child(1) input[type="checkbox"]:checked', table).each(function() {
                rows.push($(this).val());
            });

            return rows;
        }
    }
};