<?php
namespace pw\ui\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class Nestable2Asset extends AssetBundle {

    public $sourcePath = '@pw-ui/assets/nestable2';

    public $js = [
        'jquery.nestable.js'
    ];

    public $css = [
        'jquery.nestable.min.css'
    ];
}