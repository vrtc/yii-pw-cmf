<?php

namespace pw\ui;

use pw\ui\form\TreeInputHandler;
use pw\ui\icons\Icons;
use himiklab\yii2\recaptcha\ReCaptcha;
use Yii;
use pw\ui\grid\DatatableHandler;
use yii\base\Application;
use yii\base\BootstrapInterface;

class Module extends \pw\core\Module implements BootstrapInterface
{

    public function components(Application $app): array
    {
        return [
            'icons' => Icons::class,
            'reCaptcha' => [
                'name' => 'reCaptcha',
                'class' => ReCaptcha::class,
                'siteKey' => '6Le94joUAAAAAN1KA0KGACjsoFgH4-ISPFwUtkJT',
                'secret' => '6Le94joUAAAAADOr2FbXsWjSbhUy33pfQ1pgOFVx',
            ],
        ];
    }

    public function handlers(Application $app): array
    {
        $handlers = [];
        if ($app instanceof \yii\web\Application) {
            $handlers [] = [
                'class'    => Application::class,
                'event'    => Application::EVENT_BEFORE_REQUEST,
                'callback' => [DatatableHandler::class, 'onBeforeRequest']
            ];
            $handlers [] = [
                'class'    => Application::class,
                'event'    => Application::EVENT_BEFORE_REQUEST,
                'callback' => [TreeInputHandler::class, 'onBeforeRequest']
            ];
            $handlers[]  = [
                'class'    => Application::class,
                'event'    => Application::EVENT_BEFORE_REQUEST,
                'callback' => function () {

                }
            ];
        }

        return $handlers;
    }
}
